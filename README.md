# CanoP

CanoP is an applicative layer that sits on top of [p4est](http://p4est.org/)
library which provides efficient parallel algorithm implementations for managing
a simulation mesh with adaptive mesh refinement (AMR) feature. 
CanoP currently implements several CFD solvers:

- upwind scheme for the transport / advection equations
- biphasic fluid flow
- ramses : a 2nd order MUSCL-Hancock finite volume scheme for compressible Euler hydrodynamics, w/o (self / static) gravity
- mhd-kt : a cell-centered Kurgano-Tadmor type scheme for magnetohydrodynamics
- [in devel] a Bear-Nunziato based scheme for biphasic flow

# Compilation

## Requirements

- MPI 
- [p4est](http://p4est.org/)
- [hdf5](https://support.hdfgroup.org/HDF5/) : built with parallel IOs
- [lua](https://www.lua.org/) : used to store input settings files

## Building CanoP

CanoP uses CMake for the compilation. Just start with:

    $ export CXX=mpicxx
    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_PREFIX_PATH="$P4EST_ROOT" ..
    $ make

Environment variable P4EST_ROOT must be set to the location where p4est library 
is installed. If you compiled ``p4est`` yourself, this prefix is the same as the one passed
to the ``./configure`` script. All other dependencies should nominaly be detected automatically 
by cmake.

This will produce two executables: ``canoP2d.exe`` and ``canoP3d.exe``
for the two cases. 

To ease compilation, in the ``scripts`` directory there is a script that
configures and compiles everything. It can be used like (to be reviewed, old stuff):

    $ install_canoP.sh /path/to/p4est/prefix /my/install/prefix

By default, it looks for ``p4est`` in ``/usr`` and the install prefix for
``canoP`` is the same as the ``p4est`` prefix. This script is mostly made
as a starting point and should to be modified to fit your needs.

Another way to make sure that ``cmake`` cand find all the dependencies to
compile ``CanoP`` is to export some environment variables.

    $ export LD_LIBRARY_PATH=/path/to/p4est/prefix/lib:${LD_LIBRARY_PATH}
    $ export LDFLAGS="-L/path/to/p4est/prefix/lib ${LDFLAGS}"

If these variables are set, the first instructions are sufficient for ``cmake``.
To make sure they are always set, you can add them to your ``.bashrc`` or
``.zshrc`` or whatever.

# Running

After compilation the two executables can be found in ``build/src/*.exe``. To
run the program, it is recommended to write a small configuration file with
the desired options. A full commented example is available in the top level.

All the options have default values in the code, so it is not necessary to
write a file to run the program.

To run it, go:

    $ mpirun -np 4 ./build/src/canoP2d.exe settings.lua

That's it.

Pay a visit to the [wiki](https://gitlab.maisondelasimulation.fr/canoPdev/canoP/wikis/home)
 for more detailed information.
