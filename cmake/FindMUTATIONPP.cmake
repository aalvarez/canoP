#############################################################################
# This file is adapted from original file in LLNL tools called perf-dump.
# Perf-dump is written by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647187
#
# Copyright (c) 2013-2014, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# For details, see https://scalability-llnl.github.io/perf-dump
#
#############################################################################
#
# Try to find MUTATIONPP headers and libraries.
#
# Usage of this module as follows:
#
#     find_package(MUTATIONPP)
#
# Variables used by this module, they can change the default behaviour and need
# to be set before calling find_package:
#
#
#  MUTATIONPP_ROOT   Set this environment variable to the root installation of
#                    libmutationpp if the module has problems finding the
#                    proper installation path.
#
# Variables defined by this module:
#
#  MUTATIONPP_FOUND              System has MUTATIONPP libraries and headers
#  MUTATIONPP_LIBRARY            The MUTATIONPP library
#  MUTATIONPP_INCLUDE_DIR        The location of MUTATIONPP headers

# First try to see if we can find it using the CONFIG mode

find_package(mutation++ QUIET CONFIG)

if(NOT mutation++_FOUND)
  find_library(MUTATIONPP_LIBRARY
    NAMES libmutation++.so libmutation++.a mutation++
    HINTS ENV MUTATIONPP_ROOT
    PATH_SUFFIXES lib lib64
    )

  find_path(MUTATIONPP_INCLUDE_DIR
    NAMES mutation++/mutation++.h
    HINTS ENV MUTATIONPP_ROOT
    PATH_SUFFIXES include
    )

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(MUTATIONPP
    REQUIRED_VARS MUTATIONPP_LIBRARY MUTATIONPP_INCLUDE_DIR
    )


  mark_as_advanced(
    MUTATIONPP_LIBRARY
    )

  if(MUTATIONPP_FOUND AND NOT TARGET mutation++)
    add_library(mutation++ SHARED IMPORTED)

    # We need also Eigen
    find_package(Eigen REQUIRED)

    target_link_libraries(mutation++
      INTERFACE
      Eigen3::Eigen
      )

    set_target_properties(mutation++ PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${MUTATIONPP_INCLUDE_DIR}"
      IMPORTED_LOCATION "${MUTATIONPP_LIBRARY}"
      )
  endif()
endif()

