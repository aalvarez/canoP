settings = {
   solver_name = "ramses",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 0.05,

   -- the Courant number used to compute the time step
   cfl = 0.5,

   -- unsplit scheme only for Ramses
   dim_splitting = 0,

   -- scheme space order (1 or 2 = MUSCL)
   space_order = 2,

   -- time order (1 or 2 = Hancock)
   time_order = 2,

   -- type of flux limiter
   -- "minmod", "mc", ...
   limiter = "minmod",

   -- the name of the scheme
   name = "jet",

   -- minimum number of starting quadrants per process
   min_quadrants = 32,

   -- initial refinement is uniform (0, 1)
   uniform_fill = 1,

   -- gather different statistics. see statistics.h.
   statistics = 0,

   -- the minimum level of refinement of the mesh per process
   min_refine = 3,

   -- the maximum level of refinement of the mesh per process
   max_refine = 6,

   -- the threshold for refining
   epsilon_refine = 0.01,

   -- the threshold for coarsening
   epsilon_coarsen = 0.05,

   -- the type of the connectivity. see p4est_connectivity.h and
   -- connectivity.h
   -- 'unit', 'periodic', 'two', ...
   -- connectivity = 'unit',
   -- connectivity = 'periodic',
   connectivity = 'brick',
   nbrick_x = 3,
   nbrick_y = 1,
   nbrick_z = 1,
   periodic_x = 0,
   periodic_y = 0,
   periodic_z = 0,
   
   -- initial condition. options are:
   initial_condition = "jet",

   -- boundary conditions
   -- "reflective", "dirichlet", "neuman", ...
   boundary="jet",

   -- use an indicator. see indicators.h. options are: rho_gradient,
   -- khokhlov.
   indicator = "khokhlov",

   -- number of output files (-1 means, every time step)
   save_count = 50,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
   -- pressure, alpha, a.
   write_variables = "rho, rhoV, E_tot",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0
}

init_state = {

   rho = 1.0,
   pressure = 1.0,
   u = 0.0,
   v = 0.0,
   w = 0.0

}

border_inflow = {

   rho = 1.0,
   pressure = 1.0,
   u = 100.0,
   v = 0.0,
   w = 0.0,

   yCenterJet = 0.5,
   radiusJet = 0.05

}
