--
--
-- RAMSES Hydro example parameter file; Rayleigh-Taylor instability
--
-- see Athena test suite web page :
-- http://www.astro.princeton.edu/~jstone/Athena/tests/rt/rt.html
--
--
settings = {
   solver_name = "ramses",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 15.0,

   -- the Courant number used to compute the time step
   cfl = 0.8,

   -- unsplit scheme only for Ramses
   dim_splitting = 0,

   -- scheme space order (1 or 2 = MUSCL)
   space_order = 2,

   -- time order (1 or 2 = Hancock)
   time_order = 2,

   -- type of flux limiter (not used for ramses for now)
   -- "minmod", "mc", ...
   limiter = "minmod",

   -- type of Riemann solver (for Ramses only)
   riemann_solver = "hllc",

   -- the name of the scheme
   name = "rt_level_4_7",

   -- minimum number of starting quadrants per process
   min_quadrants = 16,

   -- initial refinement is uniform (0, 1)
   uniform_fill = 1,

   -- gather different statistics. see statistics.h.
   statistics = 0,

   -- the minimum level of refinement of the mesh per process
   min_refine = 4,

   -- the maximum level of refinement of the mesh per process
   max_refine = 7,

   -- the threshold for refining
   epsilon_refine = 0.005,

   -- the threshold for coarsening
   epsilon_coarsen = 0.005,

   -- the type of the connectivity. see p4est_connectivity.h and
   -- connectivity.h
   -- 'unit', 'periodic', 'two', ...
   -- connectivity = 'unit',
   connectivity = 'brick',
   nbrick_x = 3,
   nbrick_y = 1,
   nbrick_z = 1,

   periodic_x = 0,
   periodic_y = 1,
   periodic_z = 1,

   -- initial condition. options are:
   initial_condition = "rayleigh_taylor",

   -- boundary conditions
   -- "reflective", "dirichlet", "neuman", ...
   --boundary="neumann",
   boundary="reflective",

   -- use an indicator. see indicators.h. options are: rho_gradient,
   -- khokhlov.
   indicator = "khokhlov",

   -- static_gravity enabled ?
   static_gravity_enabled = 1,

   -- number of output files (-1 means, every time step)
   save_count = 50,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
   -- pressure, alpha, a.
   write_variables = "rho, rhoV, E_tot",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0,

   -- gamma
   gamma0 = 1.4

}

static_gravity_field = {

   gravity_x = -0.1,
   gravity_y = 0.0,
   gravity_z = 0.0

}

--
-- http://www.astro.princeton.edu/~jstone/Athena/tests/rt/rt.html
--
rayleigh_taylor = {

   randomEnabled = 0,

   -- amplitude of interface initial perturbation
   amplitude = 0.01,

   -- density of light fluid
   d0 = 1.0,

   -- density of heavy fluid
   d1 = 2.0,

   -- pressure
   pressure = 1.0,

   -- interface location
   x0 = 1.5,
   y0 = 0.5,
   z0 = 0.5

}
