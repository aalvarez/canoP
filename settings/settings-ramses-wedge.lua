------------------------------------------
-- The Wedge Test.
------------------------------------------
--
-- See
-- http://amroc.sourceforge.net/examples/euler/2d/html/ramp_n.htm
-- for a description of such initial conditions / border conditions.
--
-- See also
--  P. Woodward, P Colella, The Numerical Simulation of Two-Dimensional 
-- Fluid Flow with Strong Shocks, Journal of Computational Physics 54, 115.173 (1984) 

settings = {
   solver_name = "ramses",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 0.2,

   -- the Courant number used to compute the time step
   cfl = 0.8,

   -- unsplit scheme only for Ramses
   dim_splitting = 0,

   -- scheme space order (1 or 2 = MUSCL)
   space_order = 2,

   -- time order (1 or 2 = Hancock)
   time_order = 2,

   -- type of flux limiter
   -- "minmod", "mc", ...
   limiter = "minmod",

   -- the name / prefix of output files
   name = "wedge_hllc",

   -- minimum number of starting quadrants per process
   min_quadrants = 16,

   -- initial refinement is uniform (0, 1)
   uniform_fill = 1,

   -- gather different statistics. see statistics.h.
   statistics = 0,

   -- the minimum level of refinement of the mesh per process
   min_refine = 3,

   -- the maximum level of refinement of the mesh per process
   max_refine = 7,

   -- the threshold for refining
   epsilon_refine = 0.03,

   -- the threshold for coarsening
   epsilon_coarsen = 0.05,

   -- the type of the connectivity. see p4est_connectivity.h and
   -- connectivity.h
   -- 'unit', 'periodic', 'two', ...
   connectivity = 'brick',
   --connectivity = 'periodic',
   
   nbrick_x = 3,
   nbrick_y = 1,

   periodic_x = 0,
   periodic_y = 0,

   -- initial condition. options are:
   initial_condition = "wedge",

   -- boundary conditions
   -- "reflective", "dirichlet", "neuman", ...
   -- boundary="neuman",
   boundary="wedge",

   -- use an indicator. see indicators.h. options are: rho_gradient,
   -- khokhlov.
   indicator = "khokhlov",

   -- number of output files (-1 means, every time step)
   save_count = 50,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
   -- pressure, alpha, a.
   write_variables = "rho, rhoV, E_tot",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0,

   -- gamma
   gamma0 = 1.4,

   -- riemann solver
   riemann_solver = "hllc"
}

pi = 2.0*math.asin(1.0)

wedge = {
   
   -- initial shock location
   front_x = 0.1,
   front_angle = pi/3.0,

   -- inflow (post-shock)
   rho1 = 8.0,
   p1 = 116.5,
   u1 = 8.25*math.cos(pi/6.0),
   v1 = -8.25*math.sin(pi/6.0),
   w1 = 0.0,
   
   -- outflow (pre-shock)
   rho2 = 1.4,
   p2 = 1.0,
   u2 = 0.0,
   v2 = 0.0,
   w2 = 0.0,

}


-- speed of sound in region 1
a1 = math.sqrt(settings.gamma0 * wedge.p1 / wedge.rho1)

-- speed of sound in region 2
a2 = math.sqrt(settings.gamma0 * wedge.p2 / wedge.rho2)

-- shock speed (S3 in Toro, section 3.1.3, page 100)
shock_speed = wedge.u2 + a2*math.sqrt( (settings.gamma0+1.0)/(2*settings.gamma0)*wedge.p1/wedge.p2 + (settings.gamma0-1.0)/(2*settings.gamma0) )

