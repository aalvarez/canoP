-------------------------------------------------------------
--  http://nbviewer.jupyter.org/github/numerical-mooc/numerical-mooc/blob/master/lessons/02_spacetime/02_04_1DBurgers.ipynb
-------------------------------------------------------------
--

settings = {
   solver_name = "mhd_kt",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 0.6,

   -- the name / prefix of output files
   name = "ot2d_5_7",

   -- minimum number of starting quadrants per process
   min_quadrants = 16,

   -- initial refinement is uniform (0, 1)
   uniform_fill = 1,

   -- gather different statistics. see statistics.h.
   statistics = 0,

   -- the minimum level of refinement of the mesh per process
   min_refine = 5,

   -- the maximum level of refinement of the mesh per process
   max_refine = 7,

   -- the threshold for refining
   epsilon_refine = 0.01,

   -- the threshold for coarsening
   epsilon_coarsen = 0.03,

   -- the type of the connectivity. see p4est_connectivity.h and
   -- connectivity.h
   -- 'unit', 'periodic', 'two', ...
   --connectivity = 'unit',
   connectivity = 'periodic',

   -- initial condition. options are:
   initial_condition = "ot",

   -- boundary conditions
   -- "reflective", "dirichlet", "neuman", ...
   -- boundary="neuman",
   boundary="dirichlet",

   -- use an indicator. see indicators.h. options are: rho_gradient,
   -- khokhlov.
   indicator = "rho_gradient",

   -- limiter
   --limiter = "charm",
   
   -- number of output files (-1 means, every time step)
   save_count = 50,

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
   -- pressure, alpha, a.
   write_variables = "rho,rhoU,rhoV,E,Bx,By,divB",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0,

   -- gamma
   gamma0 = 1.666,

   -- cfl
   cfl=0.5,

   -- divB_damp
   divB_damp = 0.2,

   -- div b method
   --div_b_cleaning = "DIFFUSIVE",
   div_b_cleaning = "GLM",
   --div_b_cleaning = "GLM_SUBCYCLED",

   -- nb_subcycles = 5
}

