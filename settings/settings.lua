settings = {
    -- the max time of the simulation which will run from [0, tmax]
    tmax = 1.0,

    -- the Courant number used to compute the time step
    cfl = 0.5,

    -- scheme space order (1 or 2 = MUSCL)
    space_order = 2,

    -- time order (1 or 2 = Hancock)
    time_order = 2,

    -- dimensional splitting (1 = Godunov or 2 = Strang)
    dim_splitting = 2,

    -- type of flux limiter
    -- "minmod", "mc", ...
    limiter = "minmod",

    -- the name of the scheme
    name = "drop-minmod",

    -- minimum number of starting quadrants per process
    min_quadrants = 16,

    -- initial refinement is uniform (0, 1)
    uniform_fill = 1,

    -- gather different statistics. see statistics.h.
    statistics = 0,

    -- the minimum level of refinement of the mesh per process
    min_refine = 3,

    -- the maximum level of refinement of the mesh per process
    max_refine = 7,

    -- the threshold for refining
    epsilon_refine = 0.005,

    -- the threshold for coarsening
    epsilon_coarsen = 0.005,

    -- the type of the connectivity. see p4est_connectivity.h and
    -- connectivity.h
    -- 'unit', 'periodic', 'two', ...
    connectivity = 'unit',

    -- initial condition. options are:
    --  - for upwind: advection_gaussian, advection_disk, advection_diskrot,
    --  advection_manifold
    --  - for the two phase model: cvv_gaussian, cvv_disk.
    initial_condition = "cvv_drop_in_water",

    -- boundary conditions
    -- "cvv_reflective" (default), "cvv_dirichlet", "cvv_neuman", ...
    boundary="cvv_reflective",

    -- use an indicator. see indicators.h. options are: rho_gradient,
    -- khokhlov.
    indicator = "khokhlov",

    -- number of output files
    save_count =100,

    -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
    mesh_info = 0,

    -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
    -- pressure, alpha, a.
    write_variables = "rho, velocity, pressure, alpha, exact_alpha, rho_gas, rho_liquid",

    -- write floats (= 1) or doubles (= 0) to the h5 files
    single_precision = 0
}
