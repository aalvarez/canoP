settings = {
   solver_name = "advection_upwind",

   -- the max time of the simulation which will run from [0, tmax]
   tmax = 6.5,

   -- the Courant number used to compute the time step
   cfl = 0.5,

   -- dim splitting
   dim_splitting = 0,
   
   -- order of the space discretization
   order = 1,

   -- the name of the limiter. see gradient_reconstruction.h
   limiter = "minmod",

   -- the name of the scheme
   name = "disk_ring_minmod",

   -- minimum number of starting quadrants per process
   min_quadrants = 16,

   -- initial refinement is uniform (0, 1)
   uniform_fill = 1,

   -- gather different statistics. see statistics.h.
   statistics = 0,

   -- the minimum level of refinement of the mesh per process
   min_refine = 3,

   -- the maximum level of refinement of the mesh per process
   max_refine = 7,

   -- the threshold for refining
   epsilon_refine = 0.01,

   -- the threshold for coarsening
   epsilon_coarsen = 0.1,

   -- the type of the connectivity. see p4est_connectivity.h and
   -- connectivity.h
   --connectivity = 'periodic',
   connectivity = 'shell2d',

   -- the type of geometry (default is cartesian)
   -- will be used in output files and in numerical scheme
   geometry_compute = 'shell2d',
   geometry_io = 'shell2d',

   -- initial condition. options are:
   --  - for upwind: advection_gaussian, advection_disk,
   --  advection_disk_rotating, advection_gaussian, etc
   --  - for the two phase model: cvv_gaussian, cvv_disk.
   initial_condition = "advection_disk_ring",

   -- boundary conditions
   -- "cvv_reflective" (default), "cvv_dirichlet", "cvv_neuman", ...
   boundary="reflective",

   -- use an indicator. see indicators.h. options are: rho_gradient,
   -- khokhlov.
   indicator = "rho_gradient",

   -- set to 1 to write mpirank, level, treeid, etc. for each quadrant.
   mesh_info = 1,

   -- select which variables to write: rho, rho_gas, rho_liquid, velocity,
   -- pressure, alpha, a.
   write_variables = "rho, velocity",

   -- write floats (= 1) or doubles (= 0) to the h5 files
   single_precision = 0,

   -- total number of saved files during the simulation. this tries to
   -- save at every time i * (tmax / save_count).
   save_count = 20,
}


--
--
--
shell2d = {

   -- inner radius
   R1 = 0.25,

   -- outer radius
   R2 = 1.0

}

--
--
--
ring = {

   rMin = 0.25,
   rMax = 1.0
   
}
