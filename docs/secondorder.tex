\documentclass[DIV14,11pt,parskip=half*]{scrartcl}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{tikz}

% pretty links
\hypersetup{
    colorlinks=true,
    urlcolor=blue,
    citecolor=black,
    linkcolor=black
}

% Custom headers and footers
\pagestyle{fancyplain}
\fancyhead{}
\fancyfoot[L]{}
\fancyfoot[C]{}
\fancyfoot[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\setlength{\headheight}{13.6pt}

%-------------------------------------------------------------------------------
%	TITLE SECTION
%-------------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}

\title{
    \normalfont \normalsize
    \horrule{0.5pt} \\[0.4cm]
    \huge \textbf{Second Order Approximation for AMR} \\
    \horrule{2pt} \\[0.5cm]
}
\author{}
\date{}

%-------------------------------------------------------------------------------
%   SHORTCUTS
%-------------------------------------------------------------------------------

\newcommand{\pd}[2]{\dfrac{\partial #1}{\partial #2}}
\renewcommand{\div}[1]{\nabla \cdot (#1)}
\newcommand{\grad}[1]{\nabla #1}

\begin{document}

\maketitle

\section{Mesh based}

When working with differential equations, we often want to do higher order
approximation of a derivative. For example, a second order approximation
of the first derivative of a function $c(x)$:
\[
    \pd{c}{x} = \frac{4 c_{i + 1} - 3 c_{i} - c_{i + 2}}{2 \Delta x}.
\]

For this we need the values in the two cells to the left of our current cell.
In the context of AMR, can be quite hard to define, as seen in
Figure~\ref{fig:second_110}, for example.

If we consider the current cell as the big cell $i$ to the left we
essentially have two options for the value of $i + 2$ (green):
\begin{itemize}
    \item First, we can consider the cells at the same level as the cell $i$.
This means that we have to compute averages on the red $i + 1$ and the
green $i + 2$ cells to get the values at the same level.
    \item Second, we can look at actual neighbors of our cell and the
neighbors of its neighbors.
\end{itemize}

The first case can be easily handled with a recursive algorithm that
descends into the tree starting at a cell $i$ and computes the mean over
all its children, if any.

In the second case, we can have multiple complicated cases. If we consider
only the cases where the level of the neighbors is bigger of equal to that
of the current cell $i$, we have:
\begin{enumerate}
    \item Figure~\ref{fig:second_000}. In this case the cells are at the
same level and there is no issue determining the neighbors.
    \item Figure~\ref{fig:second_001}. In this case the $i + 1$ neighbor
is obvious and the $i + 2$ neighbor can be considered as the children
number as $0$ and $2$ of the green patch.
    \item Figure~\ref{fig:second_010}. In this case we can take the two
cells bordering the cell $i$ separately as two $i + 1$ neighbors. Their
neighbors to the right are obvious.
    \item Figure~\ref{fig:second_011}. The first layer of neighbors is
the same as in the previous case, but the second layer can be considered as
children $0$ and $2$ of the two cells neighboring the first layer.
\end{enumerate}

In cases 2 and 4, since either the $i + 1$ or $i + 2$ "neighbor" contains
two quadrants, we would have to compute an average to use as a single value.

\begin{figure}[!ht]
\centering
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=0.6]
    \draw [thick] (-4, 0) rectangle (0, 4);
    \draw [thick, fill=red] (0, 0) rectangle (4, 4);
    \draw [thick, fill=green] (4, 0) rectangle (8, 4);
    \node at (-2, 2) {$i$};
\end{tikzpicture}
\caption{Same level neighbors.}
\label{fig:second_000}
\end{minipage}
\hspace{10px}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=0.6]
    \draw [thick] (-4, 0) rectangle (0, 4);
    \draw [thick, fill=red] (0, 0) rectangle (4, 4);

    \foreach \x in {0,...,1}{
        \foreach \y in {0,...,1}{
            \draw [thick,fill=green]
                (\x * 2 + 4, \y * 2) rectangle (\x * 2 + 6, \y * 2 + 2);
        }
    }

    \node at (-2, 2) {$i$};
    \node at (5, 1) {$0$};
    \node at (7, 1) {$1$};
    \node at (5, 3) {$2$};
    \node at (7, 3) {$3$};
\end{tikzpicture}
\caption{Higher level $i + 2$ neighbor.}
\label{fig:second_001}
\end{minipage}
\end{figure}

\begin{figure}[!ht]
\centering
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=0.6]
    \draw [thick] (-4, 0) rectangle (0, 4);

    \foreach \x in {0,...,1}{
        \foreach \y in {0,...,1}{
            \draw [thick,fill=red]
                (\x * 2, \y * 2) rectangle (\x * 2 + 2, \y * 2 + 2);
        }
    }

    \draw [thick, fill=green] (4, 0) rectangle (8, 4);

    \node at (-2, 2) {$i$};
    \node at (1, 1) {$0$};
    \node at (3, 1) {$1$};
    \node at (1, 3) {$2$};
    \node at (3, 3) {$3$};
\end{tikzpicture}
\caption{Higher level $i + 1$ neighbor.}
\label{fig:second_010}
\end{minipage}
\hspace{10px}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=0.6]
    \draw [thick] (-4, 0) rectangle (0, 4);

    \foreach \x in {0,...,1}{
        \foreach \y in {0,...,1}{
            \draw [thick,fill=red]
                (\x * 2, \y * 2) rectangle (\x * 2 + 2, \y * 2 + 2);
        }
    }
    \draw [thick] (3, 0) -- (3, 4);
    \draw [thick] (2, 1) -- (4, 1);
    \draw [thick] (2, 3) -- (4, 3);

    \foreach \x in {0,...,1}{
        \foreach \y in {0,...,1}{
            \draw [thick,fill=green]
                (\x * 2 + 4, \y * 2) rectangle (\x * 2 + 6, \y * 2 + 2);
        }
    }

    \node at (-2, 2) {$i$};

    \node at (1, 1) {$0$};
    \node at (1, 3) {$2$};

    \node at (2.5, 0.5) {$0$};
    \node at (2.5, 1.5) {$2$};
    \node at (3.5, 0.5) {$1$};
    \node at (3.5, 1.5) {$3$};

    \node at (2.5, 2.5) {$0$};
    \node at (2.5, 3.5) {$2$};
    \node at (3.5, 2.5) {$1$};
    \node at (3.5, 3.5) {$3$};
\end{tikzpicture}
\caption{Higher level neighbors.}
\label{fig:second_011}
\end{minipage}
\end{figure}

Alternatively, if we consider that the level of the two neighboring cells
can be strictly bigger than that of the current cell, we have the extra
cases:
\begin{itemize}
    \item Figure~\ref{fig:second_100}. In this case, the values of the $i + 1$
and $i + 2$ are obvious.
    \item Figure~\ref{fig:second_110}. This is a simple case as well.
    \item Figure~\ref{fig:second_101}. In this case, the $i + 1$ cell is the
same, but the $i + 2$ cell can be considered as the two neighbors of the
$i + 1$ cell. We can also use only the lower of these two cells as the
neighbors.
\end{itemize}

\begin{figure}[!ht]
\centering
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=0.6]
    \foreach \x in {0,...,1}{
        \foreach \y in {0,...,1}{
            \draw [thick]
                (\x * 2 - 4, \y * 2) rectangle (\x * 2 - 2, \y * 2 + 2);
        }
    }

    \draw [thick, fill=red] (0, 0) rectangle (4, 4);
    \draw [thick, fill=green] (4, 0) rectangle (8, 4);

    \node at (-1, 1) {$i$};
\end{tikzpicture}
\caption{Lower level neighbors.}
\label{fig:second_100}
\end{minipage}
\hspace{10px}
\begin{minipage}{.45\linewidth}
\centering
\begin{tikzpicture}[scale=0.6]
    \foreach \x in {0,...,1}{
        \foreach \y in {0,...,1}{
            \draw [thick]
                (\x * 2 - 4, \y * 2) rectangle (\x * 2 - 2, \y * 2 + 2);
        }
    }

    \draw [thick, fill=red] (0, 0) rectangle (4, 4);
    \draw [thick, fill=green] (4, 0) rectangle (8, 4);

    \node at (-3, 1) {$i$};
\end{tikzpicture}
\caption{Lower level $i + 2$ neighbor.}
\label{fig:second_110}
\end{minipage}
\end{figure}

\begin{figure}[!ht]
\centering
\begin{tikzpicture}[scale=0.6]
    \foreach \x in {0,...,1}{
        \foreach \y in {0,...,1}{
            \draw [thick]
                (\x * 2 - 4, \y * 2) rectangle (\x * 2 - 2, \y * 2 + 2);
        }
    }

    \draw [thick, fill=red] (0, 0) rectangle (4, 4);

    \foreach \x in {0,...,1}{
        \foreach \y in {0,...,1}{
            \draw [thick, fill=green]
                (\x * 2 + 4, \y * 2) rectangle (\x * 2 + 6, \y * 2 + 2);
        }
    }


    \node at (-1, 1) {$i$};
    \node at (5, 1) {$0$};
    \node at (5, 3) {$2$};
    \node at (7, 1) {$1$};
    \node at (7, 3) {$3$};
\end{tikzpicture}
\caption{Lower level $i + 1$ neighbor.}
\label{fig:second_101}
\end{figure}

One obvious downside of such an approach is that it quickly becomes more and
more complicated as we increase the order of the approximation. The first
version, using same-level averages, does not suffer from the same downsides.
The only possible increase in this case is in the depth of the recursive
calls that calculate the average.

\section{Patch based}

An alternative to relying directly on the mesh is using a patch based
approch. That is to say, in each cell, instead of having a single value, we
have a patch of size $m^d$ where $d$ is the dimension. These patches would
automatically respect the 2:1 balance and are equivalent to just refining
a cell $m$ times. An example can be seen in Figure~\ref{fig:patch}

In this case, we can consider the local problem to be on a structured mesh
and $i + 1$ and $i + 2$ neighbors are defined intuitively.

At a local mesh level, ghost cells (effectively patches from neighboring
cells that can be either local or belonging to a different process) can be
treated easily by adding the necessary padding to our patch. The
ghost cells in the patch can be either:
\begin{itemize}
    \item cells in a neighbor's patch, if the neighbor is the same level.
    \item averages over cells in a neighbor's patch, if the neighbor is of
    a higher level.
    \item if the neighbor is of a lower level, the values of the ghost
    cells $i + 1$ and $i + 2$ will be the same as they belong to only a
    single cell in the neighbor's patch.
\end{itemize}

\begin{figure}
\centering
\begin{tikzpicture}
    \draw [ultra thick] (-0.5, -0.5) rectangle (4.5, 4.5);

    \foreach \x in {0,...,3}{
        \foreach \y in {0,...,3}{
            \draw [thick,fill=green] (\x, \y) rectangle (\x + 1, \y + 1);
        }
    }
\end{tikzpicture}
\caption{Virtual patch inside a quadrant.}
\label{fig:patch}
\end{figure}

Some interesting questions about this method involve the way in which it
interacts with the p4est mesh:
\begin{itemize}
    \item How will refining be treated? Would be compute gradients between
    quadrants or just look the gradients between cells in the patch?
    \item IO needs to be completely redone to account for these "cells inside
    cells".
    \item what else??
\end{itemize}

This sort of approach is easily generalizable to higher order approximations,
as we would only need to increase the patch size. To note, for an order
two approximation, the minimum value of $m$ is $4$. This is because we need
to account for neighboring quadrants that are of a higher level than
the current quadrant.

\end{document}
