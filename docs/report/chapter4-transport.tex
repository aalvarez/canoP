\chapter{Discretization of the Transport Equation using \texttt{p4est}}\label{chapter:transport}

In this chapter we present a discretization strategy for the transport
equation within the \text{p4est} AMR framework using a classic
\emph{Finite Volume} approach.

We consider  a domain $\Omega$ of dimension $d \in \{2, 3\}$ and the following
\emph{Initial Value Boundary Problem}:
\begin{equation}\label{eq:transport}
\begin{cases}
\pd[t]{c} + \vect{u} \cdot \grad{c} = 0, \\
c(0, \vect{x}) = c_0(\vect{x}),
\end{cases}
\end{equation}
where $c$ is the transported value, $\vect{u}$ is a given \textbf{smooth velocity
field} and $c_0(\vect{x})$ is the initial condition. These functions can be
described as follows:
\[
\begin{array}{RCRCL}
c                       & \colon & [0, T] \times \Omega & \longrightarrow & \R \\
                        &        & (t, \vect{x}) & \longmapsto     & c(t, \vect{x}),
\\[2ex]
\vect{u} = (u, v, w)    & \colon & [0, T] \times \Omega & \longrightarrow & \R^d \\
                        &        & (t, \vect{x}) & \longmapsto     & \vect{u}(t, \vect{x}),
\\[2ex]
c_0                     & \colon & \Omega & \longrightarrow & \R \\
                        &        & \vect{x}      & \longmapsto     & c_0(\vect{x}).
\end{array}
\]
The domain $\Omega$ is discretized and covered by octants that do not overlap,
but are allowed to be non-conforming due to the constraints of the adapted mesh
(see Figure~\ref{fig:schemecell}).

\begin{figure}[!ht]
\begin{center}
\input{figures/scheme_cell.tikz}
\end{center}
\caption{A quadrant with its refined and coarsened face neighbors.}
\label{fig:schemecell}
\end{figure}

Given the heterogeneous form of the neighborhood of an octant, we have
chosen to use notations adapted to non-structured meshes. Let:
\begin{itemize}
    \item $K_i$ be a cell in the mesh, the index $i$ can be considered as the
    global z-index of the octant.
    \item $l_i \in \llbracket 0, b \rrbracket$ be the level of the cell $K_i$.
    \item $l_{max}$ be the level of the most refined octant in the mesh and
    $l_{min}$ be the level of the most coarsened octant in the mesh at a time
    $t_n$.
    \item $\Delta x_i = \Delta y_i = \Delta z_i$ be the size of a cell $K_i$.
    \item $|K_i| = \Delta x^d_i$ be the area (resp. volume) in 2D (resp. 3D)
    of the cell $K_i$.
    \item $K_j$ be a neighbor of $K_i$ of level $l_j \in \{l - 1, l, l + 1\}$.
    \item $|\Gamma_{ij}|$ be the length (resp. area) of the face between
    $K_i$ and $K_j$.
    \item $n_{ij}$ be the orientation of the normal on the face $\Gamma_{ij}$
    with respect to direction of the axis that is orthogonal to $\Gamma_{ij}$:
    \[
    n_{ij} =
    \begin{cases}
    -1, & K_j \text{ is to the left of } K_i, \\
    1, & K_j \text{ is to the right of } K_i. \\
    \end{cases}
    \]
    \item $\mathcal{N}_x(i) = \set{j}{K_j \text{ is a neighbor in the $x$ direction}}$.
    This set can contain neighbors of any level, so we do not know its
    cardinality, but we know that $2 \leq |\mathcal{N}_x(i)| \leq 4$ in 2D (resp.
    $2 \leq |\mathcal{N}_x(i)| \leq 8$ in 3D). $\mathcal{N}_y(i)$ and
    $\mathcal{N}_z(i)$ are defined similarly for the $y$ and $z$ directions.
\end{itemize}

\begin{figure}[!ht]
\begin{center}
\input{figures/scheme_directional.tikz}
\end{center}
\caption{Left and right, potentially refined, neighbors of a quadrant.}
\label{fig:neighbors}
\end{figure}

\begin{remark}
In the context of AMR, where neighbors can have different sizes (see
Figure~\ref{fig:neighbors}), what we call the face between the octant $K_i$
and its neighbors $K_j$ can be only a portion of a face of $K_i$. For this
reason, the length (resp. area) $|\Gamma_{ij}|$ of the face $\Gamma_{ij}$ is
given by:
\[
|\Gamma_{ij}| = \beta_{ij} \Delta x^{d - 1}_i,
\]
where
\[
\beta_{ij} =
\begin{cases}
2^{1 - d}, & l_j > l_i, \\
1,      & l_j \leq l_i.
\end{cases}
\]
\end{remark}
\begin{remark}
$\Delta x_i = \Delta y_i = \Delta z_i$ is a hypothesis we have made for the sake of
simplicity. \texttt{p4est} has support for more complex geometries that would
require the computation of each length. For the sake of simplicity, we detail
all the computation on a reference cube $[0, 1]^d$ that was scaled from
the \texttt{p4est} reference cube $[0, 2^b]^d$. This means that the size of
each octant ca be computed using only the level of the octant $K_i$:
\[
\Delta x_i = 2^{-l_i}.
\]
\end{remark}

\section{Dimensional Splitting}

We choose to adopt a \textbf{dimensional splitting} strategy for discretizing~\eqref{eq:transport}.

For the case of \eqref{eq:transport} using dimensional splitting consists in
successively approximating~\eqref{eq:splitx},~\eqref{eq:splity} and~\eqref{eq:splitz}
that ultimately gives an update formula for the values in each octant.
\begin{numcases}{}
& $\pd[t]{} c + u \pd[x]{} c = 0$, \label{eq:splitx}\\
& $\pd[t]{} c + v \pd[y]{} c = 0$, \label{eq:splity}\\
& $\pd[t]{} c + w \pd[z]{} c = 0$. \label{eq:splitz}
\end{numcases}
We refer the reader to \cite[p. 543]{TORO} for more details about
dimensional splitting. We are left with providing a discretization for a
one-dimensional problem that will be successively applied to
\eqref{eq:splitx},~\eqref{eq:splity} and~\eqref{eq:splitz}.

Let us briefly recall a possible guideline for obtaining a Finite Volume
approximation of \eqref{eq:splitx}. Suppose for the sake of simplicity that
$c$ is smooth and verifies \eqref{eq:splitx}, then we have:
\[
\tpd[t]{} c + u \tpd{} c = \tpd[t]{} c + \tpd{} (u c) - c\,\tpd{} u = 0.
\]
By integrating over $[t_n, t_{n + 1}] \times K_i$ we obtain:
\[
\int_{t_n}^{t_{n + 1}} \int_{K_i} \tpd[t]{}c \dx[\vect{x}] \dx[t] +
\int_{t_n}^{t_{n + 1}} \int_{K_i} \tpd{} (u c) \dx[\vect{x}] \dx[t] -
\int_{t_n}^{t_{n + 1}} \int_{K_i} c\,\tpd{} u \dx[\vect{x}] \dx[t] = 0,
\]
namely:
\begin{equation}
\begin{aligned}
0 \quad = \quad &  \int_{K_i} (c(t_{n + 1}, \vect{x}) - c(t_n, \vect{x})) \dx[\vect{x}]\ \ + \\
    & \sum_{j \in \mathcal{N}_x(i)} n_{ij}
    \int_{t_n}^{t_{n + 1}} \int_{\Gamma_{ij}} (uc) (t,\vect{x}) \dx[\sigma] \dx[t]\ \ - \\
    & \int_{t_n}^{t_{n + 1}} \int_{K_i} c(t,\vect{x}),\tpd{} u(t,\vect{x}) \dx[\vect{x}] \dx[t].
\end{aligned}
\label{eq:transport1d}
\end{equation}

We note:
\begin{align*}
c_{i}^n
&\text{\qquad an approximation of\qquad }
\frac{1}{|K_i|} \int_{K_i} c(t_n, \vect{x}) \dx[\vect{x}],
\\
u_{ij}^n
&\text{\qquad an approximation of\qquad }
\frac{1}{\Delta t |\Gamma_{ij}|}
\int_{t_n}^{t_{n + 1}} \int_{\Gamma_{ij}} u(t, \vect{x}) \dx[\sigma] \dx[t],
\\
u_{ij}^n c_{ij}^n
&\text{\qquad an approximation of\qquad }
\frac{1}{\Delta t |\Gamma_{ij}|}
\int_{t_n}^{t_{n + 1}} \int_{\Gamma_{ij}}
(uc) (t,\vect{x}) \dx[\sigma] \dx[t],
\end{align*}
where $c^n_{ij}$ is the numerical flux of the discretization. The Finite
Volume approximation we consider consists in setting a recursive update formula
for $c_{i}^n$ that is a discrete equivalent of~\eqref{eq:transport1d}. Therefore
we require that:
\begin{equation}\label{eq:transportupdate}
c^{n + 1}_i = c^n_i + \frac{\Delta t}{|K_i|}
\sum_{j \in \mathcal{N}_x(i)} |\Gamma_{ij}|\ u_{ij}^n\ n_{ij} (c^n_i - c^n_{ij}).
\end{equation}

\begin{remark}
In the context of dimensional splitting, $c^n_i$ is not the solution at time
$t_n$ because intermediate values are computed for each dimension. This is an
abuse of notation that we have also used in the case of $c^{n + 1}_i$.
\end{remark}

\begin{remark}
The update formula can be simplified, using the exact formulas of $|K_i|$ and
$|\Gamma_{ij}|$, to:
\begin{equation}\label{eq:transportupdate2}
c^{n + 1}_i = c^n_i + \frac{\Delta t}{\Delta x_i}
\sum_{j \in \mathcal{N}_x(i)} \beta_{ij}\ u_{ij}^n\ n_{ij} (c^n_i - c^n_{ij}).
\end{equation}
\end{remark}

% TODO: notation for c^{n + 1} and c^n.

The update formula~\eqref{eq:transportupdate} for the one-dimensional
equation~\eqref{eq:splitx} can be obtained for deriving update relations that
provides an approximation of~\eqref{eq:splity} and~\eqref{eq:splitz}. Finally
the approximation of the solution of \eqref{eq:transportsplitting} from
$t_n$ to $t_{n + 1}$, is obtained by performing the three following update
steps:
\begin{equation}\label{eq:transportsplitting}
\left\{
\begin{aligned}
c_{i}^* & = c_{i}^n + \frac{\Delta t}{|K_i|}
\sum_{j \in \mathcal{N}_x(i)} |\Gamma_{ij}|\ u_{ij}^n\ n_{ij} (c^n_i - c^n_{ij}),
\\
c_{i}^{**} & = c_{i}^* + \frac{\Delta t}{|K_i|}
\sum_{j \in \mathcal{N}_y(i)} |\Gamma_{ij}|\ u_{ij}^n\ n_{ij} (c^*_i - c^n_{ij}),
\\
c_{i}^{n + 1} & = c_{i}^{**} + \frac{\Delta t}{|K_i|}
\sum_{j \in \mathcal{N}_z(i)} |\Gamma_{ij}|\ u_{ij}^n\ n_{ij} (c^{**}_i - c^n_{ij}).
\end{aligned}
\right.
\end{equation}

The scheme~\eqref{eq:transportsplitting} is a \emph{first order} dimensional
splitting scheme. Other splitting strategies like the Strang splitting
technique~\cite{STRANG68} can allow to obtain \emph{second order} accuracy.

\section{Flux Choice: The Upwind Scheme}

In order to complete the definition of the numerical
scheme~\eqref{eq:transportsplitting}, we only need to propose a definition for
the numerical flux $c_{ij}^n$. We choose here to adopt the simple upwind scheme,
as follows:
\begin{equation}\label{eq:upwind}
c_{ij}^n =
\begin{cases}
c_i^n & \quad \text{if } u_{ij}^n\,n_{ij} \geq 0, \\
c_j^n & \quad \text{if } u_{ij}^n\,n_{ij} < 0.
\end{cases}
\end{equation}
The upwind flux, defined above, and the update formula~\eqref{eq:transportupdate}
define a complete scheme that we can use to solve the transport equation.

\begin{property}
The scheme defined by~\eqref{eq:transportsplitting} and~\eqref{eq:upwind} is
stable under the Courant-Friedrichs-Lewy (CFL) condition:
\begin{equation}\label{eq:upwindcfl}
\Delta t \max_{s \in \{x, y, z\}} \max_{i}
\left(
-\frac{1}{|K_i|}\sum_{\substack{j \in {\mathcal{N}_{s}(i)} \\ u_{ij}^n\,n_{ij} < 0}} |\Gamma_{ij}| u_{ij}^n\,n_{ij}
\right) \leq 1.
\end{equation}
\end{property}

\begin{proof}
For the purpose of this proof, we will look at how to obtain a sufficient
condition for stability in the $x$ direction and then, by using similar
techniques in the other directions, we will derive~\eqref{eq:upwindcfl}.

A sufficient condition for $L^\infty$ stability can be obtained by imposing
a maximum principle on the update from $c^{n}_i$ to $c^{n + 1}_i$. Such
a maximum principle can be obtained by requiring that $c^{n + 1}_i$ is a
convex combination of $c^n_i$ and its downwind neighbors: $c^n_j$ when $j$ is
such that $u_{ij}^n\,n_{ij} < 0$).

We recall the simplified update formula from~\eqref{eq:transportupdate2}:
\[
c^{n + 1}_i = c^n_i + \frac{\Delta t}{\Delta x_i}
\sum_{j \in \mathcal{N}_x(i)} a_{ij}^n (c^n_i - c^n_{ij}),
\]
where:
\[
a_{ij}^n = \beta_{ij} u_{ij}^n n_{ij}.
\]
By using~\eqref{eq:upwind}, we see that the terms with $a_{ij}^n > 0$ cancel
out. We are left with:
\[
c_i^{n + 1} =
\left(1 + \frac{\Delta t}{\Delta x_i} \sum_{\substack{j \in \mathcal{N}_x(i) \\
                                       a_{ij}^n < 0}} a_{ij}^n \right) c_i^n -
\frac{\Delta t}{\Delta x_i} \sum_{\substack{j \in \mathcal{N}_x(i) \\
      a_{ij}^n < 0}} a_{ij}^n c_{j}^n.
\]
For a convex combination to be possible, we need to satisfy:
\begin{equation}\label{eq:cicoefficients}
0 \leq 1 + \frac{\Delta t}{\Delta x_i} \sum_{\substack{j \in \mathcal{N}_x(i) \\
                                           a_{ij}^n < 0}} a_{ij}^n \leq 1
\end{equation}
and, for all $j \in \mathcal{N}_x(i)$ such that $a_{ij}^n < 0$,
\begin{equation}\label{eq:cjcoefficients}
0 \leq -\frac{\Delta t}{\Delta x_i} a_{ij}^n \leq 1.
\end{equation}
Since \eqref{eq:cicoefficients} implies~\eqref{eq:cjcoefficients}, we would
only need to satisfy:
\[
0 \leq -\frac{\Delta t}{\Delta x_i} \sum_{\substack{j \in \mathcal{N}_x(i) \\
                                        a_{ij}^n < 0}} a_{ij}^n \leq 1.
\]
As $a_{ij}^n < 0$, a \emph{sufficient} condition for stability in a cell $K_i$
is:
\[
-\frac{\Delta t}{\Delta x_i} \sum_{\substack{j \in \mathcal{N}_x(i) \\
                                        a_{ij}^n < 0}} a_{ij}^n \leq 1.
\]
The condition for all cells in the mesh is then:
\begin{equation}\label{eq:cflx}
\Delta t \max_{i} \left(
-\frac{1}{\Delta x_i}\sum_{\substack{j \in \mathcal{N}_x(i) \\ a_{ij}^n < 0}} a_{ij}^n
\right) \leq 1.
\end{equation}
From~\eqref{eq:cflx}, we can deduce a sufficient condition for stability
in all directions:
\begin{equation}\label{eq:cflall}
\Delta t \max_{s \in \{x, y, z\}} \max_{i} \left(
-\frac{1}{\Delta x_i}\sum_{\substack{j \in \mathcal{N}_s(i) \\ a_{ij}^n < 0}} a_{ij}^n
\right) \leq 1.
\end{equation}
As a result, we see that $c^*_{i}$ is a convex combination of $c^n_{i}$ and the
value of its downwind neighbors, $c^{**}_{i}$ is a convex combination of
$c^*_{i}$ and the value of its downwind neighbors and $c^{n + 1}_{i}$ is a convex
combination of $c^{**}_{i}$ and the value of its downwind neighbors. This
implies that $c^{n + 1}_{i}$ is a convex combination of $c^{n}_{i}$ and the
value of its downwind neighbors at time $t_n$.

The convex combination that is imposed by~\eqref{eq:cflall} implies that
a local maximum principle has been satisfied. By respecting the maximum
principle, we can be sure that our scheme is stable, thus proving that~\eqref{eq:cflall},
that is equivalent to~\eqref{eq:upwindcfl}, is sufficient for the stability of
the scheme.
\end{proof}

The CFL condition~\eqref{eq:upwindcfl} allows us to derive a strategy for
choosing a time step. We set:
\begin{equation}\label{eq:upwinddeltat}
\Delta t = \theta \frac{\Delta x}{{\displaystyle\max_{s \in \{x, y, z\}} \max_i}
\left(-\sum_{\substack{j \in \mathcal{N}_s(i) \\ a_{ij}^n < 0}} a_{ij}^n\right)}
\end{equation}
for a \emph{Courant number} $\theta \in [0, 1]$ and a $\Delta x$ that represents
the size of the finest octant in the mesh, given by:
\[
\Delta x = 2^{-l_{max}}.
\]

\section{Time Subcycling}

We can imagine two different way of advancing the solution of our equation
in time:
\begin{itemize}
    \item By using the time step provided by \eqref{eq:upwinddeltat}
    to advance all the cells at all the different levels at the same time.
    This algorithm is described by~\ref{alg:simpleupdate}.
    \item By advancing each level of the mesh with a different time step. This
    strategy is described by Algorithm~\ref{alg:subcyclestep} and~\ref{alg:subcycle}.
\end{itemize}

\begin{algorithm}
  \caption{Advancing all levels in the $x$ direction by $\Delta t$.}
  \label{alg:simpleupdate}
  \KwData{Time step $\Delta t$}
  Initialize all $c^{*}_i$ to $c^n_i$\;
  \For{All octants $K_i$}{
    \For{All $j \in \mathcal{N}_x(i)$}{
        $c^*_i \leftarrow c^*_i + \frac{\Delta t |\Gamma_{ij}|}{|K_i|} u_{ij}^n n_{ij} (c_j^n - c_{ij}^n)$\;
    }
  }
\end{algorithm}

To advance each level at its own pace, we have to define a per-level
time step, which we choose to estimate by:
\begin{equation}\label{eq:subcycledeltat}
\Delta t_l = 2^{l_{max} - l} \Delta t,
\end{equation}
by using the $\Delta t$ estimation of the time step from~\eqref{eq:upwindcfl} as a starting
point. Since~\eqref{eq:upwindcfl} gives us a time step that is equivalent to
one computed on an uniform mesh of level $l_{max}$, we use it to estimate
the time steps at coarser levels where the size of an octant grows with a factor
of $2$ in each dimension.

\begin{remark}
More advanced methods of computing the per-level time step can be used,
but it is always necessary to synchronize them with the levels above. So,
if we have a set of $n_j$ time steps $\Delta t_l^i$ at level $l$, the following
condition has to be respected for a sub-time step at a higher level:
\[
\Delta t_{l - 1}^j = \sum_{i = 1}^{n_j} \Delta t_l^i.
\]
\end{remark}

In Algorithm~\ref{alg:subcyclestep} and Algorithm~\ref{alg:subcycle} we explain
how to do subcycling using a per-level time step as given by~\eqref{eq:subcycledeltat}.

Algorithm~\ref{alg:subcyclestep} describes how to advance a single level in
a specific direction (we have chosen $x$, but other directions are handled
in a similar fashion). The general algorithm that advances \emph{all} the
octants of the mesh by $\Delta t_{min} = 2^{l_{max} - l_{min}} \Delta t$ in
the $x$ direction is~\ref{alg:subcycle}. The final step is to
repeat~\ref{alg:subcycle} for the $y$ and $z$ directions. Note that this
is a valid method of advancing in time, but it is not equivalent to advancing
all the quadrants using the same $\Delta t$.

\begin{remark}
The key difference between both methods is that after
Algorithm~\ref{alg:simpleupdate}, we have advanced by a very small $\Delta t$,
more precisely $\Delta t$ is $2^{l_{max} - l_{min}}$ times smaller then the
one by which the solution is advanced by subcycling. This difference comes from
the difference between the finest and the coarsest levels in the
mesh and it is what makes the subcycling algorithm faster in some cases.
\end{remark}

The recursive Algorithm~\ref{alg:subcycle} is called with the coarsest level
in the mesh $l_{min}$ and the time step corresponding for this level:
\[
\Delta t_{l_{min}} = 2^{l_{max} - l_{min}} \Delta t.
\]

\begin{algorithm}[!ht]
  \caption{Advancing level $l$ in the $x$ direction.}
  \label{alg:subcyclestep}
  \KwData{Level $l$}
  \KwData{Time step at current level $\Delta t_l$}
  Initialize all $c^{*}_i$ to $c^n_i$\;
  \For{All octants $K_i$ of level $l$}{
    \For{All $j \in \mathcal{N}_x(i)$}{
      \If{$l_j \leq l_i$ \textbf{and} ($K_j$ to the right of $K_i$ \textbf{or} a boundary)}{
        $\displaystyle c^*_i \leftarrow c^*_i + \frac{\Delta t |\Gamma_{ij}|}{|K_i|} u_{ij}^n (c_j^n - c_{ij}^n)$\;
        $\displaystyle c^*_j \leftarrow c^*_j - \frac{\Delta t |\Gamma_{ji}|}{|K_j|} u_{ij}^n (c_i^n - c_{ji}^n)$\;
      }
      \If{$l_j = l_i - 1$ \textbf{and} ($K_j$ to the left of $K_i$ \textbf{or} a boundary)}{
        $\displaystyle c^*_i \leftarrow c^*_i - \frac{\Delta t |\Gamma_{ij}|}{|K_i|} u_{ij}^n (c_j^n - c_{ij}^n)$\;
        $\displaystyle c^*_j \leftarrow c^*_j + \frac{\Delta t |\Gamma_{ji}|}{|K_j|} u_{ji}^n (c_i^n - c_{ji}^n)$\;
      }
    }
  }
  \For{All ghost octants $K_i$ of level $l$ from other processes}{
    Let $K_j$ be the local neighbor of $K_i$\;
    \If{$l_j \leq l_i$ \textbf{and} $K_j$ to the right of $K_i$}{
      $\displaystyle c^*_j \leftarrow c^*_j - \frac{\Delta t |\Gamma_{ji}|}{|K_j|} u_{ji}^n (c_i^n - c_{ji}^n)$\;
    }
    \If{$l_j = l_i - 1$ \textbf{and} $K_j$ to the left of $K_i$}{
      $\displaystyle c^*_j \leftarrow c^*_j + \frac{\Delta t |\Gamma_{ji}|}{|K_j|} u_{ji}^n (c_i^n - c_{ji}^n)$\;
    }
  }
\end{algorithm}

Algorithm~\ref{alg:subcyclestep} is based on previous work from~\cite{KHOKHLOV}
and is very similar to the subcycling algorithm presented there. Two differences
set it apart:
\begin{itemize}
    \item Our proposed algorithm does not compute a \emph{mean} of the values
in a family of octants to pass it down to its parents. This is done because,
in contrast to~\cite{KHOKHLOV}, where all the tree structure is stored,
\texttt{p4est} only stores the leaves of the tree so it is not necessary to
propagate the information upwards.
    \item We extend the algorithm to work in a distributed memory environment
    like the one offered by \texttt{p4est}. In \texttt{p4est} we have two
    types of ghost cells: classical ghost cells that are used for
    \emph{boundary conditions} and ghost cells that are part of the mesh,
    but are not in the partition of the current process.
\end{itemize}

\begin{algorithm}[!ht]
  \caption{Advancing by $\Delta t$ in the $x$ direction.}
  \label{alg:subcycle}
  \KwData{Level $l$, time step at level $l$}
  $\Delta t_{l + 1} \leftarrow \frac{\Delta t_l}{2}$\;
  \texttt{subcycle} ($l + 1$, $\Delta t_{l + 1}$)\;
  \texttt{subcycle} ($l + 1$, $\Delta t_{l + 1}$)\;
  Exchange ghost layer with neighboring processes\;
  Advance level $l$ in time by $\Delta t_l$ using~\ref{alg:subcyclestep}\;
\end{algorithm}

\begin{figure}[!ht]
\begin{center}
\input{figures/subcycle_ghosts.tikz}
\end{center}
\caption{Two octants in different processes.}
\label{fig:subcycleghost}
\end{figure}

To deal with ghost cells from other processes we have added a second loop to
Algorithm~\ref{alg:subcyclestep} to guarantee that all the fluxes at
a certain level are correctly calculated. We can take the example from Figure~\ref{fig:subcycleghost}
where we have two octants of level $l$ and $l - 1$ for $o_1$ and $o_2$, respectively, in
two different processes where each is a face ghost cell of the other. The
problem arises when we loop over all cells of levels $l$ in process $p_2$
and do not, in fact, add the contribution from the ghost cell $o_1$. To fix
this, we have chosen to also loop over all ghost cells and compute the
correct interface flux with the local octants. Other solutions are also
feasible, such as looping over local cells of level $l$ and $l - 1$ during
one subcycle, exchanging the fluxes between processes, etc.

An important aspect of Algorithm~\ref{alg:subcyclestep}, as of the one
proposed in~\cite{KHOKHLOV}, is that it computes the left and right fluxes
in such a way that the same flux is not computed twice when recursing between
levels. We can see this in Figure~\ref{fig:subcycleflux}.

\begin{figure}[!ht]
\begin{center}
\input{figures/subcycle_flux.tikz}
\end{center}
\caption{Flux computations at level $l$ (in red) and level $l - 1$ (in blue).}
\label{fig:subcycleflux}
\end{figure}

Such a subcycling algorithm offers significant speedups because fewer time steps are
required when compared to the straightforward time advancing scheme described
in Algorithm~\ref{alg:simpleupdate} (coarse cells make big time steps). One
drawback of such a scheme is the need to adapt the mesh at each sub-time step.
We will see in the numerical results section that the solution will advance
too quickly and go into coarser cells if we only adapt once before starting
the subcycling procedure, thus leading to unnecessary diffusion and loss of
precision.

\section{Numerical Results}

In this section we will look at several numerical results we have obtained
for the transport equation using the \texttt{p4est} framework and the presented
AMR algorithms. In the tests we also use a second order space discretization
for comparison reasons.

\subsection{Advection on the Diagonal}

As a first test for the transport equation we will propose the advection
of a star-shaped indicator function on the diagonal of our domain
$\Omega = [0, 1]^2$.

The initial condition for the star is:
\[
c_0(x, y) =
\begin{cases}
1, & (x, y) \in \mathcal{A}, \\
0, & \text{otherwise},
\end{cases}
\]
where $\mathcal{A}$ will define our star:
\[
\mathcal{A} =
\set{(x, y)}{\frac{1}{3} - \abs{x - \frac{1}{2}} < y < \frac{2}{3} - 4\abs{x - \frac{1}{2}}}
\cup
\set{(x, y)}{\frac{1}{3} + \abs{x - \frac{1}{2}} < y < \frac{1}{2}}.
\]
We also define the velocity vector $\vect{u} = (1, 1, 0)$, the final time
$T = 1.0$ and the Courant number $\theta = 1.0$ for the first order scheme
and $\theta = 0.5$ for the second order scheme. In the case of the second
order scheme we will use the \texttt{minmod} limiter.

Using these parameters, we know that the discrete \emph{first order} approximation
of the transport equation is exact and we can indeed see this in Figure~\ref{fig:star_order1}.
Note that the solution is exact when transporting on the diagonal because of
the dimensional splitting technique we have used. Without directional
splitting, the result would be exact only if the velocity was one of the
standard basis vectors $\vect{e}_i$.

Unfortunately, given the refining criterion we have used, there are several
cells that have been diffused in the first step of the simulation (on the right
side of the star). A cure to this issue could consists in adopting a different
refinement criterion that refines more cells around the star.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/star_initial}
  \captionof{figure}{$c$ at $t = 0.0$.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/star_order1_0375}
  \captionof{figure}{$c$ at $t = 0.375$.}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/star_order1_075}
  \captionof{figure}{$c$ at $t = 0.75$.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/star_order1_final}
  \captionof{figure}{$c$ at $t = 1.0$.}
\end{subfigure}%
\caption{First order approximation of the advection of a star shape at different times.}
\label{fig:star_order1}
\end{figure}

We can see that by using a second order approximation we loose the match with
the exact solution. Nonetheless, the second order scheme is more accurate than
a the first order scheme (easily tested with $\theta \neq 1$ for the first
order scheme). We can see the results for the second order approximation in
Figure~\ref{fig:star_order2}.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/star_order2_middle}
  \captionof{figure}{$c$ at $t = 0.5$.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/star_order2_final}
  \captionof{figure}{$c$ at $t = 1.0$.}
\end{subfigure}%
\caption{Second order approximation of the advection of a star shape at different times.}
\label{fig:star_order2}
\end{figure}

In Figure~\ref{fig:star_refinement} we can also see the way in which the
refinement was performed. Since both the first order and the second order
approximations have barely any diffusion, the number of quadrants necessary to
represent the two solutions are comparable.

\begin{remark}
The refinement criterion for the star was changed to refine any cell where the
value is greater than $0.001$. This is done to limit the dissipation problem
that has appeared in the second order approximation due to passing into coarser
levels while doing dimensional splitting.
\end{remark}

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/star_refinement}
  \captionof{figure}{First order.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/star_refinement_order2}
  \captionof{figure}{Second order.}
\end{subfigure}%
\caption{Refinement at the end of the simulation for the first order and second order
approximations.}
\label{fig:star_refinement}
\end{figure}

\subsubsection{Advection with subcycling}

We have seen that the solution is correctly transported when using a normal
scheme with a single time step. This is no longer the case when doing
time subcycling.

In the case of subcycling, the time step at which we advanced the whole mesh
is given by the coarsest level. Considering the fact that the velocity is
$\vect{u} = (1, 1, 0)$, we know the time step given by the CFL conditions is
\[
\Delta t = \theta \Delta x,
\]
where $\Delta x$ is the size of a cell at a certain level. Given the minimum
level $l_{min}$ and maximum level of refinement $l_{max}$ at a certain time step,
we know that the $\Delta t$ when doing subcycling is $2^{l_{max} - l_{min}}$
times larger than the time step we would obtain with the normal time stepping
scheme.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/subcycle_initial}
  \captionof{figure}{$c$ at $t = 0$.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.7\linewidth]{img/transport/star/subcycle_step}
  \captionof{figure}{$c$ at $t = 0.015625$.}
\end{subfigure}%
\caption{Solution after a single time step using subcycling.}
\label{fig:star_subcycle}
\end{figure}

However, the fact that we are advancing several steps at finer levels has its
downsides. As we can see in Figure~\ref{fig:star_subcycle}, the solution
advances quicker than the refinement leading to a lot of diffusion. This is
obviously incorrect because the upwind scheme for the transport equation is
exact for the chosen velocity and Courant number.

A few ways to circumvent the problem come to mind, such as:
\begin{itemize}
    \item refining the mesh at each \emph{sub-time step} during subcycling.
    While this would solve our problem, it would greatly slow down the computation
    since adapting the mesh is the most costly operation.
    \item adding extra layers or refined cells to the existing mesh to be sure
    that the fine levels don't overflow into the coarser levels. This has the
    downside of potentially adding many more cells to the mesh.
\end{itemize}

Even though advancing in time with larger steps potentially speeds up the
computations a great deal, the downsides of subcycling and the proposed solutions
even out the gains. Further study is needed into the method of subcycling.

\subsection{Rotation of Zalesak's Disk}

The second example we will look at is a disk in solid body rotation. In
this example we can expect a lot of numerical diffusion from the upwind
scheme. We will try to compare the solutions using a first order approximation
and a second order approximation with the \texttt{minmod} limiter.

The initial solution we have chosen is a famous test called the \emph{Zalesak Disk}
that was first presented by S. T. Zalesak in~\cite{ZALESAK}. It is given by:
\[
c_0(\vect{x}) =
\begin{cases}
    1, & \sqrt{(x - 0.5)^2 + (y - 0.75)^2 + (z - 0.5)^2} \leq 0.1, \\
    0, & \text{otherwise}
\end{cases}
\]
and the velocity vector is:
\[
\vect{u} = (-(y - 0.5), x - 0.5, 0).
\]
We will use a final time $T = 6.283$ (to get a complete rotation) and a
Courant number $\theta = 1.0$ for the first order approximation and $\theta = 0.5$
for the second order approximation. Note that for the second order approximation,
$\theta = 0.5$ is the maximum allowed value.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.6\linewidth]{img/transport/zalesak/zalesak_initial_order1}
  \captionof{figure}{$c$ at $t = 0$.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.6\linewidth]{img/transport/zalesak/zalesak_final_order1}
  \captionof{figure}{$c$ at $t = 6.283$.}
  \label{fig:zalesak_order1_final}
\end{subfigure}%
\caption{First order approximation of the advection of the Zalesak Disk.}
\label{fig:zalesak_order1}
\end{figure}

We can see in Figure~\ref{fig:zalesak_order1} that the disk is correctly
transported and the refinement correctly follows and grows with the
dissipation. The dissipation is, however, very pronounced and we can hardly
distinguish the solution at the final time (Figure~\ref{fig:zalesak_order1_final}).
The second order approximation (see Figure~\ref{fig:zalesak_order2}) gives
a significantly improved result with hardly any dissipation. We can see however
that the solution gets deformed in other ways.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.3\linewidth]{img/transport/zalesak/zalesak_final_order2}
\caption{Second order approximation of the advection of the Zalesak Disk at time $t = 6.283$.}
\label{fig:zalesak_order2}
\end{figure}

In Figure~\ref{fig:zalesak_comparison}, we have taken a slice at $y = 0.25$ of
the first and second order approximations at time $t = 0.5 T$. We can indeed
see the difference between the exact solution and the two approximations. The
first order approximation is the worse of the two, as expected and seen
in Figure~\ref{fig:zalesak_order1}.

\begin{figure}[!ht]
\centering
\begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{img/transport/zalesak/zalesak_middle_line_order1}
  \captionof{figure}{First order.}
\end{subfigure}%
\begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{img/transport/zalesak/zalesak_initial_line_order1}
  \captionof{figure}{Exact Solution.}
\end{subfigure}%
\begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=\linewidth]{img/transport/zalesak/zalesak_middle_line_order2}
  \captionof{figure}{Second order.}
\end{subfigure}%
\caption{Comparison of the first and second order approximation at time $t = 3.1415$.}
\label{fig:zalesak_comparison}
\end{figure}

As for the mesh refinement, we can see in Figure~\ref{fig:zalesak_refinement}
that the area containing most of the dissipation from the first order scheme
has been correctly captured by our refining criteria.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.3\linewidth]{img/transport/zalesak/zalesak_refinement}
\caption{Refinement of the first order approximation at time $t = 3.1415$.}
\label{fig:zalesak_refinement}
\end{figure}

\subsection{Deformation in 2D}

The last test case that we will study for the transport equation is a
test proposed by R. J. LeVeque in~\cite{LV96}. The test consists of
stretching and un-stretching a disk in a vortex from which we will look
only at the stretching of the disk and the resulting refinement.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.3\linewidth]{img/transport/vortex/vortex_initial}
\caption{Initial condition for the vortex, as given by $c_0(\vect{x})$.}
\label{fig:vortex_initial}
\end{figure}

The initial condition is a disk centered in $(0, 0.75)$ with a radius of $0.15$:
\[
c_0(\vect{x}) =
\begin{cases}
    1, & \sqrt{x^2 + (y - 0.75)^2} \leq 0.15., \\
    0, & \text{otherwise}
\end{cases}
\]
with a rotating velocity such as:
\[
\vect{u} = (2\sin^2(\pi x) \sin(2\pi y), -\sin(2\pi x) \sin^2(\pi y)).
\]
The final time for the simulation is $T = 3.0$ and the Courant number is
$\theta = 0.5$. The test has been carried out with a second order scheme
and the \texttt{minmod} limiter.

The results of a second order approximation can be seen in
Figure~\ref{fig:vortex_order2}. The results are not very diffusive and the vortex
forms as one would expect (given previous results from~\cite{LV96}).

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.6\linewidth]{img/transport/vortex/vortex_middle_order2}
  \captionof{figure}{$c$ at $t = 1.5$.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.6\linewidth]{img/transport/vortex/vortex_final_order2}
  \captionof{figure}{$c$ at $t = 3$.}
\end{subfigure}%
\caption{Second order approximation of the vortex at intermediate time steps.}
\label{fig:vortex_order2}
\end{figure}

\begin{figure}[!ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.65\linewidth]{img/transport/vortex/vortex_middle_refinement_order1}
  \captionof{figure}{First order.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.65\linewidth]{img/transport/vortex/vortex_middle_refinement_order2}
  \captionof{figure}{Second order.}
\end{subfigure}%
\caption{Refinement of the solution at time $t = 1.5$ for the first and second order
approximations.}
\label{fig:vortex_refinement}
\end{figure}

We can see in Figure~\ref{fig:vortex_refinement} that the refinement
correctly follows the disk as it deforms. In this case, the diffusion of the
first order approximation is so great that the fine quadrants eventually cover
most of the domain, making AMR a lot less useful.
