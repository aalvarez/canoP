\chapter{Adaptive Mesh Refinement}\label{chapter:amr}

In this chapter we will try to give a short history of and introduction to the
world of \emph{Adaptive Mesh Refinement}.

When looking to solve ordinary or partial differential equations, we first
proceed by discretizing the domain. This involves choosing a set of points
from the domain and defining the unknowns of our systems at those points. This
can be achieved by creating a mesh (or grid) or by using mesh-free methods that
only involve the points, with no structure.

Even within methods involving meshes there is great variation: the mesh can
be \textbf{structured} (elements have clear neighbors in each direction) or
\textbf{unstructured} (sizes and shapes differ between elements),
\textbf{conforming} (two elements can only share a complete edge) or
\textbf{non-conforming} (an edge can be shared between more than two elements),
etc. Usually, these classic mesh-based methods involve a mesh that, once it is
constructed, does not change while the solution is being computed.

Unlike typical meshes, AMR is a technique that permits changing the mesh
while the solution to the differential equations is calculated. This can be
achieved in multiple ways, two of which we will look at in detail.

The first method is called \textbf{block-based} (or \textbf{patch-based}) AMR
and it involves stacking multiple grids on top of each other in
regions where more accuracy is required. The extra grids are always finer than
the original grid they are supposed to replace. Once the extra grids are constructed,
the equations are solved normally on the finer grids and then interpolated to the
coarser ones.

A second method involves modifying the original mesh by splitting its elements
into multiple parts and is known as \textbf{cell-based} AMR. This method is
a larger departure from the usual static meshes as it employs the use of
trees to store the mesh information and easily refine and coarsen specific cells.

Cell-based AMR can be applied equally to structured and non-structured meshes
and always produces non-conforming elements. This is due to the fact that
there will always be a cell that neighbors a refined cell (if there are
no such cells, the mesh becomes uniform).

An alternative to AMR is the use of the previously mentioned mesh-free
methods such as the \textbf{Smoothed Particle Hydrodynamics}~\cite{SPH} method
that is used to simulate fluid flows. This type of Lagrangian method is very similar
(in results) to \emph{Adaptive Mesh Refinement} because it will naturally
gather more points in the regions where the flow changes faster and thus
allows for more accuracy where needed. It does have its downsides as well,
some of which include the inherent diffusive nature of the method thanks to
the use of \emph{kernel functions}, the problematic handling of contact
discontinuities, high numerical viscosity, etc.

Another alternative AMR that has seen quite some attention lately is
\textbf{Wavelet-based AMR}. Wavelets have been used for a long time in the fields
of Signal Processing and Data Compression, in general, to impressive results.
One of the main advantages of wavelet-based AMR is its strong mathematical
foundations that allow for exact error calculation and performance
optimizations. A comprehensive comparison between mesh-based AMR and
Wavelet-based AMR is given in~\cite{WAVELET}.

In the subsequent sections we will talk about the two major types of AMR
that are of interest to us: block-based and cell-based AMR on structured
meshes.

\section{Block-based AMR}

One of the first, if not the first, description of block-based AMR is given
by M. J. Berger and J. Oliger in~\cite{BO84} (1984) with an application to hyperbolic
partial differential equations. This paper was followed by another from
M. J. Berger and P. Collela~\cite{BC89} (1989) which extended the method to
take shocks into consideration and greatly simplified the AMR-related
algorithms.

Both of these papers start with a regular uniform rectangular mesh. As the
solution progresses, we can compute local error estimators that give a criterion
for creating new, coarse grids that are contained in the initial domain. In the
original article, these grids were allowed to be rotated in 2D space, but in~\cite{BC89}
they have been aligned with the coarser grid to ease interpolation, handling
of boundary conditions and construction algorithms.

\begin{figure}[ht!]
\begin{center}
\input{figures/amr_block.tikz}
\end{center}
\caption{Example of overlapped grids.}
\label{fig:amrblock}
\end{figure}

One example of block-based AMR can be seen in Figure~\ref{fig:amrblock}. The
parent-child relationship between the grids is kept using a directed
acyclic graph or using linked lists. In the scheme proposed by~\cite{BC89},
fine grids are allowed to overlap multiple coarser grids and, of course, a
coarse grid can contain any number of finer grids.

Formally, for a set of levels $l \in \{1, \dots, b\}$ and a maximum refinement
level $b$, we define $G_{l, k}$, the $k$-th grid at level $l$. Since the
grids are uniform and rectangular, all the cells in the grid have the same
size, which is $\Delta x_l$. This gives a refining ratio of:
\[
r = \frac{\Delta x_{l + 1}}{\Delta x_l},
\]
which is usually chosen as a power of $2$ to simplify calculations. The union
of all the grids on a level gives $G_l$ and, by extension:
\[
G_1 = \bigcup_k G_{1, k} = D
\]
is the whole domain. Furthermore, the grids must be \emph{properly nested},
which is defined in~\cite{BC89} as:
\begin{itemize}
    \item A fine grid starts and ends at the corner of a cell in the coarser
    grid.
    \item Two overlapping cells have to be separated by exactly one level.
\end{itemize}

Once the boundary conditions are defined from the neighboring grids, a grid at
level $l$ is completely independent from all the others and we can solve
the equations on it as if it were the only one. This gives block-based AMR the
very useful quality of being able to naturally re-use existing codes that
do not adapt the mesh. Once the solutions have been independently calculated,
additional interpolations and flux corrections are required to transmit the
solution to lower grids and ensure qualities such as conservation.

An important part of AMR, which we will only glance at now, is finding a
good refining criterion. In~\cite{BC89}, M. J. Berger and P. Colella look at
the Euler equations and use a finite difference scheme for solving them. Using
the finite difference approximation, they define:
\[
w(x, t + \Delta t) - Qw(x, t) \approx \tau(x, t) +
\Delta t \mathcal{O}(\Delta t^{q + 1} + \Delta x^{q + 1}),
\]
where $q$ is the order of the finite difference approximation and $Q$ is an
operator that gives the desired discretization. To compute the local truncation
error, they compute the solution at time $t + 2 \Delta t$, first by doing two
steps of $\Delta t$, denoted by $Q^2$, and then by doing a single time step
with a coarsened cell size $2 \Delta x$ , denoted $Q_{2 \Delta x}$. The error
is then given by:
\[
\frac{Q^2w(x, t) - Q_{2\Delta x}w(x, t)}{2^{q + 1} - 2} =
\tau(x, t) + \mathcal{O}(\Delta x^{q + 2}).
\]
This is easily achieved on the current mesh structure by advancing two steps in
time with the regular integration scheme and doing another two with every
other point in the grid. Although their use case was rather specific, such a
method can be used with any other type of equations and discretization methods.
Other a posteriori error estimation methods can also be used to the same
effect.

Once the respective cells of the mesh are flagged for refinement, the framework
usually constructs bounding boxes over sets of flag cells and then creates new
layers of meshes with a refinement ratio $r$. This method usually leads to
refined areas where the solution is smooth and does not require a finer mesh,
thus leading to higher memory consumption than one would get with cell-based
AMR where only the required cells are adapted.

The description given in~\cite{BC89} of block-based AMR is still largely in use
today. Some of the frameworks that provide this sort of AMR are:
\begin{description}
    \item[AMRClaw] is part of the bigger \textbf{Clawpack} framework for
    solving linear and non-linear hyperbolic systems of conservation laws.
    \textbf{Clawpack} was originally written by R. LeVeque, one of
    J. Oliger's students, and the introduction of AMR techniques was done
    after a collaboration with M. J. Berger~\cite{BLV98}.
    \item[BoxLib] is another AMR framework that has support for hyperbolic,
    elliptic and parabolic PDEs. One special advantage of BoxLib is that
    it has been proven to scale very well up to 200.000 processes.
    \item[Paramesh] is another general AMR framework developed by NASA. The
    general structure is describe in~\cite{PARAMESH2000}.
    \item[Others.] There are many other AMR codes out there, such as
    \textbf{Chombo} (that shares a common history with BoxLib), \textbf{AMROC},
    \textbf{SAMRAI}, etc.
\end{description}

\section{Cell-based AMR}

We have seen in the previous section a short history of block-based AMR, but
the focus of this work is, of course, on cell-based AMR. Unlike
block-based AMR, where we would refine a bigger portion of a mesh by a
factor $r$, in cell-based AMR each cell is refined independently by a fixed
factor of $2$ in each direction.

If we consider the 1D case, we will need to successively divide into two parts
an interval $[0, L]$. All possible subintervals (or cells) given by this sort of
refinement are of the form:
\begin{equation}\label{eq:amrintervals}
I_{k, l} = \left[\frac{L}{2^l}k, \frac{L}{2^l}(k + 1)\right], \quad k \in \{0, 2^l - 1\},
\end{equation}
where $l \in \{0, b\}$ is the level of refinement that is limited to a maximum
value $b$. For a given level of refinement $l$, we have:
\[
\bigcup_{k} I_{k, l} = [0, L],
\]
but, of course, not every cell gets refined to a fixed level $l$. To handle
the individual refinement of each cell, the most natural data structure to
use is a tree, more specifically: a \textbf{binary tree} for 1D domains,
a \textbf{quadtree} for 2D domains, an \textbf{octree} for 3D.

Unlike block-based AMR, where trees were only used to handle the grid hierarchy,
cell-based AMR makes heavy use of tree structures to store the mesh and perform
modifications on it. The use of new data structures implies new difficulties
in implementing numerical methods (new integration routines, storage strategies
and load balancing techniques need to be developed) and integrating them with
existing codes.

There has been a lot of previous research into quadtrees and octrees
in the world of computer graphics, with the works of R. A. Finkel and J. L. Bentley
in~\cite{FB74} in describing quadtree algorithms and D. Meagher in~\cite{DM82}
in describing octree algorithms. Although these algorithms are not concerned
with numerical simulations, they were still very useful because they
provide efficient ways of searching, storing and modifying these data structures.

\begin{figure}[!ht]
\begin{center}
\input{figures/amr_cell_tree.tikz}
\end{center}
\caption{Example of a quadtree.}
\label{fig:amrquadtree}
\end{figure}

In Figure~\ref{fig:amrquadtree}, we can see an example of a quadtree. A
quadtree, like any other tree structure, is directed (in the sense that
there is only one way from one node to another) and does not contain any
cycles. Some of the main elements of a tree (quadtree or octree) are:
\begin{itemize}
    \item The node on top that is called the \textbf{root node}.
    \item Each node can be tied to $0$ or $4$ (resp. 8 for an octree) other
    nodes beneath it. This node is called the \textbf{parent} and the $4$
    (resp. $8$) nodes beneath it, if they exist, are called \textbf{children}.
    A group of children is sometimes referred to as a \textbf{family} and
    they have \textbf{sibling} relationships between them.
    \item If there are no nodes beneath a certain node, it is called a \textbf{leaf}.
\end{itemize}

This is an abstract definition of a tree (quadtree or octree) that makes no
reference to a discretized grid. In the case of numerical simulations,
each node is likely to represent a certain subset of the whole domain
(see~\eqref{eq:amrintervals}).

\begin{figure}[!ht]
\begin{center}
\input{figures/amr_cell_mesh.tikz}
\end{center}
\caption{Example of a refined square domain that can be represented by a quadtree.}
\label{fig:amrmesh}
\end{figure}

The information that describes the tree needs to be accessible at all times
during a simulation. Such a requirement raises several issues, especially
in high performance environments:
\begin{itemize}
    \item Storing the tree. Tree storage can be \textbf{linear} (in the sense that
    it is stored in a linear array) or make heavy use of \textbf{pointers} from
    parent to children. When storing the tree linearly, we have a choice
    of storing the whole tree or \emph{storing only the leaves}.
    \item Distributing the tree. In parallel environments, the tree structure
    that represents a physical domain has to be partitioned between different
    processes. This can be achieved using \textbf{space-filling curves}
    such as the Hilbert curve or the z-curve.
    \item Scalable algorithms. The data structures and algorithms used have
    to consume as little memory as possible to represent the tree structure
    and as little computations as possible to traverse it.
    \item Representing complex geometries. Most of the cell-based AMR
    algorithms and implementations focus solely on \textbf{square or rectangular
    domains} (see Figure~\ref{fig:amrmesh}).
\end{itemize}

Let us now briefly review $3$ examples, in chronological order, that have
addressed the above issues in different ways, each providing an improvement over the
one before.

\begin{example}
One of the first attempts to tackle these problems in the context of
numerical simulation has been done in~\cite{YMB91} with an application to
computational fluid dynamics in aeronautics. The work was done on octrees
and proposed the following way of describing the data structure: the complete
tree is stored (parents pointed to their children) and simple accumulation
indexes were used to keep track of all the cells in the mesh (see
Figure~\ref{fig:amrcell}).

\begin{figure}[!ht]
\begin{center}
\input{figures/amr_cell_original.tikz}
\end{center}
\caption{Tree Links as described in~\cite{YMB91}.}
\label{fig:amrcell}
\end{figure}

Each node in the tree contains the following information: a pointer to its
parent, 8 consecutive pointers for each of its children (these can be NULL
if the child is not refined), pointers to its neighbors and an accumulation
index that stores the number of nodes in the tree up to this point. Besides
storing the tree structure, each node also has to contain other information
such as coordinates, centroids, size, level, neighborhood, etc. that are
very necessary when doing numerical simulations.
\end{example}

\begin{example}
Storing the whole tree including neighborhood information is very costly,
memory-wise. In fact, the memory requirements are comparable to AMR using
non-structured meshes (where all the tree and the mesh connectivity has to
be stored explicitly). An improvement to the previous example was given
in~\cite{KHOKHLOV} with the introduction of the \textbf{Fully Threaded Tree}.
This new data structure improves the memory requirements of storing an octree
and is easier to parallelize.

The main obstacle to parallelization in~\cite{YMB91} was the fact that each
node contained pointers to its neighbors for easy access. While this had
some benefits, in a parallel environment, it is very difficult to guarantee
that these pointers stay valid while refining and coarsening different cells.
The fully threaded tree stores instead pointers to parents of neighboring
nodes that do not change in a refinement or coarsening pass.

The memory improvements come from the following simple observations:
\begin{itemize}
    \item When a cell is split, all its children are created simultaneously.
    Thus, they can be stored in a single contiguous array (same as in~\cite{YMB91}).
    \item Neighbor information between siblings is known automatically from
    the way they are stored in the array.
    \item The neighbors of a cell are either its siblings, children of its
    parent's siblings or one of its parent's siblings.
\end{itemize}

Much like in~\cite{YMB91}, the nodes of the tree actually hold information
about $8$ cells of the 3D domain and they are called \emph{octants}. As can be
seen in Figure~\ref{fig:amroctant}, an octant contains: a pointer to its
parent, its level, its coordinates, $6$ pointers to parent cells of
neighboring octants and its 8 children. The children usually contain the
state vector of the equations we're trying to solve and a pointer to
another node, if they are refined.

\begin{figure}[!ht]
\begin{center}
\input{figures/amr_cell_khokhlov.tikz}
\end{center}
\caption{An octant as described in~\cite{KHOKHLOV}.}
\label{fig:amroctant}
\end{figure}

A very strong constraint in cell-based AMR that is mentioned in~\cite{KHOKHLOV}
and~\cite{YMB91} is that the level difference between two neighboring octants
cannot be bigger than $1$ (see Figure~\ref{fig:amrinvalid}). This is a
constraint that is often used in AMR (it also applies to block-based AMR) and
it is required mainly to simplify the algorithms. If this constraint is not
imposed, finding a neighbor of a cell would require a recursive algorithm that
traverses the tree starting from a given sibling and the number of neighbor
would increase greatly with the gap between levels.
\begin{figure}[H]
\begin{center}
\input{figures/amr_cell_invalid.tikz}
\end{center}
\caption{Example of (a) a valid refined mesh and (b) a invalid refined meshes.}
\label{fig:amrinvalid}
\end{figure}
\end{example}

\begin{example}
Another iteration on cell-based AMR has been brought to this data
structure in~\cite{JLY2010}. The main improvement proposed here is based on
the simple observation that we could use the triplet $(i, j, l)$,
where $(i, j)$ are indexes of each cell in a uniform Cartesian mesh of
level $l$, to describe the tree. This idea is derived directly from the way
we subdivide a given interval by successive refinements, as seen
in~\ref{eq:amrintervals}.

Given the index $(i, j)$ of a cell, we could easily find the coordinates of
its children:
\[
(i_c, j_c) = \set{(2i + m, 2j + n)}{(m, n) \in \{0, 1\}^2},
\]
its parent:
\[
(i_p, j_p) = \left(\floor{\frac{i}{2}}, \floor{\frac{j}{2}}\right)
\]
and, using the origin $(x_0, y_0)$, even the coordinates in physical space:
\[
(x, y) = \left(x_0 + i\frac{L}{2^l}, y_0 + j\frac{L}{2^l}\right).
\]

\begin{notation}
$\floor{x}$ represents the integer part of $x$.
\end{notation}

\begin{figure}[!ht]
\begin{center}
\input{figures/amr_cell_numbering.tikz}
\end{center}
\caption{Grid indexes as described in~\cite{JLY2010}.}
\label{fig:amrindex}
\end{figure}

Using this indexing system there are simple ways to find the $(i, j)$
coordinates of any neighboring cell once we know those of the current cell.
For example, to find the index of a neighboring cell to the right
that is twice as big, we would simply have to check if the cell with the
following coordinates exists:
\[
\left(\floor{\frac{i + 1}{2}}, \floor{\frac{j}{2}}\right).
\]

To optimize the search for the neighboring cell,~\cite{JLY2010} has proposed
the introduction of a hash table where each octant is associated with a key:
\[
\mathrm{key} = \sum_{l = 0}^{level - 1} 2^{2l} + i 2^{level} + j,
\]
where $(i, j)$ is the index and $level$ is the level of a given octant.
This implies that the tree is no longer stored in memory using pointers, but
linearly using the above mentioned hash table. This hash table allows
$\mathcal{O}(1)$ access to all the neighbors of a given octant.
\end{example}

Some of the libraries that use cell-based AMR are:
\begin{description}
    \item[Ramses] uses the fully threaded tree data structure to implement
    the magnetohydrodynamics equations and the N-body equations for use in
    astrophysics.
    \item[Gerris] is a code that does incompressible Euler and Navier-Stokes
    equations. It has been developed by Stéphane Popinet since 2001 and
    defines its own C-like language for describing the problem, the boundary
    conditions, etc.
    \item[CLAMR] is a code that uses a \emph{hash table}-based approach to
    doing cell-based AMR on the \textbf{GPU} using the \texttt{OpenCL}
    framework. The framework was developed at Los Alamos and is better described
    in~\cite{NDTR2012}.
    \item[Others.] A review of AMR frameworks and applications using AMR can
    be found on Donna Calhoun's website~\cite{DONNA}.
\end{description}

Let us underline that both RAMSES and Gerris are not AMR frameworks, they
are \emph{applications that make use of AMR} to solve equations that appear in
astrophysics and fluid dynamics, respectively. As a consequence, the AMR functionality
is more entangled with the application code and thus harder to separate and
understand on its own. Contrary to these approaches, \texttt{p4est}~\cite{BWG11}
offers a pure AMR framework, making no suppositions on the type of equations
and methods that the user wants to implement.
