A Short Film About Subcycling
=============================

Pseudo code:

```
#!python
import antigravity

# iterators.c:update_cell_subcycle_iter_volume_fn:335
def update(level):
    for all cells of level (level):
        for all directions (x, y, z):
            cr = right neighbor
            cl = left neighbor

            if cr is a leaf of level <= (level) or a ghost:
                compute flux at interface
                update cell values
            if cl is a leaf of level (level - 1) or a ghost:
                compute flux at interface
                update cell values

# solver.c:solver_subcycle:259
def subcycle(level):
    if level > MAX_LEVEL:
        return

    for all cells of level (level):
        copy old value into new


    subcycle(level + 1)
    subcycle(level + 1)

    dtlevel = dt * pow(2, MAX_LEVEL - level)

    for all ghosts of level (level) and (level - 1):
        update ghost values from neighboring processes

    update(level)

    for all cells of level (level):
        copy new value into old
```