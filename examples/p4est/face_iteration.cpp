#include <p4est_extended.h>
#include <p4est_bits.h>
#include <p4est_iterate.h>
#include <p4est_connectivity.h>
#include <p4est_ghost.h>
#include <p4est_vtk.h>

#define UNUSED(x) ((void)x)

static int          counter;
static int          num_faces;

static void
quadrant_pprint (p4est_quadrant_t * q, int is_ghost, int rank)
{
  p4est_qcoord_t      x = (q->x) >> (P4EST_MAXLEVEL - q->level);
  p4est_qcoord_t      y = (q->y) >> (P4EST_MAXLEVEL - q->level);

  printf ("[p4est %d] x %d y %d level %d", rank, x, y, q->level);
  if (is_ghost) {
    printf (" value ghost");
  }
  else {
    printf (" value %d", q->p.user_int);
  }
  printf ("\n");
}

static void
init_fn (p4est_t * p4est, p4est_topidx_t which_tree,
         p4est_quadrant_t * quadrant)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  quadrant->p.user_int = counter;
  counter += 1;
}

static int
refine_fn (p4est_t * p4est, p4est_topidx_t which_tree,
           p4est_quadrant_t * quadrant)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  return p4est_quadrant_child_id (quadrant) == 3 && quadrant->level < 3;
}

static void
iter_face_fn (p4est_iter_face_info_t * info, void *user_data)
{
  UNUSED (user_data);

  int                 mpirank = info->p4est->mpirank;
  p4est_quadrant_t   *quad = NULL;
  p4est_iter_face_side_t *side = NULL;

  printf ("face %d\n", num_faces);

  for (size_t i = 0; i < info->sides.elem_count; ++i) {
    side = p4est_iter_fside_array_index (&info->sides, i);

    if (!side->is_hanging) {
      quad = side->is.full.quad;
      quadrant_pprint (quad, side->is.full.is_ghost, mpirank);
    }
    else {
      for (int j = 0; j < P4EST_HALF; ++j) {
        quad = side->is.hanging.quad[j];
        quadrant_pprint (quad, side->is.hanging.is_ghost[j], mpirank);
      }
    }
  }

  ++num_faces;
}

int
main (int argc, char **argv)
{
  MPI_Comm            mpicomm;
  int                 mpisize, mpirank, mpiret;
  p4est_t            *p4est;
  p4est_connectivity_t *connectivity;
  p4est_ghost_t      *ghost_layer;

  /* initialize MPI and p4est internals */
  mpiret = MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = MPI_COMM_WORLD;
  mpiret = MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  sc_init (mpicomm, 1, 1, NULL, SC_LP_ALWAYS);
  p4est_init (NULL, SC_LP_ALWAYS);

  connectivity = p4est_connectivity_new_unitsquare ();
  p4est = p4est_new_ext (mpicomm, connectivity, 4, 0, 0, 0, init_fn, NULL);

  p4est_refine (p4est, 1, refine_fn, init_fn);
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);
  p4est_vtk_write_file (p4est, NULL, "face_iteration");

  ghost_layer = p4est_ghost_new (p4est, P4EST_CONNECT_FACE);
  p4est_iterate (p4est, ghost_layer, NULL, NULL, iter_face_fn, NULL);

  /* destroy the p4est and its connectivity structure */
  p4est_destroy (p4est);
  p4est_connectivity_destroy (connectivity);
  p4est_ghost_destroy (ghost_layer);

  /* clean up and exit */
  sc_finalize ();

  mpiret = MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
