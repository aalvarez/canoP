#include <p4est_extended.h>
#include <p4est_bits.h>
#include <p4est_iterate.h>
#include <p4est_connectivity.h>
#include <p4est_vtk.h>

#define UNUSED(x) ((void)x)

int                 depth = 0;

static p4est_connectivity_t *
connectivity_new_two (void)
{
  const p4est_topidx_t num_vertices = 6;
  const p4est_topidx_t num_trees = 2;
  const p4est_topidx_t num_ctt = 0;
  const double        vertices[6 * 3] = {
    -1.0, 0, 0,
    0.0, 0, 0,
    -1.0, 1, 0,
    0.0, 1, 0,
    1.0, 0, 0,
    1.0, 1, 0
  };
  const p4est_topidx_t tree_to_vertex[2 * 4] = {
    0, 1, 2, 3,
    1, 4, 3, 5
  };
  const p4est_topidx_t tree_to_tree[2 * 4] = {
    0, 1, 0, 0,
    0, 1, 1, 1
  };
  const int8_t        tree_to_face[2 * 4] = {
    0, 0, 3, 2,
    1, 1, 3, 2
  };

  return p4est_connectivity_new_copy (num_vertices, num_trees, 0,
                                      vertices, tree_to_vertex,
                                      tree_to_tree, tree_to_face,
                                      NULL, &num_ctt, NULL, NULL);
}

static void
init_fn (p4est_t * p4est, p4est_topidx_t which_tree,
         p4est_quadrant_t * quadrant)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  quadrant->p.user_int = 0;
}

static int
refine_fn (p4est_t * p4est, p4est_topidx_t which_tree,
           p4est_quadrant_t * quadrant)
{
  UNUSED (p4est);
  p4est_qcoord_t      x, y;

  if (which_tree == 0 && quadrant->level == 1) {
    x = (quadrant->x) >> (P4EST_MAXLEVEL - 1);
    return x == 1;
  }

  if (which_tree == 1 && quadrant->level == 1) {
    return quadrant->x == 0 && quadrant->y == 0;
  }

  if (which_tree == 0 && quadrant->level == 2) {
    x = ((quadrant->x) >> (P4EST_MAXLEVEL - 2));
    y = ((quadrant->y) >> (P4EST_MAXLEVEL - 2));

    return x == 3 && y == 0;
  }

  return quadrant->level == 0;
}

static int
coarsen_fn (p4est_t * p4est, p4est_topidx_t which_tree,
            p4est_quadrant_t * quadrant[])
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (quadrant);

  p4est_qcoord_t      x =
    (quadrant[0]->x) >> (P4EST_MAXLEVEL - quadrant[0]->level);

  return x != 6;
}

int
main (int argc, char **argv)
{
  MPI_Comm            mpicomm;
  int                 mpisize, mpirank, mpiret;
  p4est_t            *p4est;
  p4est_connectivity_t *connectivity;

  /* initialize MPI and p4est internals */
  mpiret = MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = MPI_COMM_WORLD;
  mpiret = MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  sc_init (mpicomm, 1, 1, NULL, SC_LP_ALWAYS);
  p4est_init (NULL, SC_LP_ALWAYS);

  connectivity = connectivity_new_two ();
  p4est = p4est_new_ext (mpicomm, connectivity, 4, 0, 0, 0, init_fn, NULL);

  p4est_refine (p4est, 1, refine_fn, init_fn);
  p4est_vtk_write_file (p4est, NULL, "balance_refined");
  p4est_coarsen (p4est, 0, coarsen_fn, NULL);
  p4est_vtk_write_file (p4est, NULL, "balance_coarsen");
  p4est_balance (p4est, P4EST_CONNECT_FACE, init_fn);
  p4est_vtk_write_file (p4est, NULL, "balance_balanced");

  /* destroy the p4est and its connectivity structure */
  p4est_destroy (p4est);
  p4est_connectivity_destroy (connectivity);

  /* clean up and exit */
  sc_finalize ();

  mpiret = MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
