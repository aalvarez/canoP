#include <stdio.h>
#include <stdlib.h>
#include <hdf5.h>
#include <hdf5_hl.h>
#include <mpi.h>

#define DEBUG(M) printf("%s:%d " M "\n", __func__, __LINE__)
#define CHECK_MPI(r) if((r) != MPI_SUCCESS) do { DEBUG("MPI ERROR!!1111one"); exit(1); } while(0)

#define NDIM 10

char                filename[] = "parallel_io.h5";

int
main (int argc, char **argv)
{
  MPI_Comm            mpicomm = MPI_COMM_WORLD;
  int                 mpisize, mpirank, mpiret;
  hid_t               fd, dset;
  hid_t               plist;
  hid_t               filespace, memspace;

  int                 n = 32;
  int                 m = 3;
  double              h = 1.0 / n;
  double             *data;
  hsize_t             dimsf[2], count[2], offset[2];

  /* initialize MPI and p4est internals */
  mpiret = MPI_Init (&argc, &argv);
  CHECK_MPI (mpiret);
  mpiret = MPI_Comm_size (mpicomm, &mpisize);
  CHECK_MPI (mpiret);
  mpiret = MPI_Comm_rank (mpicomm, &mpirank);
  CHECK_MPI (mpiret);

  printf ("rank %d / %d\n", mpirank, mpisize);

  // open the file
  plist = H5Pcreate (H5P_FILE_ACCESS);
  H5Pset_fapl_mpio (plist, mpicomm, MPI_INFO_NULL);
  fd = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, plist);
  H5Pclose (plist);

  /*
   * Create the dataspace for the dataset.
   */
  dimsf[0] = n;
  dimsf[1] = m;
  filespace = H5Screate_simple (2, dimsf, NULL);

  /*
   * Create the dataset with default properties and close filespace.
   */
  dset = H5Dcreate2 (fd, "Coordinates", H5T_NATIVE_DOUBLE, filespace,
                     H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Sclose (filespace);

  /*
   * Each process defines dataset in memory and writes it to the hyperslab
   * in the file.
   */
  count[0] = dimsf[0] / mpisize;
  count[1] = dimsf[1];
  offset[0] = mpirank * count[0];
  offset[1] = 0;
  memspace = H5Screate_simple (2, count, NULL);

  /*
   * Select hyperslab in the file.
   */
  filespace = H5Dget_space (dset);
  H5Sselect_hyperslab (filespace, H5S_SELECT_SET, offset, NULL, count, NULL);

  /*
   * Initialize data buffer
   */
  data = (double *) malloc (sizeof (double) * count[0] * count[1]);
  for (unsigned long long i = 0; i < count[0]; ++i) {
    data[count[1] * i] = offset[0] * h + i * h;
    data[count[1] * i + 1] = 0;
    data[count[1] * i + 2] = 0;
  }

  /*
   * Create property list for collective dataset write.
   */
  plist = H5Pcreate (H5P_DATASET_XFER);
  H5Pset_dxpl_mpio (plist, H5FD_MPIO_COLLECTIVE);

  H5Dwrite (dset, H5T_NATIVE_DOUBLE, memspace, filespace, plist, data);

  // close stuff
  free (data);
  H5Dclose (dset);
  H5Sclose (filespace);
  H5Sclose (memspace);
  H5Pclose (plist);
  H5Fclose (fd);

  mpiret = MPI_Finalize ();
  CHECK_MPI (mpiret);

  return 0;
}
