find_package(HDF5 COMPONENTS C)

add_subdirectory(p4est)
add_subdirectory(poisson)


add_executable(test_icosahedron
  test_icosahedron.cpp
)
target_include_directories(test_icosahedron PUBLIC
  ${HDF5_INCLUDE_DIRS})
target_link_libraries(test_icosahedron
    canoP_shared_2d
    p4est::p4est
)

add_executable(test_connectivity
  test_connectivity.cpp
)
target_include_directories(test_connectivity PUBLIC
  ${HDF5_INCLUDE_DIRS})
target_link_libraries(test_connectivity
    canoP_shared_2d
    p4est::p4est
)

add_executable(test_connectivity_3d
  test_connectivity.cpp
)
target_compile_definitions(test_connectivity_3d PUBLIC "-DUSE_3D")
target_include_directories(test_connectivity_3d PUBLIC
  ${HDF5_INCLUDE_DIRS})
target_link_libraries(test_connectivity_3d
    canoP_shared_3d
    p4est::p4est
)

add_executable(test_lua 
    test_lua.cpp 
)
target_link_libraries(test_lua
    canoP_shared_2d
    p4est::p4est
  )

add_executable(test_lnodes_interpolation
  test_lnodes_interpolation.cpp
)
target_include_directories(test_lnodes_interpolation PUBLIC
  ${HDF5_INCLUDE_DIRS})
target_link_libraries(test_lnodes_interpolation
    canoP_shared_2d
    p4est::p4est
)

add_executable(test_lnodes_interpolation_3d
  test_lnodes_interpolation.cpp
)
target_compile_definitions(test_lnodes_interpolation_3d PUBLIC "-DUSE_3D")
target_include_directories(test_lnodes_interpolation_3d PUBLIC
  ${HDF5_INCLUDE_DIRS})
target_link_libraries(test_lnodes_interpolation_3d
    canoP_shared_3d
    p4est::p4est
)

add_executable(test_lnodes_gradient
  test_lnodes_gradient.cpp
)
target_include_directories(test_lnodes_gradient PUBLIC
  ${HDF5_INCLUDE_DIRS})
target_link_libraries(test_lnodes_gradient
    canoP_shared_2d
    p4est::p4est
)

add_executable(test_lnodes_gradient_3d
  test_lnodes_gradient.cpp
)
target_compile_definitions(test_lnodes_gradient_3d PUBLIC "-DUSE_3D")
target_include_directories(test_lnodes_gradient_3d PUBLIC
  ${HDF5_INCLUDE_DIRS})
target_link_libraries(test_lnodes_gradient_3d
    canoP_shared_3d
    p4est::p4est
)

add_executable(simple_2d
  simple_2d.cpp
)
target_link_libraries(simple_2d
    canoP_shared_2d
    p4est::p4est
)

add_executable(simple_3d
  simple_3d.cpp
)
target_link_libraries(simple_3d
    canoP_shared_3d
    p4est::p4est
)

add_subdirectory(backward-cpp)
