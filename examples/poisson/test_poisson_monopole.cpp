/*
 * Usage: test_poisson <level>
 */

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_extended.h>
#include <p4est_iterate.h>
#include <p4est_ghost.h>
#include <p4est_lnodes.h>
#include <p4est_vtk.h>
#else
#include <p8est_bits.h>
#include <p8est_extended.h>
#include <p8est_iterate.h>
#include <p8est_ghost.h>
#include <p8est_lnodes.h>
#include <p8est_vtk.h>
#endif

#include <cmath>

#include "quadrant_utils.h"
#include "connectivity.h"
#include "geometry.h"
#include "IO_Writer.h"
#include "Lnode_Utils.h"

#include "PoissonFESolver.h"

struct user_data_t
{
  double              rho; // density   - cell averaged
  double              phi; // potential - cell centered
  double              phi_ana; // analytical potential - cell centered
  double              dx_phi; // the x component of the gradient
  double              dy_phi; // the y component of the gradient
#ifdef P4_TO_P8
  double              dz_phi; // the z component of the gradient
#endif
};

struct mpi_context_t
{
  sc_MPI_Comm         mpicomm;
  int                 mpisize;
  int                 mpirank;
};

static int          refine_level = 3;
static double       R = 0.2; /* radius of initial condition */

#ifndef P4_TO_P8
static p4est_connectivity_t *
p4est_connectivity_new_tetris2 (void)
{
  const p4est_topidx_t num_vertices = 10;
  const p4est_topidx_t num_trees = 4;
  const p4est_topidx_t num_corners = 2;
  const double        vertices[10 * 3] = {
    1, 0, 0,
    2, 0, 0,
    0, 1, 0,
    1, 1, 0,
    2, 1, 0,
    3, 1, 0,
    0, 2, 0,
    1, 2, 0,
    2, 2, 0,
    3, 2, 0,
  };
  const p4est_topidx_t tree_to_vertex[4 * 4] = {
    0, 1, 3, 4,
    2, 3, 6, 7,
    3, 4, 7, 8,
    4, 5, 8, 9,
  };
  const p4est_topidx_t tree_to_tree[4 * 4] = {
    0, 0, 0, 2,
    1, 2, 1, 1,
    1, 3, 0, 2,
    2, 3, 3, 3,
  };
  const int8_t         tree_to_face[4 * 4] = {
    0, 1, 2, 2,
    0, 0, 2, 3,
    1, 0, 3, 3,
    1, 1, 2, 3,
  };
  const p4est_topidx_t tree_to_corner[4 * 4] = {
    -1, -1,  0,  1,
    -1,  0, -1, -1,
     0,  1, -1, -1,
     1, -1, -1, -1,
  };
  const p4est_topidx_t ctt_offset[2 + 1] = {
    0, 3, 6,
  };
  const p4est_topidx_t corner_to_tree[6] = {
    0, 1, 2,
    0, 2, 3,
  };
  const int8_t         corner_to_corner[6] = {
    2, 1, 0,
    3, 1, 0,
  };

  return p4est_connectivity_new_copy (num_vertices, num_trees, num_corners,
                                      vertices, tree_to_vertex,
                                      tree_to_tree, tree_to_face,
                                      tree_to_corner, ctt_offset,
                                      corner_to_tree, corner_to_corner);
}
#else
static p4est_connectivity_t *
p8est_connectivity_new_tetris (void)
{
  const p4est_topidx_t num_vertices = 20;
  const p4est_topidx_t num_trees = 4;
  const p4est_topidx_t num_edges = 2;
  const p4est_topidx_t num_corners = 0;
  const double        vertices[20 * 3] = {
    1, 0, 0,
    2, 0, 0,
    0, 1, 0,
    1, 1, 0,
    2, 1, 0,
    3, 1, 0,
    0, 2, 0,
    1, 2, 0,
    2, 2, 0,
    3, 2, 0,
    1, 0, 1,
    2, 0, 1,
    0, 1, 1,
    1, 1, 1,
    2, 1, 1,
    3, 1, 1,
    0, 2, 1,
    1, 2, 1,
    2, 2, 1,
    3, 2, 1,
  };
  const p4est_topidx_t tree_to_vertex[4 * 8] = {
    0, 1, 3, 4, 10, 11, 13, 14,
    2, 3, 6, 7, 12, 13, 16, 17,
    3, 4, 7, 8, 13, 14, 17, 18,
    4, 5, 8, 9, 14, 15, 18, 19,
  };
  const p4est_topidx_t tree_to_tree[4 * 6] = {
    0, 0, 0, 2, 0, 0,
    1, 2, 1, 1, 1, 1,
    1, 3, 0, 2, 2, 2,
    2, 3, 3, 3, 3, 3,
  };
  const int8_t         tree_to_face[4 * 6] = {
    0, 1, 2, 2, 4, 5,
    0, 0, 2, 3, 4, 5,
    1, 0, 3, 3, 4, 5,
    1, 1, 2, 3, 4, 5,
  };
  const p4est_topidx_t tree_to_edge[4 * 12] = {
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,  1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1,  0, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,  0,  1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1,
  };
  const p4est_topidx_t ett_offset[2 + 1] = {
    0, 3, 6,
  };
  const p4est_topidx_t edge_to_tree[6] = {
    0, 1, 2,
    0, 2, 3,
  };
  const int8_t         edge_to_edge[6] = {
    10, 9, 8,
    11, 9, 8,
  };
  const p4est_topidx_t ctt_offset[1] = {
    0,
  };

  return p4est_connectivity_new_copy (num_vertices, num_trees, num_edges, num_corners,
                                      vertices, tree_to_vertex,
                                      tree_to_tree, tree_to_face,
                                      tree_to_edge, ett_offset,
                                      edge_to_tree, edge_to_edge,
                                      NULL, ctt_offset, NULL, NULL);
}
#endif


const double rho_0 = 1.;
const double sph_radius = 0.2;

static double
func_rhs (double xyz[3])
{
  double r = 0.;

  for (size_t i = 0; i < P4EST_DIM; ++i)
    r += SC_SQR(xyz[i] - 0.5);
  r /= SC_SQR(sph_radius);

  return (r <= 1.) ? rho_0 : 0.;
}

static double
func_solution (double xyz[3])
{
  double r = 0.;
  double phi0, phi;

  for (size_t i = 0; i < P4EST_DIM; ++i)
    r += SC_SQR(xyz[i] - 0.5);
  r = std::sqrt(r);
  r /= sph_radius;

#ifndef P4_TO_P8
  phi0 = 2. * std::log(sph_radius) - 1.;
  phi = (r <= 1.)? SC_SQR(r) : (1. + 2. * std::log(r));
#else
  phi0 = -3.;
  phi = (r <= 1.)? SC_SQR(r) : (3. - 2. / r);
#endif

  return -rho_0 * (phi + phi0) * SC_SQR(sph_radius) / (2. * P4EST_DIM);
}

static double get_quadrant_scale(p4est_t * p4est,
                                 p4est_topidx_t which_tree,
                                 p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;

  return dx;
} /* get_quadrant_scale */

static double get_quadrant_volume(p4est_t * p4est,
                                  p4est_topidx_t which_tree,
                                  p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;

#ifndef P4_TO_P8
  return dx*dx;
#else
  return dx*dx*dx;
#endif
  
} /* get_quadrant_volume */

static void
init_fn (p4est_t * p4est, p4est_topidx_t which_tree,
         p4est_quadrant_t * quadrant)
{
  user_data_t        *data = (user_data_t *) quadrant->p.user_data;

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quadrant->level) / 2;

  // get the physical coordinates of the center of the quad
  // assume geometry is regular cartesian
  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quadrant->x + half_length,
                          quadrant->y + half_length,
#ifdef P4_TO_P8
                          quadrant->z + half_length,
#endif
                          XYZ);

  data->rho = func_rhs(XYZ);
  data->phi_ana = func_solution(XYZ);
}

static int
refine_poisson_fn (p4est_t * p4est,
		   p4est_topidx_t which_tree,
		   p4est_quadrant_t * quadrant)
{

  p4est_geometry_t *geom = (p4est_geometry_t *) p4est->user_pointer;

  /* logical coordinates */
  double              xyz[3] = { 0, 0, 0 };

  /* physical coordinates */
  double              XYZ[3] = { 0, 0, 0 };
  double              XYZ_c[3] = { 0, 0, 0 };

  double h2 = 0.5 * P4EST_QUADRANT_LEN (quadrant->level) / P4EST_ROOT_LEN;
  const double        intsize = 1.0 / P4EST_ROOT_LEN;

  double dx = 0., dy = 0., dz = 0.;
  quadrant_get_dx_dy_dz(p4est->connectivity, geom,
      quadrant, which_tree, &dx, &dy, &dz);
  dx = sqrt(SC_SQR(dx) + SC_SQR(dy) + SC_SQR(dz)) / 2.;

  /* 
   * get coordinates at cell center
   */
  xyz[0] = intsize*quadrant->x + h2;
  xyz[1] = intsize*quadrant->y + h2;
#ifdef P4_TO_P8
  xyz[2] = intsize*quadrant->z + h2;
#endif

  // from logical coordinates to physical coordinates (cartesian)
  if (geom) {
    geom->X (geom, which_tree, xyz, XYZ);
  } else {
    XYZ[0] = xyz[0];
    XYZ[1] = xyz[1];
    XYZ[2] = xyz[2];
  }

  XYZ_c[0] =  XYZ[0]-0.5;
  XYZ_c[1] =  XYZ[1]-0.5;
  XYZ_c[2] =  XYZ[2];
#ifdef P4_TO_P8
  XYZ_c[2] =  XYZ[2]-0.5;
#endif

  /* if (quadrant->level > 6)
    return 0;
  if (XYZ[2]>0 && quadrant->level>=3)
  return 0; */

  if (quadrant->level > 2+refine_level)
    return 0;

  if (fabs(sqrt((XYZ_c[0]*XYZ_c[0])+
                (XYZ_c[1]*XYZ_c[1])+
                (XYZ_c[2]*XYZ_c[2]))
           - R)
      > dx)
    return 0;

  return 1;

} // refine_poisson_fn

static void
getdata_iterate_callback(p4est_iter_volume_info_t * info, void *user_data)
{
  /* we passed the array of values to fill as the user_data in the call
     to p4est_iterate */
  sc_array_t         *rho_data = (sc_array_t *) user_data;
  double             *this_data;
  p4est_t            *p4est = info->p4est;
  p4est_quadrant_t   *q = info->quad;
  p4est_topidx_t      which_tree = info->treeid;
  p4est_locidx_t      local_id = info->quadid;  /* this is the index of q *within its tree's numbering*.  We want to convert it its index for all the quadrants on this process, which we do below */
  p4est_tree_t       *tree;
  user_data_t        *data = (user_data_t *) q->p.user_data;
  p4est_locidx_t      arrayoffset;

  tree = p4est_tree_array_index (p4est->trees, which_tree);
  local_id += tree->quadrants_offset;   /* now the id is relative to the MPI process */
  arrayoffset = local_id;

  this_data = (double*) sc_array_index (rho_data, arrayoffset);
  this_data[0] = data->rho;

} /* getdata_iterate_callback */

/* data getters */
double              qdata_get_rho (p4est_quadrant_t * q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  return data->rho;
}

double
qdata_get_phi (p4est_quadrant_t *q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;
  return data->phi;
}

double
qdata_get_phi_ana (p4est_quadrant_t *q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;
  return data->phi_ana;
}

void
qdata_get_grad_phi (p4est_quadrant_t *q, size_t dim, double *v)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  v[0] = data->dx_phi;
  v[1] = data->dy_phi;
  if(dim == 3) {
#ifdef P4_TO_P8
    v[2] = data->dz_phi;
#else
    v[2] = 0;
#endif
  }
}

/* data setters */
void
qdata_set_phi (p4est_quadrant_t *q, double val)
{
  user_data_t      *data = (user_data_t *) q->p.user_data;
  data->phi = val;
}

void
qdata_set_grad_phi (p4est_quadrant_t *q, double *val)
{
  user_data_t      *data = (user_data_t *) q->p.user_data;
  data->dx_phi = val[0];
  data->dy_phi = val[1];
#ifdef P4_TO_P8
  data->dz_phi = val[2];
#endif
}

void
write_user_data(p4est_t *p4est, p4est_geometry_t * geom, p4est_lnodes_t *lnodes,
    const char *filename, double *rho_lnodes, double *phi_lnodes, double *phi_bound_lnodes)
{

  UNUSED(filename);
  
  sc_array_t         *rho_data;    /* array of cell data to write */
  p4est_locidx_t      numquads;

  numquads = p4est->local_num_quadrants;

  /* create a vector with one value per local quadrant */
  rho_data = sc_array_new_size (sizeof(double), numquads);

  /* Use the iterator to visit every cell and fill in the data array */
  p4est_iterate (p4est, 
		 nullptr,   /* we don't need any ghost quadrants for this loop */
                 (void *) rho_data,     /* pass in array pointer so that we can fill it */
                 getdata_iterate_callback,   /* callback function to get cell data */
                 nullptr,
#ifdef P4_TO_P8
                 nullptr,
#endif
                 nullptr);

  /* the following currently only works when using p4est branch 'vtk' or 'develop' */
#if 0
  /* create VTK output context and set its parameters */
  p4est_vtk_context_t *context = p4est_vtk_context_new (p4est, filename);
  p4est_vtk_context_set_scale (context, 0.95);
  p4est_vtk_context_set_geom  (context, geom);

  p4est_vtk_write_header (context);
  context = p4est_vtk_write_cell_dataf (context, 1, 1,
					1, 
					0, 
					1,  /* 1 scalar */
					0,  /* 0 vector */
					"rho", rho_data, 
					context);
  SC_CHECK_ABORT (context != nullptr,
                  P4EST_STRING "_vtk: Error writing cell data");

  const int retval = p4est_vtk_write_footer (context);
  SC_CHECK_ABORT (!retval, P4EST_STRING "_vtk: Error writing footer");
#endif /* write with vtk file format */

  /* use canoP parallel Hdf5/Xdmf IO */
  {
    IO_Writer          *w;
    int                 write_mesh_info = 1;

    // io_writer constructor
    w = new IO_Writer (p4est, geom, filename, write_mesh_info);

    // open the new file and write our stuff
    w->open (filename);

    w->write_header (0.0);

    w->write_quadrant_attribute ("rho", qdata_get_rho,
				 H5T_NATIVE_DOUBLE);

    w->write_quadrant_attribute ("phi", qdata_get_phi,
				 H5T_NATIVE_DOUBLE);

    w->write_quadrant_attribute ("phi_ana", qdata_get_phi_ana,
				 H5T_NATIVE_DOUBLE);

    w->write_quadrant_attribute ("grad_phi", 3, qdata_get_grad_phi,
                                 H5T_NATIVE_DOUBLE);

    w->write_lnode_attribute ("rho_n", lnodes, rho_lnodes,
                              H5T_NATIVE_DOUBLE);

    w->write_lnode_attribute ("phi_n", lnodes, phi_lnodes,
                              H5T_NATIVE_DOUBLE);

    w->write_lnode_attribute ("phi_bound_n", lnodes, phi_bound_lnodes,
                              H5T_NATIVE_DOUBLE, 0.);

    // close the file
    w->write_footer();
    w->close();

    // destroy IO_Writer
    delete w;

  } // end hdf5/xdmf writer

  sc_array_destroy (rho_data);

} /* write_user_data */

/** Allocate storage for processor-relevant nodal degrees of freedom.
 *
 * \param [in] lnodes   This structure is queried for the node count.
 * \return              Allocated double array; must be freed with P4EST_FREE.
 */
static double *
allocate_vector (p4est_lnodes_t * lnodes)
{

  return P4EST_ALLOC (double, lnodes->num_local_nodes);

} /* allocate_vector */

int
main (int argc, char **argv)
{
  int                 mpiret;
  int                 wrongusage;
  const char         *usage;
  int                 log_verbosity;
  int                 i_conn;
  mpi_context_t       mpi_context, *mpi = &mpi_context;
  p4est_t            *p4est;
  p4est_connectivity_t *conn;
  p4est_mesh_t       *mesh;
  p4est_geometry_t   *geom;
  p4est_refine_t      refine_fn;
  p4est_coarsen_t     coarsen_fn;

  double             *rho_nodes, *phi_fe, *phi_bound;
  int8_t             *bc;
  p4est_ghost_t      *ghost;
  p4est_lnodes_t     *lnodes;

  double total_mass;
  double barycenter[3];

  int                 imax = 100;
  double              tol = 1e-6;

#ifndef P4_TO_P8
  const char filename[] = "poisson_monopole_2d";
#else
  const char filename[] = "poisson_monopole_3d";
#endif

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpi->mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpi->mpicomm, &mpi->mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpi->mpicomm, &mpi->mpirank);
  SC_CHECK_MPI (mpiret);

  log_verbosity = SC_LP_ALWAYS;
  sc_init (mpi->mpicomm, 1, 1, nullptr, log_verbosity);
  p4est_init (nullptr, log_verbosity);

  /* process command line arguments */
  usage =
    "Arguments: <configuration> <level> [<imax> [<tol>]]\n";
  wrongusage = 0;
  if (!wrongusage && (argc < 3 || argc > 5)) {
    wrongusage = 1;
  }
  if (wrongusage) {
    P4EST_GLOBAL_LERROR (usage);
    sc_abort_collective ("Usage error");
  }

  /* assign variables based on configuration */
  i_conn = atoi (argv[1]);
  refine_level = atoi (argv[2]);
  refine_fn = refine_poisson_fn;
  coarsen_fn = nullptr;

  if (argc > 3)
    imax = atoi (argv[3]);
  if (argc > 4)
    tol = atof (argv[4]);

  /* create connectivity and forest structures */
  geom = nullptr;
  switch (i_conn) {
#ifndef P4_TO_P8
  case 0:
    conn = p4est_connectivity_new_unitsquare ();
    break;
  case 1:
    conn = p4est_connectivity_new_brick (1, 2, 0, 0);
    break;
  case 2:
    conn = p4est_connectivity_new_tetris2 ();
    break;
  default:
    conn = p4est_connectivity_new_unitsquare ();
    break;
#else
  case 0:
    conn = p8est_connectivity_new_unitcube ();
    break;
  case 1:
    conn = p8est_connectivity_new_brick (1, 2, 3, 0, 0, 0);
    break;
  case 2:
    conn = p8est_connectivity_new_tetris ();
    break;
  default:
    conn = p8est_connectivity_new_unitcube ();
    break;
#endif
  }

  p4est = p4est_new_ext (mpi->mpicomm, conn, 15, 0, 0,
                         sizeof (user_data_t), init_fn, geom);

  /* refinement and coarsening */
  p4est_refine (p4est, 1, refine_fn, init_fn);
  if (coarsen_fn != nullptr) {
    p4est_coarsen (p4est, 1, coarsen_fn, init_fn);
  }
  //p4est_vtk_write_file (p4est, geom, "poisson_refined");

  /* balance */
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);
  //p4est_vtk_write_file (p4est, geom, "poisson_balanced");

  P4EST_GLOBAL_INFOF("Is p4est grid balanced ? : %d\n",p4est_is_balanced (p4est, P4EST_CONNECT_FULL));

  /* partition */
  p4est_partition (p4est, 0, nullptr);
  //p4est_vtk_write_file (p4est, geom, "poisson_partition");


  /* Poisson solver */

  /* Create the ghost layer to learn about parallel neighbors. */
  ghost = p4est_ghost_new (p4est, P4EST_CONNECT_FULL);

  /* Create the mesh structure to learn about global boundary faces. */
  mesh = p4est_mesh_new_ext (p4est, ghost, 1, 0, P4EST_CONNECT_FACE);

  /* Create a node numbering for continuous linear finite elements. */
  lnodes = p4est_lnodes_new (p4est, ghost, 1);


  bc = P4EST_ALLOC (int8_t, lnodes->num_local_nodes);
  lnodes_flag_boundaries(p4est, mesh, lnodes, bc);

  /* Interpolate rho_nodes from quadrants */
  rho_nodes = allocate_vector (lnodes);
  interp_quad_to_lnodes (p4est, lnodes, qdata_get_rho, get_quadrant_volume, rho_nodes);


  phi_fe = allocate_vector (lnodes);
  memset(phi_fe, 0, lnodes->num_local_nodes * sizeof(double));

  /* Compute a monopole expansion for the boundary conditions */
  compute_barycenter(p4est, geom, qdata_get_rho, get_quadrant_volume,
                     &total_mass, barycenter);
  P4EST_GLOBAL_LDEBUGF("monopole expansion: mass = %g, center = (%f, %f, %f)\n",
      total_mass, barycenter[0], barycenter[1], barycenter[2]);
  /* Pass the total mass and barycenter to the monopole function */
  auto monopole = std::bind(monopole_expansion, total_mass, barycenter,
                            std::placeholders::_1);

  /* Set boundary conditions */
  lnodes_set_boundaries(p4est, geom, lnodes, bc, monopole, phi_fe);

  /* Save boundary conditions for checking */
  phi_bound = allocate_vector (lnodes);
  memcpy(phi_bound, phi_fe, lnodes->num_local_nodes * sizeof(double));

  /* Solve Poisson equation */
  solve_poisson_lnodes (p4est, lnodes, bc, rho_nodes, phi_fe, imax, tol);

  /* Interpolate phi and grad_phi back to quadrants */
  interp_lnodes_to_quad (p4est, lnodes, phi_fe, qdata_set_phi);
  gradient_lnodes_to_quad (p4est, lnodes, get_quadrant_scale, phi_fe, qdata_set_grad_phi);

  /* End of Poisson solver */

  /* save output data */
  write_user_data(p4est, geom, lnodes, filename, rho_nodes, phi_fe, phi_bound);

  /* Clean up allocated arrays */
  P4EST_FREE (phi_fe);
  P4EST_FREE (phi_bound);
  P4EST_FREE (rho_nodes);
  P4EST_FREE (bc);

  /* destroy lnodes data */
  p4est_lnodes_destroy (lnodes);

  /* destroy the mesh structure */
  p4est_mesh_destroy (mesh);

  /* destroy the ghost structure */
  p4est_ghost_destroy (ghost);

  /* destroy the p4est and its connectivity structure */
  p4est_destroy (p4est);
  if (geom != nullptr) {
    p4est_geometry_destroy (geom);
  }
  p4est_connectivity_destroy (conn);

  /* clean up and exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
