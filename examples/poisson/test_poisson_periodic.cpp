/*
 * Usage: test_poisson <level>
 */

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_extended.h>
#include <p4est_iterate.h>
#include <p4est_ghost.h>
#include <p4est_lnodes.h>
#include <p4est_vtk.h>
#else
#include <p8est_bits.h>
#include <p8est_extended.h>
#include <p8est_iterate.h>
#include <p8est_ghost.h>
#include <p8est_lnodes.h>
#include <p8est_vtk.h>
#endif

#include "quadrant_utils.h"
#include "connectivity.h"
#include "geometry.h"
#include "IO_Writer.h"
#include "Lnode_Utils.h"

#include <cmath>

#include "PoissonFESolver.h"

struct user_data_t
{
  double              rho; // density   - cell averaged
  double              phi; // potential - cell centered
  double              phi_ana; // analytical potential - cell centered
  double              dx_phi; // the x component of the gradient
  double              dy_phi; // the y component of the gradient
#ifdef P4_TO_P8
  double              dz_phi; // the z component of the gradient
#endif
};

struct mpi_context_t
{
  sc_MPI_Comm         mpicomm;
  int                 mpisize;
  int                 mpirank;
};

static int          refine_level = 3;
static double       R = 0.4; /* radius of initial condition */

#ifndef P4_TO_P8
static int          ntrees[] = {1, 1};
#else
static int          ntrees[] = {1, 1, 1};
#endif

static double
func_rhs (double xyz[3])
{
  int    i;
  double res = 1.;
  double tmp = 0.;

  for (i = 0; i < P4EST_DIM; ++i) {
    res *= std::cos(2. * M_PI * xyz[i]);
    tmp += 1. / SC_SQR(ntrees[i]);
  }

  return 4 * SC_SQR(M_PI) * tmp * res;
}

static double
func_solution (double xyz[3])
{
  int    i;
  double res = 1.;

  for (i = 0; i < P4EST_DIM; ++i) {
    res *= std::cos(2. * M_PI * xyz[i]);
  }

  return res;
}

static double get_quadrant_scale(p4est_t * p4est,
                                 p4est_topidx_t which_tree,
                                 p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;

  return dx;
} /* get_quadrant_scale */

static double get_quadrant_volume(p4est_t * p4est,
                                  p4est_topidx_t which_tree,
                                  p4est_quadrant_t * quad)
{

  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;

#ifndef P4_TO_P8
  return dx*dx;
#else
  return dx*dx*dx;
#endif
  
} /* get_quadrant_volume */

static void
init_fn (p4est_t * p4est, p4est_topidx_t which_tree,
         p4est_quadrant_t * quadrant)
{
  user_data_t        *data = (user_data_t *) quadrant->p.user_data;

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quadrant->level) / 2;

  // get the physical coordinates of the center of the quad
  // assume geometry is regular cartesian
  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quadrant->x + half_length,
                          quadrant->y + half_length,
#ifdef P4_TO_P8
                          quadrant->z + half_length,
#endif
                          XYZ);

  /* rescale coordinates to [0, 1]^d */
  for (int i = 0; i < P4EST_DIM; ++i)
    XYZ[i] /= ntrees[i];

  data->rho = func_rhs(XYZ);
  data->phi_ana = func_solution(XYZ);
}

static int
refine_poisson_fn (p4est_t * p4est,
		   p4est_topidx_t which_tree,
		   p4est_quadrant_t * quadrant)
{

  p4est_geometry_t *geom = (p4est_geometry_t *) p4est->user_pointer;

  /* logical coordinates */
  double              xyz[3] = { 0, 0, 0 };

  /* physical coordinates */
  double              XYZ[3] = { 0, 0, 0 };
  double              XYZ_c[3] = { 0, 0, 0 };

  double h2 = 0.5 * P4EST_QUADRANT_LEN (quadrant->level) / P4EST_ROOT_LEN;
  const double        intsize = 1.0 / P4EST_ROOT_LEN;

  /* 
   * get coordinates at cell center
   */
  xyz[0] = intsize*quadrant->x + h2;
  xyz[1] = intsize*quadrant->y + h2;
#ifdef P4_TO_P8
  xyz[2] = intsize*quadrant->z + h2;
#endif
  
  // from logical coordinates to physical coordinates (cartesian)
  if (geom) {
    geom->X (geom, which_tree, xyz, XYZ);
  } else {
    XYZ[0] = xyz[0];
    XYZ[1] = xyz[1];
    XYZ[2] = xyz[2];
  }

  XYZ_c[0] =  XYZ[0]-0.5;
  XYZ_c[1] =  XYZ[1]-0.5;
  XYZ_c[2] =  XYZ[2];
#ifdef P4_TO_P8
  XYZ_c[2] =  XYZ[2]-0.5;
#endif

  /* if (quadrant->level > 6)
    return 0;
  if (XYZ[2]>0 && quadrant->level>=3)
  return 0; */

  if (quadrant->level > 2+refine_level)
    return 0;
  
  if ( fabs(sqrt((XYZ_c[0]*XYZ_c[0])+
		 (XYZ_c[1]*XYZ_c[1])+
		 (XYZ_c[2]*XYZ_c[2]*0))-R)>0.15*R && quadrant->level <= 2+refine_level)
    return 0;

  return 1;
  
} // refine_poisson_fn

static void
getdata_iterate_callback(p4est_iter_volume_info_t * info, void *user_data)
{
  /* we passed the array of values to fill as the user_data in the call
     to p4est_iterate */
  sc_array_t         *rho_data = (sc_array_t *) user_data;
  double             *this_data;
  p4est_t            *p4est = info->p4est;
  p4est_quadrant_t   *q = info->quad;
  p4est_topidx_t      which_tree = info->treeid;
  p4est_locidx_t      local_id = info->quadid;  /* this is the index of q *within its tree's numbering*.  We want to convert it its index for all the quadrants on this process, which we do below */
  p4est_tree_t       *tree;
  user_data_t        *data = (user_data_t *) q->p.user_data;
  p4est_locidx_t      arrayoffset;

  tree = p4est_tree_array_index (p4est->trees, which_tree);
  local_id += tree->quadrants_offset;   /* now the id is relative to the MPI process */
  arrayoffset = local_id;

  this_data = (double*) sc_array_index (rho_data, arrayoffset);
  this_data[0] = data->rho;

} /* getdata_iterate_callback */

/* data getters */
double              qdata_get_rho (p4est_quadrant_t * q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  return data->rho;
}

double
qdata_get_phi (p4est_quadrant_t *q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;
  return data->phi;
}

double
qdata_get_phi_ana (p4est_quadrant_t *q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;
  return data->phi_ana;
}

void
qdata_get_grad_phi (p4est_quadrant_t *q, size_t dim, double *v)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;

  v[0] = data->dx_phi;
  v[1] = data->dy_phi;
  if(dim == 3) {
#ifdef P4_TO_P8
    v[2] = data->dz_phi;
#else
    v[2] = 0;
#endif
  }
}

/* data setters */
void
qdata_set_phi (p4est_quadrant_t *q, double val)
{
  user_data_t      *data = (user_data_t *) q->p.user_data;
  data->phi = val;
}

void
qdata_set_grad_phi (p4est_quadrant_t *q, double *val)
{
  user_data_t      *data = (user_data_t *) q->p.user_data;
  data->dx_phi = val[0];
  data->dy_phi = val[1];
#ifdef P4_TO_P8
  data->dz_phi = val[2];
#endif
}

void
write_user_data(p4est_t *p4est, p4est_geometry_t * geom, p4est_lnodes_t *lnodes,
    const char *filename, double *rho_lnodes, double *phi_lnodes)
{

  UNUSED(filename);
  
  sc_array_t         *rho_data;    /* array of cell data to write */
  p4est_locidx_t      numquads;

  numquads = p4est->local_num_quadrants;

  /* create a vector with one value per local quadrant */
  rho_data = sc_array_new_size (sizeof(double), numquads);

  /* Use the iterator to visit every cell and fill in the data array */
  p4est_iterate (p4est, 
		 nullptr,   /* we don't need any ghost quadrants for this loop */
                 (void *) rho_data,     /* pass in array pointer so that we can fill it */
                 getdata_iterate_callback,   /* callback function to get cell data */
                 nullptr,
#ifdef P4_TO_P8
                 nullptr,
#endif
                 nullptr);

  /* the following currently only works when using p4est branch 'vtk' or 'develop' */
#if 0
  /* create VTK output context and set its parameters */
  p4est_vtk_context_t *context = p4est_vtk_context_new (p4est, filename);
  p4est_vtk_context_set_scale (context, 0.95);
  p4est_vtk_context_set_geom  (context, geom);

  p4est_vtk_write_header (context);
  context = p4est_vtk_write_cell_dataf (context, 1, 1,
					1, 
					0, 
					1,  /* 1 scalar */
					0,  /* 0 vector */
					"rho", rho_data, 
					context);
  SC_CHECK_ABORT (context != nullptr,
                  P4EST_STRING "_vtk: Error writing cell data");

  const int retval = p4est_vtk_write_footer (context);
  SC_CHECK_ABORT (!retval, P4EST_STRING "_vtk: Error writing footer");
#endif /* write with vtk file format */

  /* use canoP parallel Hdf5/Xdmf IO */
  {
    IO_Writer          *w;
    int                 write_mesh_info = 1;

    // io_writer constructor
    w = new IO_Writer (p4est, geom, filename, write_mesh_info);

    // open the new file and write our stuff
    w->open (filename);

    w->write_header (0.0);

    w->write_quadrant_attribute ("rho", qdata_get_rho,
				 H5T_NATIVE_DOUBLE);

    w->write_quadrant_attribute ("phi", qdata_get_phi,
				 H5T_NATIVE_DOUBLE);

    w->write_quadrant_attribute ("phi_ana", qdata_get_phi_ana,
				 H5T_NATIVE_DOUBLE);

    w->write_quadrant_attribute ("grad_phi", 3, qdata_get_grad_phi,
                                 H5T_NATIVE_DOUBLE);

    w->write_lnode_attribute ("rho_n", lnodes, rho_lnodes,
                              H5T_NATIVE_DOUBLE);

    w->write_lnode_attribute ("phi_n", lnodes, phi_lnodes,
                              H5T_NATIVE_DOUBLE);

    // close the file
    w->write_footer();
    w->close();

    // destroy IO_Writer
    delete w;

  } // end hdf5/xdmf writer

  sc_array_destroy (rho_data);

} /* write_user_data */

/** Allocate storage for processor-relevant nodal degrees of freedom.
 *
 * \param [in] lnodes   This structure is queried for the node count.
 * \return              Allocated double array; must be freed with P4EST_FREE.
 */
static double *
allocate_vector (p4est_lnodes_t * lnodes)
{

  return P4EST_ALLOC (double, lnodes->num_local_nodes);

} /* allocate_vector */

int
main (int argc, char **argv)
{
  int                 mpiret;
  int                 wrongusage;
  const char         *usage;
  int                 log_verbosity;
  mpi_context_t       mpi_context, *mpi = &mpi_context;
  p4est_t            *p4est;
  p4est_connectivity_t *connectivity;
  p4est_mesh_t       *mesh;
  p4est_geometry_t   *geom;
  p4est_refine_t      refine_fn;
  p4est_coarsen_t     coarsen_fn;

  double             *rho_nodes, *phi_fe;
  p4est_ghost_t      *ghost;
  p4est_lnodes_t     *lnodes;
  double              rho_avg;

  int                 imax = 100;
  double              tol = 1e-6;

#ifndef P4_TO_P8
  const char filename[] = "poisson_periodic_2d";
#else
  const char filename[] = "poisson_periodic_3d";
#endif

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpi->mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpi->mpicomm, &mpi->mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpi->mpicomm, &mpi->mpirank);
  SC_CHECK_MPI (mpiret);

  log_verbosity = SC_LP_ALWAYS;
  sc_init (mpi->mpicomm, 1, 1, nullptr, log_verbosity);
  p4est_init (nullptr, log_verbosity);

  /* process command line arguments */
  usage =
    "Arguments: <level> <ntrees_x> <ntrees_y>"
#ifdef P4_TO_P8
    " <ntrees_z>"
#endif
    " [<imax> [<tol>]]\n";
  wrongusage = 0;
  if (!wrongusage && (argc < P4EST_DIM + 2 || argc > P4EST_DIM + 4)) {
    wrongusage = 1;
  }
  if (wrongusage) {
    P4EST_GLOBAL_LERROR (usage);
    sc_abort_collective ("Usage error");
  }

  /* assign variables based on configuration */
  refine_level = atoi (argv[1]);
  refine_fn = refine_poisson_fn;
  coarsen_fn = nullptr;

  for (int i = 0; i < P4EST_DIM; ++i)
    ntrees[i] = atoi (argv[i + 2]);

  if (argc > P4EST_DIM + 2)
    imax = atoi (argv[P4EST_DIM + 2]);
  if (argc > P4EST_DIM + 3)
    tol = atof (argv[P4EST_DIM + 3]);

  /* create connectivity and forest structures */
  geom = nullptr;
#ifndef P4_TO_P8
  connectivity = p4est_connectivity_new_brick (ntrees[0], ntrees[1], 1, 1);
#else
  connectivity = p8est_connectivity_new_brick (ntrees[0], ntrees[1], ntrees[2], 1, 1, 1);
#endif

  p4est = p4est_new_ext (mpi->mpicomm, connectivity, 15, 0, 0,
                         sizeof (user_data_t), init_fn, geom);

  /* refinement and coarsening */
  p4est_refine (p4est, 1, refine_fn, init_fn);
  if (coarsen_fn != nullptr) {
    p4est_coarsen (p4est, 1, coarsen_fn, init_fn);
  }
  //p4est_vtk_write_file (p4est, geom, "poisson_refined");

  /* balance */
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);
  //p4est_vtk_write_file (p4est, geom, "poisson_balanced");

  P4EST_GLOBAL_INFOF("Is p4est grid balanced ? : %d\n",p4est_is_balanced (p4est, P4EST_CONNECT_FULL));

  /* partition */
  p4est_partition (p4est, 0, nullptr);
  //p4est_vtk_write_file (p4est, geom, "poisson_partition");


  /* Poisson solver */

  /* Create the ghost layer to learn about parallel neighbors. */
  ghost = p4est_ghost_new (p4est, P4EST_CONNECT_FULL);

  /* Create the mesh structure to learn about global boundary faces. */
  mesh = p4est_mesh_new_ext (p4est, ghost, 1, 0, P4EST_CONNECT_FACE);

  /* Create a node numbering for continuous linear finite elements. */
  lnodes = p4est_lnodes_new (p4est, ghost, 1);

  /* Interpolate rho_nodes from quadrants */
  rho_nodes = allocate_vector (lnodes);
  interp_quad_to_lnodes (p4est, lnodes, qdata_get_rho, get_quadrant_volume, rho_nodes);

  /* Subtract mean value */
  rho_avg = compute_average(p4est, qdata_get_rho, get_quadrant_volume);

  for(p4est_locidx_t i = 0; i < lnodes->num_local_nodes; ++i)
    rho_nodes[i] -= rho_avg;

  /* Solve Poisson equation */
  phi_fe = allocate_vector (lnodes);
  memset(phi_fe, 0, lnodes->num_local_nodes * sizeof(double));
  solve_poisson_lnodes (p4est, lnodes, nullptr, rho_nodes, phi_fe, imax, tol);

  /* Interpolate phi and grad_phi back to quadrants */
  interp_lnodes_to_quad (p4est, lnodes, phi_fe, qdata_set_phi);
  gradient_lnodes_to_quad (p4est, lnodes, get_quadrant_scale, phi_fe, qdata_set_grad_phi);

  /* End of Poisson solver */

  /* save output data */
  write_user_data(p4est, geom, lnodes, filename, rho_nodes, phi_fe);

  /* Clean up allocated arrays */
  P4EST_FREE (phi_fe);
  P4EST_FREE (rho_nodes);

  /* destroy lnodes data */
  p4est_lnodes_destroy (lnodes);

  /* destroy the mesh structure */
  p4est_mesh_destroy (mesh);

  /* destroy the ghost structure */
  p4est_ghost_destroy (ghost);

  /* destroy the p4est and its connectivity structure */
  p4est_destroy (p4est);
  if (geom != nullptr) {
    p4est_geometry_destroy (geom);
  }
  p4est_connectivity_destroy (connectivity);

  /* clean up and exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
