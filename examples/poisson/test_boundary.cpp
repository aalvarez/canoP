
#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_extended.h>
#include <p4est_iterate.h>
#include <p4est_ghost.h>
#include <p4est_lnodes.h>
#else
#include <p8est_bits.h>
#include <p8est_extended.h>
#include <p8est_iterate.h>
#include <p8est_ghost.h>
#include <p8est_lnodes.h>
#endif

#include "quadrant_utils.h"
#include "connectivity.h"
#include "geometry.h"
#include "IO_Writer.h"
#include "Lnode_Utils.h"

#ifndef P4_TO_P8
static int          refine_level = 2;
#else
static int          refine_level = 2;
#endif
static int          max_level = 7;

#ifndef P4_TO_P8
static int          ntrees[] = {1, 1};
#else
static int          ntrees[] = {1, 1, 1};
#endif

struct mpi_context_t
{
  sc_MPI_Comm         mpicomm;
  int                 mpisize;
  int                 mpirank;
};


struct user_data_t
{
  double rho;
};


static void
init_fn (p4est_t * p4est,
         p4est_topidx_t which_tree,
         p4est_quadrant_t * quad)
{
  user_data_t        *data = (user_data_t *) quad->p.user_data;

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);
  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quad->level) / 2;

  // get the physical coordinates of the center of the quad
  // assume geometry is regular cartesian
  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quad->x + half_length,
                          quad->y + half_length,
#ifdef P4_TO_P8
                          quad->z + half_length,
#endif
                          XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  data->rho = x+y;
} /* init_fn */

static int
refine_fn (p4est_t * p4est, p4est_topidx_t which_tree,
           p4est_quadrant_t * quadrant)
{
  int                 cid;
  double  X[3] = { 0, 0, 0 };
  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quadrant->level) / 2;

  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quadrant->x + half_length,
                          quadrant->y + half_length,
#ifdef P4_TO_P8
                          quadrant->z + half_length,
#endif
                          X);

  if (which_tree == 2 || which_tree == 3) {
    return 0;
  }

  if ( quadrant->level >= max_level)
    return 0;

  if (X[0]*X[0] + X[1]*X[1] < 0.5*0.5)
    return 1;

  cid = p4est_quadrant_child_id (quadrant);

  if (cid == P4EST_CHILDREN - 1 ||
      (quadrant->x >= P4EST_LAST_OFFSET (P4EST_MAXLEVEL - 2) &&
       quadrant->y >= P4EST_LAST_OFFSET (P4EST_MAXLEVEL - 2)
#ifdef P4_TO_P8
       && quadrant->z >= P4EST_LAST_OFFSET (P4EST_MAXLEVEL - 2)
#endif
       )) {
    return 1;
  }
  if ((int) quadrant->level >= (refine_level - (int) (which_tree % 3))) {
    return 0;
  }
  if (quadrant->level == 1 && cid == 2) {
    return 1;
  }
  if (quadrant->x == P4EST_QUADRANT_LEN (2) &&
      quadrant->y == P4EST_LAST_OFFSET (2)) {
    return 1;
  }
  if (quadrant->y >= P4EST_QUADRANT_LEN (2)) {
    return 0;
  }

  return 1;
}

static void
write_user_data(p4est_t *p4est, p4est_geometry_t *geom, p4est_lnodes_t *lnodes,
    const char *filename, int8_t *bc)
{
  const p4est_locidx_t nloc = lnodes->num_local_nodes;
  p4est_locidx_t       lni;
  int                  write_mesh_info = 1;
  double              *tmp_nodes;

  IO_Writer *w = new IO_Writer(p4est, geom, filename, write_mesh_info);
  w->open(filename);

  w->write_header(0.0);

  tmp_nodes = P4EST_ALLOC (double, nloc);
  for (lni = 0; lni < nloc; ++lni) {
    tmp_nodes[lni] = (double)bc[lni];
  }
  w->write_lnode_attribute("bc", lnodes, tmp_nodes,
      H5T_NATIVE_DOUBLE, -1.);

  for (lni = 0; lni < nloc; ++lni) {
    tmp_nodes[lni] = 0.0;
  }
  w->write_lnode_attribute("hang", lnodes, tmp_nodes,
      H5T_NATIVE_DOUBLE, 1.);

  w->write_footer();
  w->close();

  delete w;

  P4EST_FREE(tmp_nodes);
}

int
main (int argc, char **argv)
{
  int                 mpiret;
  int                 wrongusage;
  const char         *usage;
  int                 log_verbosity;
  mpi_context_t       mpi_context, *mpi = &mpi_context;
  p4est_t            *p4est;
  p4est_connectivity_t *connectivity;
  p4est_mesh_t       *mesh;
  p4est_geometry_t   *geom;
  p4est_coarsen_t     coarsen_fn;

  int8_t             *bc;
  p4est_ghost_t      *ghost;
  p4est_lnodes_t     *lnodes;

#ifndef P4_TO_P8
  const char filename[] = "test_boundary_2d";
#else
  const char filename[] = "test_boundary_3d";
#endif

  /* initialize MPI and p4est internals */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpi->mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpi->mpicomm, &mpi->mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpi->mpicomm, &mpi->mpirank);
  SC_CHECK_MPI (mpiret);

  log_verbosity = SC_LP_ALWAYS;
  sc_init (mpi->mpicomm, 1, 1, nullptr, log_verbosity);
  p4est_init (nullptr, log_verbosity);

  /* process command line arguments */
  usage =
    "Arguments: <level> <ntrees_x> <ntrees_y>"
#ifdef P4_TO_P8
    " <ntrees_z>"
#endif
    "\n";
  wrongusage = 0;
  if (!wrongusage && (argc != P4EST_DIM + 2)) {
    wrongusage = 1;
  }
  if (wrongusage) {
    P4EST_GLOBAL_LERROR (usage);
    sc_abort_collective ("Usage error");
  }

  /* assign variables based on configuration */
  refine_level = atoi (argv[1]);
  coarsen_fn = nullptr;

  for (int i = 0; i < P4EST_DIM; ++i)
    ntrees[i] = atoi (argv[i + 2]);

  /* create connectivity and forest structures */
  geom = nullptr;
#ifndef P4_TO_P8
  connectivity = p4est_connectivity_new_brick (ntrees[0], ntrees[1], 0, 0);
#else
  connectivity = p8est_connectivity_new_brick (ntrees[0], ntrees[1], ntrees[2], 0, 0, 0);
#endif

  p4est = p4est_new_ext (mpi->mpicomm, connectivity, 15, 0, 0,
                         sizeof (user_data_t), init_fn, geom);

  /* refinement and coarsening */
  p4est_refine (p4est, 1, refine_fn, init_fn);
  if (coarsen_fn != nullptr) {
    p4est_coarsen (p4est, 1, coarsen_fn, init_fn);
  }

  /* balance */
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);

  P4EST_GLOBAL_INFOF("Is p4est grid balanced ? : %d\n",p4est_is_balanced (p4est, P4EST_CONNECT_FULL));

  /* partition */
  p4est_partition (p4est, 0, nullptr);

  /* Poisson solver */

  /* Create the ghost layer to learn about parallel neighbors. */
  ghost = p4est_ghost_new (p4est, P4EST_CONNECT_FULL);

  /* Create the mesh structure to learn about global boundary faces. */
  mesh = p4est_mesh_new_ext (p4est, ghost, 1, 0, P4EST_CONNECT_FACE);

  /* Create a node numbering for continuous linear finite elements. */
  lnodes = p4est_lnodes_new (p4est, ghost, 1);

  /* Flag boundaries */
  bc = P4EST_ALLOC (int8_t, lnodes->num_local_nodes);
  lnodes_flag_boundaries(p4est, mesh, lnodes, bc);

  /* save output data */
  write_user_data(p4est, geom, lnodes, filename, bc);

  /* clean up */
  P4EST_FREE (bc);

  /* destroy lnodes data */
  p4est_lnodes_destroy (lnodes);

  /* destroy the mesh structure */
  p4est_mesh_destroy (mesh);

  /* destroy the ghost structure */
  p4est_ghost_destroy (ghost);

  /* destroy the p4est and its connectivity structure */
  p4est_destroy (p4est);
  if (geom != nullptr) {
    p4est_geometry_destroy (geom);
  }
  p4est_connectivity_destroy (connectivity);

  /* clean up and exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}
