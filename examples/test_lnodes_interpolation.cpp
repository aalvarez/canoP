
#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_vtk.h>
#include <p4est_algorithms.h>
#include <p4est_bits.h>
#include <p4est_extended.h>
#include <p4est_ghost.h>
#include <p4est_lnodes.h>
#else
#include <p8est_vtk.h>
#include <p8est_algorithms.h>
#include <p8est_bits.h>
#include <p8est_extended.h>
#include <p8est_ghost.h>
#include <p8est_lnodes.h>
#endif

#include "IO_Writer.h"
#include "Lnode_Utils.h"

#define UNUSED(x) ((void)(x))

static const int    zero = 0;           /**< Constant zero. */
static const int    ones = P4EST_CHILDREN - 1;  /**< One bit per dimension. */

/** Per-quadrant data for this test. */
typedef struct user_data
{
  double              rho;            /**< the state variable */
  double              rho_r;          /**< the reconstructed value */
} user_data_t;

/** Per-quadrant data arrays for this test. */
typedef struct cell_arrays
{
  sc_array_t         *rho_arr;        /**< the state variable */
  sc_array_t         *rho_r_arr;      /**< the reconstructed value */
}
cell_arrays_t;


#ifndef P4_TO_P8
static int          refine_level = 2;
#else
static int          refine_level = 2;
#endif

static int          max_level = 3;

#ifndef P4_TO_P8
static p4est_connectivity_t *
p4est_connectivity_new_lnodes_test (void)
{
  const p4est_topidx_t num_vertices = 6;
  const p4est_topidx_t num_trees = 2;
  const p4est_topidx_t num_ctt = 0;
  const double        vertices[6 * 3] = {
    0, 0, 0,
    1, 0, 0,
    0, 1, 0,
    1, 1, 0,
    0, 2, 0,
    1, 2, 0,
  };
  const p4est_topidx_t tree_to_vertex[2 * 4] = {
    2, 3, 4, 5, 0, 1, 2, 3,
  };
  const p4est_topidx_t tree_to_tree[2 * 4] = {
    0, 0, 1, 0, 1, 1, 1, 0,
  };
  const int8_t        tree_to_face[2 * 4] = {
    0, 1, 3, 3, 0, 1, 2, 2,
  };

  return p4est_connectivity_new_copy (num_vertices, num_trees, 0,
                                      vertices, tree_to_vertex,
                                      tree_to_tree, tree_to_face,
                                      NULL, &num_ctt, NULL, NULL);
}
#endif

static int
refine_fn (p4est_t * p4est, p4est_topidx_t which_tree,
           p4est_quadrant_t * quadrant)
{
  int                 cid;
  double  X[3] = { 0, 0, 0 };
  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quadrant->level) / 2;

  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quadrant->x + half_length,
                          quadrant->y + half_length,
#ifdef P4_TO_P8
                          quadrant->z + half_length,
#endif
                          X);

  if (which_tree == 2 || which_tree == 3) {
    return 0;
  }

  if ( quadrant->level >= max_level)
    return 0;

  if (X[0]*X[0] + X[1]*X[1] < 0.5*0.5)
    return 1;

  cid = p4est_quadrant_child_id (quadrant);

  if (cid == P4EST_CHILDREN - 1 ||
      (quadrant->x >= P4EST_LAST_OFFSET (P4EST_MAXLEVEL - 2) &&
       quadrant->y >= P4EST_LAST_OFFSET (P4EST_MAXLEVEL - 2)
#ifdef P4_TO_P8
       && quadrant->z >= P4EST_LAST_OFFSET (P4EST_MAXLEVEL - 2)
#endif
       )) {
    return 1;
  }
  if ((int) quadrant->level >= (refine_level - (int) (which_tree % 3))) {
    return 0;
  }
  if (quadrant->level == 1 && cid == 2) {
    return 1;
  }
  if (quadrant->x == P4EST_QUADRANT_LEN (2) &&
      quadrant->y == P4EST_LAST_OFFSET (2)) {
    return 1;
  }
  if (quadrant->y >= P4EST_QUADRANT_LEN (2)) {
    return 0;
  }

  return 1;
}

#ifndef P4_TO_P8
static int
refine_fn_lnodes_test (p4est_t * p4est, p4est_topidx_t which_tree,
                       p4est_quadrant_t * quadrant)
{
  UNUSED(p4est);
  
  int                 cid;

  cid = p4est_quadrant_child_id (quadrant);

  if (!which_tree && cid == 1) {
    return 1;
  }
  if (which_tree == 1 && cid == 2) {
    return 1;
  }
  return 0;
}
#endif

/** Allocate storage for processor-relevant nodal degrees of freedom.
 *
 * \param [in] lnodes   This structure is queried for the node count.
 * \return              Allocated double array; must be freed with P4EST_FREE.
 */
static double      *
allocate_vector_double (p4est_lnodes_t * lnodes)
{
  return P4EST_ALLOC (double, lnodes->num_local_nodes);
}

static int      *
allocate_vector_int (p4est_lnodes_t * lnodes)
{
  return P4EST_ALLOC (int, lnodes->num_local_nodes);
}


typedef struct tpoint
{
  p4est_topidx_t      tree;
  double              point[P4EST_DIM];
}
  tpoint_t;

static void
get_point (double point[P4EST_DIM], p4est_quadrant_t * q, int i, int j,
#ifdef P4_TO_P8
           int k,
#endif
           int degree)
{
  p4est_qcoord_t      len = P4EST_QUADRANT_LEN (q->level);
  double              rlen = (double) P4EST_ROOT_LEN;
  double              deg = (double) degree;
  double              qlen = ((double) len) / rlen;

  P4EST_ASSERT (0 <= i && i < degree + 1);
  P4EST_ASSERT (0 <= j && j < degree + 1);
#ifdef P4_TO_P8
  P4EST_ASSERT (0 <= k && k < degree + 1);
#endif

  point[0] = ((double) q->x) / rlen + (((double) i) / deg) * qlen;
  point[1] = ((double) q->y) / rlen + (((double) j) / deg) * qlen;
#ifdef P4_TO_P8
  point[2] = ((double) q->z) / rlen + (((double) k) / deg) * qlen;
#endif
}

static double get_quadrant_volume(p4est_t * p4est,
                                  p4est_topidx_t which_tree,
                                  p4est_quadrant_t * quad)
{
  UNUSED(p4est);
  UNUSED(which_tree);

  /* assume here cartesian geometry */
  double dx = 1.0*P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;

#ifndef P4_TO_P8
  return dx*dx;
#else
  return dx*dx*dx;
#endif

} /* get_quadrant_volume */

/** Determine the boundary status on the unit square/cube.
 * \param [in] p4est    Can be used to access the connectivity.
 * \param [in] tt       The tree number (always zero for the unit square).
 * \param [in] node     The corner node of an element to be examined.
 * \return              True for Dirichlet boundary, false otherwise.
 */
static int
is_boundary_unitsquare (p4est_t * p4est, p4est_topidx_t tt,
                        p4est_quadrant_t * node)
{
  UNUSED(p4est);
  UNUSED(tt);

  /* For this simple connectivity it is sufficient to check x, y (and z). */
  return (node->x == 0 || node->x == P4EST_ROOT_LEN ||
          node->y == 0 || node->y == P4EST_ROOT_LEN ||
#ifdef P4_TO_P8
          node->z == 0 || node->z == P4EST_ROOT_LEN ||
#endif
          0);
}

static void
init_fn (p4est_t * p4est,
         p4est_topidx_t which_tree,
         p4est_quadrant_t * quad)
{
  UNUSED(p4est);
  UNUSED(which_tree);

  user_data_t        *data = (user_data_t *) quad->p.user_data;

  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);
  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quad->level) / 2;

  // get the physical coordinates of the center of the quad
  // assume geometry is regular cartesian
  p4est_qcoord_to_vertex (p4est->connectivity, which_tree,
                          quad->x + half_length,
                          quad->y + half_length,
#ifdef P4_TO_P8
                          quad->z + half_length,
#endif
                          XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  data->rho = x+y;
  data->rho_r = -1.;
} /* init_fn */

static void
get_cell_data_callback(p4est_iter_volume_info_t * info, void *user_data)
{
  /* we passed the array of values to fill as the user_data in the call
     to p4est_iterate */
  cell_arrays_t      *cell_data = (cell_arrays_t *) user_data;
  double             *this_data;
  p4est_t            *p4est = info->p4est;
  p4est_quadrant_t   *q = info->quad;
  p4est_topidx_t      which_tree = info->treeid;
  p4est_locidx_t      local_id = info->quadid;  /* this is the index of q *within its tree's numbering*.  We want to convert it its index for all the quadrants on this process, which we do below */
  p4est_tree_t       *tree;
  user_data_t        *data = (user_data_t *) q->p.user_data;
  p4est_locidx_t      arrayoffset;

  tree = p4est_tree_array_index (p4est->trees, which_tree);
  local_id += tree->quadrants_offset;   /* now the id is relative to the MPI process */
  arrayoffset = local_id;

  this_data = (double*) sc_array_index (cell_data->rho_arr, arrayoffset);
  this_data[0] = data->rho;

  this_data = (double*) sc_array_index (cell_data->rho_r_arr, arrayoffset);
  this_data[0] = data->rho_r;

} /* get_cell_data_callback */


void write_quad_data_vtk(p4est_t *p4est, char* filename)
{
  UNUSED(p4est);
  UNUSED(filename);

  //#define USE_P4EST_VTK_NEW_INTERFACE
#ifdef USE_P4EST_VTK_NEW_INTERFACE

  cell_arrays_t       cell_data; /* arrays of cell data to write */
  p4est_locidx_t      numquads;

  numquads = p4est->local_num_quadrants;

  /* create vectors with one value per local quadrant */
  cell_data.rho_arr = sc_array_new_size (sizeof(double), numquads);
  cell_data.rho_r_arr = sc_array_new_size (sizeof(double), numquads);

  /* Use the iterator to visit every cell and fill in the data array */
  p4est_iterate (p4est,
                 NULL,   /* we don't need any ghost quadrants for this loop */
                 (void *) &cell_data,     /* pass in array pointer so that we can fill it */
                 get_cell_data_callback,   /* callback function to get cell data */
                 NULL,
#ifdef P4_TO_P8
                 NULL,
#endif
                 NULL);


  /* write vtk file */
  p4est_vtk_context_t *context = p4est_vtk_context_new (p4est, filename);
  p4est_vtk_context_set_scale (context, 0.99);  /* quadrant at almost full scale */

  /* begin writing the output files */
  context = p4est_vtk_write_header (context);
  SC_CHECK_ABORT (context != NULL,
                  P4EST_STRING "_vtk: Error writing vtk header");

  context = p4est_vtk_write_cell_dataf (context,
                                        1, 1,/* do write the refinement level of each quadrant */
                                        1,   /* do write the mpi procid of each quadrant */
                                        0,   /* do not wrap the mpi rank */
                                        2,   /* there are 2 cell scalar data arrays. */
                                        0,   /* there is no cell vector data. */
                                        "rho", cell_data.rho_arr,
                                        "rho_r", cell_data.rho_r_arr,
                                        context);
  SC_CHECK_ABORT (context != NULL,
                  P4EST_STRING "_vtk: Error writing cell data");

  const int retval = p4est_vtk_write_footer (context);
  SC_CHECK_ABORT (!retval, P4EST_STRING "_vtk: Error writing footer");

  sc_array_destroy(cell_data.rho_arr);
  sc_array_destroy(cell_data.rho_r_arr);

#else

  /* TODO: write data using something else */

#endif /* USE_P4EST_VTK_NEW_INTERFACE */
} /* write_quad_data_vtk */


void write_node_data_vtk(p4est_t *p4est, const char* filename,
        p4est_lnodes_t *lnodes, const double *lnodes_data)
{

#ifdef USE_P4EST_VTK_NEW_INTERFACE

  sc_array_t *node_data; /* array of node data to write */
  p4est_locidx_t numquads;

  numquads = p4est->local_num_quadrants;

  /* create a vector with 2^d values per local quadrant */
  node_data = sc_array_new_size (sizeof(double), P4EST_CHILDREN * numquads);
  SC_CHECK_ABORT(node_data != NULL,
      "write_node_data_vtk: array creation failure");

  /* Loop over local quadrants to fill in the output array. */
  {
    p4est_topidx_t      tt;
    p4est_tree_t       *tree;
    int                 i, k;
    int                 q, Q;
    p4est_locidx_t      lni;
    double             *node_p;

    for (tt = p4est->first_local_tree, k = 0;
        tt <= p4est->last_local_tree; ++tt) {
      tree = p4est_tree_array_index (p4est->trees, tt);
      Q = (p4est_locidx_t) tree->quadrants.elem_count;
      for (q = 0; q < Q; ++q, ++k) {
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          lni = lnodes->element_nodes[P4EST_CHILDREN * k + i];
          node_p = (double *) sc_array_index(node_data, P4EST_CHILDREN * k + i);
          *node_p = lnodes_data[lni];
        }
      } /* end for q */
    } /* end for tt */
  } /* end quadrant loop */


  /* write vtk file */
  p4est_vtk_context_t *context = p4est_vtk_context_new (p4est, filename);
  p4est_vtk_context_set_scale (context, 0.99);  /* quadrant at almost full scale */

  /* begin writing the output files */
  context = p4est_vtk_write_header (context);
  SC_CHECK_ABORT (context != NULL,
                  P4EST_STRING "_vtk: Error writing vtk header");

  context = p4est_vtk_write_point_dataf (context, 1, 0, "rho_n", node_data, context);
  SC_CHECK_ABORT (context != NULL,
                  P4EST_STRING "_vtk: Error writing cell data");

  const int retval = p4est_vtk_write_footer (context);
  SC_CHECK_ABORT (!retval, P4EST_STRING "_vtk: Error writing footer");

  sc_array_destroy(node_data);

#else

  /* TODO: write data using something else */

#endif /* USE_P4EST_VTK_NEW_INTERFACE */
} /* write_node_data_vtk */

/* data getters */
double
qdata_get_rho (p4est_quadrant_t *q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;
  return data->rho;
}

double
qdata_get_rho_r (p4est_quadrant_t *q)
{
  user_data_t        *data = (user_data_t *) q->p.user_data;
  return data->rho_r;
}

/* data setter */
void
qdata_set_rho_r (p4est_quadrant_t *q, double val)
{
  user_data_t      *data = (user_data_t *) q->p.user_data;
  data->rho_r = val;
}

static void
test_interpolation_quad_to_lnodes (p4est_t * p4est, p4est_geometry_t * geom, p4est_lnodes_t * lnodes)
{
  /* local nodes number */
  const int           nloc = lnodes->num_local_nodes;
  /* local nodes index */
  p4est_locidx_t      lni;
  /* lnodes attached data */
  double             *lnodes_data;

  lnodes_data = allocate_vector_double (lnodes);

  interp_quad_to_lnodes (p4est, lnodes, qdata_get_rho, get_quadrant_volume, lnodes_data);

  interp_lnodes_to_quad (p4est, lnodes, lnodes_data, qdata_set_rho_r);

  /* check mass conservation */
  {
    p4est_topidx_t      tt;
    p4est_tree_t       *tree;
    p4est_quadrant_t   *quad;
    sc_array_t         *tquadrants;
    int                 i, k;
    int                 q, Q;

    user_data_t        *udata;
    double              volume;
    double              ndata[P4EST_CHILDREN];

    double              mass_quad, mass_node;
    double              mass_delta, mass_delta_quad, mass_temp;

    double              mass_buf_loc[3], mass_buf_tot[3];
    int                 mpiret;

    mass_quad = 0;
    mass_node = 0;
    mass_delta = 0;

    for (tt = p4est->first_local_tree, k = 0;
         tt <= p4est->last_local_tree; ++tt) {
      tree = p4est_tree_array_index (p4est->trees, tt);
      tquadrants = &tree->quadrants;
      Q = (p4est_locidx_t) tquadrants->elem_count;
      for (q = 0; q < Q; ++q, ++k) {
        quad = p4est_quadrant_array_index (tquadrants, q);
        udata = (user_data_t *) quad->p.user_data;
        volume = get_quadrant_volume(p4est, tt, quad);

        mass_delta_quad = volume * udata->rho;
        mass_quad += mass_delta_quad;

        /* retrieve local node data, interpolating hanging nodes */
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          lni = lnodes->element_nodes[P4EST_CHILDREN * k + i];
          ndata[i] = lnodes_data[lni];
        }
        lnodes_interpolate_hanging (lnodes->face_code[k], ndata);

        /* compute volume from node data */
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          mass_temp = ndata[i] * volume / (double)P4EST_CHILDREN;
          mass_node += mass_temp;
          mass_delta_quad -= mass_temp;
        }

        mass_delta += mass_delta_quad;
      } /* end for q */
    } /* end for tt */

    mass_buf_loc[0] = mass_quad;
    mass_buf_loc[1] = mass_node;
    mass_buf_loc[2] = mass_delta;
    mpiret = sc_MPI_Reduce (mass_buf_loc, mass_buf_tot, 3, sc_MPI_DOUBLE, sc_MPI_SUM,
                            0, p4est->mpicomm);
    SC_CHECK_MPI (mpiret);

    if (p4est->mpirank == 0) {
      mass_quad = mass_buf_tot[0];
      mass_node = mass_buf_tot[1];
      mass_delta = mass_buf_tot[2];
      printf("Mass from quads: %g, mass from nodes: %g, delta: %g\n",
          mass_quad, mass_node, mass_delta);
    }
  }

  /* use canoP parallel Hdf5/Xdmf IO */
  {

    int                 write_mesh_info = 1;
    const char          filename[] = "test_lnodes_interpolation";
    double             *lnodes_ids;

    lnodes_ids = allocate_vector_double(lnodes);
    for (lni = 0; lni < nloc; ++lni) {
      lnodes_ids[lni] = (double)lni;
    }

    IO_Writer *w = new IO_Writer(p4est, geom, filename, write_mesh_info);
    w->open(filename);

    w->write_header(0.0);

    w->write_quadrant_attribute("rho", qdata_get_rho,
                                H5T_NATIVE_DOUBLE);

    w->write_quadrant_attribute("rho_r", qdata_get_rho_r,
                                H5T_NATIVE_DOUBLE);

    w->write_lnode_attribute("rho_n", lnodes, lnodes_data,
                             H5T_NATIVE_DOUBLE);

    w->write_lnode_attribute("lni_n", lnodes, lnodes_ids,
                             H5T_NATIVE_DOUBLE);

    w->write_footer();
    w->close();

    delete w;

    P4EST_FREE(lnodes_ids);

  } // end hdf5/xdmf writer

  P4EST_FREE(lnodes_data);

} /* test_interpolation_quad_to_lnodes */

int
main (int argc, char **argv)
{
  sc_MPI_Comm         mpicomm;
  int                 mpiret;
  int                 mpisize, mpirank;
  p4est_t            *p4est;
  p4est_connectivity_t *conn;
  p4est_ghost_t      *ghost_layer;
  int                 i;
  p4est_lnodes_t     *lnodes;
  //p4est_tree_t       *tree;
  //p4est_quadrant_t   *q, p, *q_ptr;
  //int                 bcount;
  //int                 f;
  //p4est_lnodes_rank_t *lrank;

  //int                 wrongusage;
  const char         *usage;


  /* initialize MPI */
  mpiret = sc_MPI_Init (&argc, &argv);
  SC_CHECK_MPI (mpiret);
  mpicomm = sc_MPI_COMM_WORLD;
  mpiret = sc_MPI_Comm_size (mpicomm, &mpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = sc_MPI_Comm_rank (mpicomm, &mpirank);
  SC_CHECK_MPI (mpiret);

  sc_init (mpicomm, 1, 1, NULL, SC_LP_DEFAULT);
  p4est_init (NULL, SC_LP_DEFAULT);

  /* process command line arguments */
  usage =
    "Arguments: <configuration_number> <level>\n"
    "   Configuration is an integer\n"
    "   Level controls the maximum depth of refinement\n";

  i = 0;
  if (argc>1)
    i = atoi(argv[1]);

  if (argc>2)
    refine_level = atoi (argv[2]);

  //refine_fn = refine_normal_fn;
  //coarsen_fn = NULL;

  /* create connectivity and forest structures */
  switch (i) {
#ifndef P4_TO_P8
  case 0:
    conn = p4est_connectivity_new_unitsquare ();
    break;
  case 1:
    conn = p4est_connectivity_new_moebius ();
    break;
  case 2:
    conn = p4est_connectivity_new_star ();
    break;
  case 3:
    conn = p4est_connectivity_new_periodic ();
    break;
  case 4:
    conn = p4est_connectivity_new_lnodes_test ();
    break;
  case 5:
    conn = p4est_connectivity_new_brick (1, 2, 0, 0);
    break;
  default:
    conn = p4est_connectivity_new_unitsquare ();
    break;
#else
  case 0:
    conn = p8est_connectivity_new_unitcube ();
    break;
  case 1:
    conn = p8est_connectivity_new_periodic ();
    break;
  case 2:
    conn = p8est_connectivity_new_rotwrap ();
    break;
  case 3:
    conn = p8est_connectivity_new_rotcubes ();
    break;
  case 4:
    conn = p8est_connectivity_new_shell ();
    break;
  case 5:
    conn = p8est_connectivity_new_brick (1, 2, 3, 0, 0, 0);
    break;
  default:
    conn = p8est_connectivity_new_unitcube ();
    break;
#endif
  }

  p4est = p4est_new_ext (mpicomm, conn, 15, 0, 0,
                         sizeof (user_data_t), init_fn, NULL);
  /*p4est = p4est_new_ext (mpicomm, conn, 15, 0, 0,
    0, NULL, NULL);*/

  /* refine recursive to make the number of elements interesting */
  p4est_refine (p4est, 1, refine_fn, init_fn);

  /* do a uniform partition */
  p4est_partition (p4est, 0, nullptr);

  /* balance and repartition the forest */
  p4est_balance (p4est, P4EST_CONNECT_FULL, init_fn);
  p4est_partition (p4est, 1, nullptr);

  /* Write the forest to disk for visualization, one file per processor. */
  //write_quad_data_vtk (p4est, "test_lnodes2_interpolation");


  /* create ghost layer before lnodes data structure */
  ghost_layer = p4est_ghost_new (p4est, P4EST_CONNECT_FULL);

  /* create lnode structure */
  lnodes = p4est_lnodes_new (p4est, ghost_layer, 1);


  /*
   * Now start the test.
   */
  test_interpolation_quad_to_lnodes(p4est, NULL, lnodes);


  /* destroy ghost (no needed after node creation */
  p4est_ghost_destroy (ghost_layer);

  /* Destroy lnodes */
  p4est_lnodes_destroy (lnodes);

  /* Destroy the p4est and the connectivity structure. */
  p4est_destroy (p4est);
  p4est_connectivity_destroy (conn);

  /* exit */
  sc_finalize ();

  mpiret = sc_MPI_Finalize ();
  SC_CHECK_MPI (mpiret);

  return 0;
}

/* EOF test_lnodes_interpolation.c */
