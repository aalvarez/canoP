import math

epsilon = 0.5
p0 = [1e+5, 1e+5]
rho0 = [1.0, 1000.0]
c = [3.0, 15.0]

def init_alpha (x, y, z):
    if x < 1.0:
        return 1 - epsilon
    else:
        return epsilon

def init_rho_gas (x, y, z):
    if x < 1.0:
        rho = 100
    else:
        rho = 1

    return rho

def init_rho_liquid (rho1):
    p = p0[0] + c[0] ** 2 * (rho1 - rho0[0])
    return (p - p0[1]) / c[1] ** 2 + rho0[1]

def compute_alpha (arho0, arho1):
    q0 = p0[1] - p0[0] - (rho0[1] * c[1] ** 2 - rho0[0] * c[0] ** 2)
    q = arho0 * c[0] ** 2 + arho1 * c[1] ** 2

    delta = (q0 + q) ** 2 - 4 * q0 * arho0 * c[0] ** 2

    print(q0, q, delta)
    if abs(q0) < 1e-12:
        print("first case")
        alpha = arho0 * c[0] ** 2 / q
    elif abs(arho0) < 1e-12:
        print ("second case")
        alpha = 0
    else:
        print("third case")
        alpha = (q + q0 - math.sqrt(delta)) / (2 * q0);

    return alpha

x = y = z = 0
rho1 = init_rho_gas (x, y, z)
rho2 = init_rho_liquid (rho1)

alpha = init_alpha (x, y, z)
alpha_computed = compute_alpha (alpha * rho1, (1 - alpha) * rho2)

print (rho1, rho2)
print(alpha)
print(alpha_computed)

x = y = z = 1
rho1 = init_rho_gas (x, y, z)
rho2 = init_rho_liquid (rho1)

alpha = init_alpha (x, y, z)
alpha_computed = compute_alpha (alpha * rho1, (1 - alpha) * rho2)

print (rho1, rho2)
print(alpha)
print(alpha_computed)
