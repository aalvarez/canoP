#ifndef USERDATA_BURGERS_H_
#define USERDATA_BURGERS_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif



namespace canop {

namespace burgers {

enum GeomDir {
  IX = 0,
  IY = 1,
  IZ = 2
};

/* number of variables <==> P4EST_DIM : one velocity per dimension */
constexpr int QDATA_NUM_VARIABLES = P4EST_DIM;

/**
 * \brief Vector of Burgers velocities variables.
 *
 *  We have
 *        U, V and W (3D only).
 */
struct QdataVar
{

  double U;
  double V;
#ifdef P4_TO_P8
  double W;
#endif // P4_TO_P8

  inline
  QdataVar() : U(0.0)
	     , V(0.0)
#ifdef P4_TO_P8
	     , W(0.0)
#endif
  {}

  inline
  QdataVar &operator+=(const QdataVar& operand) {
    this->U += operand.U;
    this->V += operand.V;
#ifdef P4_TO_P8
    this->W += operand.W;
#endif

    return *this;
  };
  
}; // struct QdataVar

using QdataRecons = QdataVar;

#ifndef P4_TO_P8
inline void QDATA_INFOF(const QdataVar& w) {
  printf("U %g | V %g\n",
	 w.U,w.V);
}
#else // 3D
inline void QDATA_INFOF(const QdataVar& w) {
  printf("U %g | V %g | W %g\n",
	 w.U,w.V,w.W);
}
#endif // P4_TO_P8

/**
 * This is the main data structure (one by quadrant/cell).
 */
struct Qdata
{
  QdataVar   w;        /*!< variable at the current time */
  QdataVar   wnext;    /*!< variable at t + dt */

}; // struct Qdata

} // namespace burgers

} // namespace canop

#endif // USERDATA_BURGERS_H_
