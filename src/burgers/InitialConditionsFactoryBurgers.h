#ifndef INITIAL_CONDITIONS_FACTORY_BURGERS_H_
#define INITIAL_CONDITIONS_FACTORY_BURGERS_H_

#include "InitialConditionsFactory.h"
#include "burgers/userdata_burgers.h"

namespace canop {

namespace burgers {

/**
 *
 */
class InitialConditionsFactoryBurgers : public InitialConditionsFactory
{

  using qdata_t = Qdata;
  
public:
  InitialConditionsFactoryBurgers();
  virtual ~InitialConditionsFactoryBurgers() {};
  
}; // class InitialConditionsFactoryBurgers

} // namespace burgers

} // namespace canop

#endif // INITIAL_CONDITIONS_FACTORY_BURGERS_H_
