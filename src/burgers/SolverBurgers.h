#ifndef SOLVER_BURGERS_H_
#define SOLVER_BURGERS_H_

#include "Solver.h"
#include "userdata_burgers.h"

#include <mpi.h>

class ConfigReader;

namespace canop {

namespace burgers {

/**
 * Concrete Solver class implementation for Burgers application.
 */
class SolverBurgers : public Solver
{

  using qdata_t = Qdata;
  
public:
  SolverBurgers(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory);
  virtual ~SolverBurgers();

  // redefine compute_time_step
  void compute_time_step();
  
  /**
   * Here we implement unsplit numerical scheme for Burgers Hydro.
   */
  void next_iteration_impl();

  /**
   * Static creation method called by the solver factory.
   */
  static Solver* create(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory)
  {
    SolverBurgers* solver = new SolverBurgers(cfg, mpicomm, useP4estMemory);

    return solver;
  }

private:
  /**
   * Custom version of base class version.
   */
  void read_config();
  
}; // class SolverBurgers

} // namespace burgers

} // namespace canop

#endif // SOLVER_BURGERS_H_
