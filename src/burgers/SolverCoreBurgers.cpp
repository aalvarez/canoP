#include "SolverCoreBurgers.h"

#include "InitialConditionsFactoryBurgers.h"
#include "BoundaryConditionsFactoryBurgers.h"
#include "IndicatorFactoryBurgers.h"

#include "ConfigReader.h"

namespace canop {

namespace burgers {

// =======================================================
// =======================================================
SolverCoreBurgers::SolverCoreBurgers(ConfigReader *cfg) :
  SolverCore<Qdata>(cfg)
{

  /*
   * Initial condition callback setup.
   */
  InitialConditionsFactoryBurgers IC_Factory;
  m_init_fn = IC_Factory.callback_byname (m_init_name);

  /*
   * Boundary condition callback setup.
   */
  BoundaryConditionsFactoryBurgers BC_Factory;
  m_boundary_fn = BC_Factory.callback_byname (m_boundary_name);

  /*
   * Refine/Coarsen callback setup.
   */
  IndicatorFactoryBurgers indic_Factory;
  m_indicator_fn = indic_Factory.callback_byname (m_indicator_name);

  /*
   * Geometric refine callback setup.
   */
  GeometricIndicatorFactory geom_indic_Factory;
  m_geom_indicator_fn = geom_indic_Factory.callback_byname (m_geom_indicator_name);

  
} // SolverCoreBurgers::SolverCoreBurgers


// =======================================================
// =======================================================
SolverCoreBurgers::~SolverCoreBurgers()
{
} // SolverCoreBurgers::~SolverCoreBurgers

} // namespace burgers

} // namespace canop
