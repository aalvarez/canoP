#include "SolverBurgers.h"
#include "SolverCoreBurgers.h"
#include "UserDataManagerBurgers.h"
#include "IteratorsBurgers.h"

#include "IO_Writer.h"

#include "canoP_base.h"
#include "utils.h"

namespace canop {

namespace burgers {

// =======================================================
// =======================================================
SolverBurgers::SolverBurgers(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory) :
  Solver(cfg,mpicomm,sizeof(SolverBurgers::qdata_t),useP4estMemory)
{

  // setup solver core
  m_solver_core = new SolverCoreBurgers(cfg);

  // setup UserData Manager
  m_userdata_mgr = new UserDataManagerBurgers(useP4estMemory);

  // we need to setup the right iterators
  m_pIter = new IteratorsBurgers();

  // solver init (this is were p4est_new is called)
  init();
  
  // only MPI rank 0 prints config (thanks to CANOP_GLOBAL_PRODUCTION macro)
  print_config();
  
} // SolverBurgers::SolverBurgers

// =======================================================
// =======================================================
SolverBurgers::~SolverBurgers()
{

  finalize();
  
  delete m_solver_core;

  delete m_userdata_mgr;

  delete m_pIter;
  
} // SolverBurgers::~SolverBurgers

// =======================================================
// =======================================================
void
SolverBurgers::compute_time_step()
{

  p4est_t            *p4est = get_p4est();
  p4est_geometry_t   *geom  = get_geom_compute ();
  MPI_Comm            mpicomm = p4est->mpicomm;
  double              min_dx = 10;

  UNUSED (geom);

  // initialize the min / max current levels
  m_minlevel = P4EST_QMAXLEVEL;
  m_maxlevel = 0;

  // compute the min of dx
  // update the min / max current levels
  p4est_iterate (p4est, 
		 NULL, 
		 /* user data */ &min_dx, 
		 m_pIter->iteratorsList[IT_ID::CFL_TIME_STEP],  /* cell_fn */
		 NULL,                        /* face_fn */
#ifdef P4_TO_P8
                 NULL,                        /* edge_fn */
#endif
                 NULL                         /* corner_fn */
		 ); 

  // get the global maximum and minimum level
  MPI_Allreduce (MPI_IN_PLACE, &(m_minlevel), 1,
                 MPI_INT, MPI_MIN, mpicomm);
  MPI_Allreduce (MPI_IN_PLACE, &(m_maxlevel), 1,
                 MPI_INT, MPI_MAX, mpicomm);

  // update the min dt using the given courant number
  if (min_dx < 0) {
    m_dt = m_tmax;
  }
  else if (!ISFUZZYNULL (min_dx)) {
    double sigma = m_cfg->config_read_double ("ramps.sigma", 0.1);
    double nu = m_cfg->config_read_double ("ramps.nu", 0.07);

    m_dt = sigma * min_dx * min_dx / nu;
    m_dt = fmin (m_dt, fabs (m_tmax - m_t));
  }
  else {
    SC_ABORT ("min_dx is 0\n");
  }

  // take the min over all the processes
  MPI_Allreduce (MPI_IN_PLACE, &(m_dt), 1, MPI_DOUBLE, MPI_MIN,
                 mpicomm);

  
} // SolverBurgers::compute_time_step

// =======================================================
// =======================================================
void
SolverBurgers::next_iteration_impl()
{

  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();
  
  // compute the cfl condition and time step at the smallest level
  compute_time_step ();

  /*
   * Here compute the viscous right hand side ?
   */

  
  /*
   * Perform update here:
   * - simple 1s order ?
   * - Runge-Kutta higher-order ?
   *
   * Whatever you'd like to be.
   *
   */
  CANOP_GLOBAL_INFO ("Perform numerical scheme time update\n");
  p4est_iterate (p4est,
		 ghost,
		 /* user data */ NULL,
		 m_pIter->iteratorsList[IT_ID::UPDATE],
		 NULL,        // face_fn
#ifdef P4_TO_P8
		 NULL,        // edge_fn
#endif
		 NULL         // corner_fn
		 );
  
   
  // update old = new for all the quads
  CANOP_GLOBAL_INFO ("Copying new into old\n");
  p4est_iterate (p4est, 
		 ghost, 
		 NULL, 
		 m_pIter->iteratorsList[IT_ID::END_STEP], 
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );
  
} // SolverBurgers::next_iteration_impl

// =======================================================
// =======================================================
void SolverBurgers::read_config()
{
  // fisrt run base class method
  Solver::read_config();

  /*
   * Initialize burgers parameters. None here.
   */
  
} // SolverBurgers::read_config

} // namespace burgers

} // namespace canop
