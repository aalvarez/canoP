#include "BoundaryConditionsFactoryBurgers.h"

#include "SolverBurgers.h"
#include "UserDataManagerBurgers.h"
#include "quadrant_utils.h"

// remember that qdata_t is here a concrete type defined in userdata_burgers.h

#include "burgers/userdata_burgers.h"
#include "ConfigReader.h"

namespace canop {

namespace burgers {

using qdata_t               = Qdata; 
using qdata_reconstructed_t = QdataRecons;

// =======================================================
// ======BOUNDARY CONDITIONS CALLBACKS====================
// =======================================================


// =======================================================
// =======================================================
/*
 * TODO: maybe just make the boundary functions return a qdata_variables_t
 * that they have initialized and then we do the copying around.
 *
 * TODO: for order 2, how does delta have to be initialized? does it?
 */
void
bc_burgers_dirichlet (p4est_t * p4est,
		     p4est_topidx_t which_tree,
		     p4est_quadrant_t * q,
		     int face,
		     qdata_t * ghost_qdata)
{
  
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);
  UNUSED (ghost_qdata);
  
} // bc_burgers_dirichlet

// =======================================================
// =======================================================
void
bc_burgers_neuman (p4est_t * p4est,
		  p4est_topidx_t which_tree,
		  p4est_quadrant_t * q,
		  int face,
		  qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (face);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverBurgers    *solver = static_cast<SolverBurgers*>(solver_from_p4est (p4est));

  UserDataManagerBurgers
    *userdata_mgr = static_cast<UserDataManagerBurgers *> (solver->get_userdata_mgr());

  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata (q);

  /* 
   * copy the entire qdata
   */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

} // bc_burgers_neuman

// =======================================================
// ======CLASS BoundaryConditionsFactoryBurgers IMPL ======
// =======================================================
BoundaryConditionsFactoryBurgers::BoundaryConditionsFactoryBurgers() :
  BoundaryConditionsFactory<Qdata>()
{

  // register the above callback's
  registerBoundaryConditions("dirichlet" , bc_burgers_dirichlet);
  registerBoundaryConditions("neuman"    , bc_burgers_neuman);

} // BoundaryConditionsFactoryBurgers::BoundaryConditionsFactoryBurgers

} // namespace burgers

} // namespace canop
