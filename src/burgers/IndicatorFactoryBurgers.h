#ifndef INDICATOR_FACTORY_BURGERS_H_
#define INDICATOR_FACTORY_BURGERS_H_

#include "IndicatorFactory.h"

#include "burgers/userdata_burgers.h"

namespace canop {

namespace burgers {

/**
 * Concrete implement of an indicator factory for the Burgers scheme.
 *
 * Valid indicator names are "khokhlov" and "U_gradient".
 * use method registerIndicator to add another one.
 */
class IndicatorFactoryBurgers : public IndicatorFactory<Qdata>
{

public:
  IndicatorFactoryBurgers();
  virtual ~IndicatorFactoryBurgers() {};

}; // class IndicatorFactoryBurgers

} // namespace burgers

} // namespace canop

#endif // INDICATOR_FACTORY_BURGERS_H_
