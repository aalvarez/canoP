#include "IndicatorFactoryBurgers.h"
#include "utils.h" // for UNUSED macro

namespace canop {

namespace burgers {

// remember that Qdata is a concrete type defined in userdata_burgers.h
using qdata_t = Qdata;


// =======================================================
// =======================================================
double
indicator_burgers_khokhlov (qdata_t * cella, qdata_t * cellb,
			    indicator_info_t * info)
{
  UNUSED (info);
  double              U[2] = { cella->w.U, cellb->w.U };
  
  /* equivalent to indicator_U_gradient */
  return indicator_scalar_gradient (U[0], U[1]);
  
} // indicator_burgers_khokhlov

// =======================================================
// =======================================================
double
indicator_burgers_U_gradient (qdata_t * cella, qdata_t * cellb,
			      indicator_info_t * info)
{
  UNUSED (info);
  double              U[2] = { cella->w.U, cellb->w.U };

  return indicator_scalar_gradient (U[0], U[1]);
  
} // indicator_burgers_U_gradient

// =======================================================
// ======CLASS IndicatorFactoryBurgers IMPL ===============
// =======================================================

IndicatorFactoryBurgers::IndicatorFactoryBurgers() :
  IndicatorFactory<Qdata>()
{

  // register the above callback's
  registerIndicator("khokhlov"    , indicator_burgers_khokhlov);
  registerIndicator("U_gradient"  , indicator_burgers_U_gradient);
  
} // IndicatorFactoryBurgers::IndicatorFactoryBurgers

} // namespace burgers

} // namespace canop
