#ifndef INDICATOR_FACTORY_RAMSES_H_
#define INDICATOR_FACTORY_RAMSES_H_

#include "IndicatorFactory.h"

#include "ramses/userdata_ramses.h"

namespace canop {

namespace ramses {

/**
 * Concrete implement of an indicator factory for the Ramses scheme.
 *
 * Valid indicator names are "khokhlov" and "rho_gradient".
 * use method registerIndicator to add another one.
 */
class IndicatorFactoryRamses : public IndicatorFactory<Qdata>
{

public:
  IndicatorFactoryRamses();
  virtual ~IndicatorFactoryRamses() {};

}; // class IndicatorFactoryRamses

} // namespace ramses

} // namespace canop

#endif // INDICATOR_FACTORY_RAMSES_H_
