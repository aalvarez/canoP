#include "UserDataManagerRamses.h"
#include "canoP_base.h"
#include "eos.h"
#include "ScalarLimiterFactory.h"

namespace canop {

namespace ramses {

using ramses_getter_t = double (UserDataManagerRamses ::*) (p4est_quadrant_t * q);
using ramses_vgetter_t = void (UserDataManagerRamses ::*) (p4est_quadrant_t * q, size_t dim, double * val);

using scalar_limiter_t = ScalarLimiterFactory::scalar_limiter_t;

// ====================================================================
// ====================================================================
UserDataManagerRamses::UserDataManagerRamses(bool useP4estMemory,
					     RamsesPar &ramsesPar) :
  UserDataManager<UserDataTypesRamses>(),
  ramsesPar(ramsesPar)
{

  UNUSED(useP4estMemory);
  
} // UserDataManagerRamses::UserDataManagerRamses

// ====================================================================
// ====================================================================
UserDataManagerRamses::~UserDataManagerRamses()
{

} // UserDataManagerRamses::~UserDataManagerRamses

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::qdata_set_variables (qdata_t * data,
					    qdatavar_t * w)
{

  memcpy (&(data->w), w, sizeof (qdatavar_t));
  memcpy (&(data->wnext), w, sizeof (qdatavar_t));
  memset (&(data->delta), 0, P4EST_DIM * sizeof (qdatarecons_t));
  
} // UserDataManagerRamses::qdata_set_variables

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::quadrant_set_state_data (p4est_quadrant_t * dst,
						p4est_quadrant_t * src)
{

  qdata_t *data_dst = quadrant_get_qdata (dst);
  qdata_t *data_src = quadrant_get_qdata (src);

  memcpy (&(data_dst->w),     &(data_src->w),     sizeof (qdatavar_t));
  memcpy (&(data_dst->wnext), &(data_src->wnext), sizeof (qdatavar_t));
  memset (&(data_dst->delta), 0,      P4EST_DIM * sizeof (qdatarecons_t));

} // UserDataManagerRamses::quadrant_set_state_variables

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::quadrant_compute_mean(p4est_quadrant_t *qmean,
					     p4est_quadrant_t *quadout[P4EST_CHILDREN])
{

  qdata_t     *child_data;
  qdatavar_t   mean;
  memset (&mean, 0, sizeof (qdatavar_t));
    
  for (int8_t i = 0; i < P4EST_CHILDREN; ++i) {
    child_data = quadrant_get_qdata (quadout[i]);

    mean.rho += child_data->w.rho / P4EST_CHILDREN;
    mean.E_tot += child_data->w.E_tot / P4EST_CHILDREN;
    for (int j = 0; j < P4EST_DIM; ++j) {
      mean.rhoV[j] += child_data->w.rhoV[j] / P4EST_CHILDREN;
    }

  }

  // finally, copy all of that into quadin
  quadrant_set_variables (qmean, &mean);

  // placeholder
#ifdef USE_RAMSES_MHD
    // do something about mag field
#endif /*USE_RAMSES_MHD*/
      
} // quadrant_compute_mean

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::qdata_print_variables (int loglevel, qdata_t * data)
{
  CANOP_LOGF (loglevel, "rho    = %-15.8lf %.8lf\n",
              data->w.rho, data->wnext.rho);
  CANOP_LOGF (loglevel, "rhoV[0]    = %-15.8lf %.8lf\n",
              data->w.rhoV[0], data->wnext.rhoV[0]);
  CANOP_LOGF (loglevel, "rhoV[1]    = %-15.8lf %.8lf\n",
              data->w.rhoV[1], data->wnext.rhoV[1]);
#ifdef USE_3D
  CANOP_LOGF (loglevel, "rhoV[2]    = %-15.8lf %.8lf\n",
              data->w.rhoV[2], data->wnext.rhoV[2]);
#endif
  CANOP_LOGF (loglevel, "E_tot  = %-15.8lf %.8lf\n",
              data->w.E_tot, data->wnext.E_tot);

} // UserDataManagerRamses::qdata_print_variables

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::quadrant_copy_w_to_wnext (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  memcpy (&(data->wnext), &(data->w), sizeof (qdatavar_t));

} // UserDataManagerRamses::quadrant_copy_w_to_wnext

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::quadrant_copy_wnext_to_w (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  memcpy (&(data->w), &(data->wnext), sizeof (qdatavar_t));

} // UserDataManagerRamses::quadrant_copy_wnext_to_w

// ====================================================================
// ====================================================================
double
UserDataManagerRamses::qdata_get_rho (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.rho;
  
} // UserDataManagerRamses::qdata_get_rho

// ====================================================================
// ====================================================================
double
UserDataManagerRamses::qdata_get_E_tot (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.E_tot;
  
} // UserDataManagerRamses::qdata_get_E_tot
 
// ====================================================================
// ====================================================================
double
UserDataManagerRamses::qdata_get_rhoV (p4est_quadrant_t * q, int direction)
{
  qdata_t *data = quadrant_get_qdata (q);

  if (direction < P4EST_DIM) {
    return data->w.rhoV[direction];
  } else {
    return 0;
  }
  
} // UserDataManagerRamses::qdata_get_rhoV

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::qdata_vget_rhoV (p4est_quadrant_t * q, size_t dim, double * val)
{
  qdata_t *data = quadrant_get_qdata (q);

  for (size_t direction = 0; direction < dim; ++direction) {
    if (direction < P4EST_DIM) {
      val[direction] = data->w.rhoV[direction];
    } else {
      val[direction] = 0;
    }
  }

} // UserDataManagerRamses::qdata_vget_rhoV

// ====================================================================
// ====================================================================
double
UserDataManagerRamses::qdata_get_deltaX_rho (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->delta[0].rho;
  
} // UserDataManagerRamses::qdata_get_deltaX_rho

// ====================================================================
// ====================================================================
double
UserDataManagerRamses::qdata_get_deltaY_rho (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->delta[1].rho;
  
} // UserDataManagerRamses::qdata_get_deltaY_rho

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::qdata_reconstructed_zero(qdatarecons_t *q)
{

  q->rho = 0.0;
  for (int i=0; i<P4EST_DIM; i++)
    q->velocity[i] = 0.0;
  q->P = 0.0;
  q->c = 0.0;

} // UserDataManagerRamses::qdata_reconstructed_zero

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::qdata_reconstructed_copy(qdatarecons_t *q1, 
						const qdatarecons_t *q2)
{

  memcpy( q1, q2, sizeof(qdatarecons_t) );

} // UserDataManagerRamses::qdata_reconstructed_copy

// ====================================================================
// ====================================================================
void UserDataManagerRamses::qdata_reconstructed_swap(qdatarecons_t *q1, 
						     qdatarecons_t *q2)
{

  std::swap( q1->rho, q2->rho);
  for (int i=0; i<P4EST_DIM; i++)
    std::swap( q1->velocity[i] , q2->velocity[i]);
  std::swap(q1->P  , q2->P);
  std::swap(q1->c  , q2->c);

} // UserDataManagerRamses::qdata_reconstructed_swap

// ====================================================================
// ====================================================================
void UserDataManagerRamses::qdata_reconstructed_change_sign(qdatarecons_t *q)
{

  /* this is only usefull when q is a slope vector */

  q->rho = -q->rho;
  for (int i=0; i<P4EST_DIM; i++)
    q->velocity[i] = -q->velocity[i];
  q->P = -q->P;
  q->c = -q->c;

} // UserDataManagerRamses::qdata_reconstructed_change_sign

// ====================================================================
// ====================================================================
void UserDataManagerRamses::compute_delta(const qdatarecons_t w,const qdatarecons_t *wn,
					  qdatarecons_t *delta,
					  scalar_limiter_t limiter,
					  double dx, double dxl, double dxr)
{

  qdatarecons_t       wl, wr;
  
  /* then we compute the reconstucted gradient 
     on each pair (w_i, w, w_{i + 1}) */
  for (int i = 0; i < P4EST_HALF; ++i) {
    wl = wn[2 * i + 0];
    wr = wn[2 * i + 1];
    
    delta[i].rho = limiter ((w.rho  - wl.rho), 
			    (wr.rho - w.rho), 
			    dx, dxl, dxr);
    
    for (int j = 0; j < P4EST_DIM; ++j) {
      delta[i].velocity[j] = limiter ((w.velocity[j]  - wl.velocity[j]),
				      (wr.velocity[j] - w.velocity[j]),
				      dx, dxl, dxr);
    }
    delta[i].P = limiter ((w.P  - wl.P), 
			  (wr.P - w.P), 
			  dx, dxl, dxr);
    
  } // end reconstucted gradient
  
  /*
   * we now have reconstructed values on the lower half and upper half of
   * quadrant, but we need a reconstructed value on the whole cell, so we just
   * limit the 2 (or 4 in 3D) values we have using the same limiter.
   */
  for (int i = 1; i < P4EST_HALF; ++i) {

    delta[0].rho = limiter_minmod (delta[0].rho, delta[i].rho, 1, 1, 1);
    for (int j = 0; j < P4EST_DIM; ++j) {
      delta[0].velocity[j] = 
	limiter_minmod (delta[0].velocity[j], delta[i].velocity[j], 1, 1, 1);
    }
    delta[0].P   = limiter_minmod (delta[0].P, delta[i].P, 1, 1, 1);
    
  }
  
} // UserDataManagerRamses::compute_delta

// ====================================================================
// ====================================================================
void
UserDataManagerRamses::reconstruct_variables (const qdatavar_t &w,
					      qdatarecons_t &r)
{

  // density, velocity and pressure
  double gamma0 = ramsesPar.gamma0;
  double smallp = ramsesPar.smallp;

  // kinetic energy per mass unit
  double eken = 0.0;

  r.rho = w.rho;
  for (int i = 0; i < P4EST_DIM; ++i) {
    r.velocity[i] = w.rhoV[i] / w.rho;
    eken += r.velocity[i]*r.velocity[i];
  }
  eken *= 0.5;

  // internal energy = total energy - kinetic energy (per mass unit)
  double eint = w.E_tot / w.rho - eken;

  // use equation of state to compute pressure and local speed of sound
  eos(r.rho, eint, &(r.P), &(r.c), gamma0, smallp);

} // UserDataManagerRamses::reconstructed_variables


// ====================================================================
// ====================================================================
qdata_getter_t
UserDataManagerRamses::qdata_getter_byname (const std::string &varName)
{

  qdata_getter_t base_getter = nullptr;
  ramses_getter_t getter = nullptr;
  
  if (!varName.compare("rho")) {
    getter = &UserDataManagerRamses::qdata_get_rho;
  } else if (!varName.compare("E_tot")) {
    getter = &UserDataManagerRamses::qdata_get_E_tot;
  }

  // for debug
  if (!varName.compare("deltaX_rho")) {
    getter = &UserDataManagerRamses::qdata_get_deltaX_rho;
  } else if (!varName.compare("deltaY_rho")) {
    getter = &UserDataManagerRamses::qdata_get_deltaY_rho;
  }

  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_getter_t>(getter);

  } else {

    // print warning
    CANOP_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
    
  }
  
  return base_getter;
  
} // UserDataManagerRamses::qdata_getter_byname


// ====================================================================
// ====================================================================
qdata_vector_getter_t
UserDataManagerRamses::qdata_vgetter_byname (const std::string &varName)
{

  qdata_vector_getter_t base_getter = nullptr;
  ramses_vgetter_t getter = nullptr;

  if (!varName.compare("rhoV")) {
    getter = &UserDataManagerRamses::qdata_vget_rhoV;
  } else if (!varName.compare("velocity")) { // XXX: this is a bug, kept for backwards-compatibility
    getter = &UserDataManagerRamses::qdata_vget_rhoV;
  }

  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_vector_getter_t>(getter);

  } else {

    // print warning
    P4EST_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());

  }

  return base_getter;

} // UserDataManagerRamses::qdata_vgetter_byname


// ====================================================================
// ====================================================================
qdata_field_type
UserDataManagerRamses::qdata_type_byname(const std::string &varName)
{

  if (!varName.compare("rho") ||
      !varName.compare("E_tot") ||
      !varName.compare("deltaX_rho") ||
      !varName.compare("deltaY_rho"))
    return QDATA_FIELD_SCALAR;

  if (!varName.compare("velocity") ||
      !varName.compare("rhoV"))
    return QDATA_FIELD_VECTOR;

  // print warning
  P4EST_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
  return QDATA_FIELD_UNKNOWN;

} // UserDataManagerRamses::qdata_type_byname

} // namespace ramses

} // namespace canop
