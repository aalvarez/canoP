#ifndef BOUNDARY_CONDITIONS_FACTORY_RAMSES_H_
#define BOUNDARY_CONDITIONS_FACTORY_RAMSES_H_

#include "BoundaryConditionsFactory.h"
#include "ramses/userdata_ramses.h"

namespace canop {

namespace ramses {

/**
 *
 */
class BoundaryConditionsFactoryRamses : public BoundaryConditionsFactory<Qdata>
{

  using qdata_t = Qdata;
  
public:
  BoundaryConditionsFactoryRamses();
  virtual ~BoundaryConditionsFactoryRamses() {};
  
}; // class BoundaryConditionsFactoryRamses

} // namespace ramses

} // namespace canop

#endif // BOUNDARY_CONDITIONS_FACTORY_RAMSES_H_
