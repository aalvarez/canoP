#include "IndicatorFactoryRamses.h"
#include "utils.h" // for UNUSED macro


namespace canop {

namespace ramses {

// remember that Qdata is a concrete type defined in userdata_ramses.h
using qdata_t = Qdata;


// =======================================================
// =======================================================
double
indicator_ramses_khokhlov (qdata_t * cella, qdata_t * cellb,
			   indicator_info_t * info)
{
  UNUSED (info);
  double              rho[2] = { cella->w.rho, cellb->w.rho };
  
  /* equivalent to indicator_rho_gradient */
  return indicator_scalar_gradient (rho[0], rho[1]);
  
} // indicator_ramses_khokhlov

// =======================================================
// =======================================================
double
indicator_ramses_rho_gradient (qdata_t * cella, qdata_t * cellb,
			       indicator_info_t * info)
{
  UNUSED (info);
  double              rho[2] = {
    cella->w.rho, cellb->w.rho
  };

  return indicator_scalar_gradient (rho[0], rho[1]);
  
} // indicator_ramses_rho_gradient

// =======================================================
// ======CLASS IndicatorFactoryRamses IMPL ===============
// =======================================================

IndicatorFactoryRamses::IndicatorFactoryRamses() :
  IndicatorFactory<Qdata>()
{

  // register the above callback's
  registerIndicator("khokhlov"    , indicator_ramses_khokhlov);
  registerIndicator("rho_gradient", indicator_ramses_rho_gradient);
  
} // IndicatorFactoryRamses::IndicatorFactoryRamses

} // namespace ramses

} // namespace canop
