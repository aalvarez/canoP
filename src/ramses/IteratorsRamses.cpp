#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h"

#ifndef P4_TO_P8
#include <p4est.h>
#include <p4est_bits.h>
#include <p4est_mesh.h>
#include <p4est_geometry.h>
#else
#include <p8est.h>
#include <p8est_bits.h>
#include <p8est_mesh.h>
#include <p8est_geometry.h>
#endif

#include <sc.h> // for SC_LP_ERROR macro
#include <cmath>

#include "SolverRamses.h"
#include "SolverCoreRamses.h"
#include "SolverWrap.h"
#include "IteratorsRamses.h"
#include "UserDataManagerRamses.h"
#include "userdata_ramses.h"

#include "IndicatorFactoryRamses.h"
#include "RiemannSolverFactoryRamses.h"

namespace canop {

namespace ramses { 

// get userdata specific stuff
using qdata_t               = Qdata;
using qdata_variables_t     = QdataVar;
using qdata_reconstructed_t = QdataRecons;

using UserDataTypesRamses = UserDataTypes<Qdata,
                                          QdataVar,
                                          QdataRecons>;

using QuadrantNeighborUtilsRamses = QuadrantNeighborUtils<UserDataTypesRamses>;

using riemann_solver_t = RiemannSolverFactoryRamses::RiemannSolverCallback;

// =======================================================
// =======================================================
/**
 * \brief Test whether the current quad should be skipped.
 *
 * This routine handles quandrant skipping for subcycling. A quadrant is skipped if:
 *  * subcycling is enabled, and
 *  * the quadrant level is not equal to the current subcycling level
 */
static bool
should_skip_quad(SolverRamses *solver, p4est_quadrant_t *quad)
{
  if (solver->m_subcycle == nullptr)
    return false;

  else
    return (quad->level != solver->m_subcycle->level);
} // should_skip_quad

// =======================================================
// =======================================================
/**
 * \brief Test whether the current interface should be skipped.
 *
 * This routine ensures that the update with fluxes through each face is done
 * only once per relevant subcycle.
 *
 * For non-subcycling solvers, a face is skipped if:
 *  * the neighbour is not a ghost, and
 *  * the current cumulative index is greater than the neighbor cumulative
 *    index
 *
 * For subcycling solvers, a face is skipped if:
 *  * the current cell is not at the current subcycling level, or
 *  * the neighbor cell is not a ghost, and
 *    + the neighbor cell is finer than the current cell
 *    + the current and neighbor cells have the same level and the current
 *      cumulative index is greater than the neighbor cumulative index, or
 */
static bool
should_skip_face(SolverRamses *solver, bool neighbor_is_ghost, bool hanging_face,
                 p4est_locidx_t quadid_cum_current, p4est_locidx_t quadid_cum_neighbor,
                 p4est_quadrant_t *quad_current, p4est_quadrant_t *quad_neighbor)
{
  if (solver->m_subcycle == nullptr) {
    if (neighbor_is_ghost)
      return false;

    return (quadid_cum_current > quadid_cum_neighbor);
  }

  else {
    if (quad_current->level != solver->m_subcycle->level)
      return true;

    if (neighbor_is_ghost)
      return false;

    if (hanging_face)
      return true;

    return (quad_current->level == quad_neighbor->level &&
        quadid_cum_current > quadid_cum_neighbor);

  }
} // should_skip_face

// =======================================================
// =======================================================
static void iterator_compute_mean_values (p4est_iter_volume_info_t * info,
					  void *user_data)
{
  //qdata_variables_t  *mean = (qdata_variables_t *) user_data;

  double              dx = quadrant_length (info->quad);
  double              area = pow (dx, P4EST_DIM);
  //qdata_t            *data = quadrant_get_qdata (info->quad);

  UNUSED (user_data);
  //UNUSED (mean);
  UNUSED (dx);
  UNUSED (area);
  //UNUSED (data);
  
} // iterator_compute_mean_values

// =======================================================
// =======================================================
static void
iterator_gather_statistics (p4est_iter_volume_info_t * info,
			    void *user_data)
{
  UNUSED (user_data);
  UNUSED (info);

} // iterator_gather_statistics

// =======================================================
// =======================================================
static void
iterator_start_step (p4est_iter_volume_info_t * info,
		     void *user_data)
{
  UNUSED (user_data);
  // qdata_t            *data = quadrant_get_qdata (info->quad);
  // memcpy (&(data->wnext), &(data->w), sizeof (qdata_variables_t));

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverRamses *solver = static_cast<SolverRamses*>(solver_from_p4est (info->p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_w_to_wnext(info->quad);
  
} // iterator_start_step

// =======================================================
// =======================================================
static void iterator_end_step (p4est_iter_volume_info_t * info,
			       void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverRamses    *solver = static_cast<SolverRamses*>(solver_from_p4est (info->p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  if (!should_skip_quad(solver, info->quad))
    userdata_mgr->quadrant_copy_wnext_to_w(info->quad);
  
} // iterator_end_step

// =======================================================
// =======================================================
static void iterator_mark_adapt (p4est_iter_volume_info_t * info,
				 void *user_data)
{
  UNUSED (user_data);

  p4est_t            *p4est = info->p4est;
  Solver             *solver = solver_from_p4est (info->p4est);
  SolverWrap         *solverWrap = solver->m_wrap;
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  SolverRamses       *solverRamses = static_cast<SolverRamses *>(solver);
  SolverCoreRamses   *solverCore = static_cast<SolverCoreRamses *>(solver->m_solver_core);
  
  p4est_ghost_t      *ghost  = solver->get_ghost();
  p4est_mesh_t       *mesh   = solver->get_mesh();
  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());
  qdata_t            *qdata;
  int                 level = info->quad->level;

  int                 boundary = 0;
  int                 face = 0;
  double              eps = 0.0;
  double              eps_max = -1.0;

  p4est_quadrant_t   *neighbor = nullptr;
  qdata_t            *ndata = nullptr;

  //indicator_t         indicator = solver->m_indicator_fn;
  indicator_info_t    iinfo;
  iinfo.face = 0;
  iinfo.epsilon = solver->m_epsilon_refine;
  iinfo.quad_current = info->quad;
  iinfo.user_data = static_cast<void*>(&(solverRamses->ramsesPar));

  /* init face neighbor iterator */
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, p4est, ghost, mesh,
                                  info->treeid, info->quadid);

  /* loop over neighbors */
  neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, NULL, NULL);
  while (neighbor) {
    ndata = QuadrantNeighborUtilsRamses::mesh_face_neighbor_data (&mfn, ghost_data, &boundary);

    /* mfn.face is initialized to 0 by p4est_mesh_face_neighbor_init2 and
     * is incremented:
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has
     *    smaller or equal level
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has a
     *    bigger level, but only after seeing the 2 (or 4) smaller neighbors.
     */
    iinfo.face = mfn.face + (mfn.subface > 0) - 1;
    iinfo.quad_neighbor = neighbor;

    /* compute the indicator */
    qdata = userdata_mgr->quadrant_get_qdata (info->quad);
    eps = solverCore->m_indicator_fn (qdata, ndata, &iinfo);
    eps_max = SC_MAX (eps_max, eps);

    /* if the neighbor is on the boundary, the data was allocated on the fly,
     * so we have to deallocate it.
     */
    if (boundary == 1) {
      sc_mempool_free (info->p4est->user_data_pool, ndata);
    }

    neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, &face, NULL);

  } // end while. all neighbors visited.

  /* we suppose that epsilon_refine >= epsilon_coarsen */
  if (level > solver->m_min_refine && eps_max < solver->m_epsilon_coarsen) {
    solverWrap->mark_coarsen(info->treeid, info->quadid);
  } else if (level < solver->m_max_refine && eps_max > solver->m_epsilon_refine) {
    solverWrap->mark_refine(info->treeid, info->quadid);
  } else {
    return;
  }

} // iterator_mark_adapt

// =======================================================
// =======================================================
/**
 * \brief Compute the CFL condition.
 *
 * An iterator over all the cells to compute the maximum velocity.
 * The maximum is stored in the user_data.
 */
static void iterator_cfl_time_step (p4est_iter_volume_info_t * info,
				    void *user_data)
{

  UNUSED (user_data);

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  SolverRamses         *solver = static_cast<SolverRamses*>(solver_from_p4est (info->p4est));
  //SolverWrap           *solverWrap = solver->m_wrap;
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* config parameter handle */
  RamsesPar       &ramsesPar = solver->ramsesPar;

  p4est_geometry_t     *geom       = solver->get_geom_compute();
  p4est_connectivity_t *conn       = p4est->connectivity;

  /* current quad */
  p4est_topidx_t        treeid = info->treeid;
  p4est_quadrant_t     *quad = info->quad;

  p4est_ghost_t      *ghost = solver->get_ghost();
  p4est_mesh_t       *mesh = solver->get_mesh();
  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());

  double             *max_v_over_dx = (double *) user_data;
  //ConfigReader       *cfg = solver->m_cfg;

  qdata_t            *data = userdata_mgr->quadrant_get_qdata (info->quad);
  qdata_reconstructed_t w_prim;
  double              velocity=0.0;

  double              scale_factor = 1.0; // for Khokhlov subcycling

  UNUSED(ghost);
  UNUSED(mesh);
  UNUSED(ghost_data);

  /* also compute the min and max levels in the mesh */
  solver->m_minlevel = SC_MIN (solver->m_minlevel, quad->level);
  solver->m_maxlevel = SC_MAX (solver->m_maxlevel, quad->level);

  /* skip this quad if needed: initial loop to compute min and max levels, or
   * subcycling on a finer level */
  if (solver->m_subcycle != nullptr) {
    if (solver->m_subcycle->min_level == -1 ||
        solver->m_subcycle->level != quad->level)
      return;
  }

  /* set the scale factor if needed */
  if (solver->m_subcycle != nullptr)
    scale_factor = pow(2, solver->m_subcycle->max_level - quad->level);

  /* get the primitive variables in the current quadrant */
  userdata_mgr->reconstruct_variables (data->w, w_prim);
  
  for (int d = 0; d < P4EST_DIM; ++d) {
  
    velocity += w_prim.c + fabs(w_prim.velocity[d]);

  }
  
  /* update the global max */
  if (ramsesPar.cylindrical_enabled) {

    double xyz[3];
    quadrant_center_vertex(conn, geom, treeid, quad, xyz);
    double r=xyz[0]; //,theta=xyz[1],z=xyz[2];

    double dr,dtheta,dz;
    quadrant_get_dx_dy_dz(conn, geom, quad, treeid, &dr, &dtheta, &dz);

    double dx = SC_MIN(dr,r*dtheta);
    
#ifdef USE_3D
    dx = SC_MIN(dx,dz);
    /* *max_v_over_dx = SC_MAX (*max_v_over_dx,
			     fabs(w_prim.velocity[0])/dr +
			     fabs(w_prim.velocity[1])/(r*dtheta) + 
			     fabs(w_prim.velocity[2])/(dz) ); */
    *max_v_over_dx = SC_MAX (*max_v_over_dx,
			     scale_factor * velocity/dx);
#else
    /* the following is very instable */
    /* *max_v_over_dx = SC_MAX (*max_v_over_dx,
			     fabs(w_prim.velocity[0])/dr +
			     fabs(w_prim.velocity[1])/(r*dtheta) ); */
    *max_v_over_dx = SC_MAX (*max_v_over_dx,
			     scale_factor * velocity/dx);
#endif

  } else if (ramsesPar.cartesian_enabled) {

    double dx,dy,dz;
    dx = dy = dz = quadrant_length (info->quad);
    //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);

    *max_v_over_dx = SC_MAX (*max_v_over_dx, velocity/dx);

  } else if (ramsesPar.unstructured_enabled) {

    double dx,dy,dz;
    quadrant_get_dx_dy_dz(conn, geom, quad, treeid, &dx, &dy, &dz);
    
    UNUSED(dz);
    
    double dl = SC_MIN(dx,dy);
#ifdef USE_3D
    dl = SC_MIN(dl,dz);
#endif
    *max_v_over_dx = SC_MAX(*max_v_over_dx, scale_factor * velocity/dl);
    
  } 
  
} // iterator_cfl_time_step


// ====================================================================
// ====================================================================
/**
 * \brief Reconstruct the gradients for some variables.
 *
 * user_data contains the direction which can be NULL if it
 * should reconstruct the gradients in all directions.
 */
static void
iterator_reconstruct_gradients (p4est_iter_volume_info_t * info,
                                void *user_data)
{
  int                *direction = (int *) user_data;
  SolverRamses       *solver = static_cast<SolverRamses*>(solver_from_p4est (info->p4est));
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* config parameter handle */
  RamsesPar &ramsesPar = solver->ramsesPar;

  qdata_t            *data = NULL;
  p4est_quadrant_t   *quad = info->quad;
  p4est_topidx_t      treeid = info->treeid;
  p4est_locidx_t      quadid = info->quadid;

  int                 start = (direction == NULL ? 0 : *direction);
  int                 end = (direction == NULL ? P4EST_DIM : *direction + 1);

  if (should_skip_quad(solver, quad))
    return;

  for (int d = start; d < end; ++d) {
    switch (solver->m_space_order) {
    case 1:
      data = userdata_mgr->quadrant_get_qdata (quad);
      memset (&(data->delta[d]), 0, sizeof (qdata_reconstructed_t));
      break;
    case 2:
      if (ramsesPar.cylindrical_enabled) {
	// TODO //reconstruct_gradients_cylindrical (solver, quad, quadid, treeid, d);
      } else {
	QuadrantNeighborUtilsRamses::reconstruct_gradients (solver, quad, quadid, treeid, d);
      }
      break;
    default:
      SC_ABORTF ("Order %d not available.", solver->m_space_order);
      break;
    }
  }

  //quadrant_check_data (quad);

} // iterator_reconstruct_gradients

/**
 * Cartesian coordinates update.
 *
 * \param[in,out] data data from both sides
 * \param[in] face the face id of interface as seen from both sides
 * \param[in] dt   time step
 * \param[in] dS   interface surface (3D) or length (2D)
 * \param[in] dV   volume of both cells  
 * \param[in] solver solver structure to retrieve riemann solver routine
 *
 */
static void
ramses_update_face (qdata_t * data[2], 
		    int face[2], 
		    double dt, 
		    double dS,
		    double dV[2],
		    Solver * solver)
{

  SolverRamses *solverRamses = static_cast<SolverRamses *>(solver);
  riemann_solver_t riemann_solver_fn = solverRamses->m_riemann_solver_fn;
  RamsesPar &ramsesPar = solverRamses->ramsesPar;

  double dtdx[2] = { dt*dS/dV[CELL_CURRENT], 
		     dt*dS/dV[CELL_NEIGHBOR]};
    
  /* we will solver Riemann problem associated to the input face data */
  qdata_reconstructed_t qleft, qright;
  qdata_variables_t flux;

  double extra = 0;

  int dir = face[CELL_CURRENT]/2;

  /* retrieve qleft / qright for the given face direction */
  if (face[CELL_CURRENT] % 2 == 0) { // we have a left interface
      
    // interface right state is in wm of face[CELL_CURRENT]
    qright = data[CELL_CURRENT]->wp[dir];
    // interface left  state is in wp of face[CELL_NEIGHBOR]
    qleft  = data[CELL_NEIGHBOR]->wm[dir];
    
  } else { // face[CELL_CURRENT] % 2 is 1  // we have a right interface
    
    // interface right state is in wm of face[CELL_NEIGHBOR]
    qright = data[CELL_NEIGHBOR]->wp[dir];
    // interface left  state is in wp of face[CELL_CURRENT]
    qleft  = data[CELL_CURRENT]->wm[dir];
    
  }

  /*
   * solve riemann problem and update current cell
   */
  if (dir == IX) {

    if (ramsesPar.static_gravity_enabled) { // apply gravity predictor
      qleft.velocity[IX] += 0.5*dt*ramsesPar.static_gravity_x;
      qleft.velocity[IY] += 0.5*dt*ramsesPar.static_gravity_y;
#ifdef USE_3D
      qleft.velocity[IZ] += 0.5*dt*ramsesPar.static_gravity_z;
#endif

      qright.velocity[IX] += 0.5*dt*ramsesPar.static_gravity_x;
      qright.velocity[IY] += 0.5*dt*ramsesPar.static_gravity_y;
#ifdef USE_3D
      qright.velocity[IZ] += 0.5*dt*ramsesPar.static_gravity_z;
#endif
    }

    // solve riemann problem
    riemann_solver_fn(qleft, qright, &flux, ramsesPar, &extra);
    
    // update current cell
    if (face[CELL_CURRENT]%2 == 0) {
      data[CELL_CURRENT]->wnext.rho      += flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    += flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] += flux.rhoV[IX] * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IY] += flux.rhoV[IY] * dtdx[0];
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] += flux.rhoV[IZ] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      -= flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    -= flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] -= flux.rhoV[IX] * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] -= flux.rhoV[IY] * dtdx[1];
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] -= flux.rhoV[IZ] * dtdx[1];
#endif

    } else {
      data[CELL_CURRENT]->wnext.rho      -= flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    -= flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] -= flux.rhoV[IX] * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IY] -= flux.rhoV[IY] * dtdx[0];
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] -= flux.rhoV[IZ] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      += flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    += flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] += flux.rhoV[IX] * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] += flux.rhoV[IY] * dtdx[1];
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] += flux.rhoV[IZ] * dtdx[1];
#endif
    }

  } else if (dir == IY) {

    if (ramsesPar.static_gravity_enabled) { // apply gravity predictor
      qleft.velocity[IX] += 0.5*dt*ramsesPar.static_gravity_x;
      qleft.velocity[IY] += 0.5*dt*ramsesPar.static_gravity_y;
#ifdef USE_3D
      qleft.velocity[IZ] += 0.5*dt*ramsesPar.static_gravity_z;
#endif

      qright.velocity[IX] += 0.5*dt*ramsesPar.static_gravity_x;
      qright.velocity[IY] += 0.5*dt*ramsesPar.static_gravity_y;
#ifdef USE_3D
      qright.velocity[IZ] += 0.5*dt*ramsesPar.static_gravity_z;
#endif
    }

    // need to swap direction IX and IY before entering riemann solver
    std::swap( qleft.velocity[IX],  qleft.velocity[IY] );
    std::swap( qright.velocity[IX], qright.velocity[IY] );

    // solve riemann problem
    riemann_solver_fn(qleft, qright, &flux, ramsesPar, &extra);

    // update current cell: TAKE CARE direction IX and IY are swapped !
    if (face[CELL_CURRENT]%2 == 0) {
      data[CELL_CURRENT]->wnext.rho      += flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    += flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] += flux.rhoV[IY] * dtdx[0];  // !!!!
      data[CELL_CURRENT]->wnext.rhoV[IY] += flux.rhoV[IX] * dtdx[0];  // !!!!
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] += flux.rhoV[IZ] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      -= flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    -= flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] -= flux.rhoV[IY] * dtdx[1];  // !!!!
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] -= flux.rhoV[IX] * dtdx[1];  // !!!!
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] -= flux.rhoV[IZ] * dtdx[1];
#endif
    } else {
      data[CELL_CURRENT]->wnext.rho      -= flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    -= flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] -= flux.rhoV[IY] * dtdx[0];  // !!!!
      data[CELL_CURRENT]->wnext.rhoV[IY] -= flux.rhoV[IX] * dtdx[0];  // !!!!
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] -= flux.rhoV[IZ] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      += flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    += flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] += flux.rhoV[IY] * dtdx[1];  // !!!!
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] += flux.rhoV[IX] * dtdx[1];  // !!!!
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] += flux.rhoV[IZ] * dtdx[1];
#endif
    }
    
  } else if (dir == IZ) {
  
    if (ramsesPar.static_gravity_enabled) { // apply gravity predictor
      qleft.velocity[IX] += 0.5*dt*ramsesPar.static_gravity_x;
      qleft.velocity[IY] += 0.5*dt*ramsesPar.static_gravity_y;
      qleft.velocity[IZ] += 0.5*dt*ramsesPar.static_gravity_z;

      qright.velocity[IX] += 0.5*dt*ramsesPar.static_gravity_x;
      qright.velocity[IY] += 0.5*dt*ramsesPar.static_gravity_y;
      qright.velocity[IZ] += 0.5*dt*ramsesPar.static_gravity_z;
    }

    // need to swap direction IX and IZ before entering riemann solver
    std::swap( qleft.velocity[IX],  qleft.velocity[IZ] );
    std::swap( qright.velocity[IX], qright.velocity[IZ] );

    // solve riemann problem
    riemann_solver_fn(qleft, qright, &flux, ramsesPar, &extra);

    // update current cell: TAKE CARE direction IX and IY are swapped !
    if (face[CELL_CURRENT]%2 == 0) {
      data[CELL_CURRENT]->wnext.rho      += flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    += flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] += flux.rhoV[IZ] * dtdx[0];  // !!!!
      data[CELL_CURRENT]->wnext.rhoV[IY] += flux.rhoV[IY] * dtdx[0];  // !!!!
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] += flux.rhoV[IX] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      -= flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    -= flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] -= flux.rhoV[IZ] * dtdx[1];  // !!!!
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] -= flux.rhoV[IY] * dtdx[1];  // !!!!
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] -= flux.rhoV[IX] * dtdx[1];
#endif
    } else {
      data[CELL_CURRENT]->wnext.rho      -= flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    -= flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] -= flux.rhoV[IZ] * dtdx[0];  // !!!!
      data[CELL_CURRENT]->wnext.rhoV[IY] -= flux.rhoV[IY] * dtdx[0];  // !!!!
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] -= flux.rhoV[IX] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      += flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    += flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] += flux.rhoV[IZ] * dtdx[1];  // !!!!
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] += flux.rhoV[IY] * dtdx[1];  // !!!!
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] += flux.rhoV[IX] * dtdx[1];
#endif
    }

  } /* end direction */

} // ramses_update_face

/**
 * Cylindrical coordinates update.
 *
 * \param[in,out] data data from both sides
 * \param[in] face the face id of interface as seen from both sides
 * \param[in] dt   time step
 * \param[in] dS   interface surface (3D) or length (2D)
 * \param[in] dV   volume of both cells  
 * \param[in] solver solver structure to retrieve riemann solver routine
 * \param[in] pressure_c    pressure at cell center (current and neighbor)
 * \param[in] dr    radial size of cells (current and neighbor)
 *
 *
 * Take care that radial direction needs a special treatment of pressure in order to preserve
 * hydrostatic equilibrium.
 * See for example Dullemond:
 * http://www.ita.uni-heidelberg.de/~dullemond/lectures/num_fluid_2009/Chapter_10.pdf
 */
static void
ramses_update_face_cylindrical (qdata_t * data[2], 
				int face[2], 
				double dt, 
				double dS,
				double dV[2],
				Solver * solver,
				double pressure_c[2],
				double radius[2],
				double dr[2])
{

  UNUSED(pressure_c);
  
  SolverRamses *solverRamses = static_cast<SolverRamses *>(solver);
  riemann_solver_t riemann_solver_fn = solverRamses->m_riemann_solver_fn;
  RamsesPar &ramsesPar = solverRamses->ramsesPar;

  double dtdx[2] = { dt*dS/dV[CELL_CURRENT], 
		     dt*dS/dV[CELL_NEIGHBOR]};

  // pressure is used as input of the riemann solver to select coordinate
  // system and in output to retrieve star pressure for Rieman solver
  // star pressure not used for the radial direction !!! Use cell center pressure instead.
  double pressure = 1.0;
    
  /* we will solver Riemann problem associated to the input face data */
  qdata_reconstructed_t qleft, qright;
  qdata_variables_t flux;

  int dir = face[CELL_CURRENT]/2;

  /* retrieve qleft / qright for the given face direction */
  if (face[CELL_CURRENT] % 2 == 0) { // we have a left interface
      
    // interface right state is in wm of face[CELL_CURRENT]
    qright = data[CELL_CURRENT]->wp[dir];
    // interface left  state is in wp of face[CELL_NEIGHBOR]
    qleft  = data[CELL_NEIGHBOR]->wm[dir];
    
  } else { // face[CELL_CURRENT] % 2 is 1  // we have a right interface
    
    // interface right state is in wm of face[CELL_NEIGHBOR]
    qright = data[CELL_NEIGHBOR]->wp[dir];
    // interface left  state is in wp of face[CELL_CURRENT]
    qleft  = data[CELL_CURRENT]->wm[dir];
    
  }

  /*
   * solve riemann problem and update current cell
   */
  if (dir == IX) { // radial direction

    // solve riemann problem
    riemann_solver_fn(qleft, qright, &flux, ramsesPar, &pressure);
    
    // update current cell
    if (face[CELL_CURRENT]%2 == 0) {

      double radius_interface = radius[CELL_CURRENT] - 0.5*dr[CELL_CURRENT];
      
      data[CELL_CURRENT]->wnext.rho      += flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    += flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] += flux.rhoV[IX] * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IY] += flux.rhoV[IY] * dtdx[0] * radius_interface / radius[CELL_CURRENT];
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] += flux.rhoV[IZ] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      -= flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    -= flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] -= flux.rhoV[IX] * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] -= flux.rhoV[IY] * dtdx[1] * radius_interface / radius[CELL_NEIGHBOR];
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] -= flux.rhoV[IZ] * dtdx[1];
#endif

      // add pressure and geometrical terms for radial direction
      //data[CELL_CURRENT]->wnext.rhoV[IX]  += pressure_c[0] * dtdx[0];
      //data[CELL_NEIGHBOR]->wnext.rhoV[IX] -= pressure_c[1] * dtdx[1];
      data[CELL_CURRENT]->wnext.rhoV[IX]  += pressure * dt / dr[0];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] -= pressure * dt / dr[1];

    } else {

      double radius_interface = radius[CELL_CURRENT] + 0.5*dr[CELL_CURRENT];

      data[CELL_CURRENT]->wnext.rho      -= flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    -= flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] -= flux.rhoV[IX] * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IY] -= flux.rhoV[IY] * dtdx[0] * radius_interface / radius[CELL_CURRENT];
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] -= flux.rhoV[IZ] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      += flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    += flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] += flux.rhoV[IX] * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] += flux.rhoV[IY] * dtdx[1] * radius_interface / radius[CELL_NEIGHBOR];
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] += flux.rhoV[IZ] * dtdx[1];
#endif

      // add pressure and geometrical terms for radial direction
      //data[CELL_CURRENT]->wnext.rhoV[IX]  -= pressure_c[0] * dtdx[0];
      //data[CELL_NEIGHBOR]->wnext.rhoV[IX] += pressure_c[1] * dtdx[1];
      data[CELL_CURRENT]->wnext.rhoV[IX]  -= pressure * dt / dr[0];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] += pressure * dt / dr[1];
      
    }

  } else if (dir == IY) { // orthoradial direction

    // need to swap direction IX and IY before entering riemann solver
    std::swap( qleft.velocity[IX],  qleft.velocity[IY] );
    std::swap( qright.velocity[IX], qright.velocity[IY] );

    // solve riemann problem
    riemann_solver_fn(qleft, qright, &flux, ramsesPar, &pressure);

    // update current cell: TAKE CARE direction IX and IY are swapped !
    if (face[CELL_CURRENT]%2 == 0) {

      data[CELL_CURRENT]->wnext.rho      += flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    += flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] += flux.rhoV[IY] * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IY] += flux.rhoV[IX] * dtdx[0];  // !!!!
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] += flux.rhoV[IZ] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      -= flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    -= flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] -= flux.rhoV[IY] * dtdx[1];  // !!!!
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] -= flux.rhoV[IX] * dtdx[1];  // !!!!
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] -= flux.rhoV[IZ] * dtdx[1];
#endif

      // add pressure and geometrical terms for orthoradial direction
      data[CELL_CURRENT]->wnext.rhoV[IY]  += pressure * dtdx[0];
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] -= pressure * dtdx[1];

    } else {

      data[CELL_CURRENT]->wnext.rho      -= flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    -= flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] -= flux.rhoV[IY] * dtdx[0];  // !!!!
      data[CELL_CURRENT]->wnext.rhoV[IY] -= flux.rhoV[IX] * dtdx[0];  // !!!!
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] -= flux.rhoV[IZ] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      += flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    += flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] += flux.rhoV[IY] * dtdx[1];  // !!!!
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] += flux.rhoV[IX] * dtdx[1];  // !!!!
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] += flux.rhoV[IZ] * dtdx[1];
#endif

      // add pressure and geometrical terms for orthoradial direction
      data[CELL_CURRENT]->wnext.rhoV[IY]  -= pressure * dtdx[0];
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] += pressure * dtdx[1];

    }
    
  } else if (dir == IZ) {
  
    // need to swap direction IX and IZ before entering riemann solver
    std::swap( qleft.velocity[IX],  qleft.velocity[IZ] );
    std::swap( qright.velocity[IX], qright.velocity[IZ] );

    // solve riemann problem
    riemann_solver_fn(qleft, qright, &flux, ramsesPar, &pressure);

    // update current cell: TAKE CARE direction IX and IY are swapped !
    if (face[CELL_CURRENT]%2 == 0) {
      data[CELL_CURRENT]->wnext.rho      += flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    += flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] += flux.rhoV[IZ] * dtdx[0];  // !!!!
      data[CELL_CURRENT]->wnext.rhoV[IY] += flux.rhoV[IY] * dtdx[0];  // !!!!
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] += flux.rhoV[IX] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      -= flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    -= flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] -= flux.rhoV[IZ] * dtdx[1];  // !!!!
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] -= flux.rhoV[IY] * dtdx[1];  // !!!!
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] -= flux.rhoV[IX] * dtdx[1];

      // add pressure and geometrical terms for z direction
      data[CELL_CURRENT]->wnext.rhoV[IZ] += pressure * dtdx[CELL_CURRENT];
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ]-= pressure * dtdx[CELL_NEIGHBOR];
#endif

    } else {
      data[CELL_CURRENT]->wnext.rho      -= flux.rho      * dtdx[0];
      data[CELL_CURRENT]->wnext.E_tot    -= flux.E_tot    * dtdx[0];
      data[CELL_CURRENT]->wnext.rhoV[IX] -= flux.rhoV[IZ] * dtdx[0];  // !!!!
      data[CELL_CURRENT]->wnext.rhoV[IY] -= flux.rhoV[IY] * dtdx[0];  // !!!!
#ifdef USE_3D
      data[CELL_CURRENT]->wnext.rhoV[IZ] -= flux.rhoV[IX] * dtdx[0];
#endif

      data[CELL_NEIGHBOR]->wnext.rho      += flux.rho      * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.E_tot    += flux.E_tot    * dtdx[1];
      data[CELL_NEIGHBOR]->wnext.rhoV[IX] += flux.rhoV[IZ] * dtdx[1];  // !!!!
      data[CELL_NEIGHBOR]->wnext.rhoV[IY] += flux.rhoV[IY] * dtdx[1];  // !!!!
#ifdef USE_3D
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ] += flux.rhoV[IX] * dtdx[1];

      // add pressure and geometrical terms for z direction
      data[CELL_CURRENT]->wnext.rhoV[IZ] -= pressure * dtdx[CELL_CURRENT];
      data[CELL_NEIGHBOR]->wnext.rhoV[IZ]+= pressure * dtdx[CELL_NEIGHBOR];
#endif

    }

  } /* end direction */

} // ramses_update_face_cylindrical

// =======================================================
// =======================================================
/**
 * \brief Update the value in a quadrant using the Ramses scheme.
 *
 * This is were the Riemann solver enters into play.
 *
 * Update actually happens on both side of the interface:
 * - when morton index  (actually quadid) of current cell is smaller than 
 *   that of neighbor
 * - when current cell is regular and neighbor is ghost
 */
static void
iterator_ramses_update (p4est_iter_volume_info_t * info, 
			void *user_data)
{

  /* get forest structure */
  p4est_t              *p4est = info->p4est;
  p4est_connectivity_t *conn  = info->p4est->connectivity;
  
  /* get solver and geometry (only for unstructured) */
  SolverRamses       *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* config parameter handle */
  RamsesPar       &ramsesPar = solver->ramsesPar;

  
  p4est_geometry_t   *geom   = solver->get_geom_compute ();

  int                *direction = (int *) user_data;
  double              dx  , dy  , dz  ; // current  cell
  double              dx_n, dy_n, dz_n; // neighbor cell
  double              dx_min;
  //double              r, theta, z;

  double              xyz[3];
  double              xyz_n[3];
  
  p4est_quadrant_t   *quad   = info->quad;
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;
  p4est_tree_t       *tree = p4est_tree_array_index (p4est->trees, treeid);
  p4est_locidx_t      quadid_cum = tree->quadrants_offset + quadid;

  /* 2 states: one for each side of the face */
  qdata_t            *data[2] = { NULL, NULL };

  /* the face Id in range 0..P4EST_FACES-1 */
  int                 qf[2] = { -1, -1 };

  double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  /* geometrical terms */
  double              radius[2]; /* radius of current and neighbor cell */
  double              dr[2]; /* radial size */
  double              dS; /* interface surface (3D) or length (2D) */
  double              dV[2]; /* cells volumes (3D) or area (2D) */

  int                 dir_start = (direction == NULL ? 0 : *direction);
  int                 dir_end = (direction == NULL ? P4EST_DIM : *direction + 1);

  /* pressure at cell center (only needed by cylindrical scheme) */
  double              pressure_center[2];
  
  UNUSED (scale);
  UNUSED (dx_min);

  /* get current cell data */
  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (quad);

  /* current cell length / location */
  if (ramsesPar.cartesian_enabled) {

    //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
    dx = dy = dz = quadrant_length (info->quad);

  } else if (ramsesPar.cylindrical_enabled) {

    // when cylindrical, actually dx is dr, and dy is dtheta
    quadrant_get_dx_dy_dz(conn, geom, quad, treeid, &dx, &dy, &dz);
    quadrant_center_vertex (conn, geom, treeid, quad, xyz);

    radius[CELL_CURRENT] = xyz[0];
    dr[CELL_CURRENT] = dx;
    
  } else if (ramsesPar.unstructured_enabled) {
    
    quadrant_get_dx_dy_dz(conn, geom, quad, treeid, &dx, &dy, &dz);
    quadrant_center_vertex (conn, geom, treeid, quad, xyz);

  } 

  /* current cell volume */
#ifdef USE_3D
  dV[CELL_CURRENT] = dx*dy*dz;
#else
  dV[CELL_CURRENT] = dx*dy;
#endif

  if (ramsesPar.cylindrical_enabled) { // dr * r*dtheta * dz
#ifdef USE_3D
  dV[CELL_CURRENT] = dx*xyz[0]*dy*dz;
#else
  dV[CELL_CURRENT] = dx*xyz[0]*dy;
#endif
  }

  /* pressure at current cell center */
  {

    //config_reader_t    *cfg = solver->cfg;

    /* get the primitive variables in the current quadrant */
      qdata_reconstructed_t w_prim;
      userdata_mgr->reconstruct_variables (data[CELL_CURRENT]->w, w_prim);

      pressure_center[CELL_CURRENT] = w_prim.P;
  }

  
  /* loop over all quadrant's faces */
  for( int dir = dir_start; dir < dir_end; ++dir) {
  
    for (int face = 0; face < 2; ++face) {

      /* get the current face in the given direction */
      qf[CELL_CURRENT] = QuadrantNeighborUtilsBase::direction_faces[dir][face];      
      
      /* 
       * get the Qdata of the neighbors on this face 
       * It is important to realize here that, if face touches the outside
       * boundary, there will be a call to our boundary_fn callback
       * which will fill the wm, wp states
       */
      QuadrantNeighborUtilsRamses::quadrant_get_face_neighbor (solver,
							       quadid,
							       treeid,
							       qf[CELL_CURRENT],
							       &n);
      qf[CELL_NEIGHBOR] = n.face;

      /* 
       *
       */
      if (n.is_hanging) {

	/* half neighbors interface loop */
	for (int i = 0; i < P4EST_HALF; ++i) {
	  
	  /* get neighbor length / location */
	  if (ramsesPar.cartesian_enabled) {
	    
	    //quadrant_get_dx_dy_dz(conn, NULL, n.is.hanging.quad[i], n.treeid, &dx_n, &dy_n, &dz_n);
	    dx_n = dy_n = dz_n = dx*0.5;

	    dV[CELL_NEIGHBOR] = dx_n * dy_n;
#if USE_3D
	    dV[CELL_NEIGHBOR] *= dz_n;
#endif

	    /* common interface length is neighbor face area */
	    dS = quadrant_face_area(dx_n, dy_n, dz_n, dir);
	    
	  } else if (ramsesPar.cylindrical_enabled) {

	    // dx_n, dy_n, dz_n are actually dr,dtheta,dz of the neighbor cell
	    quadrant_get_dx_dy_dz(conn, geom, n.is.hanging.quad[i], n.treeid, &dx_n, &dy_n, &dz_n);
	    quadrant_center_vertex (conn, geom, n.treeid, n.is.hanging.quad[i], xyz_n);

	    // radius of cell center
	    radius[CELL_NEIGHBOR] = xyz_n[0];
	    
	    // radial size
	    dr[CELL_NEIGHBOR] = dx_n;
	    
	    // neighbor quad volume
	    dV[CELL_NEIGHBOR] = dx_n * xyz_n[0] * dy_n; // dr_n * r_n * dtheta_n
#if USE_3D
	    dV[CELL_NEIGHBOR] *= dz_n;
#endif

	    /* common interface length is neighbor face area */
#if USE_3D
	    if (dir == 0) { // radial neighbor, need to compute radius of common interface
	      
	      if (face == 0) { // lowest radius * dtheta_n
		dS = (xyz[0]-dx*0.5) * dy_n * dz_n;
	      } else { // highest radidus
		dS = (xyz[0]+dx*0.5) * dy_n * dz_n;
	      }
	      
	    } else if (dir == 1) { // orthoradial neighbor
	      dS = dx_n * dz_n;  // dr_n * dz_n
	    } else { // dir == 2 : z neighbor 
	      dS = xyz_n[0] * dy_n * dx_n; // r_n*dtheta_n * dr_n
	    }
#else
	    if (dir == 0) { // radial neighbor, need to compute radius of common interface
	      
	      if (face == 0) { // lowest radius * dtheta_n
		dS = (xyz[0]-dx*0.5)*dy_n;
	      } else { // highest radidus
		dS = (xyz[0]+dx*0.5)*dy_n;
	      }
	      
	    } else { // dir == 1 (orthoradial neighbor)
	      dS = dx_n;
	    }
#endif

	  } else if (ramsesPar.unstructured_enabled) {

	    // compute volume of neighbor cell
	    quadrant_cell_volume_general(conn, geom,
					 n.is.hanging.quad[i],
					 n.treeid);

	    //quadrant_get_dx_dy_dz(conn, geom, n.is.hanging.quad[i], n.treeid, &dx_n, &dy_n, &dz_n);
	    //quadrant_center_vertex (conn, geom, n.treeid, n.is.hanging.quad[i], xyz_n);

/* 	    dV[CELL_NEIGHBOR] = dx_n * dy_n; */
/* #if USE_3D */
/* 	    dV[CELL_NEIGHBOR] *= dz_n; */
/* #endif */
	    
	    /* common interface length is neighbor face area */
	    //dS = quadrant_face_area(dx_n, dy_n, dz_n, dir);
	    dS = quadrant_face_area_general(conn, geom, n.is.hanging.quad[i], n.treeid, 2*dir+face);
	    
	  }
	  
	  
	  /* perform update only when needed */
	  if (!should_skip_face(solver, n.is.hanging.is_ghost[i], true,
                                quadid_cum, n.is.hanging.quadid_cum[i],
                                quad, n.is.hanging.quad[i])) {

	    data[CELL_NEIGHBOR] = n.is.hanging.data[i];

	    /* pressure at neighbor cell center */
	    if (ramsesPar.cylindrical_enabled) {
	      
	      /* get the primitive variables in the current quadrant */
	      qdata_reconstructed_t w_prim;
	      userdata_mgr->reconstruct_variables (data[CELL_NEIGHBOR]->w, w_prim);
	      
	      pressure_center[CELL_NEIGHBOR] = w_prim.P;
	    }

	    /*
	     * solve the interface Riemann problem, and update both sides of the interface.
	     */
	    if (ramsesPar.cartesian_enabled) {
	      
	      ramses_update_face (data, qf, solver->m_dt, dS, dV, solver);

	    } else if (ramsesPar.cylindrical_enabled) {

	      ramses_update_face_cylindrical (data, qf, solver->m_dt, dS, dV, solver, pressure_center, radius, dr);

	    } else if (ramsesPar.unstructured_enabled) {

	      /* TODO */
	      /* TODO */
	      /* TODO */

	    }
	    
	  }
	  
	} /* end for i=0..P4EST_HELF */

      } else {

	/* full neighbor (same size or larger) */
	if (ramsesPar.cartesian_enabled) {
	  
	  //quadrant_get_dx_dy_dz(conn, NULL, n.is.full.quad, n.treeid, &dx_n, &dy_n, &dz_n);
	  if (n.level < info->quad->level)
	    dx_n = dy_n = dz_n = quadrant_length (info->quad)*2;
	  else
	    dx_n = dy_n = dz_n = quadrant_length (info->quad);


	  dV[CELL_NEIGHBOR] = dx_n * dy_n;
#if USE_3D
	  dV[CELL_NEIGHBOR] *= dz_n;
#endif
	  
	  /* common interface length is current face area */
	  dS = quadrant_face_area(dx, dy, dz, dir);

	} else if (ramsesPar.cylindrical_enabled) {

	  // dx_n, dy_n, dz_n are actually dr,dtheta,dz of the neighbor cell
	  quadrant_get_dx_dy_dz(conn, geom, n.is.full.quad, n.treeid, &dx_n, &dy_n, &dz_n);
	  quadrant_center_vertex (conn, geom, n.treeid, n.is.full.quad, xyz_n);
	  
	  // radius of cell center
	  radius[CELL_NEIGHBOR] = xyz_n[0];

	  // radial size
	  dr[CELL_NEIGHBOR] = dx_n;

	  // neighbor quad volume
	  dV[CELL_NEIGHBOR] = dx_n * xyz_n[0] * dy_n; // dr_n * r_n * dtheta_n
#if USE_3D
	  dV[CELL_NEIGHBOR] *= dz_n;
#endif

	  /* common interface length is current face area */
#if USE_3D
	  if (dir == 0) { // radial neighbor, need to compute radius of common interface
	    
	    if (face == 0) { // lowest radius * dtheta_n
	      dS = (xyz[0]-dx*0.5) * dy * dz;
	    } else { // highest radidus
	      dS = (xyz[0]+dx*0.5) * dy * dz;
	    }
	    
	  } else if (dir == 1) { // orthoradial neighbor
	    dS = dx * dz;  // dr * dz
	  } else { // dir == 2 : z neighbor 
	    dS = xyz[0] * dy * dx; // r_n*dtheta_n * dr_n
	  }
#else
	  if (dir == 0) { // radial neighbor, need to compute radius of common interface
	    
	    if (face == 0) { // lowest radius * dtheta_n
	      dS = (xyz[0]-dx*0.5)*dy;
	    } else { // highest radidus
	      dS = (xyz[0]+dx*0.5)*dy;
	    }
	    
	  } else { // dir == 1 (orthoradial neighbor)
	    dS = dx;
	  }
#endif
	} else if (ramsesPar.unstructured_enabled) {
	  
/* 	  quadrant_get_dx_dy_dz(conn, geom, n.is.full.quad, n.treeid, &dx_n, &dy_n, &dz_n); */
/* 	  quadrant_center_vertex (conn, geom, n.treeid, n.is.full.quad, xyz_n); */

/* 	  dV[CELL_NEIGHBOR] = dx_n * dy_n; */
/* #if USE_3D */
/* 	  dV[CELL_NEIGHBOR] *= dz_n; */
/* #endif */
	  
/* 	  /\* common interface length is current face area *\/ */
/* 	  dS = quadrant_face_area(dx, dy, dz, dir); */

	} 

	/* perform update only when needed */
        if (!should_skip_face(solver, n.is.full.is_ghost, false,
                              quadid_cum, n.is.full.quadid_cum,
                              quad, n.is.full.quad)) {
	  
	  data[CELL_NEIGHBOR] = n.is.full.data;
	  
	  /* pressure at neighbor cell center */
	  if (ramsesPar.cylindrical_enabled) {
	    
	    /* get the primitive variables in the current quadrant */
	    qdata_reconstructed_t w_prim;
	    userdata_mgr->reconstruct_variables (data[CELL_NEIGHBOR]->w, w_prim);
	    
	    pressure_center[CELL_NEIGHBOR] = w_prim.P;
	  }

	  /*
	   * solve the interface Riemann problem, and update both sides of the interface.
	   */
	  if (ramsesPar.cartesian_enabled) {

	    ramses_update_face (data, qf, solver->m_dt, dS, dV, solver);

	  } else if (ramsesPar.cylindrical_enabled) {
	    
	    ramses_update_face_cylindrical (data, qf, solver->m_dt, dS, dV, solver, pressure_center, radius, dr);
	    
	  } else if (ramsesPar.unstructured_enabled) {
	    
	    /* TODO */
	    /* TODO */
	    /* TODO */
	    
	  }
	
	}

      } // end if (n.is_hanging)
      
      /* 
       * if cell is on the outside boundary, we need to free the extra "ghost" 
       * quadrant
       */
      if (n.tree_boundary) {
	sc_mempool_free (info->p4est->user_data_pool, n.is.full.data);
      }

    } // end for face

  } // end for dir

} // iterator_ramses_update

// =======================================================
// =======================================================
/**
 * \brief Update the value in a quadrant with gravity source term.
 *
 */
static void
iterator_ramses_gravity_source_term (p4est_iter_volume_info_t * info, 
				     void *user_data)
{
  
  UNUSED (user_data);

  /* get forest structure */
  p4est_t            *p4est = info->p4est;
  
  /* get solver and geometry (only for unstructured) */
  SolverRamses       *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));

  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());

  /* config parameter handle */
  RamsesPar       &ramsesPar = solver->ramsesPar;
  
  p4est_geometry_t   *geom   = solver->get_geom_compute ();

  UNUSED (geom);

  double              dt = solver->m_dt;

  qdata_t            *data = userdata_mgr->quadrant_get_qdata (info->quad);

  if (should_skip_quad(solver, info->quad))
    return;

  // gravity source term
  if (ramsesPar.static_gravity_enabled) {
    double rhoOld = data->w.rho;
    double rhoNew = data->wnext.rho;
    data->wnext.rhoV[IX] += 0.5 * dt * ramsesPar.static_gravity_x * (rhoOld + rhoNew);
    data->wnext.rhoV[IY] += 0.5 * dt * ramsesPar.static_gravity_y * (rhoOld + rhoNew);
#ifdef USE_3D
    data->wnext.rhoV[IZ] += 0.5 * dt * ramsesPar.static_gravity_z * (rhoOld + rhoNew);
#endif /* USE_3D */
  }

} // iterator_ramses_gravity_source_term


// =======================================================
// =======================================================
/**
 * \brief Perform trace in a quadrant using the Ramses scheme.
 *
 * Trace use local slopes to compute interpolated value at faces from
 * cell-centered values, as well as a half time step update.
 *
 * As output, we fill for each quadrant qdata->wm and qdata->wp,
 * the reconstructed states on cell faces using MUSCL-Hancock approach.
 *
 * This is valid for both cartesian and cylindrical coordinates.
 *
 */
static void
iterator_ramses_trace (p4est_iter_volume_info_t * info,
		       void *user_data)
{
  UNUSED (user_data);

  p4est_t               *p4est = info->p4est;
  p4est_connectivity_t  *conn = p4est->connectivity;

  /* get solver and geometry (only for unstructured) */
  SolverRamses       *solver = static_cast<SolverRamses*>(solver_from_p4est (p4est));
  
  UserDataManagerRamses
    *userdata_mgr = static_cast<UserDataManagerRamses *> (solver->get_userdata_mgr());
  
  /* config parameter handle */
  RamsesPar       &ramsesPar = solver->ramsesPar;

  p4est_geometry_t      *geom = solver->get_geom_compute();

  p4est_topidx_t         treeid = info->treeid;
  p4est_quadrant_t      *quad = info->quad;
  qdata_t               *data = userdata_mgr->quadrant_get_qdata (info->quad);

  qdata_reconstructed_t  wc;             /* cell-centered primitive variables */
  qdata_reconstructed_t *wm[P4EST_DIM];  /* trace reconstructed states */
  qdata_reconstructed_t *wp[P4EST_DIM];  /* trace reconstructed states */

  qdata_reconstructed_t *delta[P4EST_DIM];

  double                 dt = solver->m_dt;

  double gamma  = ramsesPar.gamma0;
  double smallr = ramsesPar.smallr;
  
  double xyz_center[3];

  double r,p,u,v,w;
  double sr0, sp0, su0, sv0, sw0;
  double drx, dpx, dux, dvx, dwx;
  double dry, dpy, duy, dvy, dwy;
  double drz, dpz, duz, dvz, dwz;

  // default values for cartesian geometry
  double dx,dy,dz;
  double dx2,dy2,dz2;
  double dtdx, dtdy, dtdz;
  UNUSED(dz2);
  
  
  // for cylindrical
  double dr, dtheta;
  double radius, theta;
  UNUSED(theta);

  if (should_skip_quad(solver, quad))
    return;
  
  if (ramsesPar.cartesian_enabled) {

    /* get center vertex cartesian coordinates */
    quadrant_center_vertex(conn, NULL, info->treeid, info->quad, xyz_center);
    
    dx = dy = dz = quadrant_length (info->quad);
    //quadrant_get_dx_dy_dz(conn, NULL, quad, treeid, &dx, &dy, &dz);
    dx2 = dx*0.5;
    dy2 = dy*0.5;
    dz2 = dz*0.5;

  } if (ramsesPar.cylindrical_enabled) {

    /*
     * For cylindrical:
     * - dx is dr
     * - dy is dtheta
     * - dz
     */
    /* get center vertex cartesian coordinates */
    quadrant_center_vertex(conn, geom, info->treeid, info->quad, xyz_center);
    radius = xyz_center[0]; // use radius because "r" is already taken
    theta  = xyz_center[1];
    //z      = xyz_center[2];
    
    quadrant_get_dx_dy_dz(conn, geom, quad, treeid, &dr, &dtheta, &dz);

    dx = dr;
    dy = dtheta;
    
    dx2 = dx*0.5;
    dy2 = dy*0.5;
    dz2 = dz*0.5;
    
  } else if (ramsesPar.unstructured_enabled) {

    // we need to adapt dx,dy,dz to geometry

    /* get center vertex cartesian coordinates */
    quadrant_center_vertex(conn, geom, info->treeid, info->quad, xyz_center);

    quadrant_get_dx_dy_dz(conn, geom, quad, treeid, &dx, &dy, &dz);
    dx2 = dx*0.5;
    dy2 = dy*0.5;
    dz2 = dz*0.5;

  }

  dtdx = dt/dx;
  dtdy = dt/dy;
  dtdz = 0;
#ifdef USE_3D
  dtdz = dt/dz;
#endif
  
  /* get the primitive variables in the current quadrant */
  userdata_mgr->reconstruct_variables (data->w, wc);

  for (int dir = 0; dir < P4EST_DIM; ++dir) {
    wm[dir] = &(data->wm[dir]);
    wp[dir] = &(data->wp[dir]);

    /* retrieve slope */
    delta[dir] = &(data->delta[dir]); 

    /* initialize reconstructed states to 0 */
    memset (wm[dir],    0, sizeof (qdata_reconstructed_t));
    memset (wp[dir],    0, sizeof (qdata_reconstructed_t));

  } // end for dir

  /* retrieve primitive variables in current quadrant */
  r = wc.rho;
  p = wc.P;
  u = wc.velocity[IX];
  v = wc.velocity[IY];
#ifdef USE_3D
  w = wc.velocity[IZ];
#else
  w = 0.0;
#endif
  
  /* retrieve variations = dx * slopes  */
  drx = dx2*delta[IX]->rho;
  dpx = dx2*delta[IX]->P;
  dux = dx2*delta[IX]->velocity[IX];
  dvx = dx2*delta[IX]->velocity[IY];
#ifdef USE_3D
  dwx = dx2*delta[IX]->velocity[IZ];
#else
  dwx = 0.0;
#endif

  dry = dy2*delta[IY]->rho;
  dpy = dy2*delta[IY]->P;
  duy = dy2*delta[IY]->velocity[IX];
  dvy = dy2*delta[IY]->velocity[IY];
#ifdef USE_3D
  dwy = dy2*delta[IY]->velocity[IZ];
#else
  dwy = 0.0;
#endif

#ifdef USE_3D
  drz = dz2*delta[IZ]->rho;
  dpz = dz2*delta[IZ]->P;
  duz = dz2*delta[IZ]->velocity[IX];
  dvz = dz2*delta[IZ]->velocity[IY];
  dwz = dz2*delta[IZ]->velocity[IZ];
#else
  drz = 0.0;
  dpz = 0.0;
  duz = 0.0;
  dvz = 0.0;
  dwz = 0.0;
#endif

  if (ramsesPar.cartesian_enabled) {  
    
    /* source terms (with transverse derivatives) */
    sr0 = (-u*drx-dux*r      )*dtdx + (-v*dry-dvy*r      )*dtdy + (-w*drz-dwz*r      )*dtdz;
    su0 = (-u*dux-dpx/r      )*dtdx + (-v*duy            )*dtdy + (-w*duz            )*dtdz; 
    sv0 = (-u*dvx            )*dtdx + (-v*dvy-dpy/r      )*dtdy + (-w*dvz            )*dtdz;
    sw0 = (-u*dwx            )*dtdx + (-v*dwy            )*dtdy + (-w*dwz-dpz/r      )*dtdz; 
    sp0 = (-u*dpx-dux*gamma*p)*dtdx + (-v*dpy-dvy*gamma*p)*dtdy + (-w*dpz-dwz*gamma*p)*dtdz;

  } else if (ramsesPar.cylindrical_enabled) { // i.i. "cylindrical" connectivity
    
    double xc = radius; // distance of cell center to origin

    /* 
     * Take care that 
     * direction x is      radial
     * direction y is orthoradial
     *
     * u is      radial velocity
     * v is orthoradial velocity
     *
     */
    /*sr0 = (-u*drx-dux*r)              *dtdx + (-v*dry-dvy*r)              /xc*dtdy + (-w*drz-dwz*r)              *dtdz;
      su0 = (-u*dux-(dpx+B*dBx+C*dCx)/r)*dtdx + (-v*duy+B*dAy/r)            /xc*dtdy + (-w*duz+C*dAz/r)            *dtdz; 
      sv0 = (-u*dvx+A*dBx/r)            *dtdx + (-v*dvy-(dpy+A*dAy+C*dCy)/r)/xc*dtdy + (-w*dvz+C*dBz/r)            *dtdz;
      sw0 = (-u*dwx+A*dCx/r)            *dtdx + (-v*dwy+B*dCy/r)            /xc*dtdy + (-w*dwz-(dpz+A*dAz+B*dBz)/r)*dtdz; 
      sp0 = (-u*dpx-dux*gamma*p)        *dtdx + (-v*dpy-dvy*gamma*p)        /xc*dtdy + (-w*dpz-dwz*gamma*p)        *dtdz;*/

    sr0 = (-u*drx-dux*r)   *dtdx
      +   (-v*dry-dvy*r)/xc*dtdy
      +   (-w*drz-dwz*r)   *dtdz;
    
    su0 = (-u*dux-dpx/r)   *dtdx
      +   (-v*duy      )/xc*dtdy
      +   (-w*duz      )   *dtdz; 

    sv0 = (-u*dvx      )   *dtdx
      +   (-v*dvy-dpy/r)/xc*dtdy
      +   (-w*dvz      )   *dtdz;

    sw0 = (-u*dwx      )   *dtdx
      +   (-v*dwy      )/xc*dtdy
      +   (-w*dwz-dpz/r)   *dtdz; 

    sp0 = (-u*dpx-dux*gamma*p)   *dtdx
      +   (-v*dpy-dvy*gamma*p)/xc*dtdy
      +   (-w*dpz-dwz*gamma*p)   *dtdz;

    // Geometrical terms
    sr0 = sr0 - ( r*u)/xc*0.5*dt;
    su0 = su0 + ( v*v)/xc*0.5*dt;
    sv0 = sv0 + (-u*v)/xc*0.5*dt;
    sp0 = sp0 - gamma*p*u   /xc*0.5*dt;
        
  } else if (ramsesPar.unstructured_enabled) { 

    /* TODO */
    /* TODO */
    /* TODO */
    
  }

  /* Update in time the  primitive variables */
  r = r + sr0;
  u = u + su0; // if cylindrical this is      radial dir
  v = v + sv0; // if cylindrical this is orthoradial dir
  w = w + sw0;
  p = p + sp0;

  /* Right state at left interface */
  wp[0]->rho          = r - drx;
  wp[0]->P            = p - dpx;
  wp[0]->velocity[IX] = u - dux;
  wp[0]->velocity[IY] = v - dvx;
#ifdef USE_3D
  wp[0]->velocity[IZ] = w - dwx;
#endif
  wp[0]->rho = fmax(smallr, wp[0]->rho);

  /* Left state at right interface */
  wm[0]->rho          = r + drx;
  wm[0]->P            = p + dpx;
  wm[0]->velocity[IX] = u + dux;
  wm[0]->velocity[IY] = v + dvx;
#ifdef USE_3D
  wm[0]->velocity[IZ] = w + dwx;
#endif
  wm[0]->rho = fmax(smallr, wm[0]->rho);
  
  /* Top state at bottom interface */
  wp[1]->rho          = r - dry;
  wp[1]->P            = p - dpy;
  wp[1]->velocity[IX] = u - duy;
  wp[1]->velocity[IY] = v - dvy;
#ifdef USE_3D
  wp[1]->velocity[IZ] = w - dwy;
#endif
  wp[1]->rho = fmax(smallr, wp[1]->rho);
  
  // Bottom state at top interface
  wm[1]->rho          = r + dry;
  wm[1]->P            = p + dpy;
  wm[1]->velocity[IX] = u + duy;
  wm[1]->velocity[IY] = v + dvy;
#ifdef USE_3D
  wm[1]->velocity[IZ] = w + dwy;
#endif
  wm[1]->rho = fmax(smallr, wm[1]->rho);
  
#ifdef USE_3D
  // Back state at front interface
  wp[2]->rho          = r - drz;
  wp[2]->P            = p - dpz;
  wp[2]->velocity[IX] = u - duz;
  wp[2]->velocity[IY] = v - dvz;
  wp[2]->velocity[IZ] = w - dwz;
  wp[2]->rho = fmax(smallr, wp[2]->rho);
  
  // Front state at back interface
  wm[2]->rho          = r + drz;
  wm[2]->P            = p + dpz;
  wm[2]->velocity[IX] = u + duz;
  wm[2]->velocity[IY] = v + dvz;
  wm[2]->velocity[IZ] = w + dwz;
  wm[2]->rho = fmax(smallr, wm[2]->rho);
#endif
 
} // iterator_ramses_trace

// =======================================================
// =======================================================

// =======================================================
// =======================================================

// =======================================================
// =======================================================
IteratorsRamses::IteratorsRamses() : IteratorsBase() {

  this->fill_iterators_list();
  
} // IteratorsRamses::IteratorsRamses

// =======================================================
// =======================================================
IteratorsRamses::~IteratorsRamses()
{
  
} // IteratorsRamses::~IteratorsRamses

// =======================================================
// =======================================================
void IteratorsRamses::fill_iterators_list()
{

  iteratorsList[IT_ID::COMPUTE_MEAN_VALUES] = iterator_compute_mean_values;
  iteratorsList[IT_ID::GATHER_STATISTICS]   = iterator_gather_statistics;
  iteratorsList[IT_ID::START_STEP]          = iterator_start_step;
  iteratorsList[IT_ID::END_STEP]            = iterator_end_step;
  iteratorsList[IT_ID::MARK_ADAPT]          = iterator_mark_adapt;
  iteratorsList[IT_ID::CFL_TIME_STEP]       = iterator_cfl_time_step;
  iteratorsList[IT_ID::RECONSTRUCT_GRADIENT]= iterator_reconstruct_gradients;
  iteratorsList[IT_ID::UPDATE]              = iterator_ramses_update;
  iteratorsList[IT_ID::TRACE]               = iterator_ramses_trace;
  iteratorsList[IT_ID::GRAVITY_SOURCE_TERM] = iterator_ramses_gravity_source_term;
  
} // IteratorsRamses::fill_iterators_list

} // namespace ramses

} // namespace canop
