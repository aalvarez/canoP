#include "IndicatorFactoryAdvectionUpwind.h"
#include "utils.h" // for UNUSED macro

namespace canop {

namespace advection_upwind {

// needs a concrete qdata_t implementation
using qdata_t = Qdata;

// =======================================================
// =======================================================
double
indicator_advection_upwind_rho_gradient (qdata_t * cella, qdata_t * cellb,
					 indicator_info_t * info)
{
  
  UNUSED (info);
  double              rho[2] = {
    cella->w.rho, cellb->w.rho
  };
  
  return indicator_scalar_gradient (rho[0], rho[1]);
  
} // indicator_advection_upwind_rho_gradient

// =======================================================
// ======CLASS IndicatorFactoryAdvectionUpwind IMPL ======
// =======================================================

IndicatorFactoryAdvectionUpwind::IndicatorFactoryAdvectionUpwind() :
  IndicatorFactory<Qdata>()
{

  // register the above callback's
  // for upwind khokhlov and rho gradient are identical
  // keep the name in the map for compatibility with old canoP
  // parameter file.
  registerIndicator("khokhlov"    , indicator_advection_upwind_rho_gradient);
  registerIndicator("rho_gradient", indicator_advection_upwind_rho_gradient);
  
} // IndicatorFactoryAdvectionUpwind::IndicatorFactoryAdvectionUpwind

} // namespace canop

} // namespace advection_upwind
