#include "SolverCoreAdvectionUpwind.h"

#include "InitialConditionsFactoryAdvectionUpwind.h"
#include "BoundaryConditionsFactoryAdvectionUpwind.h"
#include "IndicatorFactoryAdvectionUpwind.h"

#include "ConfigReader.h"

namespace canop {

namespace advection_upwind {

// =======================================================
// =======================================================
SolverCoreAdvectionUpwind::SolverCoreAdvectionUpwind(ConfigReader *cfg) :
  SolverCore<Qdata>(cfg)
{

  /*
   * Initial condition callback setup.
   */
  InitialConditionsFactoryAdvectionUpwind IC_Factory;
  m_init_fn = IC_Factory.callback_byname (m_init_name);

  /*
   * Boundary condition callback setup.
   */
  BoundaryConditionsFactoryAdvectionUpwind BC_Factory;
  m_boundary_fn = BC_Factory.callback_byname (m_boundary_name);

  /*
   * Refine/Coarsen callback setup.
   */
  IndicatorFactoryAdvectionUpwind indic_Factory;
  m_indicator_fn = indic_Factory.callback_byname (m_indicator_name);

  /*
   * Geometric refine callback setup.
   */
  GeometricIndicatorFactory geom_indic_Factory;
  m_geom_indicator_fn = geom_indic_Factory.callback_byname (m_geom_indicator_name);

  
} // SolverCoreAdvectionUpwind::SolverCoreAdvectionUpwind


// =======================================================
// =======================================================
SolverCoreAdvectionUpwind::~SolverCoreAdvectionUpwind()
{
} // SolverCoreAdvectionUpwind::~SolverCoreAdvectionUpwind

} // namespace canop

} // namespace advection_upwind
