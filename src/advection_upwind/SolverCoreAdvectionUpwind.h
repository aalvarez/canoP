#ifndef SOLVER_CORE_ADVECTION_UPWIND_H_
#define SOLVER_CORE_ADVECTION_UPWIND_H_

#include "SolverCore.h"

#include "userdata_advection_upwind.h"

class ConfigReader;

namespace canop {

namespace advection_upwind {

/**
 * This is were Initial and Border Conditions callbacks are setup.
 */
class SolverCoreAdvectionUpwind : public SolverCore<Qdata>
{

public:
  SolverCoreAdvectionUpwind(ConfigReader *cfg);
  ~SolverCoreAdvectionUpwind();

  /**
   * Static creation method called by the solver factory.
   */
  static SolverCoreBase* create(ConfigReader *cfg)
  {
    return new SolverCoreAdvectionUpwind(cfg);
  }
  
}; // SolverCoreAdvectionUpwind

} // namespace canop

} // namespace advection_upwind

#endif // SOLVER_CORE_ADVECTION_UPWIND_H_
