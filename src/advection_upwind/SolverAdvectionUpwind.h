#ifndef SOLVER_ADVECTIONUPWIND_H_
#define SOLVER_ADVECTIONUPWIND_H_

#include "Solver.h"
#include "userdata_advection_upwind.h"

namespace canop {

namespace advection_upwind {

class SolverAdvectionUpwind : public Solver
{

  using qdata_t = Qdata;
  
public:
  SolverAdvectionUpwind(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory);
  virtual ~SolverAdvectionUpwind();
  
  /**
   * override base class method.
   */
  void read_config();
  
  /**
   * Here we implement numerical scheme for AdvectionUpwind application.
   */
  void next_iteration_impl();

  /**
   * Compute CFL condition, override base class method.
   */
  void compute_time_step();

  /**
   * Static creation method called by the solver factory.
   */
  static Solver* create(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory)
  {
    return new SolverAdvectionUpwind(cfg, mpicomm, useP4estMemory);
  }

  /**
   * advection velocity field.
   */
  vector_init_t        m_velocity_field_fn;  
  
private:
  /**
   * Compute CFL condition, specialized version when using unstructured mesh.
   */
  void compute_time_step_upwind_unstructured();

  /**
   * Apply numerical scheme in a directionnally split way.
   */
  void single_directional(int direction);

  /**
   * Next iteration for unstructured mesh.
   */
  void unstructured_update();
  
}; // class SolverAdvectionUpwind

} // namespace canop

} // namespace advection_upwind

#endif // SOLVER_ADVECTIONUPWIND_H_
