#include "InitialConditionsFactoryAdvectionUpwind.h"

#include "canoP_base.h"
#include "SolverAdvectionUpwind.h"
#include "UserDataManagerAdvectionUpwind.h"
#include "quadrant_utils.h"

#include "advection_upwind/userdata_advection_upwind.h"

namespace canop {

namespace advection_upwind {

using qdata_t               = Qdata; 
using qdata_variables_t     = QdataVar; 
using qdata_reconstructed_t = QdataRecons;

/* defines the level of void in the SOD-like test cases */
static const double        lambda = 1.0e-7;

// =======================================================
/*
 * rho init
 */
// =======================================================

/**************************************************************************
 *                            SCALAR DISK
 * _straight: using one of the velocities above
 * _rotating: using the rotating velocity
 **************************************************************************/
static double
disk_rho (double x, double y, double z, double t)
{
  UNUSED (z);
  UNUSED (t);
  
  double              r = SC_SQR (x - 0.2) + SC_SQR (y - 0.5) +
#ifdef USE_3D
    SC_SQR (z - 0.5) +
#endif
    0;

  return (sqrt (r) < 0.1 ? 1.0 : 0.0);
}

static double UNUSED_FUNCTION
uniform_zero (double x, double y, double z, double t)
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

  return 0.0;
}

static double UNUSED_FUNCTION
uniform_05 (double x, double y, double z, double t)
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

  return 0.5;
}

static double
uniform_1 (double x, double y, double z, double t)
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

  return 1.0;
}

static void
gaussian_velocity_main (double x, double y, double z, double u[P4EST_DIM]);

double
gaussian_scalar (double x, double y, double z, double t)
{
  double              u[3];
  gaussian_velocity_main (x, y, z, u);

  double              cx = (0.5 + u[0] * t);
  double              cy = (0.5 + u[1] * t);
#ifdef USE_3D
  double              cz = (0.5 + u[2] * t);
#endif

  double              r = sqrt (fmin (pow (x - (cx - floor (cx + 0.5)), 2),
                                      pow (x - cx + floor (cx - 0.5), 2)) +
                                fmin (pow (y - (cy - floor (cy + 0.5)), 2),
                                      pow (y - cy + floor (cy - 0.5), 2)) +
#ifdef USE_3D
                                fmin (pow (z - (cz - floor (cz + 0.5)), 2),
                                      pow (z - cz + floor (cz - 0.5), 2)) +
#endif
                                0);

  return (r > 0.3 ? lambda : lambda + (1.0-lambda) * pow (cos (M_PI * r / 0.6), 4));
}

static double
star_rho(double x, double y, double z, double t)
{
  UNUSED (z);
  UNUSED (t);

  double left = 1.0 / 3.0 - fabs (x - 0.5);
  double right = 2.0 / 3.0  - 4.0 * fabs (x - 0.5);
  double left2 = 1.0 / 3.0 + fabs (x - 0.5);

  return (((left < y && y < right) || (left2 < y && y < 0.5))
#ifdef USE_3D
      && (0.4 < z && z < 0.6)
#endif
      ? 1.0 : 0.0);
}

static double
gaussian_rho (double x, double y, double z, double t)
{
  return gaussian_scalar (x, y, z, t);
}

static double
vortex_rho (double x, double y, double z, double t)
{
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

  return (x > 0.45 || x < 0.1 ? 0 : 1);
}

// =======================================================
/*
 * velocity init.
 */
// =======================================================
static void
gaussian_velocity_main (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  u[0] = 1.0;
  u[1] = 1.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

void
horizontal_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  u[0] = 1.0;
  u[1] = 0.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

void
vertical_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  u[0] = 0.0;
  u[1] = 1.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

void
zero_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  u[0] = 0.0;
  u[1] = 0.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

void
diagonal_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  u[0] = 1.0;
  u[1] = 1.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

void
rotating_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (z);

  u[0] = -(y - 0.5);
  u[1] = x - 0.5;
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

void
ring_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (z);

  u[0] = -y;
  u[1] = x;
  /* u[0] = x; */
  /* u[1] = y; */
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

void
rotating_center_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (z);

  const double        beta = 5;
  const double        x0 = 0.5;
  const double        y0 = 0.5;

  double              r = sqrt (SC_SQR (x - x0) + SC_SQR (y - y0) +
#ifdef USE_3D
                                SC_SQR (z - x0) +
#endif
                                0);

  u[0] = -beta * exp (-r / 0.5) * (y - y0);
  u[1] = beta * exp (-r / 0.5) * (x - x0);
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

vector_init_t
init_velocity_field_byname (const std::string &name)
{

  if ( !name.compare ("uniform") ) {
    return horizontal_velocity;
  }

  if ( !name.compare ("advection_gaussian") ) {

    return diagonal_velocity;

  } else if ( !name.compare ("advection_disk") ) {

    return horizontal_velocity;

  } else if ( !name.compare ("advection_disk_rotating") ) {

    return rotating_velocity;

  } else if ( !name.compare ("advection_disk_ring") ) {
    
    return ring_velocity;
    
  } else if ( !name.compare ("advection_star") ) {
    
    return diagonal_velocity;
    
  } else if ( !name.compare ("advection_vortex") ) {
    
    return rotating_center_velocity;
    
  } else {

    CANOP_GLOBAL_INFOF ("No velocity field specified for \"%s\".\n", name.c_str());
    return nullptr;
    
  }

} // init_velocity_field_byname

// =======================================================
// =======================================================
static void
init_me (p4est_t * p4est,
	 p4est_topidx_t which_tree,
	 p4est_quadrant_t * quad,
         scalar_init_t init_rho,
         vector_init_t init_velocity)
{
  UNUSED (init_rho);
  UNUSED (init_velocity);

  Solver *solver = solver_from_p4est (p4est);

  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());
  
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  p4est_connectivity_t *conn = p4est->connectivity;
  
  /* get the coordinates of the center of the quad */
  double  X[3] = { 0, 0, 0 };

  quadrant_center_vertex (conn, geom, which_tree, quad, X);

  w.rho = init_rho (X[0], X[1], X[2], 0);
  init_velocity (X[0], X[1], X[2], w.velocity);
    
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_me

vector_init_t
init_velocity_field_byname (const char *name)
{

  if (strcmp (name, "uniform") == 0) {
    return horizontal_velocity;
  }

  if (strcmp (name, "advection_gaussian") == 0) {
    return diagonal_velocity;
  }
  else if (strcmp (name, "advection_disk") == 0) {
    return horizontal_velocity;
  }
  else if (strcmp (name, "advection_disk_rotating") == 0) {
    return rotating_velocity;
  }
  else if (strcmp (name, "advection_disk_ring") == 0) {
    return ring_velocity;
  }
  else if (strcmp (name, "advection_star") == 0) {
    return diagonal_velocity;
  }
  else if (strcmp (name, "advection_vortex") == 0) {
    return rotating_center_velocity;
  } else {
    CANOP_GLOBAL_INFOF ("No velocity field specified for \"%s\".\n", name);
    return NULL;
  }

} // init_velocity_field_byname

// =======================================================
// =======INITIAL CONDITIONS CALLBACKS====================
// =======================================================
void
init_uniform_scalar (p4est_t * p4est,
		     p4est_topidx_t which_tree,
                     p4est_quadrant_t * quad)
{
  init_me (p4est, which_tree, quad,
           uniform_1, horizontal_velocity);
}

void
init_gaussian (p4est_t * p4est,
	       p4est_topidx_t which_tree,
               p4est_quadrant_t * quad)
{
  init_me (p4est, which_tree, quad,
           gaussian_rho, diagonal_velocity);
}

void
init_disk_rotating (p4est_t * p4est,
                    p4est_topidx_t which_tree,
		    p4est_quadrant_t * quad)
{
  init_me (p4est, which_tree, quad,
           disk_rho, rotating_velocity);
}

void
init_disk_ring (p4est_t * p4est,
		p4est_topidx_t which_tree,
		p4est_quadrant_t * quad)
{
  init_me (p4est, which_tree, quad,
           disk_rho, ring_velocity);
}

void
init_disk_straight (p4est_t * p4est,
                    p4est_topidx_t which_tree,
		    p4est_quadrant_t * quad)
{
  init_me (p4est, which_tree, quad,
           disk_rho, horizontal_velocity);
}

void
init_star (p4est_t * p4est,
           p4est_topidx_t which_tree,
	   p4est_quadrant_t * quad)
{
  init_me (p4est, which_tree, quad,
           star_rho, diagonal_velocity);
}

void
init_vortex (p4est_t * p4est,
             p4est_topidx_t which_tree,
	     p4est_quadrant_t * quad)
{
  init_me (p4est, which_tree, quad,
           vortex_rho, rotating_center_velocity);
}


// ================================================================
// =======CLASS InitialConditionsFactoryAdvectionupwind IMPL ======
// ================================================================
InitialConditionsFactoryAdvectionUpwind::InitialConditionsFactoryAdvectionUpwind() :
  InitialConditionsFactory()
{

  // register the above callback's
  registerIC("uniform"                 , init_uniform_scalar);
  registerIC("advection_gaussian"      , init_gaussian);
  registerIC("advection_disk"          , init_disk_straight);
  registerIC("advection_disk_rotating" , init_disk_rotating);
  registerIC("advection_disk_ring"     , init_disk_ring);
  registerIC("advection_star"          , init_star);
  registerIC("advection_vortex"        , init_vortex);
  
} // InitialConditionsFactoryAdvectionUpwind

} // namespace canop

} // namespace advection_upwind
