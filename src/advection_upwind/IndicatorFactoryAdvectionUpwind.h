#ifndef INDICATOR_FACTORY_ADVECTION_UPWIND_H_
#define INDICATOR_FACTORY_ADVECTION_UPWIND_H_

#include "IndicatorFactory.h"

#include <advection_upwind/userdata_advection_upwind.h>

namespace canop {

namespace advection_upwind {

/**
 * Concrete implement of an indicator factory for the Advection Upwind scheme.
 *
 * Valid indicator names are "khokhlov" and "rho_gradient".
 * use method registerIndicator to add another one.
 */
class IndicatorFactoryAdvectionUpwind : public IndicatorFactory<Qdata>
{

public:
  IndicatorFactoryAdvectionUpwind();
  virtual ~IndicatorFactoryAdvectionUpwind() {};

}; // class IndicatorFactoryAdvectionUpwind

} // namespace canop

} // namespace advection_upwind

#endif // INDICATOR_FACTORY_ADVECTION_UPWIND_H_
