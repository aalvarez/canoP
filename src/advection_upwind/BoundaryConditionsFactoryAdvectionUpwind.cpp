#include "BoundaryConditionsFactoryAdvectionUpwind.h"

#include "SolverAdvectionUpwind.h"
#include "UserDataManagerAdvectionUpwind.h"
#include "quadrant_utils.h"

// remember that qdata_t is here a concrete type defined in userdata_advection_upwind.h

#include "advection_upwind/userdata_advection_upwind.h"
//#include "advection_upwind/scheme_advection_upwind_utils.h"
#include "ConfigReader.h"

namespace canop {

namespace advection_upwind {

using qdata_t               = Qdata;
using qdata_reconstructed_t = QdataRecons;

// =======================================================
// ======BOUNDARY CONDITIONS CALLBACKS====================
// =======================================================


// =======================================================
// =======================================================
/*
 * TODO: maybe just make the boundary functions return a qdata_variables_t
 * that they have initialized and then we do the copying around.
 *
 * TODO: for order 2, how does delta have to be initialized? does it?
 */
void
bc_advection_upwind_dirichlet (p4est_t * p4est,
			       p4est_topidx_t which_tree,
			       p4est_quadrant_t * q,
			       int face,
			       qdata_t * ghost_qdata)
{

  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (q);
  UNUSED (face);
  UNUSED (ghost_qdata);

} // bc_advection_upwind_dirichlet

// =======================================================
// =======================================================
void
bc_advection_upwind_neuman (p4est_t * p4est,
			    p4est_topidx_t which_tree,
			    p4est_quadrant_t * q,
			    int face,
			    qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);
  UNUSED (face);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est (p4est));

  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());

  qdata_t *qdata = userdata_mgr->quadrant_get_qdata (q);

  /* 
   * copy the entire qdata is necessary for advection_upwind scheme
   * so that we have the slope and MUSCL-Hancock extrapoled states
   * and can perform the trace operation
   */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

} // bc_advection_upwind_neuman

// =======================================================
// =======================================================
void
bc_advection_upwind_reflective (p4est_t * p4est,
				p4est_topidx_t which_tree,
				p4est_quadrant_t * q,
				int face,
				qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  /* get face normal direction */
  int                 direction = face / 2;

  /* get current cell data */
  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est (p4est));
  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());
  qdata_t *qdata = userdata_mgr->quadrant_get_qdata (q);

  /* copy current into the ghost outside-boundary cell */
  userdata_mgr->qdata_copy(ghost_qdata, qdata);

  /* reverse the normal velocity */
  ghost_qdata->w.velocity[direction] = -ghost_qdata->w.velocity[direction];
  ghost_qdata->wnext.velocity[direction] = -ghost_qdata->wnext.velocity[direction];

} // bc_advection_upwind_reflexive

// =======================================================
// =======================================================
void
bc_advection_upwind_open_tube (p4est_t * p4est,
			       p4est_topidx_t which_tree,
			       p4est_quadrant_t * q,
			       int face,
			       qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  if (face == 0 || face == 1) {
    bc_advection_upwind_neuman (p4est, which_tree, q, face, ghost_qdata);
  }
  else {
    bc_advection_upwind_reflective (p4est, which_tree, q, face, ghost_qdata);
  }
} // bc_advection_upwind_open_tube

// =======================================================
// =======================================================
void
bc_advection_upwind_open_box (p4est_t * p4est,
			      p4est_topidx_t which_tree,
			      p4est_quadrant_t * q,
			      int face,
			      qdata_t * ghost_qdata)
{
  UNUSED (p4est);
  UNUSED (which_tree);

  if (face == 3) {
    bc_advection_upwind_neuman (p4est, which_tree, q, face, ghost_qdata);
  }
  else {
    bc_advection_upwind_reflective (p4est, which_tree, q, face, ghost_qdata);
  }
} // bc_advection_upwind_open_box

// =================================================================
// ======CLASS BoundaryConditionsFactoryAdvection_Upwind IMPL ======
// =================================================================
BoundaryConditionsFactoryAdvectionUpwind::BoundaryConditionsFactoryAdvectionUpwind() :
  BoundaryConditionsFactory<Qdata>()
{

  // register the above callback's
  registerBoundaryConditions("dirichlet" , bc_advection_upwind_dirichlet);
  registerBoundaryConditions("neuman"    , bc_advection_upwind_neuman);
  registerBoundaryConditions("reflective", bc_advection_upwind_reflective);
  registerBoundaryConditions("open_tube" , bc_advection_upwind_open_tube);
  registerBoundaryConditions("open_box"  , bc_advection_upwind_open_box);


} // BoundaryConditionsFactoryAdvectionUpwind::BoundaryConditionsFactoryAdvectionUpwind

} // namespace canop

} // namespace advection_upwind
