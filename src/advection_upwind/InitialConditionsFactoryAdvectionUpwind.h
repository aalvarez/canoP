#ifndef INITIAL_CONDITIONS_FACTORY_ADVECTION_UPWIND_H_
#define INITIAL_CONDITIONS_FACTORY_ADVECTION_UPWIND_H_

#include "InitialConditionsFactory.h"
#include "advection_upwind/userdata_advection_upwind.h"

namespace canop {

namespace advection_upwind {

vector_init_t
init_velocity_field_byname (const std::string &name);

/**
 *
 */
class InitialConditionsFactoryAdvectionUpwind : public InitialConditionsFactory
{

  using qdata_t = Qdata;
  
public:
  InitialConditionsFactoryAdvectionUpwind();
  virtual ~InitialConditionsFactoryAdvectionUpwind() {};
  
}; // class InitialConditionsFactoryAdvectionUpwind

} // namespace canop

} // namespace advection_upwind

#endif // INITIAL_CONDITIONS_FACTORY_ADVECTION_UPWIND_H_
