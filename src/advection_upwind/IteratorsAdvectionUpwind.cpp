#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_mesh.h>
#include <p4est_geometry.h>
#else
#include <p8est_bits.h>
#include <p8est_mesh.h>
#include <p8est_geometry.h>
#endif

#include <sc.h> // for SC_LP_ERROR macro
#include <cmath>

#include "SolverAdvectionUpwind.h"
#include "SolverCoreAdvectionUpwind.h"
#include "SolverWrap.h"
#include "IteratorsAdvectionUpwind.h"
#include "UserDataManagerAdvectionUpwind.h"
#include "userdata_advection_upwind.h"

#include "IndicatorFactoryAdvectionUpwind.h"

namespace canop {

namespace advection_upwind {

// get userdata specific stuff
using qdata_t               = Qdata;
using qdata_variables_t     = QdataVar;
using qdata_reconstructed_t = QdataRecons;

using UserDataTypesAdvectionUpwind =
  UserDataTypes<Qdata,
		QdataVar,
		QdataRecons>;

using QuadrantNeighborUtilsAdvectionUpwind =
  QuadrantNeighborUtils<UserDataTypesAdvectionUpwind>;

constexpr int IT_IDD::CFL_TIME_STEP_UNSTRUCTURED;
constexpr int IT_IDD::UPDATE_UNSTRUCTURED;

// =======================================================
// =======================================================
static void iterator_compute_mean_values (p4est_iter_volume_info_t * info,
					  void *user_data)
{

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est (info->p4est));

  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());

  
  qdata_variables_t  *mean = (qdata_variables_t *) user_data;

  double              dx = quadrant_length (info->quad);
  double              area = pow (dx, P4EST_DIM);
  qdata_t            *qdata = userdata_mgr->quadrant_get_qdata (info->quad);

  mean->rho += area * qdata->w.rho;
  
} // iterator_compute_mean_values

// =======================================================
// =======================================================
static void iterator_gather_statistics (p4est_iter_volume_info_t * info,
					void *user_data)
{
  UNUSED (user_data);
  UNUSED (info);
  
  // p4est_t            *p4est = info->p4est;
  // SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est(p4est));

  // UserDataManagerAdvectionUpwind
  //   *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());

  // p4est_geometry_t   *geom_compute = solver->get_geom_compute();

  // scalar_init_t       alpha_exact_fn = init_exact_byname (solver->init_name);
  // double              alpha_exact = 0.0;

  // p4est_quadrant_t   *quad = info->quad;
  // double              dx = quadrant_length (quad);
  // double              area = pow (dx, P4EST_DIM);

  // qdata_t            *data = quadrant_get_data (quad);

  // double              alpha = data->w.rho;

  // if (alpha_exact_fn == nullptr) {
  //   return;
  // }

  // // get the coordinates of the center of the quad
  // double              X[3] = { 0, 0, 0 };
  // quadrant_center_vertex (p4est->connectivity, geom_compute, info->treeid, quad, X);

  // /* compute the two error norms, weighted by the area of the quadrant */
  // alpha_exact = alpha_exact_fn (X[0], X[1], X[2], solver->t);

  // solver->stats->norml1 += area * fabs (alpha - alpha_exact);
  
  // solver->stats->norml2 += area * SC_SQR (alpha - alpha_exact);

  // solver->minlevel = SC_MIN (solver->minlevel, info->quad->level);
  // solver->maxlevel = SC_MAX (solver->maxlevel, info->quad->level);
  
} // iterator_gather_statistics

// =======================================================
// =======================================================
static void iterator_start_step (p4est_iter_volume_info_t * info,
				 void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est (info->p4est));

  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_w_to_wnext(info->quad);
  
} // iterator_start_step

// =======================================================
// =======================================================
static void iterator_end_step (p4est_iter_volume_info_t * info,
			       void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est (info->p4est));

  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_wnext_to_w(info->quad);
  
} // iterator_end_step

// =======================================================
// =======================================================
static void iterator_mark_adapt (p4est_iter_volume_info_t * info,
				 void *user_data)
{
  UNUSED (user_data);

  p4est_t            *p4est = info->p4est;
  Solver             *solver = solver_from_p4est (p4est);
  SolverWrap         *solverWrap = solver->m_wrap;

  UserDataManagerAdvectionUpwind *userdata_mgr =
    static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());
  
  SolverCoreAdvectionUpwind *solverCore =
    static_cast<SolverCoreAdvectionUpwind *>(solver->m_solver_core);

  p4est_ghost_t      *ghost  = solver->get_ghost();
  p4est_mesh_t       *mesh   = solver->get_mesh();
  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());
  qdata_t            *qdata;
  int                 level = info->quad->level;

  int                 boundary = 0;
  int                 face = 0;
  double              eps = 0.0;
  double              eps_max = -1.0;

  p4est_quadrant_t   *neighbor = nullptr;
  qdata_t            *ndata = nullptr;

  //indicator_t         indicator = solver->m_indicator_fn;
  indicator_info_t    iinfo;
  iinfo.face = 0;
  iinfo.epsilon = solver->m_epsilon_refine;
  iinfo.quad_current = info->quad;
  iinfo.user_data = nullptr;

  /* init face neighbor iterator */
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, p4est, ghost, mesh,
                                  info->treeid, info->quadid);

  /* loop over neighbors */
  neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, NULL, NULL);
  while (neighbor) {
    ndata = QuadrantNeighborUtilsAdvectionUpwind::mesh_face_neighbor_data (&mfn, ghost_data, &boundary);

    /* mfn.face is initialized to 0 by p4est_mesh_face_neighbor_init2 and
     * is incremented:
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has
     *    smaller or equal level
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has a
     *    bigger level, but only after seeing the 2 (or 4) smaller neighbors.
     */
    iinfo.face = mfn.face + (mfn.subface > 0) - 1;
    iinfo.quad_neighbor = neighbor;

    /* compute the indicator */
    qdata = userdata_mgr->quadrant_get_qdata (info->quad);
    eps = solverCore->m_indicator_fn (qdata, ndata, &iinfo);
    eps_max = SC_MAX (eps_max, eps);

    /* if the neighbor is on the boundary, the data was allocated on the fly,
     * so we have to deallocate it.
     */
    if (boundary == 1) {
      sc_mempool_free (info->p4est->user_data_pool, ndata);
    }

    neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, &face, NULL);

  } // end while. all neighbors visited.

  /* we suppose that epsilon_refine >= epsilon_coarsen */
  if (level > solver->m_min_refine && eps_max < solver->m_epsilon_coarsen) {
    solverWrap->mark_coarsen(info->treeid, info->quadid);
  } else if (level < solver->m_max_refine && eps_max > solver->m_epsilon_refine) {
    solverWrap->mark_refine(info->treeid, info->quadid);
  } else {
    return;
  }

} // iterator_mark_adapt

// =======================================================
// =======================================================
static upwind_variables_t
upwind_variables_init (qdata_t * data, int face, double dx, double dt)
{
  upwind_variables_t  w;
  qdata_reconstructed_t delta = data->delta[face / 2];

  /*
   * delta contains the second order gradient reconstruction that should be 0
   * when doing a simple first order approximation.
   */
  w.n = 2.0 * (face % 2 == 1) - 1.0;
  w.velocity = data->w.velocity[face / 2];
  // MUSCL in space
  // w.rho = data->w.rho + 0.5 * w.n * dx * delta.rho;
  // MUSCL Hancock reconstruction
  w.rho = data->w.rho + 0.5 * w.n * dx * delta.rho - 0.5*w.velocity*dt*delta.rho ;

  return w;
  
} // upwind_variables_init

// =======================================================
// =======================================================
static upwind_variables_t
upwind_variables_init_unstructured (qdata_t * data,
				    double normal_vector[3],
				    double dtdx)
{
  UNUSED(dtdx);
  
  upwind_variables_t  w;
  //qdata_reconstructed_t delta = data->delta[face / 2];

  /*
   * delta contains the second order gradient reconstruction that should be 0
   * when doing a simple first order approximation.
   * 
   * TODO : NOT IMPLEMENTED
   */

  /*
   * project velocity vector along face normal, 
   * normal_vector is a unit vector
   */
  w.velocity =
    data->w.velocity[0]*normal_vector[0] +
    data->w.velocity[1]*normal_vector[1] +
    data->w.velocity[2]*normal_vector[2];
  
  // MUSCL in space
  // w.rho = data->w.rho + 0.5 * w.n * dx * delta.rho;
  // MUSCL Hancock reconstruction
  w.rho = data->w.rho;

  return w;
  
} // upwind_variables_init_unstructured

// =======================================================
// =======================================================
static void
upwind_update_face (qdata_t * data[2],
		    int face[2],
		    double dt,
		    double dx,
                    double scale)
{

  upwind_variables_t  flux;
  upwind_variables_t  w  = upwind_variables_init (data[0], face[0], dx, dt);
  upwind_variables_t  wn = upwind_variables_init (data[1], face[1], dx * scale,dt);

  /* compute the edge velocity and flux for rho */
  flux.velocity = w.n * (w.velocity + wn.velocity) / 2.0;
  flux.rho = (flux.velocity > 0 ? w.rho : wn.rho);

  /* add the flux to the cell */
  dx *= pow (SC_MAX (1.0 / scale, 1.0), P4EST_DIM - 1);
  data[0]->wnext.rho -= flux.velocity * dt / dx * flux.rho;

} // upwind_update_face

// =======================================================
// =======================================================
static void
upwind_update_face_unstructured (qdata_t * data[2],
				 double normal_vector[3],
				 double dt,
				 double dS,
				 double dV[2],
				 int neighbor_is_smaller)
{
  UNUSED(neighbor_is_smaller);
  
  upwind_variables_t  flux;

  double dtdx[2] = { dt*dS/dV[CELL_CURRENT], 
		     dt*dS/dV[CELL_NEIGHBOR]};

  upwind_variables_t  w = upwind_variables_init_unstructured (data[0],
							      normal_vector,
							      dtdx[0]);
  upwind_variables_t  wn = upwind_variables_init_unstructured (data[1],
							       normal_vector,
							       dtdx[1]);
  
  /* compute the edge velocity and flux for rho */
  flux.velocity = (w.velocity + wn.velocity) / 2.0;
  flux.rho = (flux.velocity > 0 ? w.rho : wn.rho);

  /* add the flux to the cell */
  //dtdx /= pow (SC_MAX (1.0 / scale, 1.0), P4EST_DIM - 1);
  data[0]->wnext.rho -= flux.velocity * dtdx[0] * flux.rho;
    
} // upwind_update_face_unstructured

// =======================================================
// =======================================================
static void
iterator_upwind_update (p4est_iter_volume_info_t * info,
			void *user_data)
{

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est (info->p4est));

  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());

  int                 direction = *(int *) user_data;
  double              dx = quadrant_length (info->quad);
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  qdata_t            *data[2] = { nullptr, nullptr };
  int                 qf[2] = { -1, -1 };
  double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  data[0] = userdata_mgr->quadrant_get_qdata (info->quad);
  
  for (int face = 0; face < 2; ++face) {
    /* get the current face in the given direction */
    qf[0] = QuadrantNeighborUtilsBase::direction_faces[direction][face];

    /* get the qdata_t of the neighbors on this face */
    QuadrantNeighborUtilsAdvectionUpwind::quadrant_get_face_neighbor (solver,
								      quadid,
								      treeid,
								      qf[0],
								      &n);
    qf[1] = n.face;

    scale = pow (2, info->quad->level - n.level);
    if (n.is_hanging) {
      /* half neighbor */
      for (int i = 0; i < P4EST_HALF; ++i) {
        data[1] = n.is.hanging.data[i];
        upwind_update_face (data, qf, solver->m_dt, dx, scale);
      }
    }
    else {
      /* full neighbor */
      data[1] = n.is.full.data;
      upwind_update_face (data, qf, solver->m_dt, dx, scale);
    }
  }
  
} // iterator_upwind_update

// =======================================================
// =======================================================
static void
iterator_upwind_update_unstructured (p4est_iter_volume_info_t * info,
				     void *user_data)
{
  UNUSED(user_data);
  
  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est (info->p4est));

  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());

  p4est_connectivity_t *conn = info->p4est->connectivity;
  
  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t   *geom = solver->get_geom_compute ();

  p4est_quadrant_t    *quad = info->quad; /* current cell's quadrant */
  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  //double              dx = quadrant_length (quad);

  qdata_t            *data[2] = { NULL, NULL };
  //int                 qf[2] = { -1, -1 };
  //double              scale = 0.0;
  quadrant_neighbor_t<qdata_t> n;

  double              dS; /* interface surface */
  double              dV[2]; /* cells volumes (current and neighbor) */

  data[CELL_CURRENT] = userdata_mgr->quadrant_get_qdata (info->quad);
  dV[CELL_CURRENT] = quadrant_cell_volume_general(conn, geom, quad, treeid);
  
  for (int face = 0; face < P4EST_FACES; ++face) {

    /* get face area */
    dS = quadrant_face_area_general(conn, geom, quad, treeid, face);

    /* get the current face in the given direction */
    //qf[CELL_CURRENT] = direction_faces[direction][face];

    /* get face normal vector */
    double normal_vector[3];
    quadrant_face_normal(conn, geom, quad, treeid, face, normal_vector);
    
    /* make sure the normal vector is outgoing for current cell */
    {
      
      /* center cell coordinates */
      double XYZc[3];
      quadrant_center_vertex (conn, geom, treeid, quad, XYZc);

      /* face center coordinates */
      double XYZf[3];
      quadrant_face_center (conn, geom, treeid, quad, (face_id_t) face, XYZf);

      double dcf[3] = {XYZf[0] - XYZc[0],
		       XYZf[1] - XYZc[1],
		       XYZf[2] - XYZc[2]};

      double dotProd =
	normal_vector[0] * dcf[0] +
	normal_vector[1] * dcf[1] +
	normal_vector[2] * dcf[2];

      if (dotProd < 0) { /* the normal is not ougoing, so inverse */
      	normal_vector[0] = -normal_vector[0];
      	normal_vector[1] = -normal_vector[1];
      	normal_vector[2] = -normal_vector[2];
      }

      {
	/* restore velocity field */
	double u[P4EST_DIM];
	solver->m_velocity_field_fn(XYZc[0],XYZc[1],XYZc[2],u);
	data[0]->w.velocity[0] = u[0];
	data[0]->w.velocity[1] = u[1];
#ifdef USE_3D
	data[0]->w.velocity[2] = 0.0; /* TO MODIFY for icosahedron */
#endif
      }
      
    }

    /* get the qdata_t of the neighbors on this face */
    QuadrantNeighborUtilsAdvectionUpwind::quadrant_get_face_neighbor (solver,
								      quadid,
								      treeid,
								      face,
								      &n);
    //qf[CELL_NEIGHBOR] = n.face;
    
    //scale = pow (2, info->quad->level - n.level);
    if (n.is_hanging) {
      /* half neighbor */

      /* need to rescale face area */
      dS /= P4EST_HALF;
      
      for (int i = 0; i < P4EST_HALF; ++i) {

	// compute volume of neighbor cell
	dV[CELL_NEIGHBOR] = quadrant_cell_volume_general(conn, geom, n.is.hanging.quad[i], n.treeid);
	  
	//dS = quadrant_face_area_general(conn, geom, n.is.hanging.quad[i], n.treeid, n.face);

	data[CELL_NEIGHBOR] = n.is.hanging.data[i];
	{
	  double XYZc[3];
	  quadrant_face_center (conn, geom, n.treeid, n.is.hanging.quad[i], (face_id_t) n.face, XYZc);
	  double u[P4EST_DIM];
	  solver->m_velocity_field_fn(XYZc[0],XYZc[1],XYZc[2],u);

	  
	  data[1]->w.velocity[0] = u[0];
	  data[1]->w.velocity[1] = u[1];
#ifdef USE_3D
	  data[1]->w.velocity[2] = 0.0; /* TO MODIFY for icosahedron */
#endif
	}

        upwind_update_face_unstructured (data, normal_vector, solver->m_dt, dS, dV, 1);
      }
    }
    else {
      /* full neighbor */
      dV[CELL_NEIGHBOR] = quadrant_cell_volume_general(conn, geom, n.is.full.quad, n.treeid);
      data[CELL_NEIGHBOR] = n.is.full.data;
      
      {
	double XYZc[3];
	quadrant_face_center (conn, geom, n.treeid, n.is.full.quad, (face_id_t) n.face, XYZc);
	double u[P4EST_DIM];
	solver->m_velocity_field_fn(XYZc[0],XYZc[1],XYZc[2],u);

	data[1]->w.velocity[0] = u[0];
	data[1]->w.velocity[1] = u[1];
#ifdef USE_3D
	data[1]->w.velocity[2] = 0.0; /* TO MODIFY for icosahedron */
#endif
      }
      upwind_update_face_unstructured (data, normal_vector, solver->m_dt, dS, dV, 0);
    }
  }
} // iterator_upwind_unstructured_update

// =======================================================
// =======================================================
static void
iterator_upwind_cfl (p4est_iter_volume_info_t * info,
		     void *user_data)
{

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est (info->p4est));

  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());
  
  p4est_ghost_t      *ghost = solver->get_ghost ();
  p4est_mesh_t       *mesh = solver->get_mesh ();
  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());
  double             *max_v_over_dx = (double *) user_data;

  p4est_quadrant_t   *neighbor = nullptr;

  qdata_t            *data[2] = { nullptr, nullptr };
  data[0] = userdata_mgr->quadrant_get_qdata (info->quad);
  
  int                 boundary = 0;

  double              edge_velocity = 0.0;
  int                 face = 0;
  int                 n = 0;    /* face normal */
  int                 d = 0;    /* face direction: x, y, z => (0, 1, 2) */

  /* init face neighbor iterator */
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, info->p4est, ghost, mesh,
                                  info->treeid, info->quadid);

  /* loop over neighbors */
  neighbor = p4est_mesh_face_neighbor_next (&mfn,
					    nullptr, /* num tree */
					    nullptr, /* num quad */
					    nullptr, /* num face */
					    nullptr); /* MPI rank owner */
  while (neighbor) {
    /* get the neighbor's variables */
    data[1] = QuadrantNeighborUtilsAdvectionUpwind::mesh_face_neighbor_data (&mfn, ghost_data, &boundary);
    userdata_mgr->qdata_check_variables (data[1]);

    /* compute the correct face ids */
    face = mfn.face + (mfn.subface > 0) - 1;

    /* normal orientation on the current face and the direction */
    n = 2 * (face % 2) - 1;
    d = face / 2;

    /* compute the edge_velocity */
    edge_velocity =
      fabs (n * (data[0]->w.velocity[d] + data[1]->w.velocity[d]) / 2.0);

    /* update the global max */
    {
      double dx = quadrant_length (info->quad);
      *max_v_over_dx = SC_MAX (*max_v_over_dx, edge_velocity/dx);
    }

    /*
     * if the neighbor is on the boundary, new data has been allocated by
     * mesh_face_neighbor_data that we have to deallocate
     */
    if (boundary == 1) {
      sc_mempool_free (info->p4est->user_data_pool, data[1]);
    }

    neighbor = p4est_mesh_face_neighbor_next (&mfn, nullptr, nullptr, nullptr, nullptr);
  }

  /* also compute the min and max levels in the mesh */
  solver->m_minlevel = SC_MIN (solver->m_minlevel, info->quad->level);
  solver->m_maxlevel = SC_MAX (solver->m_maxlevel, info->quad->level);

} // iterator_upwind_cfl

// =======================================================
// =======================================================
static void 
iterator_upwind_cfl_unstructured (p4est_iter_volume_info_t * info, void *user_data)
{

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverAdvectionUpwind *solver = static_cast<SolverAdvectionUpwind*>(solver_from_p4est (info->p4est));

  UserDataManagerAdvectionUpwind
    *userdata_mgr = static_cast<UserDataManagerAdvectionUpwind *> (solver->get_userdata_mgr());

  
  p4est_ghost_t        *ghost = solver->get_ghost ();
  p4est_mesh_t         *mesh = solver->get_mesh ();
  p4est_connectivity_t *conn = info->p4est->connectivity;
  p4est_geometry_t     *geom = solver->get_geom_compute ();

  p4est_quadrant_t   *quad = info->quad; /* current cell's quadrant */
  //p4est_locidx_t     quadid = info->quadid;
  p4est_topidx_t     treeid = info->treeid;

  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());

  double             *max_v_over_dx = (double *) user_data;

  p4est_quadrant_t   *neighbor = nullptr;
  qdata_t            *data[2] = { nullptr, nullptr };
  data[0] = userdata_mgr->quadrant_get_qdata (info->quad);

  int                 boundary = 0;

  double              edge_velocity = 0.0;
  int                 face = 0;

  double              dx;
  double              dS; /* interface surface */
  double              dV; /* cell volume */

  dV = quadrant_cell_volume_general(conn, geom, quad, treeid);
  
  /* init face neighbor iterator */
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, info->p4est, ghost, mesh,
                                  info->treeid, info->quadid);

  /* loop over neighbors */
  neighbor = p4est_mesh_face_neighbor_next (&mfn, nullptr, nullptr, nullptr, nullptr);
  while (neighbor) {
    /* get the neighbor's variables */
    data[1] = QuadrantNeighborUtilsAdvectionUpwind::mesh_face_neighbor_data (&mfn, ghost_data, &boundary);
    userdata_mgr->qdata_check_variables (data[1]);

    /* compute the correct face ids */
    face = mfn.face + (mfn.subface > 0) - 1;

    /* compute area element */
    dS = quadrant_face_area_general(conn, geom, quad, treeid, face);
    
    /* 
     * get face normal vector (no need to be outgoing)
     */
    double normal_vector[3];
    quadrant_face_normal(conn, geom, quad, treeid, face, normal_vector);
        
    /* compute the edge_velocity */
    edge_velocity = fabs (
			  data[0]->w.velocity[0]*normal_vector[0] +
			  data[0]->w.velocity[1]*normal_vector[1] +
			  data[0]->w.velocity[2]*normal_vector[2] +
			  data[1]->w.velocity[0]*normal_vector[0] +
			  data[1]->w.velocity[1]*normal_vector[1] +
			  data[1]->w.velocity[2]*normal_vector[2]) / 2.0;
    
    /* update the global max */
    {
      dx = dV/dS;
      *max_v_over_dx = SC_MAX (*max_v_over_dx, edge_velocity/dx);
    }

    /*
     * if the neighbor is on the boundary, new data has been allocated by
     * mesh_face_neighbor_data that we have to deallocate
     */
    if (boundary == 1) {
      sc_mempool_free (info->p4est->user_data_pool, data[1]);
    }

    neighbor = p4est_mesh_face_neighbor_next (&mfn, nullptr, nullptr, nullptr, nullptr);
  }

  /* also compute the min and max levels in the mesh */
  solver->m_minlevel = SC_MIN (solver->m_minlevel, info->quad->level);
  solver->m_maxlevel = SC_MAX (solver->m_maxlevel, info->quad->level);

} // iterator_upwind_unstructured_cfl

// =======================================================
// =======================================================

// =======================================================
// =======================================================

// =======================================================
// =======================================================
IteratorsAdvectionUpwind::IteratorsAdvectionUpwind() : IteratorsBase() {

  this->fill_iterators_list();
  
} // IteratorsAdvectionUpwind::IteratorsAdvectionUpwind

// =======================================================
// =======================================================
IteratorsAdvectionUpwind::~IteratorsAdvectionUpwind()
{
  
} // IteratorsAdvectionUpwind::~IteratorsAdvectionUpwind

// =======================================================
// =======================================================
void IteratorsAdvectionUpwind::fill_iterators_list()
{

  iteratorsList[IT_IDD::COMPUTE_MEAN_VALUES] = iterator_compute_mean_values;
  iteratorsList[IT_IDD::GATHER_STATISTICS]   = iterator_gather_statistics;
  iteratorsList[IT_IDD::START_STEP]          = iterator_start_step;
  iteratorsList[IT_IDD::END_STEP]            = iterator_end_step;
  iteratorsList[IT_IDD::MARK_ADAPT]          = iterator_mark_adapt;
  iteratorsList[IT_IDD::CFL_TIME_STEP]       = iterator_upwind_cfl;
  iteratorsList[IT_IDD::CFL_TIME_STEP_UNSTRUCTURED] = iterator_upwind_cfl_unstructured;
  iteratorsList[IT_IDD::UPDATE]              = iterator_upwind_update;
  iteratorsList[IT_IDD::UPDATE_UNSTRUCTURED] = iterator_upwind_update_unstructured;
  
} // IteratorsAdvectionUpwind::fill_iterators_list

} // namespace canop

} // namespace advection_upwind
