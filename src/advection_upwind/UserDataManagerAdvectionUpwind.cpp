#include "UserDataManagerAdvectionUpwind.h"

#include "canoP_base.h"

namespace canop {

namespace advection_upwind {

/**
 * Aliases to give a name to a UserDataManager member used to get data.
 */
using advection_upwind_getter_t =
  double (UserDataManagerAdvectionUpwind ::*) (p4est_quadrant_t * q);
using advection_upwind_vgetter_t =
  void (UserDataManagerAdvectionUpwind ::*) (p4est_quadrant_t * q, size_t dim, double * val);

// ====================================================================
// ====================================================================
UserDataManagerAdvectionUpwind::UserDataManagerAdvectionUpwind(bool useP4estMemory) :
  UserDataManager<UserDataTypesAdvectionUpwind>()
{

  UNUSED(useP4estMemory);
  
} // UserDataManagerAdvectionUpwind::UserDataManagerAdvectionUpwind

// ====================================================================
// ====================================================================
UserDataManagerAdvectionUpwind::~UserDataManagerAdvectionUpwind()
{

} // UserDataManagerAdvectionUpwind::~UserDataManagerAdvectionUpwind

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::qdata_set_variables (qdata_t * data,
						     qdatavar_t * w)
{

  memcpy (&(data->w), w, sizeof (qdatavar_t));
  memcpy (&(data->wnext), w, sizeof (qdatavar_t));
  memset (&(data->delta), 0, P4EST_DIM * sizeof (qdatarecons_t));
  
} // UserDataManagerAdvectionUpwind::qdata_set_variables

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::quadrant_set_state_data (p4est_quadrant_t * dst,
							 p4est_quadrant_t * src)
{

  qdata_t *data_dst = quadrant_get_qdata (dst);
  qdata_t *data_src = quadrant_get_qdata (src);

  memcpy (&(data_dst->w),     &(data_src->w),     sizeof (qdatavar_t));
  memcpy (&(data_dst->wnext), &(data_src->wnext), sizeof (qdatavar_t));
  memset (&(data_dst->delta), 0,      P4EST_DIM * sizeof (qdatarecons_t));

} // UserDataManagerAdvectionUpwind::quadrant_set_state_variables

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::quadrant_compute_mean(p4est_quadrant_t *qmean,
						      p4est_quadrant_t *quadout[P4EST_CHILDREN])
{

  qdata_t     *child_data;
  qdatavar_t   mean;
  memset (&mean, 0, sizeof (qdatavar_t));
    
  for (int8_t i = 0; i < P4EST_CHILDREN; ++i) {
    child_data = quadrant_get_qdata (quadout[i]);

    mean.rho += child_data->w.rho / P4EST_CHILDREN;
    for (int j = 0; j < P4EST_DIM; ++j) {
      mean.velocity[j] += child_data->w.velocity[j] / P4EST_CHILDREN;
    }

  }

  // finally, copy all of that into quadin
  quadrant_set_variables (qmean, &mean);
      
} // quadrant_compute_mean

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::qdata_print_variables (int loglevel, qdata_t * data)
{
  CANOP_LOGF (loglevel, "rho    = %-15.8lf %.8lf\n",
              data->w.rho, data->wnext.rho);
  CANOP_LOGF (loglevel, "velocity[0]    = %-15.8lf %.8lf\n",
              data->w.velocity[0], data->wnext.velocity[0]);
  CANOP_LOGF (loglevel, "velocity[1]    = %-15.8lf %.8lf\n",
              data->w.velocity[1], data->wnext.velocity[1]);
#ifdef USE_3D
  CANOP_LOGF (loglevel, "velocity[2]    = %-15.8lf %.8lf\n",
              data->w.velocity[2], data->wnext.velocity[2]);
#endif

} // UserDataManagerAdvectionUpwind::qdata_print_variables

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::quadrant_copy_w_to_wnext (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  memcpy (&(data->wnext), &(data->w), sizeof (qdatavar_t));

} // UserDataManagerAdvectionUpwind::quadrant_copy_w_to_wnext

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::quadrant_copy_wnext_to_w (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  memcpy (&(data->w), &(data->wnext), sizeof (qdatavar_t));

} // UserDataManagerAdvectionUpwind::quadrant_copy_wnext_to_w

// ====================================================================
// ====================================================================
double
UserDataManagerAdvectionUpwind::qdata_get_rho (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.rho;
  
} // UserDataManagerAdvectionUpwind::qdata_get_rho
 
// ====================================================================
// ====================================================================
double
UserDataManagerAdvectionUpwind::qdata_get_velocity (p4est_quadrant_t * q, int direction)
{
  qdata_t *data = quadrant_get_qdata (q);

  if (direction < P4EST_DIM) {
    return data->w.velocity[direction];
  } else {
    return 0;
  }
  
} // UserDataManagerAdvectionUpwind::qdata_get_velocity

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::qdata_vget_velocity (p4est_quadrant_t * q, size_t dim, double * val)
{
  qdata_t *data = quadrant_get_qdata (q);

  for (size_t direction = 0; direction < dim; ++direction) {
    if (direction < P4EST_DIM) {
      val[direction] = data->w.velocity[direction];
    } else {
      val[direction] = 0;
    }
  }

} // UserDataManagerAdvectionUpwind::qdata_vget_velocity

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::qdata_reconstructed_zero(qdatarecons_t *q)
{

  q->rho = 0.0;

} // UserDataManagerAdvectionUpwind::qdata_reconstructed_zero

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::qdata_reconstructed_copy(qdatarecons_t *q1, 
							 const qdatarecons_t *q2)
{

  memcpy( q1, q2, sizeof(qdatarecons_t) );

} // UserDataManagerAdvectionUpwind::qdata_reconstructed_copy

// ====================================================================
// ====================================================================
void UserDataManagerAdvectionUpwind::qdata_reconstructed_swap(qdatarecons_t *q1, 
							      qdatarecons_t *q2)
{

  std::swap( q1->rho, q2->rho);

} // UserDataManagerAdvectionUpwind::qdata_reconstructed_swap

// ====================================================================
// ====================================================================
void UserDataManagerAdvectionUpwind::qdata_reconstructed_change_sign(qdatarecons_t *q)
{

  /* this is only usefull when q is a slope vector */

  q->rho = -q->rho;
  // for (int i=0; i<P4EST_DIM; i++)
  //   q->velocity[i] = -q->velocity[i];

} // UserDataManagerAdvectionUpwind::qdata_reconstructed_change_sign

// ====================================================================
// ====================================================================
void UserDataManagerAdvectionUpwind::compute_delta(const qdatarecons_t w,const qdatarecons_t *wn,
						   qdatarecons_t *delta,
						   scalar_limiter_t limiter,
						   double dx, double dxl, double dxr)
{
  
  qdatarecons_t       wl, wr;

  /* then we compute the reconstucted gradient on each pair (w_i, w, w_{i + 1}) */
  for (int i = 0; i < P4EST_HALF; ++i) {
    wl = wn[2 * i + 0];
    wr = wn[2 * i + 1];
    
    /* reconstruct only first and second conservative variables */
    delta[i].rho = limiter ((w.rho - wl.rho), (wr.rho - w.rho), dx, dxl, dxr);
  } // end reconstucted gradient
  
  /*
   * we now have reconstructed values on the lower half and upper half of
   * quadrant, but we need a reconstructed value on the whole cell, so we just
   * limit the 2 (or 4 in 3D) values we have using the same limiter.
   */
  for (int i = 1; i < P4EST_HALF; ++i) {
    delta[0].rho = limiter_minmod (delta[0].rho, delta[i].rho, 1, 1, 1);
  }
  
} // UserDataManagerAdvectionUpwind::compute_delta
  
// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::qdata_reconstructed_check (qdatarecons_t w,
							   const char *plus)
{
  
  SC_CHECK_ABORTF (!isnan (w.rho), "rho%s is nan\n", plus);
  SC_CHECK_ABORTF (!isinf (w.rho), "rho%s is inf\n", plus);
  
} // UserDataManagerAdvectionUpwind::qdata_reconstructed_check

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::qdata_variables_check (qdatavar_t w,
						       const char *plus)
{
  
  SC_CHECK_ABORTF (!isnan (w.rho), "rho%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.velocity[0]), "ux%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.velocity[1]), "uy%s is nan\n", plus);
#ifdef P4_TO_P8
  SC_CHECK_ABORTF (!isnan (w.velocity[2]), "uz%s is nan\n", plus);
#endif

  SC_CHECK_ABORTF (!isinf (w.rho), "rho%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.velocity[0]), "ux%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.velocity[1]), "uy%s is inf\n", plus);
#ifdef P4_TO_P8
  SC_CHECK_ABORTF (!isinf (w.velocity[2]), "uz%s is inf\n", plus);
#endif
  
} // UserDataManagerAdvectionUpwind::qdata_variables_check

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::qdata_check_variables (qdata_t * data)
{
  
  qdata_variables_check (data->w, "");
  qdata_variables_check (data->wnext, "+");
  qdata_reconstructed_check (data->delta[0], "_dx");
  qdata_reconstructed_check (data->delta[1], "_dy");
#ifdef P4_TO_P8
  qdata_reconstructed_check (data->delta[2], "_dz");
#endif
  
} // UserDataManagerAdvectionUpwind::qdata_check_variables

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::quadrant_check_data (p4est_quadrant_t * q)
{
  
  qdata_check_variables (quadrant_get_qdata (q));
  
} // UserDataManagerAdvectionUpwind::quadrant_check_data

// ====================================================================
// ====================================================================
void
UserDataManagerAdvectionUpwind::reconstruct_variables (const qdatavar_t &w,
						       qdatarecons_t &r)
{

  r.rho = w.rho;
  
} // UserDataManagerAdvectionUpwind::reconstruct_variables

// ====================================================================
// ====================================================================
qdata_getter_t
UserDataManagerAdvectionUpwind::qdata_getter_byname (const std::string &varName)
{

  qdata_getter_t base_getter = nullptr;
  advection_upwind_getter_t getter = nullptr;
  
  if (!varName.compare("rho")) {
    getter = &UserDataManagerAdvectionUpwind::qdata_get_rho;
  }
  
  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_getter_t>(getter);

  } else {

    // print warning
    CANOP_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
    
  }
  
  return base_getter;
  
} // UserDataManagerAdvectionUpwind::qdata_getter_byname


// ====================================================================
// ====================================================================
qdata_vector_getter_t
UserDataManagerAdvectionUpwind::qdata_vgetter_byname (const std::string &varName)
{

  qdata_vector_getter_t base_getter = nullptr;
  advection_upwind_vgetter_t getter = nullptr;

  if (!varName.compare("velocity")) {
    getter = &UserDataManagerAdvectionUpwind::qdata_vget_velocity;
  }

  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_vector_getter_t>(getter);

  } else {

    // print warning
    P4EST_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());

  }

  return base_getter;

} // UserDataManagerAdvectionUpwind::qdata_vgetter_byname


// ====================================================================
// ====================================================================
qdata_field_type
UserDataManagerAdvectionUpwind::qdata_type_byname(const std::string &varName)
{

  if (!varName.compare("rho"))
    return QDATA_FIELD_SCALAR;

  if (!varName.compare("velocity"))
    return QDATA_FIELD_VECTOR;

  // print warning
  P4EST_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
  return QDATA_FIELD_UNKNOWN;

} // UserDataManagerAdvectionUpwind::qdata_type_byname

} // namespace advection_upwind

} // namespace canop
