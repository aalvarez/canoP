#include "SolverMHD_KT.h"
#include "SolverCoreMHD_KT.h"
#include "UserDataManagerMHD_KT.h"
#include "IteratorsMHD_KT.h"
#include "ScalarLimiterFactory.h"

#include "IO_Writer.h"

#include "canoP_base.h"
#include "utils.h"

namespace canop {

namespace mhd_kt {

// =======================================================
// =======================================================
SolverMHD_KT::SolverMHD_KT(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory) :
  Solver(cfg,mpicomm,sizeof(SolverMHD_KT::qdata_t),useP4estMemory)
{

  // setup solver core
  m_solver_core = new SolverCoreMHD_KT(cfg);

  // setup UserData Manager
  m_userdata_mgr = new UserDataManagerMHD_KT(useP4estMemory);

  // we need to setup the right iterators
  m_pIter = new IteratorsMHD_KT();

  // solver init (this is were p4est_new is called)
  init();
  
  // only MPI rank 0 prints config (thanks to CANOP_GLOBAL_PRODUCTION macro)
  print_config();

  // initialize div b method
  std::string div_b_method_name = cfg->config_read_string("settings.div_b_cleaning", "GLM");
  if (div_b_method_name == "DIFFUSION") {
    div_b_method = DIV_B_DIFFUSION;
  } else if (div_b_method_name == "GLM") {
    div_b_method = DIV_B_GLM;
  } else if (div_b_method_name == "GLM_SUBCYCLED") {
    div_b_method = DIV_B_GLM_SUBCYCLED;
  }
  
  // initialize limiter
  ScalarLimiterFactoryTmpl<regular_limiter_t> limiter_Factory;
  std::string limiter_name = cfg->config_read_string("settings.limiter", "vanalbada");
  m_compute_phi = limiter_Factory.limiter_byname (limiter_name);

  // the following allow the simulation to run in the case where
  // the parameter file contains typo in limiter name
  if (m_compute_phi == nullptr) {
    CANOP_GLOBAL_PRODUCTIONF ("### WARNING ### Unrecognized limiter name: \"%s\" -- Using \"vanalbada\" instead\n", limiter_name.c_str());
    m_compute_phi = limiter_Factory.limiter_byname ("vanalbada");
  }
  
  // additionnal initialisation
#ifdef USE_MUTATIONPP

  // Generate the default options for the He-H mixture
  Mutation::MixtureOptions opts("He-H");
  opts.setStateModel("EquilTP");
  
  // Load the mixture with the new options
  m_mix = new Mutation::Mixture(opts);

#endif
  
} // SolverMHD_KT::SolverMHD_KT

// =======================================================
// =======================================================
SolverMHD_KT::~SolverMHD_KT()
{

  finalize();
  
  delete m_solver_core;

  delete m_userdata_mgr;

  delete m_pIter;

#ifdef USE_MUTATIONPP
  delete m_mix;
#endif
  
} // SolverMHD_KT::~SolverMHD_KT

// =======================================================
// =======================================================
void
SolverMHD_KT::compute_time_step()
{

  p4est_t            *p4est = get_p4est();
  p4est_geometry_t   *geom  = get_geom_compute ();
  MPI_Comm            mpicomm = p4est->mpicomm;
  double              min_dt = 1;
  UNUSED (geom);

  // initialize the min / max current levels
  m_minlevel = P4EST_QMAXLEVEL;
  m_maxlevel = 0;

  // compute the min of dx
  // update the min / max current levels
  p4est_iterate (p4est,
                 NULL,
                 /* user data */ &min_dt,
                 m_pIter->iteratorsList[IT_ID::CFL_TIME_STEP],  /* cell_fn */
                 NULL,                        /* face_fn */
#ifdef P4_TO_P8
                 NULL,                        /* edge_fn */
#endif
                 NULL                         /* corner_fn */
                 );
  

  // get the global maximum and minimum level
  MPI_Allreduce (MPI_IN_PLACE, &(m_minlevel), 1,
                 MPI_INT, MPI_MIN, mpicomm);
  MPI_Allreduce (MPI_IN_PLACE, &(m_maxlevel), 1,
                 MPI_INT, MPI_MAX, mpicomm);
  
  // update the min dt using the given courant number
  if (min_dt < 0) {
    m_dt = m_tmax;
  }
  else if (!ISFUZZYNULL (min_dt)) {
    m_dt = m_cfl*min_dt;
  }
  else {
    SC_ABORT ("min_dt is 0\n");
  }

  // take the min over all the processes
  MPI_Allreduce (MPI_IN_PLACE, &(m_dt), 1, MPI_DOUBLE, MPI_MIN,
                 mpicomm);

  
} // SolverMHD_KT::compute_time_step

// =======================================================
// =======================================================
void
SolverMHD_KT::next_iteration_impl()
{

  p4est_t            *p4est = get_p4est ();
  p4est_ghost_t      *ghost = get_ghost ();
  
  // compute the cfl condition and time step at the smallest level
  compute_time_step ();

  /*
   * Here compute the viscous right hand side ?
   */

 
  /*
   * Perform update here:
   * - simple 1s order ?
   * - Runge-Kutta higher-order ?
   *
   * Whatever you'd like to be.
   *
   */

  // ONLY NEED IF PERFORMING DIFFUSION CLEANING (DIV B)
  if (div_b_method == DIV_B_DIFFUSION) {
    /*
     * compute  B field at face
     */
    CANOP_GLOBAL_INFO ("B field at faces \n");
    p4est_iterate (p4est,
		   ghost,
		   /* user data */ NULL,
		   m_pIter->iteratorsList[IT_IDD::BFIELD_FACE],
		   NULL,        // face_fn
#ifdef P4_TO_P8
		   NULL,        // edge_fn
#endif
		   NULL         // corner_fn
		   );
  }
  

  // exchange the data again to get all the trace-reconstructed values
  //p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );

  /*
   * reconstruct gradients
   */
  CANOP_GLOBAL_INFO ("Compute slopes \n");
  p4est_iterate (p4est,
                 ghost,
                 /* user data */ NULL,
                 m_pIter->iteratorsList[IT_IDD::LIMITER],
                 NULL,        // face_fn
#ifdef P4_TO_P8
                 NULL,        // edge_fn
#endif
                 NULL         // corner_fn
                 );
 
  // exchange the data again to get all the trace-reconstructed values
  p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );

  CANOP_GLOBAL_INFO ("Perform numerical scheme time update\n");
  p4est_iterate (p4est,
		 ghost,
		 /* user data */ NULL,
		 m_pIter->iteratorsList[IT_ID::UPDATE],
		 NULL,        // face_fn
#ifdef P4_TO_P8
		 NULL,        // edge_fn
#endif
		 NULL         // corner_fn
		 );

  // exchange the data again to get all the trace-reconstructed values
  //  p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );

  if (div_b_method == DIV_B_DIFFUSION) {
    //   CANOP_GLOBAL_INFO ("Perform div(B) constraint with diffusive method\n");
    p4est_iterate (p4est,
		   ghost,
		   /* user data */ NULL,
		   m_pIter->iteratorsList[IT_IDD::UPDATE_B_DIFFUSIVE],
		   NULL,        // face_fn
#ifdef P4_TO_P8
		   NULL,        // edge_fn
#endif
		   NULL         // corner_fn
		   );
  }

  if (div_b_method == DIV_B_GLM_SUBCYCLED) {
    
    // exchange the data again to get all the trace-reconstructed values
    //p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );

    // need to copy mag field wnext (update of the first stage) into w
    // before subcycling
    p4est_iterate (p4est,
		   ghost,
		   /* user data */ NULL,
		   m_pIter->iteratorsList[IT_IDD::COPY_MAG_FIELD],
		   NULL,        // face_fn
#ifdef P4_TO_P8
		   NULL,        // edge_fn
#endif
		   NULL         // corner_fn
		   );

    
    int nb_subcycles = m_cfg->config_read_int("settings.glm_nb_subcycles", 5);
    
    for (int i = 0; i<nb_subcycles; ++i) {

      subcycle_iter = i;
      
      // compute limited slopes for B_star and psi
      CANOP_GLOBAL_INFO ("Compute slopes - subcycled GLM\n");
      p4est_iterate (p4est,
		     ghost,
		     /* user data */ NULL,
		     m_pIter->iteratorsList[IT_IDD::LIMITER],  // !!!!!!!!!!
		     NULL,        // face_fn
#ifdef P4_TO_P8
		     NULL,        // edge_fn
#endif
		     NULL         // corner_fn
		     );

      // synchronize ghost
      p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );

      // update B and psi (no right hand side)
      // the last iteration must also take care the right hand side
      p4est_iterate (p4est,
		     ghost,
		     /* user data */ NULL,
		     m_pIter->iteratorsList[IT_IDD::UPDATE_GLM_SUBCYCLED],
		     NULL,        // face_fn
#ifdef P4_TO_P8
		     NULL,        // edge_fn
#endif
		     NULL         // corner_fn
		     );
      
    } //

  }
  
  /*
   * Compute div(B) in each cell
   */
  CANOP_GLOBAL_INFO ("compute div(B)  at faces \n");
  p4est_iterate (p4est,
                 ghost,
                 /* user data */ NULL,
                 m_pIter->iteratorsList[IT_IDD::DIV_B],
                 NULL,        // face_fn
#ifdef P4_TO_P8
                 NULL,        // edge_fn
#endif
                 NULL         // corner_fn
                 );

  // exchange the data again to get all the trace-reconstructed values
  //  p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );  

  /*
   * Compute Transport properties in each cell
   */
//   CANOP_GLOBAL_INFO ("compute Transport properties  at faces \n");
//   p4est_iterate (p4est,
//                  ghost,
//                  /* user data */ NULL,
//                  m_pIter->iteratorsList[IT_IDD::TRANSP],
//                  NULL,        // face_fn
// #ifdef P4_TO_P8
//                  NULL,        // edge_fn
// #endif
//                  NULL         // corner_fn
//                  );
  // exchange the data again to get all the trace-reconstructed values
  //  p4est_ghost_exchange_data (p4est, ghost, get_ghost_data_ptr() );



   
  // update old = new for all the quads
  CANOP_GLOBAL_INFO ("Copying new into old\n");
  p4est_iterate (p4est, 
		 ghost, 
		 NULL, 
		 m_pIter->iteratorsList[IT_ID::END_STEP], 
		 NULL,          // face_fn
#ifdef P4_TO_P8
                 NULL,          // edge_fn
#endif
                 NULL           // corner_fn
		 );
  
} // SolverMHD_KT::next_iteration_impl

// =======================================================
// =======================================================
void SolverMHD_KT::read_config()
{
  // fisrt run base class method
  Solver::read_config();

  /*
   * Initialize mhd_kt parameters. None here.
   */
  
} // SolverMHD_KT::read_config

} // namespace mhd_kt

} // namespace canoP
