#include "mhd.h"

namespace canop {

namespace mhd_kt {


// ===================================================================
// ===================================================================
double  compute_pressure_mhd(const qdata_variables_t &qdata,
			     double gamma)
{
  
  double Ekin = 0.5*((qdata.rhoU*qdata.rhoU+qdata.rhoV*qdata.rhoV)/(qdata.rho));
  double Emag = (qdata.Bx*qdata.Bx+qdata.By*qdata.By)/(8.0*M_PI);
  
#ifdef P4_TO_P8
  Ekin += 0.5*qdata.rhoW*qdata.rhoW/(qdata.rho); 
  Emag += (qdata.Bz*qdata.Bz)/(8.0*M_PI);
#endif
  double Eint=qdata.E-Ekin-Emag;
  
  double pressure = (gamma-1)*Eint; 

  return pressure;

} // compute_pressure

// ===================================================================
// ===================================================================
void compute_flux_mhd(const qdata_variables_t &q,
		      qdata_variables_t &flux,
		      int dir,
		      double gamma,
                      double ch,
		      div_b_cleaning_t cleaning
		      )
{
  // compute pressure
  double p = compute_pressure_mhd(q,gamma);
  
  double tau11_B=0.0;
  double tau12_B=0.0;
  double tau22_B=0.0;
#ifdef P4_TO_P8
  double tau31_B=0.0;
  double tau23_B=0.0;
  double tau33_B=0.0;

  if (dir == IX) {
    double u = q.rhoU/q.rho;
    double v = q.rhoV/q.rho;
    double w = q.rhoW/q.rho;    

    tau11_B= (1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By+q.Bz*q.Bz) - q.Bx*q.Bx);    
    tau12_B=-(1./(4.*M_PI))*q.Bx*q.By;
    tau31_B=-(1./(4.*M_PI))*q.Bx*q.Bz;

    flux.rho =    q.rhoU;     //   rho u
   
    flux.rhoU = u*q.rhoU + p + tau11_B;   // u rho u + p
    flux.rhoV = u*q.rhoV + tau12_B;     // u rho v
    flux.rhoW = u*q.rhoW + tau31_B;     // u rho w
  
    flux.E    = u*(q.E+p) + (tau11_B*u + tau12_B*v + tau31_B*w);    // u (E+p)

    // GLM
    if (cleaning == DIV_B_GLM) {
      flux.Bx   = q.psi;
      flux.psi  = q.Bx * ch * ch;
    } else {
      flux.Bx   = 0.0; 
      flux.psi  = 0.0;
    }  
    flux.By   = u*q.By - v*q.Bx;
    flux.Bz   = u*q.Bz - w*q.Bx;
    
  }
  else if (dir == IY) {
    double u = q.rhoU/q.rho;
    double v = q.rhoV/q.rho;
    double w = q.rhoW/q.rho;

    tau22_B= (1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By+q.Bz*q.Bz) - q.By*q.By);  
    tau12_B=-(1./(4.*M_PI))*q.Bx*q.By ;
    tau23_B=-(1./(4.*M_PI))*q.Bz*q.By ;    

    flux.rho  =   q.rhoV;     //   rho v

    flux.rhoU = v*q.rhoU+tau12_B;     // v rho u
    flux.rhoV = v*q.rhoV+p+tau22_B;   // v rho v + p
    flux.rhoW = v*q.rhoW+tau23_B;     // v rho w

    flux.E    = v*(q.E+p)+(tau12_B*u+tau22_B*v+tau23_B*w);    // v (E+p)

    // GLM
    if (cleaning == DIV_B_GLM) {
      flux.By   = q.psi;
      flux.psi  = q.By * ch * ch;
    } else {
      flux.By   = 0;
      flux.psi  = 0.0;
    }
    flux.Bx   = v*q.Bx-u*q.By;
    flux.Bz   = v*q.Bz-w*q.By;
  }
  else if (dir == IZ) {
    double u = q.rhoU/q.rho;
    double v = q.rhoV/q.rho;
    double w = q.rhoW/q.rho;
    
    tau33_B   = (1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By+q.Bz*q.Bz) - q.Bz*q.Bz);
    tau23_B   = -(1./(4.*M_PI))*q.By*q.Bz;
    tau31_B   = -(1./(4.*M_PI))*q.Bz*q.Bx;

    flux.rho  =  q.rhoW;     //   rho w
   
    flux.rhoU = w*q.rhoU + tau31_B ;     // w rho u
    flux.rhoV = w*q.rhoV + tau23_B;     // w rho v
    flux.rhoW = w*q.rhoW+p + tau33_B ;   // w rho w + p
   
    flux.E    = w*(q.E+p) + (tau31_B*u + tau23_B*v + tau33_B*w);    // w (E+p)
    
    // GLM
    if (cleaning == DIV_B_GLM) {
      flux.Bz   = q.psi;
      flux.psi  = q.Bz * ch * ch;
    } else {
      flux.Bz   = 0;
      flux.psi  = 0.0;
    }
    flux.Bx   = w*q.Bx - u*q.Bz;
    flux.By   = w*q.By - v*q.Bz;

  }

#else   // 2D

  if (dir == IX) {
    tau11_B =  (1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By) - q.Bx*q.Bx);
    tau12_B = -(1./(4.*M_PI))*q.Bx*q.By;    

    flux.rho  = q.rhoU;                 // rho u
    flux.rhoU = q.rhoU*q.rhoU/q.rho+p+tau11_B;   // rho u^2 + p + (By²-Bx²)/8M_PI
    flux.rhoV = q.rhoU*q.rhoV/q.rho +tau12_B;     // rho u v -Bx*By/4*M_PI
    flux.E    = q.rhoU/q.rho*(q.E+p)+(tau11_B*q.rhoU/q.rho+tau12_B*q.rhoV/q.rho) ; // u (E+p)   

    if (cleaning == DIV_B_GLM) {
      // If GLM MHD method :
      flux.Bx  = q.psi;
      flux.psi = q.Bx * ch * ch;
    } else {
      // If diffusive method  or GLM sybcycled
      flux.Bx  = 0.0;
      flux.psi = 0.0;
    }
    flux.By = (q.rhoU/q.rho)*q.By-(q.rhoV/q.rho)*q.Bx;   
    
  } 
  else if (dir == IY) {
    tau22_B =  (1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By) - q.By*q.By);
    tau12_B = -(1./(4.*M_PI))*q.Bx*q.By;
    
    flux.rho  = q.rhoV;                 // rho v
    flux.rhoU = q.rhoV*q.rhoU/q.rho+tau12_B;     // rho v u
    flux.rhoV = q.rhoV*q.rhoV/q.rho+p+tau22_B;   // rho v^2 + p 
    flux.E    = q.rhoV/q.rho*(q.E+p)+(tau12_B*q.rhoU/q.rho+tau22_B*q.rhoV/q.rho); // v (E+p)

    if (cleaning == DIV_B_GLM)  {
      // If GLM MHD method
      flux.By  = q.psi;
      flux.psi = q.By * ch * ch;
    } else {
      // If diffusive method or GLM subcycled
      flux.By  = 0.0;
      flux.psi = 0.0;
    }
    flux.Bx  = -(q.rhoU/q.rho)*q.By+(q.rhoV/q.rho)*q.Bx;
  }
  
#endif
  
} // compute_flux_mhd

// ===================================================================
// ===================================================================
void compute_flux_mhd_diffusive(const qdata_variables_t &q,
				qdata_variables_t &flux,
				int dir,
				double gamma)
{
  // compute pressure
  double p = compute_pressure_mhd(q,gamma);
  
  double tau11_B=0.0;
  double tau12_B=0.0;
  double tau22_B=0.0;
#ifdef P4_TO_P8
  double tau31_B=0.0;
  double tau23_B=0.0;
  double tau33_B=0.0;

  if (dir == IX) {
    double u = q.rhoU/q.rho;
    double v = q.rhoV/q.rho;
    double w = q.rhoW/q.rho;    

    tau11_B= (1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By+q.Bz*q.Bz) - q.Bx*q.Bx);    
    tau12_B=-(1./(4.*M_PI))*q.Bx*q.By;
    tau31_B=-(1./(4.*M_PI))*q.Bx*q.Bz;

    flux.rho =    q.rhoU;     //   rho u
   
    flux.rhoU = u*q.rhoU + p + tau11_B;   // u rho u + p
    flux.rhoV = u*q.rhoV + tau12_B;     // u rho v
    flux.rhoW = u*q.rhoW + tau31_B;     // u rho w
  
    flux.E    = u*(q.E+p) + (tau11_B*u + tau12_B*v + tau31_B*w);    // u (E+p)

    // diffusive div b clean
    flux.Bx   = 0.0; 
    flux.By   = u*q.By - v*q.Bx;
    flux.Bz   = u*q.Bz - w*q.Bx;
    
  }
  else if (dir == IY) {
    double u = q.rhoU/q.rho;
    double v = q.rhoV/q.rho;
    double w = q.rhoW/q.rho;

    tau22_B= (1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By+q.Bz*q.Bz) - q.By*q.By);  
    tau12_B=-(1./(4.*M_PI))*q.Bx*q.By ;
    tau23_B=-(1./(4.*M_PI))*q.Bz*q.By ;    

    flux.rho  =   q.rhoV;     //   rho v

    flux.rhoU = v*q.rhoU+tau12_B;     // v rho u
    flux.rhoV = v*q.rhoV+p+tau22_B;   // v rho v + p
    flux.rhoW = v*q.rhoW+tau23_B;     // v rho w

    flux.E    = v*(q.E+p)+(tau12_B*u+tau22_B*v+tau23_B*w);    // v (E+p)

    // diffusive div b clean
    flux.Bx   = v*q.Bx-u*q.By;
    flux.By   = 0;
    flux.Bz   = v*q.Bz-w*q.By;
  }
  else if (dir == IZ) {
    double u = q.rhoU/q.rho;
    double v = q.rhoV/q.rho;
    double w = q.rhoW/q.rho;
    
    tau33_B   = (1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By+q.Bz*q.Bz) - q.Bz*q.Bz);
    tau23_B   = -(1./(4.*M_PI))*q.By*q.Bz;
    tau31_B   = -(1./(4.*M_PI))*q.Bz*q.Bx;

    flux.rho  =  q.rhoW;     //   rho w
   
    flux.rhoU = w*q.rhoU + tau31_B ;     // w rho u
    flux.rhoV = w*q.rhoV + tau23_B;     // w rho v
    flux.rhoW = w*q.rhoW+p + tau33_B ;   // w rho w + p
   
    flux.E    = w*(q.E+p) + (tau31_B*u + tau23_B*v + tau33_B*w);    // w (E+p)
    
    // diffusive div b clean
    flux.Bx   = w*q.Bx - u*q.Bz;
    flux.By   = w*q.By - v*q.Bz;
    flux.Bz   = 0;
  }

#else   // 2D

  if (dir == IX) {
    tau11_B=(1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By) - q.Bx*q.Bx);
    tau12_B = -(1./(4.*M_PI))*q.Bx*q.By;    

    flux.rho  = q.rhoU;                 // rho u
    flux.rhoU = q.rhoU*q.rhoU/q.rho+p+tau11_B;   // rho u^2 + p + (By²-Bx²)/8M_PI
    flux.rhoV = q.rhoU*q.rhoV/q.rho +tau12_B;     // rho u v -Bx*By/4*M_PI
    flux.E    = q.rhoU/q.rho*(q.E+p)+(tau11_B*q.rhoU/q.rho+tau12_B*q.rhoV/q.rho) ; // u (E+p)   

    // If diffusive method :
    flux.Bx=0.;
    flux.By=(q.rhoU/q.rho)*q.By-(q.rhoV/q.rho)*q.Bx;
  } 
  else if (dir == IY) {
    tau22_B =  (1./(4.*M_PI))*(0.5*(q.Bx*q.Bx+q.By*q.By) - q.By*q.By);
    tau12_B = -(1./(4.*M_PI))*q.Bx*q.By;
    
    flux.rho  = q.rhoV;                 // rho v
    flux.rhoU = q.rhoV*q.rhoU/q.rho+tau12_B;     // rho v u
    flux.rhoV = q.rhoV*q.rhoV/q.rho+p+tau22_B;   // rho v^2 + p 
    flux.E    = q.rhoV/q.rho*(q.E+p)+(tau12_B*q.rhoU/q.rho+tau22_B*q.rhoV/q.rho); // v (E+p)

    // If diffusive method :
    flux.Bx=-(q.rhoU/q.rho)*q.By+(q.rhoV/q.rho)*q.Bx;
    flux.By=0.;
  }
  
#endif
  
} // compute_flux_mhd_diffusive

// ===================================================================
// ===================================================================
void compute_flux_mhd_glm(const qdata_variables_t &q,
			  qdata_variables_t &flux,
			  int dir,
			  double gamma,
			  double ch)
{

  UNUSED(gamma);
  
#ifdef P4_TO_P8
  
  if (dir == IX) {

    flux.Bx   = q.psi;
    flux.psi  = q.Bx * ch * ch;
    
  }
  
  else if (dir == IY) {
    
    flux.By   = q.psi;
    flux.psi  = q.By * ch * ch;
    
  }
  
  else if (dir == IZ) {
    
    flux.Bz   = q.psi;
    flux.psi  = q.Bz * ch * ch;
    
  }
  
#else   // 2D

  if (dir == IX) {
    flux.Bx  = q.psi;
    flux.psi = q.Bx * ch * ch;    
  }
  
  else if (dir == IY) {
    flux.By  = q.psi;
    flux.psi = q.By * ch * ch;
  }
  
#endif
  
} // compute_flux_mhd_glm

} // namespace mhd_kt

} // namespace canoP
