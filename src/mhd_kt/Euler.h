#ifndef EULER_EQUATIONS_H_
#define EULER_EQUATIONS_H_
#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h"

#include "UserDataManagerMHD_KT.h"
#include "userdata_mhd_kt.h"

namespace canop {

namespace mhd_kt {

// get userdata specific stuff
using qdata_t               = Qdata;
using qdata_variables_t     = QdataVar;
using qdata_reconstructed_t = QdataRecons;

using UserDataTypesMHD_KT = UserDataTypes<Qdata,
					  QdataVar,
					  QdataRecons>;

using QuadrantNeighborUtilsMHD_KT = QuadrantNeighborUtils<UserDataTypesMHD_KT>;

double compute_pressure(const qdata_variables_t &qdata, double gamma);

void compute_flux(const qdata_variables_t &qdata,
		  qdata_variables_t &flux,
		  int dir,
		  double gamma);

} // namespace mhd_kt

} // namespace canoP

#endif // EULER_EQUATIONS_H_
