#include "SolverCoreMHD_KT.h"

#include "InitialConditionsFactoryMHD_KT.h"
#include "BoundaryConditionsFactoryMHD_KT.h"
#include "IndicatorFactoryMHD_KT.h"

#include "ConfigReader.h"

namespace canop {

namespace mhd_kt {

// =======================================================
// =======================================================
SolverCoreMHD_KT::SolverCoreMHD_KT(ConfigReader *cfg) :
  SolverCore<Qdata>(cfg)
{

  /*
   * Initial condition callback setup.
   */
  InitialConditionsFactoryMHD_KT IC_Factory;
  m_init_fn = IC_Factory.callback_byname (m_init_name);

  /*
   * Boundary condition callback setup.
   */
  BoundaryConditionsFactoryMHD_KT BC_Factory;
  m_boundary_fn = BC_Factory.callback_byname (m_boundary_name);

  /*
   * Refine/Coarsen callback setup.
   */
  IndicatorFactoryMHD_KT indic_Factory;
  m_indicator_fn = indic_Factory.callback_byname (m_indicator_name);

  
} // SolverCoreMHD_KT::SolverCoreMHD_KT


// =======================================================
// =======================================================
SolverCoreMHD_KT::~SolverCoreMHD_KT()
{
} // SolverCoreMHD_KT::~SolverCoreMHD_KT

} // namespace mhd_kt

} // namespace canoP
