#ifndef USERDATA_MANAGER_MHD_KT_H_
#define USERDATA_MANAGER_MHD_KT_H_

#include "UserDataManager.h"
#include "userdata_mhd_kt.h"

namespace canop {

namespace mhd_kt {

/* For this simple case, qdatavar_t and qdatarecons_t are just aliased to double */
using UserDataTypesMHD_KT = UserDataTypes<Qdata,
					  QdataVar,
					  QdataRecons>;


class UserDataManagerMHD_KT : public UserDataManager<UserDataTypesMHD_KT>
{
  
public:
  UserDataManagerMHD_KT(bool useP4estMemory);
  virtual ~UserDataManagerMHD_KT();

  virtual void qdata_set_variables (qdata_t * data,
				    qdatavar_t * w);
  
  virtual void quadrant_set_state_data (p4est_quadrant_t * dst,
					p4est_quadrant_t * src);

  virtual void quadrant_compute_mean(p4est_quadrant_t *qmean,
				     p4est_quadrant_t *quadout[P4EST_CHILDREN]);

  
  virtual void qdata_print_variables (int loglevel, qdata_t * data);

  virtual void quadrant_copy_w_to_wnext (p4est_quadrant_t * q);
  virtual void quadrant_copy_wnext_to_w (p4est_quadrant_t * q);
  void quadrant_copy_wnext_to_w_mag (p4est_quadrant_t * q);
  
  double qdata_get_rho (p4est_quadrant_t * q);
  double qdata_get_rhoU (p4est_quadrant_t * q);
  double qdata_get_rhoV (p4est_quadrant_t * q);
  double qdata_get_rhoW (p4est_quadrant_t * q);
  double qdata_get_E (p4est_quadrant_t * q);
  double qdata_get_Bx (p4est_quadrant_t * q);
  double qdata_get_By (p4est_quadrant_t * q);
  double qdata_get_Bz (p4est_quadrant_t * q);
  double qdata_get_psi (p4est_quadrant_t * q);
  
  double qdata_get_div_B (p4est_quadrant_t * q);
  double qdata_get_Bxfy (p4est_quadrant_t * q);
  double qdata_get_Byfx (p4est_quadrant_t * q);
  double qdata_get_lam_h (p4est_quadrant_t * q);
  double qdata_get_eta_h (p4est_quadrant_t * q);
  double qdata_get_lam_e (p4est_quadrant_t * q);
  /**
   * Return a pointer to a member that is a qdata field "getter".
   */
  virtual qdata_getter_t qdata_getter_byname(const std::string &varName);

  /**
   * Return the field type for the given variable.
   */
  virtual qdata_field_type qdata_type_byname(const std::string &varName);

}; // UserDataManagerMHD_KT

} // namespace mhd_kt

} // namespace canoP

#endif // USERDATA_MANAGER_MHD_KT_H_
