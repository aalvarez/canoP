#ifndef USERDATA_MHD_KT_H_
#define USERDATA_MHD_KT_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

namespace canop {

namespace mhd_kt {

enum GeomDir {
  IX = 0,
  IY = 1,
  IZ = 2
};

/* number of variables <==> P4EST_DIM : one velocity per dimension */
constexpr int QDATA_NUM_VARIABLES = P4EST_DIM;


/**
 * \brief Vector of MHD_KT variables.
 *
 *  We have
 *       rho, rhoU, rhoV, rhoW, E, Bx, By, Bz .
 */
struct QdataVar
{
  
  double rho;
  double rhoU;
  double rhoV;
#ifdef P4_TO_P8
  double rhoW;
#endif // P4_TO_P8
  double E;
  double Bx;
  double By;
#ifdef P4_TO_P8
  double Bz;
#endif // P4_TO_P8  

  // Magnetic field centered face
  double Bxfy; //!< Value of Bx at face +y
  double Byfx; //!< Value of By at face +x   

  // divergence cleaning related variables
  double divB;
  double psi;

  // transport coefficients
  double lam_h;
  double eta_h;
  double lam_e;
   

  inline
  QdataVar() : rho(0.0)
             , rhoU(0.0)
	     , rhoV(0.0)
#ifdef P4_TO_P8
	     , rhoW(0.0)
#endif
             , E(0.0)
             , Bx(0.0)
             , By(0.0)
#ifdef P4_TO_P8
             , Bz(0.0)
#endif
             , Bxfy(0.0)
             , Byfx(0.0) 
             , divB(0.0)
             , psi(0.0)
             , lam_h(0.0)
             , eta_h(0.0)
             , lam_e(0.0)
  {}

  inline
  QdataVar &operator+=(const QdataVar& operand) {
    this->rho  += operand.rho;
    this->rhoU += operand.rhoU;
    this->rhoV += operand.rhoV;
#ifdef P4_TO_P8
    this->rhoW += operand.rhoW;
#endif
    this->E  += operand.E;
    this->Bx += operand.Bx;
    this->By += operand.By;
#ifdef P4_TO_P8
    this->Bz += operand.Bz;
#endif    
    this->psi += operand.psi;

    return *this;
  };
  
  inline
  QdataVar &operator-=(const QdataVar& operand) {
    this->rho  -= operand.rho;
    this->rhoU -= operand.rhoU;
    this->rhoV -= operand.rhoV;
#ifdef P4_TO_P8
    this->rhoW -= operand.rhoW;
#endif
    this->E    -= operand.E;
    this->Bx   -= operand.Bx;
    this->By   -= operand.By;
#ifdef P4_TO_P8
    this->Bz   -= operand.Bz;
#endif
    this->psi   -= operand.psi;
    return *this;
  }; // operator -=
  
  inline
  QdataVar &operator*=(double scale) {
    this->rho  *= scale;
    this->rhoU *= scale;
    this->rhoV *= scale;
#ifdef P4_TO_P8
    this->rhoW *= scale;
#endif
    this->E    *= scale;
    this->Bx   *= scale;
    this->By   *= scale;
#ifdef P4_TO_P8
    this->Bz   *= scale;
#endif
    this->psi   *= scale;
    return *this;
  }; // operator *=

  /// not really necessary, since operator *= exists
  inline
  QdataVar &operator/=(double scale) {
    this->rho  /= scale;
    this->rhoU /= scale;
    this->rhoV /= scale;
#ifdef P4_TO_P8
    this->rhoW /= scale;
#endif
    this->E    /= scale;
    this->Bx   /= scale;
    this->By   /= scale;
#ifdef P4_TO_P8
    this->Bz   /= scale;
#endif
    this->psi   /= scale;

    return *this;
  }; // operator /=

  inline  
  void set_zero() {
    this->rho  = 0;
    this->rhoU = 0;
    this->rhoV = 0;
#ifdef P4_TO_P8
    this->rhoW = 0;
#endif
    this->E    = 0;
    this->Bx   = 0;
    this->By   = 0;
#ifdef P4_TO_P8
    this->Bz   = 0;
#endif
    this->psi   = 0;
  }; // set_zero
  
}; // struct QdataVar

using QdataRecons = QdataVar;

#ifndef P4_TO_P8
inline void QDATA_INFOF(const QdataVar& w) {
  printf("rho %g | rhoU %g | rhoV %g | E %g | Bx %g | By %g | divB %g | psi %g | lam_h %g | eta_h %g | lam_e %g\n",
	 w.rho,w.rhoU,w.rhoV,w.E,w.Bx,w.By,w.divB,w.psi,w.lam_h,w.eta_h,w.lam_e);
}
#else // 3D
inline void QDATA_INFOF(const QdataVar& w) {
  printf("rho %g | rhoU %g | rhoV %g | rhoW %g | E %g | Bx %g | By %g | Bz %g | divB %g | psi %g | lam_h %g | eta_h %g | lam_e %g\n",
	 w.rho,w.rhoU,w.rhoV,w.rhoW,w.E,w.Bx,w.By,w.Bz,w.divB,w.psi,w.lam_h,w.eta_h,w.lam_e);
}
#endif // P4_TO_P8

/**
 * This is the main data structure (one by quadrant/cell).
 */
struct Qdata
{
  QdataVar   w;        /*!< variable at the current time */
  QdataVar   wnext;    /*!< variable at t + dt */
#ifdef P4_TO_P8
  QdataVar   dw[3];
#else
  QdataVar   dw[2]; /*!< Slope at interface */
#endif
  
}; // struct Qdata

} // namespace mhd_kt

} // namespace canop

#endif // USERDATA_MHD_KT_H_
