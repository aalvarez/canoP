#include "InitialConditionsFactoryMHD_KT.h"

#include "SolverMHD_KT.h"
#include "UserDataManagerMHD_KT.h"
#include "quadrant_utils.h"

#include "mhd_kt/userdata_mhd_kt.h"

namespace canop {

namespace mhd_kt {

using qdata_t               = Qdata; 
using qdata_variables_t     = QdataVar; 
using qdata_reconstructed_t = QdataRecons;

// =======================================================
// =======INITIAL CONDITIONS CALLBACKS====================
// =======================================================

/**************************************************************************/
/* MHD_KT : OT                                                            */
/**************************************************************************/
/**
 * Initial conditions is a Orszag-Tang vortex.
 *
 * \sa http://www.astro.princeton.edu/~jstone/Athena/tests/orszag-tang/pagesource.html
 *
 */
void
init_mhd_kt_ot (p4est_t * p4est,
		p4est_topidx_t which_tree, 
		p4est_quadrant_t * quad)
{

  SolverMHD_KT *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());
  
  // geometry (if NULL, it means cartesian by default)
  p4est_geometry_t *geom = solver->get_geom_compute();
  
  // get config reader
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  // now start genuine initialization
  qdata_variables_t   w;
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  double r;
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (r);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);
  
  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];
  
  // minus pi**2/nu
  double gamma = cfg->config_read_double ("settings.gamma0", 5.0/3);
  const double pi = M_PI;
  double p  = 5/(12*pi);
  double B0 = 1.0;
  double d0 = gamma * p; // should 25/(36*pi) when gamma 5/3

  
#ifdef USE_3D

  double kt = cfg->config_read_double ("orszag-tang.kt", 1.0);

  w.rho  = d0;
  w.rhoU = -d0 * sin(2*pi*y);
  w.rhoV =  d0 * sin(2*pi*x);
  w.rhoW =  0.0;
  w.Bx   = -B0 * sin(2*pi*y) * cos(2*pi*kt*z);
  w.By   =  B0 * sin(4*pi*x) * cos(2*pi*kt*z);
  w.Bz   = 0.0;
  w.E    =  p / (gamma-1.0) +
    0.5 * ( w.rhoU*w.rhoU + w.rhoV*w.rhoV + w.rhoW*w.rhoW)/w.rho +
    (w.Bx*w.Bx + w.By*w.By + w.Bz*w.Bz) / (8*pi);

  w.divB = 0.0;
  w.psi  = 0.0;
  w.lam_h=0.0;
  w.eta_h=0.0; 
  w.lam_e=0.0;
 
#else // 2D

  // Orszag-Tang
  w.rho  =  d0;
  w.rhoU = -d0 * sin(2*pi*y);
  w.rhoV =  d0 * sin(2*pi*x);
  w.Bx   = -B0 * sin(2*pi*y);
  w.By   =  B0 * sin(4*pi*x);
  w.E    =  p / (gamma-1.0) +
    0.5 * ( w.rhoU*w.rhoU + w.rhoV*w.rhoV)/w.rho +
    (w.Bx*w.Bx+w.By*w.By) / (8*pi);

  w.Bxfy = w.Bx;
  w.Byfx = w.By;
  w.divB=0.0;
  w.psi=0.0;
  w.lam_h=0.0;
  w.eta_h=0.0;
  w.lam_e=0.0;  

#endif // 2D/3D
  
  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_mhd_kt_ot

// *************************************************************************
// MHD_KT : Brio-Wu shock tube
// *************************************************************************
/**
 * Initial conditions is the BrioWu shock tube.
 *
 * \sa see http://flash.uchicago.edu/site/flashcode/user_support/flash4_ug_4p5/node182.html#SECTION010121000000000000000
 *
 */
void
init_mhd_kt_brio_wu (p4est_t * p4est,
		     p4est_topidx_t which_tree, 
		     p4est_quadrant_t * quad)
{

  SolverMHD_KT *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());
  
  // geometry (if NULL, it means cartesian by default)
  p4est_geometry_t *geom = solver->get_geom_compute();
  
  // get config reader
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  // now start genuine initialization
  qdata_variables_t   w;
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  double r;
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (r);
  
  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);
  
  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];
  
  // minus pi**2/nu
  double gamma = cfg->config_read_double ("settings.gamma0", 5.0/3);
  const double pi = M_PI;
  
  
#ifdef USE_3D

  // brio-Wu
  if ( ( x >= (1./3.) ) && ( x < (2./3.) )  ) {

    w.rho  =  1.0;
    w.rhoU =  0.0;
    w.rhoV =  0.0;
    w.rhoW =  0.0;
    w.Bx   =  0.75;
    w.By   =  1.0;
    w.Bz   =  0.0;
    w.E    =  1.0 / (gamma-1.0) +
      0.5 * ( w.rhoU*w.rhoU +
	      w.rhoV*w.rhoV +
	      w.rhoW*w.rhoW)/w.rho +
      (w.Bx*w.Bx +
       w.By*w.By +
       w.Bz*w.Bz) / (8*pi);
    
  } else {
    
    w.rho  =  0.125;
    w.rhoU =  0.0;
    w.rhoV =  0.0;
    w.rhoW =  0.0;
    w.Bx   =  0.75;
    w.By   = -1.0;
    w.E    =  0.1 / (gamma-1.0) +
      0.5 * ( w.rhoU*w.rhoU +
	      w.rhoV*w.rhoV)/w.rho +
      (w.Bx*w.Bx +
       w.By*w.By) / (8*pi);
  }

  // TODO : init Bxfy, etc ...
  
#else // 2D
  
  // brio-Wu
  if ( ( x >= (1./3.) ) && ( x < (2./3.) )  ) {

    w.rho  =  1.0;
    w.rhoU =  0.0;
    w.rhoV =  0.0;
    w.Bx   =  0.75;
    w.By   =  1.0;
    w.E    =  1.0 / (gamma-1.0) +
      0.5 * ( w.rhoU*w.rhoU +
	      w.rhoV*w.rhoV)/w.rho +
      (w.Bx*w.Bx +
       w.By*w.By) / (8*pi);
    
  } else {
    
    w.rho  =  0.125;
    w.rhoU =  0.0;
    w.rhoV =  0.0;
    w.Bx   =  0.75;
    w.By   = -1.0;
    w.E    =  0.1 / (gamma-1.0) +
      0.5 * ( w.rhoU*w.rhoU +
	      w.rhoV*w.rhoV)/w.rho +
      (w.Bx*w.Bx +
       w.By*w.By) / (8*pi);
  }
  
  w.Bxfy = w.Bx;
  w.Byfx = w.By;
  w.divB=0.0; 
  w.psi=0.0;
  w.lam_h=0.0;
  w.eta_h=0.0;
  w.lam_e=0.0;

#endif // 2D/3D
  
  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_mhd_kt_brio_wu

// *************************************************************************
// MHD_KT : rotor test
// *************************************************************************
/**
 * Initial conditions is the rotor test.
 *
 * Some references:
 * - Balsara and Spicer, 1999, JCP, 149, 270.
 * - G. Toth, "The div(B)=0 constraint in shock-capturing MHD codes",
 *   JCP, 161, 605 (2000).
 */
void
init_mhd_kt_rotor (p4est_t * p4est,
		   p4est_topidx_t which_tree, 
		   p4est_quadrant_t * quad)
{

  SolverMHD_KT *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());
  
  // geometry (if NULL, it means cartesian by default)
  p4est_geometry_t *geom = solver->get_geom_compute();
  
  // get config reader
  ConfigReader    *cfg = solver->m_cfg;
  UNUSED (cfg);

  // now start genuine initialization
  qdata_variables_t   w;
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  
  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);
  
  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];
  
  // minus pi**2/nu
  const double gamma   = cfg->config_read_double ("settings.gamma0", 5.0/3);

  // rotor problem parameters
  const double r0      = cfg->config_read_double("rotor.r0",0.1);
  const double r1      = cfg->config_read_double("rotor.r1",0.115);
  const double u0      = cfg->config_read_double("rotor.u0",1.0);
  const double p0      = cfg->config_read_double("rotor.p0",1.0);
  const double b0      = cfg->config_read_double("rotor.b0",5.0);
  const double xCenter = cfg->config_read_double("rotor.xc",0.5);
  const double yCenter = cfg->config_read_double("rotor.yc",0.5);

  // distance to center
  const double r = sqrt( (x-xCenter)*(x-xCenter) +
			 (y-yCenter)*(y-yCenter) );
  
  const double f_r = (r1-r)/(r1-r0);

#ifdef USE_3D

  // rotor
  if (r<=r0) {
    w.rho  =  10.0;
    w.rhoU = -w.rho*f_r*u0*(y-yCenter)/r0;
    w.rhoV =  w.rho*f_r*u0*(x-xCenter)/r0;
    w.rhoW =  0.0;
  } else if (r<=r1) {
    w.rho  = 1+9*f_r;
    w.rhoU = -w.rho*f_r*u0*(y-yCenter)/r;
    w.rhoV =  w.rho*f_r*u0*(x-xCenter)/r;
    w.rhoW =  0.0;
  } else {
    w.rho  = 1.0;
    w.rhoU = 0.0;
    w.rhoV = 0.0;
    w.rhoW = 0.0;
  }
  
  w.Bx   =  b0;
  w.By   =  0.0;
  w.Bz   =  0.0;
  w.E    =  p0 / (gamma-1.0) +
      0.5 * ( w.rhoU*w.rhoU +
	      w.rhoV*w.rhoV +
	      w.rhoW*w.rhoW)/w.rho +
    (w.Bx*w.Bx +
     w.By*w.By +
     w.Bz*w.Bz) / (8*M_PI);
  
  // TODO : init Bxfy, etc ...
  
#else // 2D
  
  // rotor
  if (r<=r0) {
    w.rho  =  10.0;
    w.rhoU = -w.rho*f_r*u0*(y-yCenter)/r0;
    w.rhoV =  w.rho*f_r*u0*(x-xCenter)/r0;
  } else if (r<=r1) {
    w.rho  = 1+9*f_r;
    w.rhoU = -w.rho*f_r*u0*(y-yCenter)/r;
    w.rhoV =  w.rho*f_r*u0*(x-xCenter)/r;
  } else {
    w.rho  = 1.0;
    w.rhoU = 0.0;
    w.rhoV = 0.0;
  }
  
  w.Bx   =  b0;
  w.By   =  0.0;
  w.E    =  p0 / (gamma-1.0) +
      0.5 * ( w.rhoU*w.rhoU +
	      w.rhoV*w.rhoV)/w.rho +
      (w.Bx*w.Bx +
       w.By*w.By) / (8*M_PI);
    
  
  w.Bxfy = w.Bx;
  w.Byfx = w.By;
  w.divB=0.0;
  w.psi=0.0;
  w.lam_h=0.0;
  w.eta_h=0.0;
  w.lam_e=0.0;
#endif // 2D/3D
  
  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_mhd_kt_rotor

/**************************************************************************/
/* MHD_KT : blast                                                         */
/**************************************************************************/
/**
 *
 * \brief blast
 *
 * The initial solution for rho is a disk which center is (xc,yc,zc) and 
 * radius R.
 * Initial velocity is zero.
 *
 * See http://www.astro.princeton.edu/~jstone/Athena/tests/blast/blast.html
 */
void
init_mhd_kt_blast (p4est_t * p4est,
		   p4est_topidx_t which_tree, 
		   p4est_quadrant_t * quad)
{

  SolverMHD_KT *solver = static_cast<SolverMHD_KT*>(solver_from_p4est (p4est));

  UserDataManagerMHD_KT
    *userdata_mgr = static_cast<UserDataManagerMHD_KT *> (solver->get_userdata_mgr());

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t   *geom        = solver->get_geom_compute();

  /* get config reader */
  ConfigReader    *cfg = solver->m_cfg;

  /* now start genuine initialization */
  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));
  
  // get the coordinates of the center of the quad
  double  XYZ[3] = { 0, 0, 0 };
  double  x,y,z;
  UNUSED (z);

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quad, XYZ);

  x = XYZ[0];
  y = XYZ[1];
  z = XYZ[2];

  // disk center, radius
  double x_c; 
  double y_c; 
  double z_c;
  double radius;
  
  x_c = cfg->config_read_double ("blast.x_c", 1.0);
  y_c = cfg->config_read_double ("blast.y_c", 1.0);
#ifdef USE_3D
  z_c = cfg->config_read_double ("blast.z_c", 1.0);
#endif
  UNUSED (z_c);

  radius = cfg->config_read_double ("blast.radius", 0.25);
  double radius2 = radius*radius;

  // density
  double density_in  = cfg->config_read_double ("blast.d_in",  1.0);
  double density_out = cfg->config_read_double ("blast.d_out", 1.0);

  // pressure
  double pressure_in  = cfg->config_read_double ("blast.p_in",  10.0);
  double pressure_out = cfg->config_read_double ("blast.p_out", 0.1);

  // velocity
  double velocity_x = cfg->config_read_double ("blast.velocity_x", 0.0);
  double velocity_y = cfg->config_read_double ("blast.velocity_y", 0.0);
  double velocity_z = cfg->config_read_double ("blast.velocity_z", 0.0);

  // magnetic field components
  double Bx = cfg->config_read_double ("blast.Bx", 0.0);
  double By = cfg->config_read_double ("blast.By", 0.0);
  double Bz = cfg->config_read_double ("blast.Bz", 0.0);

  UNUSED (velocity_z);

  const double gamma = cfg->config_read_double ("settings.gamma0", 5.0/3);
  const double pi = M_PI;

  double d2 = 
    SC_SQR(x-x_c) + 
    SC_SQR(y-y_c);
#ifdef USE_3D
  d2 += SC_SQR(z-z_c);
#endif

  if (d2 < radius2) {

    w.rho = density_in;
    w.rhoU = w.rho*velocity_x;
    w.rhoV = w.rho*velocity_y;
#ifdef USE_3D
    w.rhoW = w.rho*velocity_z;
#endif
    w.Bx = Bx;
    w.By = By;
#ifdef USE_3D
    w.Bz = Bz;
#endif
    
#ifdef USE_3D
    w.E    = pressure_in / (gamma-1.0) +
      0.5 * ( w.rhoU*w.rhoU + 
	      w.rhoV*w.rhoV + 
	      w.rhoW*w.rhoW ) / w.rho +
      (w.Bx*w.Bx + w.By*w.By + w.Bz*w.Bz) / (8*pi);
#else
    w.E    = pressure_in / (gamma-1.0) +
      0.5 * ( w.rhoU*w.rhoU + 
	      w.rhoV*w.rhoV ) / w.rho +
      (w.Bx*w.Bx + w.By*w.By            ) / (8*pi);
#endif

  } else {

    w.rho = density_out + 0.0 * (   ((double) (rand()) )/(RAND_MAX+1.0)*2-1  );
    w.rhoU = 0;
    w.rhoV = 0;
#ifdef USE_3D
    w.rhoW = 0;
#endif
    w.E = pressure_out / (gamma-1.0);
  }

  // finally set quadrant's userdata
  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);

} // init_ramses_blast

// =======================================================
// =======CLASS InitialConditionsFactoryMHD_KT IMPL ======
// =======================================================
InitialConditionsFactoryMHD_KT::InitialConditionsFactoryMHD_KT() :
  InitialConditionsFactory()
{

  // register the above callback's
  registerIC("ot" ,      init_mhd_kt_ot);
  registerIC("BrioWu",   init_mhd_kt_brio_wu);
  registerIC("rotor",    init_mhd_kt_rotor);
  registerIC("blast" ,	 init_mhd_kt_blast);
  
} // InitialConditionsFactoryMHD_KT

} // namespace mhd_kt

} // namespace canoP
