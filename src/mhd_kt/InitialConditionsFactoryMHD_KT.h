#ifndef INITIAL_CONDITIONS_FACTORY_MHD_KT_H_
#define INITIAL_CONDITIONS_FACTORY_MHD_KT_H_

#include "InitialConditionsFactory.h"
#include "mhd_kt/userdata_mhd_kt.h"

namespace canop {

namespace mhd_kt {

/**
 *
 */
class InitialConditionsFactoryMHD_KT : public InitialConditionsFactory
{

  using qdata_t = Qdata;
  
public:
  InitialConditionsFactoryMHD_KT();
  virtual ~InitialConditionsFactoryMHD_KT() {};
  
}; // class InitialConditionsFactoryMHD_KT

} // namespace mhd_kt

} // namespace canoP

#endif // INITIAL_CONDITIONS_FACTORY_MHD_KT_H_
