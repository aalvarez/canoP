#ifndef BOUNDARY_CONDITIONS_FACTORY_MHD_KT_H_
#define BOUNDARY_CONDITIONS_FACTORY_MHD_KT_H_

#include "BoundaryConditionsFactory.h"
#include "mhd_kt/userdata_mhd_kt.h"

namespace canop {

namespace mhd_kt {

/**
 *
 */
class BoundaryConditionsFactoryMHD_KT : public BoundaryConditionsFactory<Qdata>
{

  using qdata_t = Qdata;
  
public:
  BoundaryConditionsFactoryMHD_KT();
  virtual ~BoundaryConditionsFactoryMHD_KT() {};
  
}; // class BoundaryConditionsFactoryMHD_KT

} // namespace mhd_kt

} // namespace canoP

#endif // BOUNDARY_CONDITIONS_FACTORY_MHD_KT_H_
