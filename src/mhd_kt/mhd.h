#ifndef MHD_EQUATIONS_H_
#define MHD_EQUATIONS_H_
#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h"

#include "UserDataManagerMHD_KT.h"
#include "userdata_mhd_kt.h"

namespace canop {

namespace mhd_kt {


/**
 * enumerate methods of divergence
 */
enum div_b_cleaning_t {
  DIV_B_DIFFUSION,
  DIV_B_GLM,
  DIV_B_GLM_SUBCYCLED
};
  
// get userdata specific stuff
using qdata_t               = Qdata;
using qdata_variables_t     = QdataVar;
using qdata_reconstructed_t = QdataRecons;

using UserDataTypesMHD_KT = UserDataTypes<Qdata,
					  QdataVar,
					  QdataRecons>;

using QuadrantNeighborUtilsMHD_KT = QuadrantNeighborUtils<UserDataTypesMHD_KT>;

double  compute_pressure_mhd(const qdata_variables_t &qdata,
			     double gamma);
/**
 * Compute flux to update conservative variables.
 * 
 * UNUSED - TO BE REMOVED
 *
 * \note The diffusion method to clean divergence B is only available for 2D.
 * 
 * \param[in] primitive variables state
 * \param[out] output flux
 * \param[in] ch is a parameter only used if using GLM / GLM_SUBCYCLED
 * \param[in] cleaning is the type of divergence b cleaning (diffusion / GLM / GLM_SUBCYCLED)
 */
void compute_flux_mhd(const qdata_variables_t &qdata,
		      qdata_variables_t &flux,
		      int dir,
		      double gamma,
                      double ch,
		      div_b_cleaning_t cleaning);
/**
 * Compute flux to update conservative variables.
 *
 * \note if another divergence B cleaning (GLM or GLM_SUBCYCLED) is chosen, then magnetic field 
 * and psi will be modified elsewhere.
 */
void compute_flux_mhd_diffusive(const qdata_variables_t &qdata,
				qdata_variables_t &flux,
				int dir,
				double gamma);

/**
 * Compute flux to update magnetic field and psi.
 *
 * Only useful for divergence B cleaning (GLM or GLM_SUBCYCLED).
 */
void compute_flux_mhd_glm(const qdata_variables_t &qdata,
			  qdata_variables_t &flux,
			  int dir,
			  double gamma,
			  double ch);

} // namespace mhd_kt

} // namespace canoP

#endif // MHD_EQUATIONS_H_
