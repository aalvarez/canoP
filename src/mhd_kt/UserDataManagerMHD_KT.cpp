#include "UserDataManagerMHD_KT.h"
#include "canoP_base.h"
#include "eos.h"

namespace canop {

namespace mhd_kt {

using mhd_kt_getter_t = double (UserDataManagerMHD_KT ::*) (p4est_quadrant_t * q);

// ====================================================================
// ====================================================================
UserDataManagerMHD_KT::UserDataManagerMHD_KT(bool useP4estMemory) :
  UserDataManager<UserDataTypesMHD_KT>()
{

  UNUSED(useP4estMemory);
  
} // UserDataManagerMHD_KT::UserDataManagerMHD_KT

// ====================================================================
// ====================================================================
UserDataManagerMHD_KT::~UserDataManagerMHD_KT()
{

} // UserDataManagerMHD_KT::~UserDataManagerMHD_KT

// ====================================================================
// ====================================================================
void
UserDataManagerMHD_KT::qdata_set_variables (qdata_t * data,
					    qdatavar_t * w)
{

  memcpy (&(data->w), w, sizeof (qdatavar_t));
  memcpy (&(data->wnext), w, sizeof (qdatavar_t));
  memset (&(data->dw[IX]), 0, sizeof (qdatavar_t));
  memset (&(data->dw[IY]), 0, sizeof (qdatavar_t));
#ifdef P4_TO_P8
  memset (&(data->dw[IZ]), 0, sizeof (qdatavar_t));
#endif
 
} // UserDataManagerMHD_KT::qdata_set_variables

// ====================================================================
// ====================================================================
void
UserDataManagerMHD_KT::quadrant_set_state_data (p4est_quadrant_t * dst,
						p4est_quadrant_t * src)
{

  qdata_t *data_dst = quadrant_get_qdata (dst);
  qdata_t *data_src = quadrant_get_qdata (src);

  memcpy (&(data_dst->w),     &(data_src->w),     sizeof (qdatavar_t));
  memcpy (&(data_dst->wnext), &(data_src->wnext), sizeof (qdatavar_t));
  memcpy (&(data_dst->dw[IX]), &(data_src->dw[IX]), sizeof (qdatavar_t));
  memcpy (&(data_dst->dw[IY]), &(data_src->dw[IY]), sizeof (qdatavar_t));
#ifdef P4_TO_P8
  memcpy (&(data_dst->dw[IZ]), &(data_src->dw[IZ]), sizeof (qdatavar_t));
#endif

} // UserDataManagerMHD_KT::quadrant_set_state_variables

// ====================================================================
// ====================================================================
void
UserDataManagerMHD_KT::quadrant_compute_mean(p4est_quadrant_t *qmean,
					      p4est_quadrant_t *quadout[P4EST_CHILDREN])
{

  qdata_t     *child_data;
  qdatavar_t   mean; // initialized to zero
  
  for (int8_t i = 0; i < P4EST_CHILDREN; ++i) {

    child_data = quadrant_get_qdata (quadout[i]);
    mean.rho += child_data->w.rho / P4EST_CHILDREN;
    mean.rhoU += child_data->w.rhoU / P4EST_CHILDREN;
    mean.rhoV += child_data->w.rhoV / P4EST_CHILDREN;
#ifdef P4_TO_P8
    mean.rhoW += child_data->w.rhoW / P4EST_CHILDREN;
#endif
    mean.E += child_data->w.E / P4EST_CHILDREN;
    mean.Bx += child_data->w.Bx / P4EST_CHILDREN;
    mean.By += child_data->w.By / P4EST_CHILDREN;
#ifdef P4_TO_P8
    mean.Bz += child_data->w.Bz / P4EST_CHILDREN;
#endif
    mean.psi += child_data->w.psi / P4EST_CHILDREN;
    mean.divB += child_data->w.divB / P4EST_CHILDREN;
     mean.lam_h += child_data->w.lam_h / P4EST_CHILDREN; 
     mean.eta_h += child_data->w.eta_h / P4EST_CHILDREN;
     mean.lam_e += child_data->w.lam_e / P4EST_CHILDREN;

  }
  // Slopes will be computed at each time step , so no need to average them

  // finally, copy all of that into qmean
  quadrant_set_variables (qmean, &mean);

} // quadrant_compute_mean

// ====================================================================
// ====================================================================
void
UserDataManagerMHD_KT::qdata_print_variables (int loglevel, qdata_t * data)
{
  CANOP_LOGF (loglevel, "rho    = %-15.8lf %.8lf\n",
              data->w.rho, data->wnext.rho);
  CANOP_LOGF (loglevel, "rhoU    = %-15.8lf %.8lf\n",
              data->w.rhoU, data->wnext.rhoU);
  CANOP_LOGF (loglevel, "rhoV    = %-15.8lf %.8lf\n",
              data->w.rhoV, data->wnext.rhoV);
#ifdef P4_TO_P8
  CANOP_LOGF (loglevel, "rhoW    = %-15.8lf %.8lf\n",
              data->w.rhoW, data->wnext.rhoW);
#endif
  CANOP_LOGF (loglevel, "E    = %-15.8lf %.8lf\n",
              data->w.E, data->wnext.E);

  CANOP_LOGF (loglevel, "Bx    = %-15.8lf %.8lf\n",
              data->w.Bx, data->wnext.Bx);

  CANOP_LOGF (loglevel, "By    = %-15.8lf %.8lf\n",
              data->w.By, data->wnext.By);

#ifdef P4_TO_P8
  CANOP_LOGF (loglevel, "Bz    = %-15.8lf %.8lf\n",
              data->w.Bz, data->wnext.Bz);
#endif
  
  CANOP_LOGF (loglevel, "divB    = %-15.8lf %.8lf\n",
              data->w.divB, data->wnext.divB);
  
  CANOP_LOGF (loglevel, "psi    = %-15.8lf %.8lf\n",
              data->w.psi, data->wnext.psi);
  
  CANOP_LOGF (loglevel, "lam_h    = %-15.8lf %.8lf\n",
              data->w.lam_h, data->wnext.lam_h); 

  CANOP_LOGF (loglevel, "eta_h    = %-15.8lf %.8lf\n",
              data->w.eta_h, data->wnext.eta_h);

  CANOP_LOGF (loglevel, "lam_e    = %-15.8lf %.8lf\n",
              data->w.lam_e, data->wnext.lam_e);

} // UserDataManagerMHD_KT::qdata_print_variables

// ====================================================================
// ====================================================================
void
UserDataManagerMHD_KT::quadrant_copy_w_to_wnext (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  data->wnext = data->w;
  
} // UserDataManagerMHD_KT::quadrant_copy_w_to_wnext

// ====================================================================
// ====================================================================
void
UserDataManagerMHD_KT::quadrant_copy_wnext_to_w (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  data->w = data->wnext;
  
} // UserDataManagerMHD_KT::quadrant_copy_wnext_to_w

// ====================================================================
// ====================================================================
void
UserDataManagerMHD_KT::quadrant_copy_wnext_to_w_mag (p4est_quadrant_t * q)
{
  
  qdata_t *data = quadrant_get_qdata (q);

//   data->w.rho = data->wnext.rho;
//   data->w.rhoU = data->wnext.rhoU;
//   data->w.rhoV = data->wnext.rhoV;
// #ifdef P4_TO_P8
//   data->w.rhoW = data->wnext.rhoW;
// #endif
//   data->w.E = data->wnext.E;
  
  data->w.Bx = data->wnext.Bx;
  data->w.By = data->wnext.By;
#ifdef P4_TO_P8
  data->w.Bz = data->wnext.Bz;
#endif
  data->w.psi = data->wnext.psi;

//   data->w.Bxfy = data->wnext.Bxfy;
//   data->w.Byfx = data->wnext.Byfx;

  //data->w = data->wnext;

} // UserDataManagerMHD_KT::quadrant_copy_wnext_to_w_mag

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_rho (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.rho;

} // UserDataManagerMHD_KT::qdata_get_rho

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_Bxfy (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.Bxfy;

} // UserDataManagerMHD_KT::qdata_get_Bxfy

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_Byfx (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.Byfx;

} // UserDataManagerMHD_KT::qdata_get_Byfx



// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_rhoU (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.rhoU;
  
} // UserDataManagerMHD_KT::qdata_get_rhoU

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_rhoV (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.rhoV;
  
} // UserDataManagerMHD_KT::qdata_get_rhoV

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_rhoW (p4est_quadrant_t * q)
{
#ifdef P4_TO_P8
  qdata_t *data = quadrant_get_qdata (q);
  return data->w.rhoW;
#else
  UNUSED(q);
  return 0.0;
#endif
  
} // UserDataManagerMHD_KT::qdata_get_rhoW

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_E (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.E;

} // UserDataManagerMHD_KT::qdata_get_E

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_Bx (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.Bx;

} // UserDataManagerMHD_KT::qdata_get_Bx


// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_By (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.By;

} // UserDataManagerMHD_KT::qdata_get_By

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_Bz (p4est_quadrant_t * q)
{
#ifdef P4_TO_P8
  qdata_t *data = quadrant_get_qdata (q);
  return data->w.Bz;
#else
  UNUSED(q);
  return 0.0;
#endif

} // UserDataManagerMHD_KT::qdata_get_Bz

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_div_B (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.divB;

} // UserDataManagerMHD_KT::qdata_get_div_B

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_psi (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.psi;

} // UserDataManagerMHD_KT::qdata_get_psi

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_lam_h (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.lam_h;

} // UserDataManagerMHD_KT::qdata_get_lam_h

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_eta_h (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.eta_h;

} // UserDataManagerMHD_KT::qdata_get_eta_h

// ====================================================================
// ====================================================================
double
UserDataManagerMHD_KT::qdata_get_lam_e (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.lam_e;

} // UserDataManagerMHD_KT::qdata_get_lam_e



// ====================================================================
// ====================================================================
qdata_getter_t
UserDataManagerMHD_KT::qdata_getter_byname (const std::string &varName)
{

  qdata_getter_t base_getter = nullptr;
  mhd_kt_getter_t getter = nullptr;
  
  if (!varName.compare("rho")) {
    getter = &UserDataManagerMHD_KT::qdata_get_rho;
  }

  if (!varName.compare("rhoU")) {
    getter = &UserDataManagerMHD_KT::qdata_get_rhoU;
  }

  if (!varName.compare("rhoV")) {
    getter = &UserDataManagerMHD_KT::qdata_get_rhoV;
  }
  
  if (!varName.compare("rhoW")) {
    getter = &UserDataManagerMHD_KT::qdata_get_rhoW;
  }
  if (!varName.compare("E")) {
    getter = &UserDataManagerMHD_KT::qdata_get_E;
  }  
  if (!varName.compare("Bx")) {
    getter = &UserDataManagerMHD_KT::qdata_get_Bx;
  }
  if (!varName.compare("By")) {
    getter = &UserDataManagerMHD_KT::qdata_get_By;
  }
  if (!varName.compare("Bz")) {
    getter = &UserDataManagerMHD_KT::qdata_get_Bz;
  }
  if (!varName.compare("divB")) {
    getter = &UserDataManagerMHD_KT::qdata_get_div_B;
  }
  if (!varName.compare("psi")) {
    getter = &UserDataManagerMHD_KT::qdata_get_psi;
  }
  if (!varName.compare("lam_h")) {
    getter = &UserDataManagerMHD_KT::qdata_get_lam_h;
  }
  if (!varName.compare("eta_h")) {
    getter = &UserDataManagerMHD_KT::qdata_get_eta_h;
  }
  if (!varName.compare("lam_e")) {
    getter = &UserDataManagerMHD_KT::qdata_get_lam_e;
  }


  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_getter_t>(getter);

  } else {

    // print warning
    CANOP_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
    
  }
  
  return base_getter;
  
} // UserDataManagerMHD_KT::qdata_getter_byname


// ====================================================================
// ====================================================================
qdata_field_type
UserDataManagerMHD_KT::qdata_type_byname(const std::string &varName)
{

  if (!varName.compare("rho") ||
      !varName.compare("rhoU") ||
      !varName.compare("rhoV") ||
      !varName.compare("rhoW") ||
      !varName.compare("E") ||
      !varName.compare("Bx") ||
      !varName.compare("By") ||
      !varName.compare("Bz") ||
      !varName.compare("divB") ||
      !varName.compare("psi") ||
      !varName.compare("lam_h") ||
      !varName.compare("eta_h") ||
      !varName.compare("lam_e"))
    return QDATA_FIELD_SCALAR;

  // print warning
  P4EST_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
  return QDATA_FIELD_UNKNOWN;

} // UserDataManagerMHD_KT::qdata_type_byname

} // namespace mhd_kt

} // namespace canoP
