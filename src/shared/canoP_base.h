#ifndef CANOP_BASE_H_
#define CANOP_BASE_H_

#include <sc.h>

#ifdef P4EST_ENABLE_DEBUG
#define CANOP_ASSERT(c) SC_CHECK_ABORT ((c), "Assertion '" #c "'")
#define CANOP_EXECUTE_ASSERT_FALSE(expression)                          \
  do { int _canoP_i = (int) (expression);                               \
       SC_CHECK_ABORT (!_canoP_i, "Expected false: '" #expression "'"); \
  } while (0)
#define CANOP_EXECUTE_ASSERT_TRUE(expression)                           \
  do { int _canoP_i = (int) (expression);                               \
       SC_CHECK_ABORT (_canoP_i, "Expected true: '" #expression "'");   \
  } while (0)
#define CANOP_DEBUG_EXECUTE(expression)                 \
  do { (void) (expression); } while (0)
#else
#define CANOP_ASSERT(c) SC_NOOP ()
#define CANOP_EXECUTE_ASSERT_FALSE(expression)          \
  do { (void) (expression); } while (0)
#define CANOP_EXECUTE_ASSERT_TRUE(expression)           \
  do { (void) (expression); } while (0)
#define CANOP_DEBUG_EXECUTE(expression) SC_NOOP ()
#endif

/* macros for memory allocation, will abort if out of memory */
/** allocate a \a t-array with \a n elements */
#define CANOP_ALLOC(t,n)          (t *) sc_malloc (canoP_package_id,    \
                                                   (n) * sizeof(t))
/** allocate a \a t-array with \a n elements and zero */
#define CANOP_ALLOC_ZERO(t,n)     (t *) sc_calloc (canoP_package_id,    \
                                                   (size_t) (n), sizeof(t))
/** reallocate the \a t-array \a p with \a n elements */
#define CANOP_REALLOC(p,t,n)      (t *) sc_realloc (canoP_package_id,   \
                                                    (p), (n) * sizeof(t))
/** duplicate a string */
#define CANOP_STRDUP(s)                 sc_strdup (canoP_package_id, (s))
/** free an allocated array */
#define CANOP_FREE(p)                   sc_free (canoP_package_id, (p))

/* log helper macros */
#define CANOP_GLOBAL_LOG(p,s)                           \
  SC_GEN_LOG (canoP_package_id, SC_LC_GLOBAL, (p), (s))
#define CANOP_LOG(p,s)                                  \
  SC_GEN_LOG (canoP_package_id, SC_LC_NORMAL, (p), (s))
void                CANOP_GLOBAL_LOGF (int priority, const char *fmt, ...)
  __attribute__ ((format (printf, 2, 3)));
void                CANOP_LOGF (int priority, const char *fmt, ...)
  __attribute__ ((format (printf, 2, 3)));
#ifndef __cplusplus
#define CANOP_GLOBAL_LOGF(p,f,...)                                      \
  SC_GEN_LOGF (canoP_package_id, SC_LC_GLOBAL, (p), (f), __VA_ARGS__)
#define CANOP_LOGF(p,f,...)                                             \
  SC_GEN_LOGF (canoP_package_id, SC_LC_NORMAL, (p), (f), __VA_ARGS__)
#endif

/* convenience global log macros will only print if identifier <= 0 */
#define CANOP_GLOBAL_TRACE(s) CANOP_GLOBAL_LOG (SC_LP_TRACE, (s))
#define CANOP_GLOBAL_LDEBUG(s) CANOP_GLOBAL_LOG (SC_LP_DEBUG, (s))
#define CANOP_GLOBAL_VERBOSE(s) CANOP_GLOBAL_LOG (SC_LP_VERBOSE, (s))
#define CANOP_GLOBAL_INFO(s) CANOP_GLOBAL_LOG (SC_LP_INFO, (s))
#define CANOP_GLOBAL_STATISTICS(s) CANOP_GLOBAL_LOG (SC_LP_STATISTICS, (s))
#define CANOP_GLOBAL_PRODUCTION(s) CANOP_GLOBAL_LOG (SC_LP_PRODUCTION, (s))
#define CANOP_GLOBAL_ESSENTIAL(s) CANOP_GLOBAL_LOG (SC_LP_ESSENTIAL, (s))
#define CANOP_GLOBAL_LERROR(s) CANOP_GLOBAL_LOG (SC_LP_ERROR, (s))
void                CANOP_GLOBAL_TRACEF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_GLOBAL_LDEBUGF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_GLOBAL_VERBOSEF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_GLOBAL_INFOF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_GLOBAL_STATISTICSF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_GLOBAL_PRODUCTIONF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_GLOBAL_ESSENTIALF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_GLOBAL_LERRORF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
#ifndef __cplusplus
#define CANOP_GLOBAL_TRACEF(f,...)                      \
  CANOP_GLOBAL_LOGF (SC_LP_TRACE, (f), __VA_ARGS__)
#define CANOP_GLOBAL_LDEBUGF(f,...)                     \
  CANOP_GLOBAL_LOGF (SC_LP_DEBUG, (f), __VA_ARGS__)
#define CANOP_GLOBAL_VERBOSEF(f,...)                    \
  CANOP_GLOBAL_LOGF (SC_LP_VERBOSE, (f), __VA_ARGS__)
#define CANOP_GLOBAL_INFOF(f,...)                       \
  CANOP_GLOBAL_LOGF (SC_LP_INFO, (f), __VA_ARGS__)
#define CANOP_GLOBAL_STATISTICSF(f,...)                         \
  CANOP_GLOBAL_LOGF (SC_LP_STATISTICS, (f), __VA_ARGS__)
#define CANOP_GLOBAL_PRODUCTIONF(f,...)                         \
  CANOP_GLOBAL_LOGF (SC_LP_PRODUCTION, (f), __VA_ARGS__)
#define CANOP_GLOBAL_ESSENTIALF(f,...)                          \
  CANOP_GLOBAL_LOGF (SC_LP_ESSENTIAL, (f), __VA_ARGS__)
#define CANOP_GLOBAL_LERRORF(f,...)                     \
  CANOP_GLOBAL_LOGF (SC_LP_ERROR, (f), __VA_ARGS__)
#endif
#define CANOP_GLOBAL_NOTICE     CANOP_GLOBAL_STATISTICS
#define CANOP_GLOBAL_NOTICEF    CANOP_GLOBAL_STATISTICSF

/* convenience log macros that are active on every processor */
#define CANOP_TRACE(s) CANOP_LOG (SC_LP_TRACE, (s))
#define CANOP_LDEBUG(s) CANOP_LOG (SC_LP_DEBUG, (s))
#define CANOP_VERBOSE(s) CANOP_LOG (SC_LP_VERBOSE, (s))
#define CANOP_INFO(s) CANOP_LOG (SC_LP_INFO, (s))
#define CANOP_STATISTICS(s) CANOP_LOG (SC_LP_STATISTICS, (s))
#define CANOP_PRODUCTION(s) CANOP_LOG (SC_LP_PRODUCTION, (s))
#define CANOP_ESSENTIAL(s) CANOP_LOG (SC_LP_ESSENTIAL, (s))
#define CANOP_LERROR(s) CANOP_LOG (SC_LP_ERROR, (s))
void                CANOP_TRACEF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_LDEBUGF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_VERBOSEF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_INFOF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_STATISTICSF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_PRODUCTIONF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_ESSENTIALF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
void                CANOP_LERRORF (const char *fmt, ...)
  __attribute__ ((format (printf, 1, 2)));
#ifndef __cplusplus
#define CANOP_TRACEF(f,...)                     \
  CANOP_LOGF (SC_LP_TRACE, (f), __VA_ARGS__)
#define CANOP_LDEBUGF(f,...)                    \
  CANOP_LOGF (SC_LP_DEBUG, (f), __VA_ARGS__)
#define CANOP_VERBOSEF(f,...)                   \
  CANOP_LOGF (SC_LP_VERBOSE, (f), __VA_ARGS__)
#define CANOP_INFOF(f,...)                      \
  CANOP_LOGF (SC_LP_INFO, (f), __VA_ARGS__)
#define CANOP_STATISTICSF(f,...)                        \
  CANOP_LOGF (SC_LP_STATISTICS, (f), __VA_ARGS__)
#define CANOP_PRODUCTIONF(f,...)                        \
  CANOP_LOGF (SC_LP_PRODUCTION, (f), __VA_ARGS__)
#define CANOP_ESSENTIALF(f,...)                         \
  CANOP_LOGF (SC_LP_ESSENTIAL, (f), __VA_ARGS__)
#define CANOP_LERRORF(f,...)                    \
  CANOP_LOGF (SC_LP_ERROR, (f), __VA_ARGS__)
#endif
#define CANOP_NOTICE            CANOP_STATISTICS
#define CANOP_NOTICEF           CANOP_STATISTICSF

/* extern declarations */
/** the libsc package id for canoP (set in canoP_init()) */
extern int canoP_package_id;

static inline void
canoP_log_indent_push ()
{
  sc_log_indent_push_count (canoP_package_id, 1);
}

static inline void
canoP_log_indent_pop ()
{
  sc_log_indent_pop_count (canoP_package_id, 1);
}

/** Registers canoP with the SC Library and sets the logging behavior.
 * This function is optional.
 * This function must only be called before additional threads are created.
 * If this function is not called or called with log_handler == NULL,
 * the default SC log handler will be used.
 * If this function is not called or called with log_threshold == SC_LP_DEFAULT,
 * the default SC log threshold will be used.
 * The default SC log settings can be changed with sc_set_log_defaults ().
 */
void                canoP_init (sc_log_handler_t log_handler,
                                int log_threshold);

#endif // CANOP_BASE_H_
