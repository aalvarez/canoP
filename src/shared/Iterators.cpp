#include "Iterators.h"

#include "Solver.h"
#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h" // for quadrant_print, quadrant_length

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_mesh.h>
#include <p4est_geometry.h>
#else
#include <p8est_bits.h>
#include <p8est_mesh.h>
#include <p8est_geometry.h>
#endif

#include <sc.h>

constexpr int IT_ID::QUADRANT_PRINT;
constexpr int IT_ID::COMPUTE_MEAN_VALUES;
constexpr int IT_ID::GATHER_STATISTICS;
constexpr int IT_ID::START_STEP;
constexpr int IT_ID::END_STEP;
constexpr int IT_ID::MARK_ADAPT;
constexpr int IT_ID::CFL_TIME_STEP;
constexpr int IT_ID::RECONSTRUCT_GRADIENT;
constexpr int IT_ID::TRACE;
constexpr int IT_ID::UPDATE;
constexpr int IT_ID::GRAVITY_SOURCE_TERM;
constexpr int IT_ID::LEVEL_STATISTICS;

// =======================================================
// =======================================================
IteratorsBase::IteratorsBase()
{

  // provide a default list of iterators
  fill_iterators_list();
  
} // IteratorsBase::IteratorsBase

// =======================================================
// =======================================================
IteratorsBase::~IteratorsBase()
{

} // IteratorsBase::~IteratorsBase

// =======================================================
// =======================================================
void IteratorsBase::fill_iterators_list()
{
  
  // provide default values for some iterators (for those that do
  // not depend on qdata_t)
  iteratorsList[IT_ID::QUADRANT_PRINT]   = iterator_quadrant_print;
  iteratorsList[IT_ID::LEVEL_STATISTICS] = iterator_level_statistics;

} // IteratorsBase::fill_iterators_list

// =======================================================
// =======================================================
void iterator_quadrant_print (p4est_iter_volume_info_t * info,
			      void *user_data)
{

  UNUSED(user_data);
  quadrant_print (SC_LP_ERROR, info->quad);

} // iterator_quadrant_print

void iterator_level_statistics (p4est_iter_volume_info_t * info,
                                void *user_data)
{
  Solver *solver = solver_from_p4est (info->p4est);
  unsigned long *nquads = static_cast<unsigned long *>(user_data);

  nquads[info->quad->level - solver->m_min_refine] += 1;
} // iterator_level_statistics
