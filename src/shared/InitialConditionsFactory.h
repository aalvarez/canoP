#ifndef INITIAL_CONDITIONS_FACTORY_H_
#define INITIAL_CONDITIONS_FACTORY_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

#include <map>

#include "canoP_base.h"

/**
 * \brief Function type to initialize one of the scalars.
 * Only used with Advection_Upwind and Bifluid solver.
 */
using scalar_init_t = double (*) (double x, double y, double z, double t);

/**
 * \brief Function type to initialize a 2D or 3D vector.
 * Only used with Advection_Upwind and Bifluid solver.
 */
using vector_init_t = void   (*) (double x, double y, double z,
				  double u[P4EST_DIM]);

/**
 * An abstract base class to define a common interface for concrete initial
 * conditions factories.
 *
 * The main purpose is to return an IC_Callback, i.e. a function pointer
 * that will be passed to p4est later.
 *
 * The idea here it to define a map between a name and the actual
 * initial condition function defined by the function pointer type 
 * IC_Callback.
 *
 * Each derived class will have to define and register its own IC_Callback's.
 *
 * The IC_CallbackMap is empty, and supposed to be filled in the concrete 
 * derived class.
 */
class InitialConditionsFactory {

public:
  InitialConditionsFactory() {};
  virtual ~InitialConditionsFactory() {};
  
  /**
   * \brief Define IC_Callback as a function pointer.
   *
   * Just an alias to p4est_init_t.
   *
   * Initial conditions functions: compute a local value used to provide
   * userdata to a (leaf) quadrant.
   *
   */
  // using IC_Callback = void (*) (p4est_t * p4est,
  // 				p4est_topidx_t which_tree,
  // 				p4est_quadrant_t * q);
  using IC_Callback = p4est_init_t;

  
private:
  /**
   * Map to associate a label with an initial condition function pointer.
   */
  using IC_CallbackMap = std::map<std::string, IC_Callback>;
  IC_CallbackMap m_callbackMap;
  
public:
  /**
   * Routine to insert a IC function pointer into the map.
   * Note that this register function can be used to serve 
   * at least two different purposes:
   * - in the concrete factory: register existing callback's
   * - in some client code, register a callback from a plugin code, at runtime.
   */
  void registerIC(const std::string& key, IC_Callback cb) {
    m_callbackMap[key] = cb;
  };

  /**
   * \brief Retrieve one of the possible IC function pointer by name.
   *
   * Allowed default names are defined in the concrete factory.
   */
  virtual IC_Callback callback_byname (const std::string &name) {
    // find the initial conditions function pointer in the register map
    typename IC_CallbackMap::iterator it = m_callbackMap.find(name);
    
    // if found, just return it
    if ( it != m_callbackMap.end() )
      return it->second;
    
    // if not found, return null pointer
    // it is the responsability of the client code to deal with
    // the possibility to have a nullptr callback (does nothing).
    //
    // Note that the concrete factory may chose a different behavior
    // and return a valid default callback (why not ?).
    CANOP_GLOBAL_PRODUCTION  ("#### WARNING: ####\n");
    CANOP_GLOBAL_PRODUCTIONF ("%s: is not recognized as a valid initial condition key.\n",name.c_str());
    CANOP_GLOBAL_PRODUCTION  ("#### WARNING: ####\n");
    return nullptr;
  };
  
}; // class InitialConditionsFactory

#endif // INITIAL_CONDITIONS_FACTORY_H_
