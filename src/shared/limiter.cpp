#include "limiter.h"
#include "ScalarLimiterFactory.h"

// ===============================================
// ===============================================
double compute_phi_van_albada1(double r)
{
  
  double phi = 0.0;
  
  if (r<=0.0) {
    phi = 0.0;
  } else {
    phi = (r*r + r)/(r*r + 1); //van Albada 1
  }

  return phi;
  
} // compute_phi_van_albada1

// ===============================================
// ===============================================
double compute_phi_ospre(double r)
{
  
  double phi = 0.0;
  
  if (r<=0.0) {
    phi = 0.0;
  } else {
    phi = 1.5*(r*r+r)/(r*r+r+1); // ospre
  }

  return phi;
  
} // compute_phi_ospre

// ===============================================
// ===============================================
double compute_phi_charm(double r)
{
  
  double phi = 0.0;
  
  if(r<=0.0) {
    phi = 0.0;
  } else {
    phi = r*(3.*r+1.)/((r+1.)*(r+1.));
  }

  return phi;
  
} // compute_phi_charm

// =======================================================
// =======================================================
template<>
ScalarLimiterFactoryTmpl<regular_limiter_t>::ScalarLimiterFactoryTmpl()
{

  registerLimiter("vanalbada1",  compute_phi_van_albada1);
  registerLimiter("vanalbada",   compute_phi_van_albada1); // alias
  registerLimiter("ospre",       compute_phi_ospre);
  registerLimiter("charm",       compute_phi_charm);

} // ScalarLimiterFactoryTmpl<regular_limiter_t> constructor
