#include <IndicatorFactory.h>

#include <cmath>

// we'll have to figure out later, if in c++11, it's better
// to use the std namespace for math function.

// =======================================================
// ==== Some Utilities ===================================
// =======================================================

// =======================================================
// =======================================================
double
indicator_scalar_gradient (double qi, double qj)
{

  double max = fmax (fabs (qi), fabs (qj));

  if (max < 0.001) {
    return 0;
  }

  max = fabs (qi - qj) / max;
  return fmax (fmin (max, 1.0), 0.0);

} // indicator_scalar_gradient


// =======================================================
// ==== CLASS IndicatorFactory IMPL ======================
// =======================================================

// everything is in IndicatorFactory.h
