#ifndef QUADRANT_UTILS_H
#define QUADRANT_UTILS_H

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#include <p4est_connectivity.h>
#include <p4est_geometry.h>
#include <p4est_iterate.h>
#include <p4est_mesh.h>
#else
#include <p8est.h>
#include <p8est_connectivity.h>
#include <p8est_geometry.h>
#include <p8est_iterate.h>
#include <p8est_mesh.h>
#endif

#include "utils.h"

enum cell_location_t {
  CELL_CURRENT = 0,
  CELL_NEIGHBOR = 1
};

enum direction_t {
  DIR_X=0,
  DIR_Y=1,
  DIR_Z=2
};

enum face_id_t {
  FACE_LEFT = 0,
  FACE_RIGHT = 1,
  FACE_XMIN = 0,
  FACE_XMAX = 1,
  FACE_YMIN = 2,
  FACE_YMAX = 3,
  FACE_ZMIN = 4,
  FACE_ZMAX = 5
};

#ifndef USE_3D
/** Transform a quadrant coordinate into the space spanned by tree vertices and taking geometry into account.
 * \param [in] connectivity     Connectivity must provide the vertices.
 * \param [in] geom             Geometry.
 * \param [in] treeid           Identify the tree that contains x, y.
 * \param [in] x, y             Quadrant coordinates relative to treeid.
 * \param [out] vxyz            Transformed coordinates in vertex space.
 */
void
p4est_qcoord_to_vertex_with_geom (p4est_connectivity_t * conn,
				  p4est_geometry_t * geom,
				  p4est_topidx_t treeid,
				  p4est_qcoord_t x,
				  p4est_qcoord_t y,
				  double vxyz[3]);
#else
/** Transform a quadrant coordinate into the space spanned by tree vertices and taking geometry into account.
 * \param [in] connectivity     Connectivity must provide the vertices.
 * \param [in] geom             Geometry.
 * \param [in] treeid           Identify the tree that contains x, y.
 * \param [in] x, y             Quadrant coordinates relative to treeid.
 * \param [out] vxyz            Transformed coordinates in vertex space.
 */
void
p8est_qcoord_to_vertex_with_geom (p4est_connectivity_t * conn,
				  p4est_geometry_t * geom,
				  p4est_topidx_t treeid,
				  p4est_qcoord_t x,
				  p4est_qcoord_t y,
				  p4est_qcoord_t z,
				  double vxyz[3]);


#define p4est_qcoord_to_vertex_with_geom p8est_qcoord_to_vertex_with_geom
#endif /* USE_3D */

/**
 * \brief A variant of the print function offered by p4est.
 *
 * Unlike the p4est_quadrant_print() function, we write the (x, y, z)
 * coordinates not shifted to the left. This allows for easier identification
 * of the quarant in the mesh because in this form, (x, y, z) are the
 * coordinates of the quadrant as if the mesh was uniformely refined
 * at the level of the given quadrant.
 *
 * \param q The quadrant.
 */
void
quadrant_print (int log_priority, p4est_quadrant_t * q);

/**
 * \brief Compute the real length of a quadrant.
 *
 * This function will compute dx (= dy).
 *
 * NOTE: This function assumes the tree in which the quadrant resides is
 * not distorted in any way.
 *
 * \param [in] quad A quadrant.
 * \return The length (height and width) of the quadrant.
 */
double
quadrant_length (p4est_quadrant_t * quad);

/**
 * \brief Compute the real length of a quadrant in cylindrical coordinates.
 *
 * This function will compute dx, dy and dz (actually : dr, r*dtheta and dz).
 *
 * \param [in] quad A quadrant.
 * \param [in] geom the geometry
 * \param [out] dx  radial length
 * \param [out] dy  orthoradial length
 * \param [out] dz  length along z axis
 */
void
quadrant_length_dxdydz_cylindrical (p4est_quadrant_t * quad,
				    p4est_topidx_t     which_tree,
				    p4est_geometry_t * geom,
				    double           * dx,
				    double           * dy,
				    double           * dz);

/**
 * \brief Compute the real length of a quadrant based on its level.
 *
 * This function will compute dx (= dy).
 *
 * NOTE: This function assumes the tree in which the quadrant resides is
 * not distorted in any way.
 *
 * \param [in] level The level of a quadrant.
 * \return The length (height and width) of the quadrant.
 */
double
quadrant_length_level (int level);

/**
 * No Comment.
 */
double
quadrant_length_ext (p4est_connectivity_t * c,
		     p4est_geometry_t * geom,
		     p4est_quadrant_t * q,
		     p4est_topidx_t treeid, int face);

/**
 * Get dx,dy,dz for a given quad.
 */
void
quadrant_get_dx_dy_dz (p4est_connectivity_t * conn,
		       p4est_geometry_t * geom,
		       p4est_quadrant_t * quad,
		       p4est_topidx_t which_tree,
		       double *dx,
		       double *dy,
		       double *dz);

#ifndef P4_TO_P8
/**
 * \brief Compute the real coordinates of the 2 corners of a given face.
 *
 * The first corner is v[0..2] and the second one is v[3..5]. They are given
 * in z-order, as defined by p4est_face_corners in p4est_connectivity.h
 *
 * \param[in] face faceId in [0..3] used as 1st entry in p4est_face_corners
 * \param[out] v   vertex coordinates in real space of each corner
 * 
 * \sa p4est_quadrant_corner_node
 *
 * \note Remember that p4est_face_corners gives cornerId in z-order,
 * so that 2 opposite faces will have the "same orientation".
 */
void
quadrant_face_vertices (p4est_connectivity_t * c,
			p4est_geometry_t * geom,
			p4est_quadrant_t * q,
			p4est_topidx_t treeid,
			int face, double v[6]);
#else
/**
 * \brief Compute the real coordinates of the 4 corners of a given face.
 *
 * The first corner is v[0..2], the second one is v[3..5], ...
 * They are given in z-order, as defined by p8est_face_corners in 
 * p8est_connectivity.h
 *
 * \param[in] face faceId in [0..5] used as 1st entry in p8est_face_corners
 * \param[out] v   vertex coordinates in real space of each corner
 *
 * \sa p8est_quadrant_corner_node
 *
 * \note Remember that p4est_face_corners gives cornerId in z-order,
 * so that 2 opposite faces will have the "same orientation".
 */
void
quadrant_face_vertices (p4est_connectivity_t * c,
			p4est_geometry_t * geom,
			p4est_quadrant_t * q,
			p4est_topidx_t treeid,
			int face, double v[12]);

#endif /* P4_TO_P8 */

/**
 * \brief Compute the real length of a face.
 */
double
quadrant_length_face (p4est_connectivity_t * c,
		      p4est_quadrant_t * q,
		      p4est_topidx_t treeid, int face);

/**
 * \brief Compute the normal to the given face, orientation along the z-order direction.
 *
 *     ^
 *  ___|___
 * |       |
 * |=>     |=>
 * |   ^   |
 * |___|___|
 * 
 *
 *  --- ---
 * | 0 | 1 |
 *  --- ---
 * ------------------> x
 * 
 *  Let's consider the common face between quad0 and quad1.
 * The normal to that face computed using either quad0 or quad1, will be same
 * along the "increasing" direction (z-order), here "x".
 */
void
quadrant_face_normal (p4est_connectivity_t * c,
		      p4est_geometry_t * geom,
		      p4est_quadrant_t * q,
		      p4est_topidx_t treeid,
		      int face, double n[3]);

/**
 * \brief Compute the coordinates of the center of a quadrant.
 *
 * Uses the p4est_qcoord_to_vertex() function to compute the coordinates
 * of the lower left corner and the quadrant_length() function to compute
 * the length of the quadrant. The center is then:
 *          x_c = x_0 + h / 2
 *
 * \param [in]  connectivity The connectivity of the tree.
 * \param [in]  geom         The geometry (mapping between logical and
 *                           physical space)
 * \param [in]  tree         The tree id of the tree quad belongs to.
 * \param [in]  quad         A quadrant.
 * \param [out] xyz          The coordinates of the center.
 */
void
quadrant_center_vertex (p4est_connectivity_t *connectivity,
			p4est_geometry_t *geom,
			p4est_topidx_t tree,
			p4est_quadrant_t * quad,
			double xyz[3]);

/**
 * \brief Compute the coordinates of a vertex of a quadrant.
 *
 * Uses the p4est_qcoord_to_vertex() function to compute the coordinates
 * of the lower left corner and the quadrant_length() function to compute
 * the length of the quadrant.
 *
 * \param [in]  connectivity The connectivity of the tree.
 * \param [in]  geom         The geometry (mapping between logical and
 *                           physical space)
 * \param [in]  tree         The tree id of the tree quad belongs to.
 * \param [in]  quad         A quadrant.
 * \param [in]  vertexId     The vertex Id (from 0 to 2^d-1 in z-order). 
 * \param [out] xyz          The coordinates of the center.
 */
void
quadrant_vertex_coord (p4est_connectivity_t *connectivity,
		       p4est_geometry_t *geom,
		       p4est_topidx_t tree,
		       p4est_quadrant_t * quad,
		       int              vertexId,
		       double xyz[3]);

/**
 * \brief Compute the coordinates of the center of a face of a quadrant.
 *
 * \sa quadrant_center_vertex
 *
 * \param [in]  connectivity The connectivity of the tree.
 * \param [in]  geom         The geometry (mapping between logical and
 *                           physical space)
 * \param [in]  tree         The tree id of the tree quad belongs to.
 * \param [in]  quad         A quadrant.
 * \param [in]  faceId       Identify which face we're dealing with.
 * \param [out] xyz          The coordinates of the face center.
 */
void
quadrant_face_center (p4est_connectivity_t *connectivity,
		      p4est_geometry_t *geom,
		      p4est_topidx_t tree,
		      p4est_quadrant_t * quad,
		      face_id_t faceId,
		      double xyz[3]);

/**
 * \brief Get the level of a side.
 *
 * A side can contain either a full quadrant or two hanging quadrants of
 * the same level.
 *
 * \param [in] side The side.
 * \return The level on that side.
 */
int
face_side_level (p4est_iter_face_side_t * side);

/**
 * return integer coordinates in unit of the level quadrant length.
 */
void
quadrant_get_level_integer_index (p4est_quadrant_t * quad,
				  int ijk[3]);

/**
 * return cell face area. Handle 2D/3D.
 */
double
quadrant_face_area(double dx, double dy, double dz, int direction);

/**
 * return cell face area in the general case (unstructured mesh). Handle 2D/3D.
 */
double
quadrant_face_area_general(p4est_connectivity_t * c,
			   p4est_geometry_t * geom,
			   p4est_quadrant_t * q,
			   p4est_topidx_t treeid,
			   int faceId);

/**
 * return cell volume in cartesian regular geometry. Handle 2D/3D.
 */
double
quadrant_cell_volume(double dx, double dy, double dz);

/**
 * return cell volume in the general case (unstructured mesh). Handle 2D/3D.
 *
 * - in 2D : divide quadrangle into 2 triangles
 * - in 3D : divide quadrangle into 5 tetrahedrons
 */
double
quadrant_cell_volume_general(p4est_connectivity_t * c,
			     p4est_geometry_t * geom,
			     p4est_quadrant_t * q,
			     p4est_topidx_t treeid);

#endif /* QUADRANT_UTILS_H */
