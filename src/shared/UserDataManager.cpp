#include "UserDataManager.h"

#include "Solver.h"

// =======================================================
// =======================================================
UserDataManagerBase::UserDataManagerBase()
{

} // UserDataManagerBase::UserDataManagerBase

// =======================================================
// =======================================================
UserDataManagerBase::~UserDataManagerBase()
{

} // UserDataManagerBase::~UserDataManagerBase

// =======================================================
// =======================================================
void *
UserDataManagerBase::get_ghost_data_ptr()
{

  return nullptr;

} // UserDataManagerBase::get_ghost_data_ptr

// =======================================================
// =======================================================
void
UserDataManagerBase::ghost_data_allocate(int new_size)
{

  UNUSED(new_size);
  
  // will be implemented in derived class
  
} // UserDataManagerBase::ghost_data_allocate

