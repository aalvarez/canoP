#ifndef USERDATA_MANAGER_H_
#define USERDATA_MANAGER_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

#include "utils.h" // for UNUSED macro
#include "quadrant_utils.h" // for quadrant_print

#include "ScalarLimiterFactory.h" // for scalar_limiter_t type
using scalar_limiter_t = ScalarLimiterFactory::scalar_limiter_t;

#include "UserDataTypes.h"

#include <vector> // for ghost_data vector

/**
 * A generic type for a function pointer returning a field of a quadrant data.
 *
 * TODO
 */
class UserDataManagerBase; // forward declaration
using qdata_getter_t = double (UserDataManagerBase::*) (p4est_quadrant_t * q);

/**
 * Another function pointer, filling in a vector
 */
using qdata_vector_getter_t = void (UserDataManagerBase::*) (p4est_quadrant_t * q, size_t dim, double * val);

/**
 * The supported field types for quadrant data.
 */
enum qdata_field_type
{
  QDATA_FIELD_UNKNOWN,
  QDATA_FIELD_SCALAR,
  QDATA_FIELD_VECTOR
};

/**
 * Define an empty base class, non-template.
 */
class UserDataManagerBase {

public:
  UserDataManagerBase();
  virtual ~UserDataManagerBase();
  
  virtual void quadrant_set_state_data (p4est_quadrant_t * dst,
					p4est_quadrant_t * src) = 0;

  virtual void quadrant_compute_mean(p4est_quadrant_t *qmean,
				     p4est_quadrant_t *quadout[]) = 0;

  // ghost data array pointer (re-allocated every time p4est mesh distribution changes) 
  virtual void * get_ghost_data_ptr();
  virtual void   ghost_data_allocate(int new_size);

  /**
   * Return a pointer to a UserDataManager member function used to get a field value of a qdata_t struct.
   */
  virtual qdata_getter_t qdata_getter_byname(const std::string &varName) { UNUSED(varName); return nullptr; };

  /**
   * Return a pointer to a UserDataManager member function used to get a vector field value of a qdata_t struct.
   */
  virtual qdata_vector_getter_t qdata_vgetter_byname(const std::string &varName) { UNUSED(varName); return nullptr; };

  /**
   * Return the field type for the given variable.
   */
  virtual qdata_field_type qdata_type_byname(const std::string &varName) { UNUSED(varName); return QDATA_FIELD_UNKNOWN; };

}; // UserDataManagerBase

/**
 *
 * UserData manager template class.
 *
 * template parameter should be a typelist object (struct UserDataTypes)
 * we expected this class to provides at least these two types.
 */
template<typename someUserDataTypes>
class UserDataManager : public UserDataManagerBase
{

public:
  using qdata_t        = typename someUserDataTypes::qdata_t;
  using qdatavar_t     = typename someUserDataTypes::qdatavar_t;
  using qdatarecons_t  = typename someUserDataTypes::qdatarecons_t;
  
public:
  UserDataManager() : UserDataManagerBase() {};
  virtual ~UserDataManager() {};

  std::vector<qdata_t> m_ghost_data;
  
  void * get_ghost_data_ptr() { return (void *) &(m_ghost_data[0]); };

  virtual void ghost_data_allocate(int new_size) {
    // resize a std::vector
    m_ghost_data.resize(new_size);
  };
  
  /*
   * some utility methods.
   */

  /*
   * default implementation assuming the use of p4est internal
   * memory pool for userdata (one qdata_t variable per leaf quadrant).
   */

  /**
   * Return a qdata_t struct holding a copy of data located at quadrant q.
   */
  virtual qdata_t* quadrant_get_qdata (p4est_quadrant_t * q)
  {
    
    return (qdata_t *) q->p.user_data;
    
  } // quadrant_get_qdata


  /**
   * Set qdata_t variables at location specified by quadrant q.
   */
  virtual void quadrant_set_qdata (p4est_quadrant_t * dst, qdata_t * src) {

    qdata_t *dst_data = quadrant_get_qdata (dst);
    memcpy (dst_data, src, sizeof (qdata_t));
    
  } // quadrant_set_qdata


  /**
   * Copy qdata associated to src quadrant into the one associated to
   * dst quadrant.
   */
  virtual void quadrant_copy_qdata (p4est_quadrant_t * dst,
				    p4est_quadrant_t * src) {
    
    qdata_t *src_data = quadrant_get_qdata (src);
    qdata_t *dst_data = quadrant_get_qdata (dst);
    
    memcpy (dst_data, src_data, sizeof (qdata_t));
    
  } // quadrant_copy_qdata


  /**
   * Print quadrant qdata.
   */
  virtual void quadrant_print_qdata (int loglevel, p4est_quadrant_t * q) {
    
    quadrant_print (loglevel, q);
    qdata_print_variables (loglevel, quadrant_get_qdata (q));
    
  } // quadrant_print_qdata


  /**
   *
   */
  virtual void qdata_print_variables (int loglevel, qdata_t * data) {

    UNUSED(loglevel);
    UNUSED(data);
    // actual implementation in derived UserDataManager.
    
  } // qdata_print_variables

  
  /**
   * copy w at location specified by p4est quadrant dst.
   */
  virtual void quadrant_set_variables (p4est_quadrant_t * dst,
				       qdatavar_t * w) {
    
    qdata_set_variables (quadrant_get_qdata (dst), w);
    
  } // quadrant_set_variables


  /**
   * Callback used to update data when refining (copying parent cell data).
   *
   * Actual implementation in derived class.
   */
  virtual void quadrant_set_state_data (p4est_quadrant_t * dst,
					p4est_quadrant_t * src) { UNUSED(dst); UNUSED(src); };

  /**
   * Callback used when coarsening (average parent cell data).
   *
   * Actual implementation in derived class.
   */
  virtual void quadrant_compute_mean(p4est_quadrant_t *qmean,
				     p4est_quadrant_t *quadout[]) { UNUSED(qmean); UNUSED(quadout); };

  /**
   * Set qdata variables from a state vector (qdatavar_t).
   *
   * Pure virtual: must be implemented in derived class
   * because it might depend on specific qdata_t details.
   *
   */
  virtual void qdata_set_variables (qdata_t * data,
				    qdatavar_t * w) = 0;
  
  /**
   * copy data_src into data_dest.
   */
  virtual void qdata_copy(qdata_t *data_dst, 
			  qdata_t *data_src) {

    memcpy (data_dst, data_src, sizeof(qdata_t));
    
  } // qdata_copy


  /**
   * Copy state data (qdatavar_t) at
   */
  virtual void quadrant_copy_qdatavar (p4est_quadrant_t * dst,
				       p4est_quadrant_t * src) { UNUSED(dst); UNUSED(src); };
  
  virtual void quadrant_copy_qdatavar_data (p4est_quadrant_t * dst,
					    p4est_quadrant_t * src) {
  
    qdata_t *src_data = quadrant_get_qdata (src);
    qdata_t *dst_data = quadrant_get_qdata (dst);
    memcpy (&(dst_data->w), &(src_data->w), sizeof (qdatavar_t));
    
  } // quadrant_copy_qdatavar_data


  /**
   * Compute reconstructed (primitive variables).
   */
  virtual void reconstruct_variables (const qdatavar_t &w,
				      qdatarecons_t &r) { UNUSED(w); UNUSED(r);};

  /**
   * Compute delta (apply limiter to neighbor finite diffenres values).
   */
  virtual void compute_delta(const qdatarecons_t w,const qdatarecons_t *wn,
			     qdatarecons_t *delta,
			     scalar_limiter_t limiter,
			     double dx, double dxl, double dxr) {
    UNUSED(w); UNUSED(wn); UNUSED(delta); UNUSED(limiter);
    UNUSED(dx); UNUSED(dxl); UNUSED(dxr);
  };
  
  
  /**
   * A Generic method to get a variable field by name.
   */
  // TODO ??
  
}; // class UserDataManager

#endif // USERDATA_MANAGER_H_
