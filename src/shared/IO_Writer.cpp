#include "IO_Writer.h"

#include "canoP_base.h"
#include "UserDataManager.h"

#include <mpi.h>

#ifndef P4_TO_P8
#define IO_NODES_PER_CELL 4
#define IO_TOPOLOGY_TYPE "Quadrilateral"
#else
#define IO_NODES_PER_CELL 8
#define IO_TOPOLOGY_TYPE "Hexahedron"
#endif

#define IO_XDMF_NUMBER_TYPE "NumberType=\"Float\" Precision=\"4\""

#ifndef IO_MPIRANK_WRAP
#define IO_MPIRANK_WRAP 128
#endif

#ifndef IO_HDF5_COMPRESSION
#define IO_HDF5_COMPRESSION 3
#endif

/**
 * \brief Convert a H5T_NATIVE_* type to an XDMF NumberType
 */
static const char  *
hdf5_native_type_to_string (hid_t type)
{
  
  if (type == H5T_NATIVE_INT || type == H5T_NATIVE_LONG) {
    return "NumberType=\"Int\"";
  }

  if (type == H5T_NATIVE_UINT || type == H5T_NATIVE_ULONG) {
    return "NumberType=\"UInt\"";
  }

  if (type == H5T_NATIVE_CHAR) {
    return "NumberType=\"Char\"";
  }

  if (type == H5T_NATIVE_UCHAR) {
    return "NumberType=\"UChar\"";
  }

  if (type == H5T_NATIVE_FLOAT || type == H5T_NATIVE_DOUBLE) {
    return IO_XDMF_NUMBER_TYPE;
  }

  CANOP_GLOBAL_INFOF ("Unsupported number type %ld.\n", (long int)type);
  return IO_XDMF_NUMBER_TYPE;
  
} // hdf5_native_type_to_string

/**
 * \brief Create a new list of nodes without any ghost data.
 *
 * When there is no ghost data, p4est_nodes_new doesn't compute all the
 * global information about owned nodes and offsets, so we do it here
 * instead.
 */
static p4est_nodes_t *
nodes_new(p4est_t * p4est)
{
  
  p4est_nodes_t      *nodes = p4est_nodes_new(p4est, nullptr);
  int                 rank = p4est->mpirank;
  MPI_Comm            mpicomm = p4est->mpicomm;

  nodes->global_owned_indeps = P4EST_ALLOC(p4est_locidx_t, p4est->mpisize);
  nodes->num_owned_indeps = nodes->global_owned_indeps[rank] =
    nodes->indep_nodes.elem_count +
#ifdef P4_TO_P8
    nodes->edge_hangings.elem_count +
#endif
    nodes->face_hangings.elem_count;

  MPI_Allgather(&(nodes->num_owned_indeps), 1, P4EST_MPI_LOCIDX,
		nodes->global_owned_indeps, 1, P4EST_MPI_LOCIDX, mpicomm);

  nodes->offset_owned_indeps = 0;
  for (int i = 0; i < rank; ++i) {
    nodes->offset_owned_indeps += nodes->global_owned_indeps[i];
  }

  return nodes;
  
} // nodes_new

/**
 * \brief Write the coordinates of the 4 (or 8) nodes of a quadrant into data.
 *
 * Usually, data will be a huge array containing all the local nodes of a
 * process. To write the nodes for a specific quadrant, we call this function
 * with (data + offset) where the offset is calculated as:
 *    offset = 3 * P4EST_CHILDREN * current_quadrant_id
 *
 * This routine is only used when a p4est_node_t is not available, i.e. the scale
 * parameter is smaller than 1.0.
 *
 */
static void
io_fill_coordinates(p4est_t * p4est, 
		    p4est_geometry_t *geom,
		    p4est_topidx_t which_tree,
		    p4est_quadrant_t * q,
		    float *data,
		    double scale)
{
  double              xyz[3] = { 0, 0, 0 };
  double              XYZ[3] = { 0, 0, 0 };
  double              eta_x, eta_y, eta_z = 0.;

  int                 k = 0;
  double              h2 =
    0.5 * P4EST_QUADRANT_LEN(q->level) / P4EST_ROOT_LEN;
  const double        intsize = 1.0 / P4EST_ROOT_LEN;


#ifdef P4_TO_P8
  for (int zi = 0; zi < 2; ++zi) {
    eta_z = intsize*q->z + h2 * (1.0 + (zi * 2 - 1) * scale);
#endif
    for (int yi = 0; yi < 2; ++yi) {
      eta_y = intsize*q->y + h2 * (1.0 + (yi * 2 - 1) * scale);
      
      for (int xi = 0; xi < 2; ++xi) {
        CANOP_ASSERT (0 <= k && k < P4EST_CHILDREN);
        eta_x = intsize*q->x + h2 * (1.0 + (xi * 2 - 1) * scale);

	if (geom != nullptr) {

	  xyz[0] = eta_x;
	  xyz[1] = eta_y;
	  xyz[2] = eta_z;
	  // from logical coordinates to physical cartesian coordinates
	  geom->X(geom, which_tree, xyz, XYZ);
	  data[3 * k + 0] = (float) XYZ[0];
	  data[3 * k + 1] = (float) XYZ[1];
	  data[3 * k + 2] = (float) XYZ[2];

	} else {

	  // cartesian geometry, use the regular qcoord_to_vertex function
	  // to retrieve physical coordinates
	  p4est_qcoord_to_vertex(p4est->connectivity, which_tree, q->x, q->y,
#ifdef P4_TO_P8
				 q->z,
#endif
				 xyz);
	  
	  XYZ[0] = xyz[0] + h2 * (1.0 + (xi * 2 - 1) * scale);
	  XYZ[1] = xyz[1] + h2 * (1.0 + (yi * 2 - 1) * scale);
#ifdef P4_TO_P8
	  XYZ[2] = xyz[2] + h2 * (1.0 + (zi * 2 - 1) * scale);
#endif

	  data[3 * k + 0] = (float) XYZ[0];
	  data[3 * k + 1] = (float) XYZ[1];
	  data[3 * k + 2] = (float) XYZ[2];
	}
        ++k;
      }
    }
#ifdef P4_TO_P8
  }
#endif
} // io_fill_coordinates


// =======================================================
// =======================================================
IO_Writer::IO_Writer(p4est_t * p4est, 
		     p4est_geometry_t *geom,
		     const std::string &basename, 
		     int mesh_info)
{

  m_p4est = p4est;
  m_geom  = geom;
  m_nodes = nullptr;
  m_scale = 1.0;
  m_write_tree = (int8_t) mesh_info;
  m_write_level = (int8_t) mesh_info;
  m_write_rank = (int8_t) mesh_info;

  m_basename = "";
  m_hdff = 0;
  m_xmff = nullptr;

  if (m_p4est->mpirank == 0 && basename.size() ) {
    std::string filename;
    filename = basename + "_main.xmf";

    CANOP_GLOBAL_INFOF("Writing main XMDF file \"%s\".\n", filename.c_str());
    m_main_xmff = fopen(filename.c_str(), "w");
    io_xmf_write_main_header();
  }
  else {
    m_main_xmff = nullptr;
  }

} // IO_Writer::IO_Writer

// =======================================================
// =======================================================
IO_Writer::~IO_Writer()
{
  
  /*
   * Only rank 0 needs to close the main xdmf file.
   */
  if (m_p4est->mpirank == 0 && m_main_xmff) {
    io_xmf_write_main_footer();

    fflush(m_main_xmff);
    fclose(m_main_xmff);
  }

  // destroy the nodes structure if present
  if (m_nodes != nullptr) {
    p4est_nodes_destroy(m_nodes);
    m_nodes = nullptr;
  }

  // close other file
  close();
  
} // IO_Writer::~IO_Writer

// =======================================================
// =======================================================
void
IO_Writer::open(const std::string &basename)
{
  hid_t               plist;
  std::string         filename;

  /*
   * Open parallel HDF5 resources.
   */
  plist = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_fapl_mpio(plist, m_p4est->mpicomm, MPI_INFO_NULL);

  filename = basename + ".h5";
  m_basename = basename;
  m_hdff = H5Fcreate(filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, plist);
  H5Pclose(plist);

  if (m_p4est->mpirank == 0) {
    filename = basename + ".xmf";
    m_xmff = fopen(filename.c_str(), "w");

    if (m_main_xmff) {
      io_xmf_write_main_include(filename);
    }
  }
  
} // IO_Writer::open

// =======================================================
// =======================================================
void
IO_Writer::close()
{
  // close HDF5 file descriptor
  if (m_hdff) {
    H5Fflush(m_hdff, H5F_SCOPE_GLOBAL);
    H5Fclose(m_hdff);
    m_hdff = 0;
  }

  // close XDMF file descriptor
  if (m_xmff) {
    fflush(m_xmff);
    fclose(m_xmff);
    m_xmff = nullptr;
  }

  // destroy nodes, in case the writer is reused later
  if (m_nodes) {
    p4est_nodes_destroy(m_nodes);
    m_nodes = nullptr;
  }
  
} // IO_Writer::close

// =======================================================
// =======================================================
int
IO_Writer::write_mesh(p4est_t * p4est,
		      p4est_geometry_t *geom,
		      const std::string &basename)
{
  IO_Writer *w = new IO_Writer(p4est, geom, basename, 1);
  w->open(basename);

  CANOP_GLOBAL_INFOF("Writing to file %s.h5\n", basename.c_str());

  w->write_header(0);
  w->write_footer();

  delete w;

  return 0;
  
} // IO_Writer::write_mesh

// =======================================================
// =======================================================
int
IO_Writer::write_header(double time)
{
  p4est_t            *p4est = m_p4est;
  int                 mpirank = p4est->mpirank;

  p4est_locidx_t      local_num_quads = p4est->local_num_quadrants;
  p4est_gloidx_t      global_num_quads = p4est->global_num_quadrants;

  // get the number of nodes to write considering the scale

  // debug only: each cell is rescaled to be slightly smaller
  if (m_scale < 1.0) {

    m_local_num_nodes = P4EST_CHILDREN * local_num_quads;
    m_global_num_nodes = P4EST_CHILDREN * global_num_quads;

    m_start_nodes = P4EST_CHILDREN * p4est->global_first_quadrant[mpirank];
    
  } else { // this is the regular io output

    // ask p4est to create a global list of nodes for us
    if(m_nodes == nullptr)
      m_nodes = nodes_new(p4est);

    // retrieve the local number of nodes (local to current MPI task)
    m_local_num_nodes = m_nodes->indep_nodes.elem_count;

    // compute the total number of nodes acrross all MPI tasks
    m_global_num_nodes = 0;
    for (int i = 0; i < p4est->mpisize; ++i) {
      m_global_num_nodes += m_nodes->global_owned_indeps[i];
    }

    // retrieve offset to the first node that current MPI task owns
    m_start_nodes = m_nodes->offset_owned_indeps;
    
  }

  // write the xmdf file first
  if (mpirank == 0) {
    io_xmf_write_header(time);
  }

  // and write stuff into the hdf file
  io_hdf_write_coordinates(m_nodes);
  io_hdf_write_connectivity(m_nodes);
  io_hdf_write_tree();
  io_hdf_write_level();
  io_hdf_write_rank();

  return 0;

} // IO_Writer::write_header

// =======================================================
// =======================================================
int
IO_Writer::write_attribute(const std::string &name,
			   void *data,
			   size_t dim,
			   io_attribute_type_t ftype,
			   hid_t dtype,
			   hid_t wtype)
{
  int                 mpirank = m_p4est->mpirank;

  hsize_t             dims[2] = { 0, 0 };
  hsize_t             count[2] = { 0, 0 };
  hsize_t             start[2] = { 0, 0 };
  int                 rank = 0;

  if (ftype == IO_CELL_SCALAR || ftype == IO_CELL_VECTOR) {

    dims[0] = m_p4est->global_num_quadrants;
    dims[1] = dim;

    count[0] = m_p4est->local_num_quadrants;
    count[1] = dims[1];

    start[0] = m_p4est->global_first_quadrant[mpirank];
    start[1] = 0;

  } else {

    dims[0] = m_global_num_nodes;
    dims[1] = dim;

    count[0] = m_local_num_nodes;
    count[1] = dims[1];

    start[0] = m_start_nodes;
    start[1] = 0;

  }

  if (ftype == IO_CELL_SCALAR || ftype == IO_NODE_SCALAR) {
    rank = 1;
  } else {
    rank = 2;
  }

  // TODO: find a better way to pass the number type
  if (mpirank == 0) {
    const char         *dtype_str = hdf5_native_type_to_string(dtype);
    io_xmf_write_attribute(name, dtype_str, ftype, dims);
  }

  io_hdf_writev(m_hdff, name, data, dtype, wtype, rank, dims, count, start);
  return 0;
  
} // IO_Writer::write_attribute

// =======================================================
// =======================================================
int
IO_Writer::write_quadrant_attribute(const std::string &name,
				    UserDataManagerBase* userdata_mgr,
				    const qdata_getter_t get_fn,
				    hid_t type_id)
{
  p4est_t            *p4est = m_p4est;
  const p4est_locidx_t Ntotal = p4est->local_num_quadrants;

  p4est_locidx_t      k = 0;
  p4est_locidx_t      t = 0;
  p4est_tree_t       *tree = nullptr;
  sc_array_t         *quadrants = nullptr;
  p4est_quadrant_t   *quadrant = nullptr;
  double             *data = nullptr;

  // allocate the data for the scalars
  data = CANOP_ALLOC(double, Ntotal);

  // get it
  for (t = p4est->first_local_tree; t <= p4est->last_local_tree; ++t) {
    tree = p4est_tree_array_index(p4est->trees, t);
    quadrants = &tree->quadrants;

    for (size_t i = 0; i < quadrants->elem_count; ++i) {
      quadrant = p4est_quadrant_array_index(quadrants, i);

      data[k++] = (userdata_mgr->*get_fn)(quadrant);
    }
  }

  write_attribute(name, data, 0, IO_CELL_SCALAR,
		  H5T_NATIVE_DOUBLE, type_id);

  CANOP_FREE(data);
  
  return 0;

} // IO_Writer::write_quadrant_attribute

// =======================================================
// =======================================================
int
IO_Writer::write_quadrant_attribute(const std::string &name,
				    const simple_data_getter_t get_fn,
				    hid_t type_id)
{
  p4est_t            *p4est = m_p4est;
  const p4est_locidx_t Ntotal = p4est->local_num_quadrants;

  p4est_locidx_t      k = 0;
  p4est_locidx_t      t = 0;
  p4est_tree_t       *tree = nullptr;
  sc_array_t         *quadrants = nullptr;
  p4est_quadrant_t   *quadrant = nullptr;
  double             *data = nullptr;

  // allocate the data for the scalars
  data = CANOP_ALLOC(double, Ntotal);

  // get it
  for (t = p4est->first_local_tree; t <= p4est->last_local_tree; ++t) {
    tree = p4est_tree_array_index(p4est->trees, t);
    quadrants = &tree->quadrants;

    for (size_t i = 0; i < quadrants->elem_count; ++i) {
      quadrant = p4est_quadrant_array_index(quadrants, i);

      data[k++] = (*get_fn)(quadrant);
    }
  }

  write_attribute(name, data, 0, IO_CELL_SCALAR,
		  H5T_NATIVE_DOUBLE, type_id);

  CANOP_FREE(data);
  
  return 0;

} // IO_Writer::write_quadrant_attribute

// =======================================================
// =======================================================
int
IO_Writer::write_quadrant_attribute(const std::string &name,
				    UserDataManagerBase* userdata_mgr,
				    size_t dim,
				    const qdata_vector_getter_t get_fn,
				    hid_t type_id)
{
  p4est_t            *p4est = m_p4est;
  const p4est_locidx_t Ntotal = p4est->local_num_quadrants;

  p4est_locidx_t      k = 0;
  p4est_locidx_t      t = 0;
  p4est_tree_t       *tree = nullptr;
  sc_array_t         *quadrants = nullptr;
  p4est_quadrant_t   *quadrant = nullptr;
  double             *data = nullptr;
  double             *v = nullptr;

  // allocate space for the temporary vector
  v = CANOP_ALLOC(double, dim);

  // allocate the data for the scalars
  data = CANOP_ALLOC(double, dim * Ntotal);

  // get it
  for (t = p4est->first_local_tree; t <= p4est->last_local_tree; ++t) {
    tree = p4est_tree_array_index(p4est->trees, t);
    quadrants = &tree->quadrants;

    for (size_t i = 0; i < quadrants->elem_count; ++i) {
      quadrant = p4est_quadrant_array_index(quadrants, i);

      (userdata_mgr->*get_fn)(quadrant, dim, v);
      for (size_t j = 0; j < dim; ++j) {
        data[k * dim + j] = v[j];
      }

      ++k;
    }
  }

  write_attribute(name, data, dim, IO_CELL_VECTOR,
		  H5T_NATIVE_DOUBLE, type_id);

  CANOP_FREE(data);
  CANOP_FREE(v);

  return 0;

} // IO_Writer::write_quadrant_attribute

// =======================================================
// =======================================================
int
IO_Writer::write_quadrant_attribute(const std::string &name,
				    size_t dim,
				    const simple_data_vector_getter_t get_fn,
				    hid_t type_id)
{
  p4est_t            *p4est = m_p4est;
  const p4est_locidx_t Ntotal = p4est->local_num_quadrants;

  p4est_locidx_t      k = 0;
  p4est_locidx_t      t = 0;
  p4est_tree_t       *tree = nullptr;
  sc_array_t         *quadrants = nullptr;
  p4est_quadrant_t   *quadrant = nullptr;
  double             *data = nullptr;
  double             *v = nullptr;

  // allocate space for the temporary vector
  v = CANOP_ALLOC(double, dim);

  // allocate the data for the scalars
  data = CANOP_ALLOC(double, dim * Ntotal);

  // get it
  for (t = p4est->first_local_tree; t <= p4est->last_local_tree; ++t) {
    tree = p4est_tree_array_index(p4est->trees, t);
    quadrants = &tree->quadrants;

    for (size_t i = 0; i < quadrants->elem_count; ++i) {
      quadrant = p4est_quadrant_array_index(quadrants, i);

      (*get_fn)(quadrant, dim, v);
      for (size_t j = 0; j < dim; ++j) {
        data[k * dim + j] = v[j];
      }

      ++k;
    }
  }

  write_attribute(name, data, dim, IO_CELL_VECTOR,
		  H5T_NATIVE_DOUBLE, type_id);

  CANOP_FREE(data);
  CANOP_FREE(v);

  return 0;

} // IO_Writer::write_quadrant_attribute

// =======================================================
// =======================================================
int
IO_Writer::write_lnode_attribute(const std::string &name,
				  p4est_lnodes_t *lnodes,
				  double *lnode_data,
				  hid_t type_id)
{
  p4est_t              *p4est = m_p4est;
  p4est_nodes_t        *nodes = m_nodes;
  const p4est_locidx_t  Ntotal = m_local_num_nodes;

  p4est_locidx_t      k, t, q, nq;
  p4est_locidx_t      knode, il, in;
  size_t i, j;

  p4est_lnodes_code_t c, fhang;
#ifdef P4_TO_P8
  p4est_lnodes_code_t ehang;
#endif
  p4est_lnodes_code_t ones = P4EST_CHILDREN - 1;
  int                ef;

  p4est_tree_t       *tree = nullptr;
  sc_array_t         *quadrants = nullptr;
  double             *data = nullptr;

  double             interp_data[P4EST_CHILDREN];
  const double       factor = 1. / P4EST_HALF;
  double             sum;

  CANOP_ASSERT(lnodes->degree == 1);
  CANOP_ASSERT(nodes != nullptr);

  // allocate the data for the scalars
  data = CANOP_ALLOC(double, Ntotal);

  // Interpolate lnode data to hanging nodes
  k = 0;
  for (t = p4est->first_local_tree; t <= p4est->last_local_tree; ++t) {
    tree = p4est_tree_array_index (p4est->trees, t);
    quadrants = &tree->quadrants;
    nq = quadrants->elem_count;

    for (q = 0; q < nq; ++q, ++k) {
      //quadrant = p4est_quadrant_array_index (quadrants, i);
      if (lnodes->face_code[k] == 0) {
        // There's no hanging node, simply copy the data
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          knode = P4EST_CHILDREN * k + i;
          il = lnodes->element_nodes[knode];
          in = nodes->local_nodes[knode];
          data[in] = lnode_data[il];
        }
      }
      else {
        // Decode face code information
        c = lnodes->face_code[k] & ones;
        fhang = (lnodes->face_code[k] >> P4EST_DIM) & ones;
#ifdef P4_TO_P8
        ehang = (lnodes->face_code[k] >> (2*P4EST_DIM)) & ones;
#endif

        // Fetch lnode data
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          knode = P4EST_CHILDREN * k + i;
          il = lnodes->element_nodes[knode];
          interp_data[i] = lnode_data[il];
        }

        // Interpolate face-hanging nodes
        for (i = 0; i < P4EST_DIM; ++i) {
          if (fhang & 1) {
            ef = p4est_corner_faces[c][i];
            sum = 0.;
            for (j = 0; j < P4EST_HALF; ++j) {
              sum += interp_data[p4est_face_corners[ef][j]];
            }
            interp_data[c ^ ones ^ (1 << i)] = factor * sum;
          }
          fhang >>= 1;
        }

#ifdef P4_TO_P8
        // Interpolate edge-hanging nodes
        for (i = 0; i < P4EST_DIM; ++i) {
          if (ehang & 1) {
            ef = p8est_corner_edges[c][i];
            interp_data[c ^ (1 << i)] = .5 * (
                interp_data[p8est_edge_corners[ef][0]] +
                interp_data[p8est_edge_corners[ef][1]]
                );
          }
          ehang >>= 1;
        }
#endif

        // Copy interpolated data
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          knode = P4EST_CHILDREN * k + i;
          in = nodes->local_nodes[knode];
          data[in] = interp_data[i];
        }
      }
    } /* loop on quadrants */
  } /* loop on trees */

  write_attribute(name, data, 0, IO_NODE_SCALAR,
                  H5T_NATIVE_DOUBLE, type_id);

  CANOP_FREE(data);

  return 0;
} // IO_Writer::write_lnode_attribute

// =======================================================
// =======================================================
int
IO_Writer::write_lnode_attribute(const std::string &name,
				  p4est_lnodes_t *lnodes,
				  double *lnode_data,
				  hid_t type_id,
                  double hang_default)
{
  p4est_t              *p4est = m_p4est;
  p4est_nodes_t        *nodes = m_nodes;
  const p4est_locidx_t  Ntotal = m_local_num_nodes;

  p4est_locidx_t      k, t, q, nq;
  p4est_locidx_t      knode, il, in;
  size_t i;

  p4est_lnodes_code_t c, fhang;
#ifdef P4_TO_P8
  p4est_lnodes_code_t ehang;
#endif
  p4est_lnodes_code_t ones = P4EST_CHILDREN - 1;

  p4est_tree_t       *tree = nullptr;
  sc_array_t         *quadrants = nullptr;
  double             *data = nullptr;

  double             interp_data[P4EST_CHILDREN];

  CANOP_ASSERT(lnodes->degree == 1);
  CANOP_ASSERT(nodes != nullptr);

  // allocate the data for the scalars
  data = CANOP_ALLOC(double, Ntotal);

  // Take care of hanging nodes
  k = 0;
  for (t = p4est->first_local_tree; t <= p4est->last_local_tree; ++t) {
    tree = p4est_tree_array_index (p4est->trees, t);
    quadrants = &tree->quadrants;
    nq = quadrants->elem_count;

    for (q = 0; q < nq; ++q, ++k) {
      //quadrant = p4est_quadrant_array_index (quadrants, i);
      if (lnodes->face_code[k] == 0) {
        // There's no hanging node, simply copy the data
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          knode = P4EST_CHILDREN * k + i;
          il = lnodes->element_nodes[knode];
          in = nodes->local_nodes[knode];
          data[in] = lnode_data[il];
        }
      }
      else {
        // Decode face code information
        c = lnodes->face_code[k] & ones;
        fhang = (lnodes->face_code[k] >> P4EST_DIM) & ones;
#ifdef P4_TO_P8
        ehang = (lnodes->face_code[k] >> (2*P4EST_DIM)) & ones;
#endif

        // Fetch lnode data
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          knode = P4EST_CHILDREN * k + i;
          il = lnodes->element_nodes[knode];
          interp_data[i] = lnode_data[il];
        }

        // Interpolate face-hanging nodes
        for (i = 0; i < P4EST_DIM; ++i) {
          if (fhang & 1) {
            interp_data[c ^ ones ^ (1 << i)] = hang_default;
          }
          fhang >>= 1;
        }

#ifdef P4_TO_P8
        // Interpolate edge-hanging nodes
        for (i = 0; i < P4EST_DIM; ++i) {
          if (ehang & 1) {
            interp_data[c ^ (1 << i)] = hang_default;
          }
          ehang >>= 1;
        }
#endif

        // Copy interpolated data
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          knode = P4EST_CHILDREN * k + i;
          in = nodes->local_nodes[knode];
          data[in] = interp_data[i];
        }
      }
    } /* loop on quadrants */
  } /* loop on trees */

  write_attribute(name, data, 0, IO_NODE_SCALAR,
                  H5T_NATIVE_DOUBLE, type_id);

  CANOP_FREE(data);

  return 0;
} // IO_Writer::write_lnode_attribute

// =======================================================
// =======================================================
int
IO_Writer::write_footer()
{
  int mpirank = m_p4est->mpirank;

  if (mpirank == 0) {
    io_xmf_write_footer();
  }

  return 0;
  
} // IO_Writer::write_footer

// =======================================================
// =======================================================
// Private members
// =======================================================
// =======================================================


// =======================================================
// =======================================================
void
IO_Writer::io_xmf_write_main_header()
{

  FILE               *fd = m_main_xmff;

  fprintf(fd, "<?xml version=\"1.0\" ?>\n");
  fprintf(fd, "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
  fprintf(fd, "<Xdmf xmlns:xi=\"http://www.w3.org/2001/XInclude\""
	  " Version=\"2.0\">\n");
  fprintf(fd, "  <Domain Name=\"MainTimeSeries\">\n");
  fprintf(fd, "    <Grid Name=\"MainTimeSeries\" GridType=\"Collection\""
	  " CollectionType=\"Temporal\">\n");
  
} // IO_Writer::io_xmf_write_main_header

// =======================================================
// =======================================================
void
IO_Writer::io_xmf_write_header(double time)
{

  FILE               *fd = this->m_xmff;
  size_t              global_num_nodes = this->m_global_num_nodes;
  size_t              global_num_quads = this->m_p4est->global_num_quadrants;

  fprintf(fd, "<?xml version=\"1.0\" ?>\n");
  fprintf(fd, "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
  fprintf(fd, "<Xdmf Version=\"2.0\">\n");
  fprintf(fd, "  <Domain>\n");
  fprintf(fd, "    <Grid Name=\"%s\" GridType=\"Uniform\">\n", this->m_basename.c_str());
  fprintf(fd, "      <Time TimeType=\"Single\" Value=\"%g\" />\n", time);

  // Connectivity
  fprintf(fd, "      <Topology TopologyType=\"%s\" NumberOfElements=\"%lu\""
	  ">\n", IO_TOPOLOGY_TYPE, global_num_quads);
  fprintf(fd, "        <DataItem Dimensions=\"%lu %d\" DataType=\"Int\""
	  " Format=\"HDF\">\n", global_num_quads, IO_NODES_PER_CELL);
  fprintf(fd, "         %s.h5:/connectivity\n", this->m_basename.c_str());
  fprintf(fd, "        </DataItem>\n");
  fprintf(fd, "      </Topology>\n");

  // Points
  fprintf(fd, "      <Geometry GeometryType=\"XYZ\">\n");
  fprintf(fd, "        <DataItem Dimensions=\"%lu 3\" %s Format=\"HDF\">\n",
	  global_num_nodes, IO_XDMF_NUMBER_TYPE);
  fprintf(fd, "         %s.h5:/coordinates\n", this->m_basename.c_str());
  fprintf(fd, "        </DataItem>\n");
  fprintf(fd, "      </Geometry>\n");

} // IO_Writer::io_xmf_write_header

// =======================================================
// =======================================================
void
IO_Writer::io_xmf_write_main_include(const std::string &name)
{

  FILE *fd = this->m_main_xmff;

  fprintf(fd, "      <xi:include href=\"%s\""
	  " xpointer=\"xpointer(//Xdmf/Domain/Grid)\" />\n", name.c_str());

} // IO_Writer::io_xmf_write_main_include

// =======================================================
// =======================================================
void
IO_Writer::io_xmf_write_attribute(const std::string &name,
				  const std::string &number_type,
				  io_attribute_type_t type,
				  hsize_t dims[2])
{

  FILE              *fd = this->m_xmff;
  const std::string &basename = this->m_basename;
  
  fprintf(fd, "      <Attribute Name=\"%s\"", name.c_str());
  switch(type) {
  case IO_CELL_SCALAR:
    fprintf(fd, " AttributeType=\"Scalar\" Center=\"Cell\">\n");
    break;
  case IO_CELL_VECTOR:
    fprintf(fd, " AttributeType=\"Vector\" Center=\"Cell\">\n");
    break;
  case IO_NODE_SCALAR:
    fprintf(fd, " AttributeType=\"Scalar\" Center=\"Node\">\n");
    break;
  case IO_NODE_VECTOR:
    fprintf(fd, " AttributeType=\"Vector\" Center=\"Node\">\n");
    break;
  default:
    CANOP_LERROR("Unsupported field type.\n");
    return;
  }

  fprintf(fd, "        <DataItem %s Format=\"HDF\"", number_type.c_str());
  if (type == IO_CELL_SCALAR || type == IO_NODE_SCALAR) {
    fprintf(fd, " Dimensions=\"%llu\">\n", dims[0]);
  }
  else {
    fprintf(fd, " Dimensions=\"%llu %llu\">\n", dims[0], dims[1]);
  }

  fprintf(fd, "         %s.h5:/%s\n", basename.c_str(), name.c_str());
  fprintf(fd, "        </DataItem>\n");
  fprintf(fd, "      </Attribute>\n");

} // IO_Writer::io_xmf_write_attribute

// =======================================================
// =======================================================
void
IO_Writer::io_xmf_write_main_footer()
{
  
  FILE *fd = this->m_main_xmff;

  fprintf(fd, "    </Grid>\n");
  fprintf(fd, "  </Domain>\n");
  fprintf(fd, "</Xdmf>\n");
  
} // IO_Writer::io_xmf_write_main_footer

// =======================================================
// =======================================================
void
IO_Writer::io_xmf_write_footer()
{
  
  FILE *fd = this->m_xmff;
  
  fprintf(fd, "    </Grid>\n");
  fprintf(fd, "  </Domain>\n");
  fprintf(fd, "</Xdmf>\n");
  
} // IO_Writer::io_xmf_write_footer

// =======================================================
// =======================================================
void
IO_Writer::io_hdf_writev(hid_t fd, const std::string &name, void *data,
			 hid_t dtype_id, hid_t wtype_id, hid_t rank,
			 hsize_t dims[], hsize_t count[], hsize_t start[])
{
  int                 status;
  hsize_t             size = 1;
  hid_t               filespace = 0;
  hid_t               memspace = 0;
  hid_t               dataset = 0;
  hid_t               dataset_properties = 0;
  hid_t               write_properties = 0;

  CANOP_GLOBAL_INFOF("Writing \"%s\" of size %llu / %llu\n",
		  name.c_str(), count[0], dims[0]);

  // compute size of the dataset
  for (int i = 0; i < rank; ++i) {
    size *= count[i];
  }

  // create the layout in the file and in the memory of the current process
  filespace = H5Screate_simple(rank, dims, nullptr);
  memspace = H5Screate_simple(rank, count, nullptr);

  // set some properties
  dataset_properties = H5Pcreate(H5P_DATASET_CREATE);
  write_properties = H5Pcreate(H5P_DATASET_XFER);
  H5Pset_dxpl_mpio(write_properties, H5FD_MPIO_COLLECTIVE);

  // create the dataset and the location of the local data
  dataset = H5Dcreate2(fd, name.c_str(), wtype_id, filespace,
		       H5P_DEFAULT, dataset_properties, H5P_DEFAULT);
  H5Sselect_hyperslab(filespace, H5S_SELECT_SET, start, nullptr, count, nullptr);

  if (dtype_id != wtype_id) {
    status = H5Tconvert(dtype_id, wtype_id, size, data, nullptr, H5P_DEFAULT);
    SC_CHECK_ABORT(status >= 0, "H5Tconvert failed!");
  }
  H5Dwrite(dataset, wtype_id, memspace, filespace, write_properties, data);

  H5Dclose(dataset);
  H5Sclose(filespace);
  H5Sclose(memspace);
  H5Pclose(dataset_properties);
  H5Pclose(write_properties);

} // IO_Writer::io_hdf_writev

// =======================================================
// =======================================================
void
IO_Writer::io_hdf_write_coordinates(p4est_nodes_t * nodes)
{
  p4est_t            *p4est = this->m_p4est;
  p4est_geometry_t   *geom  = this->m_geom;

  sc_array_t         *trees = p4est->trees;
  sc_array_t         *quadrants = nullptr;
  sc_array_t         *indeps = &(nodes->indep_nodes);
#if 0 // not needed: see comment below about hanging nodes
  sc_array_t         *face_hangings = &(nodes->face_hangings);
#ifdef P4_TO_P8
  sc_array_t         *edge_hangings = &(nodes->edge_hangings);
#endif
#endif

  p4est_tree_t       *tree = nullptr;
  p4est_quadrant_t   *q = nullptr;
  p4est_indep_t      *inode = nullptr;
  double              xyz[3];

  hsize_t             dims[2] = { 0, 0 };
  hsize_t             count[2] = { 0, 0 };
  hsize_t             start[2] = { 0, 0 };
  float              *data;

  p4est_locidx_t      local_num_quads = 0;
  p4est_locidx_t      offset;
  p4est_topidx_t      jt = 0;
  size_t              jq = 0;
  size_t              ji = 0;
#if 0 // not needed: see comment below about hanging nodes
  size_t              jf = 0;
#ifdef P4_TO_P8
  size_t              je = 0;
#endif
#endif

  // get the dimensions and offset of the node coordinates array
  dims[0] = this->m_global_num_nodes;
  dims[1] = 3;

  count[0] = this->m_local_num_nodes;
  count[1] = 3;

  start[0] = this->m_start_nodes;
  start[1] = 0;

  data = CANOP_ALLOC(float, count[0] * count[1]);

  /*
   * construct the list of node coordinates
   */

  // nodes == nullptr should only happen when m_scale is smaller than 1.0
  if (nodes == nullptr) {

    for (jt = p4est->first_local_tree; jt <= p4est->last_local_tree; ++jt) {
      tree = p4est_tree_array_index(trees, jt);
      quadrants = &(tree->quadrants);

      for (jq = 0; jq < quadrants->elem_count; ++jq) {
        q = p4est_quadrant_array_index(quadrants, jq);

        // the offset of the first node of the current quadrant
        offset = 3 * P4EST_CHILDREN * local_num_quads;

        // compute the coordinates of the P4EST_CHILDREN nodes and add them to
        // the data array
        io_fill_coordinates(p4est, geom, jt, q, data + offset, this->m_scale);

        // next quad
        ++local_num_quads;
      }
    }
    
  } else {

    // this is the regular way of computing nodes coordinates, i.e. through
    // a p4est_nodes_t structure

    // loop over local non-hanging nodes
    for (ji = 0; ji < indeps->elem_count; ++ji) {
      inode = (p4est_indep_t *) sc_array_index(indeps, ji);

      // is current connectivity associated to a "geometry" mapping ?
      if (geom != nullptr) {

	const double        intsize = 1.0 / P4EST_ROOT_LEN;
	double xyz_logic[3] = {0, 0, 0};
	xyz_logic[0] = intsize*inode->x;
	xyz_logic[1] = intsize*inode->y;
#ifdef P4_TO_P8
	xyz_logic[2] = intsize*inode->z;
#endif
	/* from logical coordinates to physical cartesian coordinates */
	geom->X(geom, inode->p.which_tree, xyz_logic, xyz);

      } else {

	/* retrieve corners of the tree */
	p4est_qcoord_to_vertex(p4est->connectivity, 
			       inode->p.which_tree,
			       inode->x, inode->y,
#ifdef P4_TO_P8
			       inode->z,
#endif
			       xyz);
      }

      data[3 * ji + 0] = (float) xyz[0];
      data[3 * ji + 1] = (float) xyz[1];
      data[3 * ji + 2] = (float) xyz[2];

    }

    /* The following code is useless because in our case all nodes are
     * considered independent. This comes from tha fact that p4est_nodes_new is
     * called with ghost = nullptr, which in turn calls p4est_nodes_new_local. As a
     * consequence, the information about hanging nodes is not computed and all
     * nodes are in the nodes->indep_nodes (a.k.a. indeps) array.
     */
#if 0
    for (jf = ji; jf < (ji + face_hangings->elem_count); ++jf) {
      inode = (p4est_indep_t *) sc_array_index(face_hangings, jf - ji);

      if (geom != nullptr) {
	const double        intsize = 1.0 / P4EST_ROOT_LEN;
	double xyz_logic[3] = {0, 0, 0};
	xyz_logic[0] = intsize*inode->x;
	xyz_logic[1] = intsize*inode->y;
#ifdef P4_TO_P8
	xyz_logic[2] = intsize*inode->z;
#endif
	/* from logical coordinates to physical coordinates */
	geom->X(geom, inode->p.which_tree, xyz_logic, xyz);
	
      } else {
	
	/* retrieve corners of the tree */
	p4est_qcoord_to_vertex(p4est->connectivity, 
			       inode->p.which_tree,
			       inode->x, inode->y,
#ifdef P4_TO_P8
			       inode->z,
#endif
			       xyz);
      }

      data[3 * jf + 0] = (float) xyz[0];
      data[3 * jf + 1] = (float) xyz[1];
      data[3 * jf + 2] = (float) xyz[2];
    }

#ifdef P4_TO_P8
    for (je = jf; je < (jf + edge_hangings->elem_count); ++je) {
      inode = (p4est_indep_t *) sc_array_index (edge_hangings, je - jf);

      if (geom != nullptr) {
	
        const double        intsize = 1.0 / P4EST_ROOT_LEN;
        double xyz_logic[3] = {0, 0, 0};
        xyz_logic[0] = intsize*inode->x;
        xyz_logic[1] = intsize*inode->y;
        xyz_logic[2] = intsize*inode->z;
        /* from logical coordinates to physical coordinates */
        geom->X (geom, inode->p.which_tree, xyz_logic, xyz);
        
      } else {
        
        /* retrieve corners of the tree */
        p4est_qcoord_to_vertex (p4est->connectivity, 
                                inode->p.which_tree,
                                inode->x, inode->y,
                                inode->z,
                                xyz);
      }

      data[3 * je + 0] = (float) xyz[0];
      data[3 * je + 1] = (float) xyz[1];
      data[3 * je + 2] = (float) xyz[2];
    }
#endif
#endif
  }

  // write the node coordinates
  io_hdf_writev(this->m_hdff, "coordinates", data,
		H5T_NATIVE_FLOAT, H5T_NATIVE_FLOAT, 2, dims, count, start);
  CANOP_FREE(data);
  
} // IO_Writer::io_hdf_write_coordinates

// =======================================================
// =======================================================
void
IO_Writer::io_hdf_write_connectivity(p4est_nodes_t * nodes)
{
  p4est_t            *p4est = this->m_p4est;
  int                 mpirank = p4est->mpirank;

  p4est_locidx_t      local_num_quads = p4est->local_num_quadrants;
  p4est_gloidx_t      global_num_quads = p4est->global_num_quadrants;

  p4est_locidx_t      node[P4EST_CHILDREN] = {
    0, 1, 3, 2
#ifdef P4_TO_P8
    , 4, 5, 7, 6
#endif
  };

  hsize_t             dims[2] = { 0, 0 };
  hsize_t             count[2] = { 0, 0 };
  hsize_t             start[2] = { 0, 0 };
  int                *data;

  p4est_locidx_t      jl = 0;
  p4est_locidx_t      jc = 0;
  p4est_locidx_t      jn = 0;
  p4est_locidx_t      idx = 0;
  p4est_locidx_t      offset;

  // get the dimensions and offsets for each connectivity array
  dims[0] = global_num_quads;
  dims[1] = P4EST_CHILDREN;

  count[0] = local_num_quads;
  count[1] = P4EST_CHILDREN;

  start[0] = p4est->global_first_quadrant[mpirank];
  start[1] = 0;

  data = CANOP_ALLOC(int, count[0] * count[1]);

  // get connectivity data
  for (jl = 0; jl < local_num_quads; ++jl) {
    for (jc = 0; jc < P4EST_CHILDREN; ++jc) {
      idx = P4EST_CHILDREN * jl + jc;
      offset = jn + node[jc];

      if (nodes == nullptr) {
        data[idx] = this->m_start_nodes + offset;
      }
      else {
        data[idx] = this->m_start_nodes + nodes->local_nodes[offset];
      }
    }

    jn += P4EST_CHILDREN;
  }

  // write the node coordinates
  io_hdf_writev(this->m_hdff, "connectivity", data,
		H5T_NATIVE_INT, H5T_NATIVE_INT, 2, dims, count, start);
  CANOP_FREE(data);
  
} // IO_Writer::io_hdf_write_connectivity

// =======================================================
// =======================================================
void
IO_Writer::io_hdf_write_tree()
{
  if (!this->m_write_tree) {
    return;
  }

  p4est_t            *p4est = this->m_p4est;

  p4est_tree_t       *tree = nullptr;
  p4est_topidx_t      jt = 0;
  size_t              jl = 0;
  size_t              jq = 0;

  int                *data;
  data = CANOP_ALLOC(int, p4est->local_num_quadrants);

  // gather the tree id for each local quadrant
  for (jt = p4est->first_local_tree; jt <= p4est->last_local_tree; ++jt) {
    tree = p4est_tree_array_index(p4est->trees, jt);

    for (jq = 0; jq < tree->quadrants.elem_count; ++jq, ++jl) {
      data[jl] = jt;
    }
  }

  this->write_attribute("treeid", data, 0, IO_CELL_SCALAR,
			H5T_NATIVE_INT, H5T_NATIVE_INT);
  CANOP_FREE(data);
  
} // IO_Writer::io_hdf_write_tree

// =======================================================
// =======================================================
void
IO_Writer::io_hdf_write_level()
{
  if (!this->m_write_level) {
    return;
  }

  p4est_t            *p4est = this->m_p4est;

  p4est_tree_t       *tree = nullptr;
  sc_array_t         *quadrants = nullptr;
  p4est_quadrant_t   *q = nullptr;

  p4est_topidx_t      jt = 0;
  p4est_locidx_t      jl = 0;
  size_t              jq = 0;

  int                *data;
  data = CANOP_ALLOC(int, p4est->local_num_quadrants);

  // gather the tree id for each local quadrant
  for (jt = p4est->first_local_tree; jt <= p4est->last_local_tree; ++jt) {
    tree = p4est_tree_array_index(p4est->trees, jt);
    quadrants = &(tree->quadrants);

    for (jq = 0; jq < quadrants->elem_count; ++jq, ++jl) {
      q = p4est_quadrant_array_index(quadrants, jq);
      data[jl] = q->level;
    }
  }

  this->write_attribute("level", data, 0, IO_CELL_SCALAR,
			H5T_NATIVE_INT, H5T_NATIVE_INT);
  CANOP_FREE(data);
  
} // IO_Writer::io_hdf_write_level

// =======================================================
// =======================================================
void
IO_Writer::io_hdf_write_rank()
{
  if (!this->m_write_rank) {
    return;
  }

  p4est_t            *p4est = this->m_p4est;
  int                 mpirank = p4est->mpirank % IO_MPIRANK_WRAP;

  p4est_tree_t       *tree = nullptr;
  p4est_topidx_t      jt = 0;
  p4est_locidx_t      jl = 0;
  size_t              jq = 0;

  int                *data;
  data = CANOP_ALLOC(int, p4est->local_num_quadrants);

  // gather the tree id for each local quadrant
  for (jt = p4est->first_local_tree; jt <= p4est->last_local_tree; ++jt) {
    tree = p4est_tree_array_index(p4est->trees, jt);

    for (jq = 0; jq < tree->quadrants.elem_count; ++jq, ++jl) {
      data[jl] = mpirank;
    }
  }

  this->write_attribute("mpirank", data, 0, IO_CELL_SCALAR,
			H5T_NATIVE_INT, H5T_NATIVE_INT);
  CANOP_FREE(data);
  
} // IO_Writer::io_hdf_write_rank

