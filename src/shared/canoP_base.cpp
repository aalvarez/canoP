
#include "canoP_base.h"

int                 canoP_package_id = -1;

void
canoP_init (sc_log_handler_t log_handler, int log_threshold)
{
  canoP_package_id = sc_package_register (log_handler, log_threshold,
                                          "canoP", "The CanoP CFD code.");
}

#ifndef __cplusplus
#undef CANOP_GLOBAL_LOGF
#undef CANOP_LOGF
#undef CANOP_GLOBAL_TRACEF
#undef CANOP_GLOBAL_LDEBUGF
#undef CANOP_GLOBAL_VERBOSEF
#undef CANOP_GLOBAL_INFOF
#undef CANOP_GLOBAL_STATISTICSF
#undef CANOP_GLOBAL_PRODUCTIONF
#undef CANOP_GLOBAL_ESSENTIALF
#undef CANOP_GLOBAL_LERRORF
#undef CANOP_TRACEF
#undef CANOP_LDEBUGF
#undef CANOP_VERBOSEF
#undef CANOP_INFOF
#undef CANOP_STATISTICSF
#undef CANOP_PRODUCTIONF
#undef CANOP_ESSENTIALF
#undef CANOP_LERRORF
#endif

#ifndef SC_SPLINT

void
CANOP_GLOBAL_LOGF (int priority, const char *fmt, ...)
{
  va_list             ap;

  va_start (ap, fmt);
  sc_logv ("<unknown>", 0, canoP_package_id, SC_LC_GLOBAL, priority, fmt, ap);
  va_end (ap);
}

void
CANOP_LOGF (int priority, const char *fmt, ...)
{
  va_list             ap;

  va_start (ap, fmt);
  sc_logv ("<unknown>", 0, canoP_package_id, SC_LC_NORMAL, priority, fmt, ap);
  va_end (ap);
}

#define CANOP_LOG_IMP(n,p)                              \
  void                                                  \
  CANOP_GLOBAL_ ## n ## F (const char *fmt, ...)        \
  {                                                     \
    va_list             ap;                             \
    va_start (ap, fmt);                                 \
    sc_logv ("<unknown>", 0, canoP_package_id,          \
             SC_LC_GLOBAL, SC_LP_ ## p, fmt, ap);       \
    va_end (ap);                                        \
  }                                                     \
  void                                                  \
  CANOP_ ## n ## F (const char *fmt, ...)               \
  {                                                     \
    va_list             ap;                             \
    va_start (ap, fmt);                                 \
    sc_logv ("<unknown>", 0, canoP_package_id,          \
             SC_LC_NORMAL, SC_LP_ ## p, fmt, ap);       \
    va_end (ap);                                        \
  }

CANOP_LOG_IMP (TRACE, TRACE)
CANOP_LOG_IMP (LDEBUG, DEBUG)
CANOP_LOG_IMP (VERBOSE, VERBOSE)
CANOP_LOG_IMP (INFO, INFO)
CANOP_LOG_IMP (STATISTICS, STATISTICS)
CANOP_LOG_IMP (PRODUCTION, PRODUCTION)
CANOP_LOG_IMP (ESSENTIAL, ESSENTIAL)
CANOP_LOG_IMP (LERROR, ERROR)

#endif
