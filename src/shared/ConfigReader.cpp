#include <sc.h>
#include <sc_containers.h>

#include "ConfigReader.h"

#include "canoP_base.h"

/**
 * \brief Get the value from an expression containing a string.
 *
 * \param [in] L The lua state.
 * \param [in] expr The expression to evaluate.
 *
 * \return A fully allocated string containing the value held or returned by
 *         expression. The result can be NULL if the expression does not
 *         return a string.
 *
 * \sa luaL_dostring, lua_isstring, lua_tolstring.
 */
static char        *
lua_stringexpr (lua_State * L, const char *expr)
{
  size_t              length = 0;
  char               *r = NULL;

  const char         *tmp = NULL;
  char                buf[BUFSIZ];

  /* Assign the Lua expression to a Lua global variable. */
  snprintf (buf, BUFSIZ, "evalExpr=%s", expr);
  if (!luaL_dostring (L, buf)) {
    /* Get the value of the global variable */
    lua_getglobal (L, "evalExpr");
    if (lua_isstring (L, -1)) {
      /* lua docs say there is no guarantee r will be valid after
       * removing it from the stack (lua_pop), so we copy it to
       * another string, just in case
       */
      tmp = lua_tolstring (L, -1, &length);
      r = CANOP_STRDUP (tmp);
    }
    lua_pop (L, 1);
  }

  return r;
}

/**
 * \brief Get the value from an expression containing a number.
 *
 * \param [in] L The lua state.
 * \param [in] expr The expression to evaluate.
 * \param [out] r The resulting double.
 *
 * \return 1 if the expression returns a valid number, 0 otherwise.
 */
static int
lua_numberexpr (lua_State * L, const char *expr, double *r)
{
  int                 retval = 0;
  char                buf[BUFSIZ];

  snprintf (buf, BUFSIZ, "evalExpr=%s", expr);
  if (!luaL_dostring (L, buf)) {
    lua_getglobal (L, "evalExpr");

    if (lua_isnumber (L, -1)) {
      *r = lua_tonumber (L, -1);
      retval = 1;
    }
    lua_pop (L, 1);
  }

  return retval;
}

/**
 * \brief Get the value from an expression containing an integer.
 *
 * \param [in] L The lua state.
 * \param [in] expr The expression to evaluate.
 * \param [out] r The resulting integer.
 *
 * \return 1 if the expression returns a valid number, 0 otherwise.
 */
static int
lua_intexpr (lua_State * L, const char *expr, int *r)
{
  double              value;

  if (lua_numberexpr (L, expr, &value)) {
    *r = (int) value;
    return 1;
  }

  return 0;
}

int
config_log_verbosity (const char *name)
{
  if (name == NULL) {
    return SC_LP_PRODUCTION;
  }

  if (strcmp (name, "ALWAYS") == 0) {
    return SC_LP_ALWAYS;
  }
  else if (strcmp (name, "TRACE") == 0) {
    return SC_LP_TRACE;
  }
  else if (strcmp (name, "DEBUG") == 0) {
    return SC_LP_DEBUG;
  }
  else if (strcmp (name, "VERBOSE") == 0) {
    return SC_LP_VERBOSE;
  }
  else if (strcmp (name, "INFO") == 0) {
    return SC_LP_INFO;
  }
  else if (strcmp (name, "STATISTICS") == 0) {
    return SC_LP_STATISTICS;
  }
  else if (strcmp (name, "PRODUCTION") == 0) {
    return SC_LP_PRODUCTION;
  }
  else if (strcmp (name, "ESSENTIAL") == 0) {
    return SC_LP_ESSENTIAL;
  }
  else if (strcmp (name, "ERROR") == 0) {
    return SC_LP_ERROR;
  }
  else if (strcmp (name, "SILENT") == 0) {
    return SC_LP_SILENT;
  }

  return SC_LP_PRODUCTION;
}

// =======================================================
// ==== CLASS CONFIGREADER IMPL ==========================
// =======================================================

// =======================================================
// =======================================================
ConfigReader::ConfigReader (const char *filename)
{
  
  this->L = luaL_newstate ();

  // Load lua libraries.
  static const luaL_Reg luaLibs[] =
    {
      {"base", luaopen_base},
      {"math", luaopen_math},
      {NULL, NULL}
    };

  // Loop through all the functions in the array of library functions
  // and load them into the interpreter instance.
  const luaL_Reg * lib = luaLibs;
  for (; lib->func != NULL; lib++)
    {
      lib->func(this->L);
      lua_settop(this->L, 0);
    }
  
  // Run the file through the interpreter instance.
  int lua_err = luaL_dofile (this->L, filename);
  if (lua_err) {
    CANOP_LERRORF ("Cannot run file %s -- %s\n", 
		filename,
		lua_tostring(this->L,-1));
    lua_close (this->L);
  }

  // TODO: do we really need to do bookeeping for the strings?
  this->strings = sc_list_new (NULL);

} // ConfigReader::ConfigReader

// =======================================================
// =======================================================
ConfigReader::~ConfigReader()
{
  lua_close (this->L);

  // free all the strings we have allocated
  while (this->strings->first) {
    CANOP_FREE (sc_list_pop (this->strings));
  }

  sc_list_destroy (this->strings);

} // ConfigReader::~ConfigReader

// =======================================================
// =======================================================
int
ConfigReader::config_read_int (const char *key, int preset)
{
  
  int                 value;
  int                 retval = lua_intexpr (this->L, key, &value);

  return (retval ? value : preset);
  
} // ConfigReader::config_read_int

// =======================================================
// =======================================================
double
ConfigReader::config_read_double (const char *key, double preset)
{
  
  double              value;
  int                 retval = lua_numberexpr (this->L, key, &value);

  return (retval ? value : preset);
  
} // ConfigReader::config_read_double

// =======================================================
// =======================================================
const char *
ConfigReader::config_read_string (const char *key, const char *preset)
{
  
  const char         *r = lua_stringexpr (this->L, key);

  sc_list_append (this->strings, (void *) r);
  return (r ? r : preset);
  
} // ConfigReader::config_read_string

// =======================================================
// =======================================================
std::string
ConfigReader::config_read_std_string (const char *key, const char *preset)
{
  
  const char         *r = lua_stringexpr (this->L, key);

  sc_list_append (this->strings, (void *) r);
  return ( r ? std::string(r) : std::string(preset) );
  
} // ConfigReader::config_read_std_string

// kate: indent-width 2; tab-width 2;
// vim: set ts=2 sw=2 sts=2:
