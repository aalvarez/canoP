#include "quadrant_utils.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#else
#include <p8est_bits.h>
#endif

#include "canoP_base.h"

#ifndef USE_3D
/** Transform a quadrant coordinate into the space spanned by tree vertices and taking geometry into account.
 * \param [in] connectivity     Connectivity must provide the vertices.
 * \param [in] geom             Geometry.
 * \param [in] treeid           Identify the tree that contains x, y.
 * \param [in] x, y             Quadrant coordinates relative to treeid.
 * \param [out] vxyz            Transformed coordinates in vertex space.
 */
void
p4est_qcoord_to_vertex_with_geom (p4est_connectivity_t * conn,
				  p4est_geometry_t * geom,
				  p4est_topidx_t treeid,
				  p4est_qcoord_t x,
				  p4est_qcoord_t y,
				  double vxyz[3])
{

  UNUSED (conn);
  const double intsize = 1.0 / P4EST_ROOT_LEN;
  double xyz_logic[3] = { intsize*x,
			  intsize*y,
			  0. };

  // from logical coordinates to physical coordinates
  geom->X (geom, treeid, xyz_logic, vxyz);

}
#else
/** Transform a quadrant coordinate into the space spanned by tree vertices and taking geometry into account.
 * \param [in] connectivity     Connectivity must provide the vertices.
 * \param [in] geom             Geometry.
 * \param [in] treeid           Identify the tree that contains x, y.
 * \param [in] x, y             Quadrant coordinates relative to treeid.
 * \param [out] vxyz            Transformed coordinates in vertex space.
 */
void
p8est_qcoord_to_vertex_with_geom (p4est_connectivity_t * conn,
				  p4est_geometry_t * geom,
				  p4est_topidx_t treeid,
				  p4est_qcoord_t x,
				  p4est_qcoord_t y,
				  p4est_qcoord_t z,
				  double vxyz[3])
{
  
  UNUSED (conn);
  const double intsize = 1.0 / P4EST_ROOT_LEN;
  double xyz_logic[3] = { intsize*x,
			  intsize*y,
			  intsize*z };

  // from logical coordinates to physical coordinates
  geom->X (geom, treeid, xyz_logic, vxyz);

}
#endif /* USE_3D */

void
quadrant_print (int log_priority, p4est_quadrant_t * q)
{
  p4est_qcoord_t      x = (q->x) >> (P4EST_MAXLEVEL - q->level);
  p4est_qcoord_t      y = (q->y) >> (P4EST_MAXLEVEL - q->level);
#ifdef P4_TO_P8
  p4est_qcoord_t      z = (q->z) >> (P4EST_MAXLEVEL - q->level);

  CANOP_LOGF (log_priority, "x %d y %d z %d level %d\n", x, y, z, q->level);
#else
  CANOP_LOGF (log_priority, "x %d y %d level %d\n", x, y, q->level);
#endif
} /* quadrant_print */

double
quadrant_length (p4est_quadrant_t * quad)
{
  return (double) P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;
} /* quadrant_length */

/*
 * OUTDATED. Do not use !
 */
void
quadrant_length_dxdydz_cylindrical (p4est_quadrant_t * quad,
				    p4est_topidx_t     which_tree,
				    p4est_geometry_t * geom,
				    double           * dx,
				    double           * dy,
				    double           * dz)
{

  /*
   * Length of a cell in cylindrical geometry:
   *
   * Take care that in shell2d geometry/connectivity:
   * first direction is orthoradial, then radial, then z
   *
   * dx is r*dtheta
   * dy is dr
   * dz is dz
   */

  /* logical coordinates */
  double              xyz[3] = { 0, 0, 0 };

  /* physical coordinates */
  double              XYZ[3] = { 0, 0, 0 };

  double              r, r1, r2;
  double              theta, theta1, theta2, dtheta;
  UNUSED(theta);
  
  double h2 = 0.5 * P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;
  const double        intsize = 1.0 / P4EST_ROOT_LEN;

  /* 
   * get coordinates at cell center
   */
  xyz[0] = intsize*quad->x + h2; /* angular */
  xyz[1] = intsize*quad->y + h2; /* radial  */
#ifdef P4_TO_P8
  xyz[2] = intsize*quad->z + h2;
#endif

  // from logical coordinates to physical coordinates (cartesian)
  geom->X (geom, which_tree, xyz, XYZ);
  
  // cylindrical coordinates at cell center
  r     = sqrt( XYZ[0]*XYZ[0] + XYZ[1]*XYZ[1]);
  theta = atan2( XYZ[1], XYZ[0] );

  /* 
   * get coordinates at middle of face 0 (theta = theta0) 
   */
  xyz[0] = intsize*quad->x;      /* angular */
  xyz[1] = intsize*quad->y + h2; /* radial  */
#ifdef P4_TO_P8
  xyz[2] = intsize*quad->z + h2;
#endif

  // from logical coordinates to physical coordinates (cartesian)
  geom->X (geom, which_tree, xyz, XYZ);
  theta1 = atan2( XYZ[1], XYZ[0] );

  /* 
   * get coordinate at middle of face 1 (theta = theta0 + dtheta)
   */
  xyz[0] = intsize*quad->x + 2*h2; /* angular */
  xyz[1] = intsize*quad->y + h2;   /* radial  */
#ifdef P4_TO_P8
  xyz[2] = intsize*quad->z + h2;
#endif

  // from logical coordinates to physical coordinates (cartesian)
  geom->X (geom, which_tree, xyz, XYZ);
  theta2 = atan2( XYZ[1], XYZ[0] );

  /* 
   * get coordinates at middle of face 2 (r = r0) 
   */
  xyz[0] = intsize*quad->x + h2; /* angular */
  xyz[1] = intsize*quad->y;      /* radial  */
#ifdef P4_TO_P8
  xyz[2] = intsize*quad->z + h2;
#endif

  // from logical coordinates to physical coordinates (cartesian)
  geom->X (geom, which_tree, xyz, XYZ);
  r1 = sqrt( XYZ[0]*XYZ[0] + XYZ[1]*XYZ[1]);

  /* 
   * get coordinates at middle of face 3 (r = r0 + dr) 
   */
  xyz[0] = intsize*quad->x + h2;    /* angular */
  xyz[1] = intsize*quad->y + 2*h2;  /* radial  */
#ifdef P4_TO_P8
  xyz[2] = intsize*quad->z + h2;
#endif

  // from logical coordinates to physical coordinates (cartesian)
  geom->X (geom, which_tree, xyz, XYZ);
  r2 = sqrt( XYZ[0]*XYZ[0] + XYZ[1]*XYZ[1]);

  // dtheta
  dtheta = fabs(theta1-theta2);
  dtheta = fmin(dtheta, fabs(theta1-theta2-2*M_PI));
  dtheta = fmin(dtheta, fabs(theta1-theta2+2*M_PI));

  // dx is r*dtheta
  *dx = r * dtheta;

  // dy is dr
  *dy=r2-r1;

  // dz is dz !
  *dz = 1.0;

} /* quadrant_length_dxdydz_cylindrical */


double
quadrant_length_level (int level)
{
  return (double) P4EST_QUADRANT_LEN (level) / P4EST_ROOT_LEN;
} /* quadrant_length_level */

double
quadrant_length_ext (p4est_connectivity_t * c,
		     p4est_geometry_t * geom,
		     p4est_quadrant_t * q,
                     p4est_topidx_t treeid,
		     int face)
{
  double              v[6] = { 0, 0, 0, 0, 0, 0 };

  quadrant_face_vertices (c, geom, q, treeid, face, v);

  return sqrt (SC_SQR (v[3] - v[0]) + SC_SQR (v[4] - v[1]) +
#ifdef P4_TO_P8
               SC_SQR (v[5] - v[2]) +
#endif
               0);
} /* quadrant_length_ext */

void
quadrant_get_dx_dy_dz (p4est_connectivity_t * conn,
		       p4est_geometry_t * geom,
		       p4est_quadrant_t * quad,
		       p4est_topidx_t which_tree,
		       double *dx,
		       double *dy,
		       double *dz)
{


  /* get corner coordinates */
  double  v0[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 0, v0);
  double  v1[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 1, v1);
  double  v2[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 2, v2);
#ifdef USE_3D
  double  v4[3] = { 0, 0, 0 };
  quadrant_vertex_coord(conn, geom, which_tree, quad, 4, v4);
#endif
  
  double dl[3] = { v1[0]-v0[0],
		   v1[1]-v0[1],
		   v1[2]-v0[2]};
  
  *dx = sqrt( SC_SQR(dl[0]) +
	      SC_SQR(dl[1]) +
	      SC_SQR(dl[2]) );

  dl[0] = v2[0]-v0[0];
  dl[1] = v2[1]-v0[1];
  dl[2] = v2[2]-v0[2];

  *dy = sqrt( SC_SQR(dl[0]) +
	      SC_SQR(dl[1]) +
	      SC_SQR(dl[2]) );

#ifdef USE_3D
  dl[0] = v4[0]-v0[0];
  dl[1] = v4[1]-v0[1];
  dl[2] = v4[2]-v0[2];

  *dz = sqrt( SC_SQR(dl[0]) +
	      SC_SQR(dl[1]) +
	      SC_SQR(dl[2]) );  
#else
  *dz = 0.0;
#endif
  
  
} /* quadrant_get_dx_dy_dz */

#ifndef P4_TO_P8
void
quadrant_face_vertices (p4est_connectivity_t * c,
			p4est_geometry_t * geom,
                        p4est_quadrant_t * q,
                        p4est_topidx_t treeid, 
			int face, 
			double v[6])
{

  p4est_qcoord_t      h = P4EST_QUADRANT_LEN (q->level);
  int                 d[P4EST_DIM]; /* binary representation of corner number */

  int cornerId;
  
  for ( cornerId = 0; cornerId < 2; cornerId++) {

    /* retrieve binary representation */
    d[0] = (p4est_face_corners[face][cornerId] & 0x01);
    d[1] = (p4est_face_corners[face][cornerId] & 0x02) >> 1;
    
    if (geom != NULL) {
      
      p4est_qcoord_to_vertex_with_geom (c, geom, treeid,
					q->x + d[0] * h,
					q->y + d[1] * h,
					v + 3*cornerId);
    } else {
      
      p4est_qcoord_to_vertex (c, treeid,
			      q->x + d[0] * h,
			      q->y + d[1] * h,
			      v + 3*cornerId);
    }

  } /* end for cornerId */
  
} /* quadrant_face_vertices */

#else

void
quadrant_face_vertices (p4est_connectivity_t * c,
			p4est_geometry_t * geom,
			p4est_quadrant_t * q,
			p4est_topidx_t treeid,
			int face, 
			double v[12])
{
  p4est_qcoord_t      h = P4EST_QUADRANT_LEN (q->level);
  int                 d[P4EST_DIM];

  int cornerId; /* z-order sweep */

  for ( cornerId = 0; cornerId < 4; cornerId++) {

    d[0] = (p4est_face_corners[face][cornerId] & 0x01);
    d[1] = (p4est_face_corners[face][cornerId] & 0x02) >> 1;
    d[2] = (p4est_face_corners[face][cornerId] & 0x04) >> 2;

    if (geom != NULL) {

      p4est_qcoord_to_vertex_with_geom (c, geom, treeid, 
					q->x + d[0] * h, 
					q->y + d[1] * h,
					q->z + d[2] * h,
					v + 3*cornerId);

    } else {
      
      p4est_qcoord_to_vertex (c, treeid, 
			      q->x + d[0] * h, 
			      q->y + d[1] * h,
			      q->z + d[2] * h,
			      v + 3*cornerId);
    }
    
  } /* end for cornerId */

} /* quadrant_face_vertices */
#endif /* P4_TO_P8 */


void
quadrant_face_normal (p4est_connectivity_t * c,
		      p4est_geometry_t * geom,
		      p4est_quadrant_t * q,
                      p4est_topidx_t treeid,
		      int face,
		      double n[P4EST_DIM])
{
#ifdef USE_3D

  /* vertex0 : v[0..2] */
  /* vertex1 : v[3..5] */
  /* vertex2 : v[6..8] */
  /* vertex3 : v[9..11] */
  double              v[12];

  double *v0 = v;
  double *v1 = v+3;
  double *v2 = v+6;
  /*double *v3 = v+9;*/

  int dir = face/2;
  
  quadrant_face_vertices (c, geom, q, treeid, face, v);

  double v01[3] = { v1[0]-v0[0],
		    v1[1]-v0[1],
		    v1[2]-v0[2]};

  double v02[3] = { v2[0]-v0[0],
		    v2[1]-v0[1],
		    v2[2]-v0[2]};

  /* if dir is even, then normal is v01 ^ v02 */
  /* if dir is odd,  then normal is v02 ^ v01 */
  
  if (dir%2 == 0) { /* dir is even */

    n[0] = v01[1]*v02[2] - v01[2]*v02[1];
    n[1] = v01[2]*v02[0] - v01[0]*v02[2];
    n[2] = v01[0]*v02[1] - v01[1]*v02[0];
    
  } else {         /* dir is odd */

    n[0] = v02[1]*v01[2] - v02[2]*v01[1];
    n[1] = v02[2]*v01[0] - v02[0]*v01[2];
    n[2] = v02[0]*v01[1] - v02[1]*v01[0];

  }

  /* normalization */
  double norm = sqrt ( n[0]*n[0] + n[1]*n[1] + n[2]*n[2] );
  n[0] /= norm;
  n[1] /= norm;
  n[2] /= norm;

#else

  /* vertex0 : v[0..2] */
  /* vertex1 : v[3..5] */
  double  v[6];
  double *v0 = v;
  double *v1 = v+3;
  quadrant_face_vertices (c, geom, q, treeid, face, v);

  /* opposite face : just flip last bit of face */
  int face_opposite = face ^ 0x1;
  double  w[6];
  double *w0 = w;
  double *w1 = w+3;
  quadrant_face_vertices (c, geom, q, treeid, face_opposite, w);

  double v01[3] = { v1[0]-v0[0],
		    v1[1]-v0[1],
		    v1[2]-v0[2]};

  double v0w1[3] = { w1[0]-v0[0],
		     w1[1]-v0[1],
		     w1[2]-v0[2]};
  
  double w0v1[3] = { v1[0]-w0[0],
		     v1[1]-w0[1],
		     v1[2]-w0[2]};

  /* v0w1 ^ w0v1 */
  double quad_normal[3] = {v0w1[1]*w0v1[2] - v0w1[2]*w0v1[1],
			   v0w1[2]*w0v1[0] - v0w1[0]*w0v1[2],
			   v0w1[0]*w0v1[1] - v0w1[1]*w0v1[0]};
  
  /* if (dir == 0) { /\* face=0,1 ==> normal along dir0 *\/ */
  /*   n[0] =  (v1[1] - v0[1]); */
  /*   n[1] = -(v1[0] - v0[0]); */
  /*   n[2] = 0; */
  /* } else {           /\* face=2,3 ==> normal along dir 1 *\/ */
  /*   n[0] = -(v1[1] - v0[1]); */
  /*   n[1] =  (v1[0] - v0[0]); */
  /*   n[2] = 0; */
  /* } */

  /* compute v01 ^ quad_normal to obtain face normal */
  n[0] = v01[1]*quad_normal[2] - v01[2]*quad_normal[1];
  n[1] = v01[2]*quad_normal[0] - v01[0]*quad_normal[2];
  n[2] = v01[0]*quad_normal[1] - v01[1]*quad_normal[0];

  /* normalization */
  double norm = sqrt ( n[0]*n[0] + n[1]*n[1] + n[2]*n[2] );
  n[0] /= norm;
  n[1] /= norm;
  n[2] /= norm;
  
#endif
} /* quadrant_face_normal */


void
quadrant_center_vertex (p4est_connectivity_t * connectivity,
			p4est_geometry_t *geom,
                        p4est_topidx_t tree,
                        p4est_quadrant_t * quad, double xyz[3])
{

  p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quad->level) / 2;

  if (geom != NULL) {
    
    double            h2 =
      0.5 * P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;
    const double      intsize = 1.0 / P4EST_ROOT_LEN;

    double            xyz_logic[3] = { 0., 0., 0. };

#ifdef P4_TO_P8
    xyz_logic[2] = intsize*quad->z + h2;
#endif
    xyz_logic[1] = intsize*quad->y + h2;
    xyz_logic[0] = intsize*quad->x + h2;

    // from logical coordinates to physical coordinates
    geom->X (geom, tree, xyz_logic, xyz);

  } else { // regular cartesian geometry

    p4est_qcoord_to_vertex (connectivity, tree, 
			    quad->x + half_length,
			    quad->y + half_length,
#ifdef P4_TO_P8
			    quad->z + half_length,
#endif
			    xyz);

  } // end cartesian geometry

} /* end quadrant_center_vertex */

void
quadrant_vertex_coord (p4est_connectivity_t * connectivity,
		       p4est_geometry_t *geom,
		       p4est_topidx_t tree,
		       p4est_quadrant_t * quad, 
		       int                vertexId,
		       double xyz[3])
{

  p4est_qcoord_t      quad_length = P4EST_QUADRANT_LEN (quad->level);

  CANOP_ASSERT(vertexId >= 0);
  CANOP_ASSERT(vertexId <  P4EST_CHILDREN);

  // use binary decomposition of vertexId
  const int    off_x = (vertexId & 0x01);
  const int    off_y = (vertexId & 0x02) >> 1;
  const int    off_z = (vertexId & 0x04) >> 2;
  UNUSED(off_z);

  if (geom != NULL) {

    double            h2 = 1.0 *
      P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;
    const double      intsize = 1.0 / P4EST_ROOT_LEN;

    double            xyz_logic[3] = { 0., 0., 0. };

#ifdef P4_TO_P8
    xyz_logic[2] = intsize*quad->z + off_z * h2;
#endif
    xyz_logic[1] = intsize*quad->y + off_y * h2;
    xyz_logic[0] = intsize*quad->x + off_x * h2;

    // from logical coordinates to physical coordinates
    geom->X (geom, tree, xyz_logic, xyz);

  } else { // regular cartesian geometry

    p4est_qcoord_to_vertex (connectivity, tree, 
			    quad->x + off_x * quad_length,
			    quad->y + off_y * quad_length,
#ifdef P4_TO_P8
			    quad->z + off_z * quad_length,
#endif
			    xyz);

  } // end cartesian geometry

} /* end quadrant_vertex_coord */


void
quadrant_face_center (p4est_connectivity_t * connectivity,
		      p4est_geometry_t *geom,
		      p4est_topidx_t tree,
		      p4est_quadrant_t * quad, 
		      face_id_t faceId,
		      double xyz[3])
{

  if (geom != NULL) {

    double            h2 =
      0.5 * P4EST_QUADRANT_LEN (quad->level) / P4EST_ROOT_LEN;
    const double      intsize = 1.0 / P4EST_ROOT_LEN;

    double            xyz_logic[3] = { 0., 0., 0. };

    // cell center
#ifdef P4_TO_P8
    xyz_logic[2] = intsize*quad->z + h2;
#endif
    xyz_logic[1] = intsize*quad->y + h2;
    xyz_logic[0] = intsize*quad->x + h2;

    // face center
    if (     faceId == FACE_XMIN)
      xyz_logic[0] -= h2;
    else if (faceId == FACE_XMAX)
      xyz_logic[0] += h2;

    if (     faceId == FACE_YMIN)
      xyz_logic[1] -= h2;
    else if (faceId == FACE_YMAX)
      xyz_logic[1] += h2;

#ifdef P4_TO_P8
    if (     faceId == FACE_ZMIN)
      xyz_logic[2] -= h2;
    else if (faceId == FACE_ZMAX)
      xyz_logic[2] += h2;
#endif

    // from logical coordinates to physical coordinates
    geom->X (geom, tree, xyz_logic, xyz);

  } else { // regular cartesian geometry

    p4est_qcoord_t      half_length = P4EST_QUADRANT_LEN (quad->level) / 2;
    p4est_qcoord_t      x,y,z;

    UNUSED(z);

    // cell center
    x = quad->x + half_length;
    y = quad->y + half_length;
#ifdef P4_TO_P8
    z = quad->z + half_length;
#endif

    if (faceId == FACE_XMIN)
      x -= half_length;
    else
      x += half_length;

    if (faceId == FACE_YMIN)
      y -= half_length;
    else
      y += half_length;

#ifdef P4_TO_P8
    if (faceId == FACE_ZMIN)
      z -= half_length;
    else
      z += half_length;
#endif

    // compute physical coordinates
    p4est_qcoord_to_vertex (connectivity, tree, 
			    x,
			    y,
#ifdef P4_TO_P8
			    z,
#endif
			    xyz);

  } // end cartesian geometry

} /* end quadrant_face_center */

int
face_side_level (p4est_iter_face_side_t * side)
{
  if (side->is_hanging) {
    return side->is.hanging.quad[0]->level;
  }

  return side->is.full.quad->level;
} /* face_side_level */
void
quadrant_get_level_integer_index (p4est_quadrant_t * quad,
				  int ijk[3])
{

  ijk[0] = quad->x/P4EST_QUADRANT_LEN(quad->level);
  ijk[1] = quad->y/P4EST_QUADRANT_LEN(quad->level);
#ifdef P4_TO_P8
  ijk[2] = quad->z/P4EST_QUADRANT_LEN(quad->level);
#endif

} /* quadrant_get_level_integer_index */

double
quadrant_face_area(double dx, double dy, double dz, int direction)
{

  double dS;
  UNUSED(dz);

#ifdef USE_3D
  if (direction == DIR_X)
    dS = dy*dz;
  else if (direction == DIR_Y)
    dS = dz*dx;
  else
    dS = dx*dy;
#else
  if (direction == DIR_X)
    dS = dy;
  else
    dS = dx;
#endif

  return dS;
} /* quadrant_face_area */

double
quadrant_face_area_general(p4est_connectivity_t * c,
			   p4est_geometry_t * geom,
			   p4est_quadrant_t * q,
			   p4est_topidx_t treeid,
			   int faceId)
{

  double area=0;
  
#ifdef USE_3D

  /* 
   * divide a quadrangle into 2 triangles 
   * - 0,1,2
   * - 1,2,3
   */

  /* 
   * area of a triangle A,B,C :
   * S = 1/2 || AB ^ AC ||, where
   * AB = (b0, b1, b2)
   * AC = (c0, c1, c2)
   * so that S = 1/2 * sqrt ( (b1c2-b2c1)**2 +
   *                          (b2c0-b0c2)**2 +
   *                          (b0c1-b1c0)**2 )
   */

  /* retriave face corner coordinates : 3*P4EST_CHILDREN values */
  double v[3*P4EST_CHILDREN];
  quadrant_face_vertices (c, geom, q, treeid, faceId, v);

  /* divide face into 2 triangles */
  int triangle[2][3] = {
    {0,1,2},
    {1,2,3}
  };

  int iTriangle;
  
  for (iTriangle = 0; iTriangle<2; iTriangle++) {

    double *v0 = v+triangle[iTriangle][0]*3;
    double *v1 = v+triangle[iTriangle][1]*3;
    double *v2 = v+triangle[iTriangle][2]*3;
    
    double e01[3] = { v1[0]-v0[0],
		      v1[1]-v0[1],
		      v1[2]-v0[2]};
    
    double e02[3] = { v2[0]-v0[0],
		      v2[1]-v0[1],
		      v2[2]-v0[2]};
    
    area += 0.5 * sqrt( SC_SQR(e01[1]*e02[2]-e01[2]*e02[1]) +
			SC_SQR(e01[2]*e02[0]-e01[0]*e02[2]) +
			SC_SQR(e01[0]*e02[1]-e01[1]*e02[0]) );
  }
  
#else /* 2D */

  /* vertex0 : v[0..2] */
  /* vertex1 : v[3..5] */
  double  v[6];
  double *v0 = v;
  double *v1 = v+3;
  quadrant_face_vertices (c, geom, q, treeid, faceId, v);

  double v01[3] = { v1[0]-v0[0],
		    v1[1]-v0[1],
		    v1[2]-v0[2]};

  /* length of edge 0-1 */
  area = sqrt( SC_SQR(v01[0]) +
	       SC_SQR(v01[1]) +
	       SC_SQR(v01[2]) );
  
#endif

  return area;
  
} /* quadrant_face_area_general */


double
quadrant_cell_volume(double dx, double dy, double dz)
{

  UNUSED(dz);

#ifdef USE_3D
  return dx*dy*dz;
#else
  return dx*dy;
#endif

} /* quadrant_cell_volume */

double
quadrant_cell_volume_general(p4est_connectivity_t * c,
			     p4est_geometry_t * geom,
			     p4est_quadrant_t * q,
			     p4est_topidx_t treeid)
{

  double volume=0;
  
  p4est_qcoord_t      h = P4EST_QUADRANT_LEN (q->level);
  int                 d[P4EST_DIM]; /* binary representation of corner number */

  int cornerId;

#ifdef USE_3D
  
  double v[3*P4EST_CHILDREN];
  
  /* retrieve corner coordinates in real space */
  for ( cornerId = 0; cornerId < P4EST_CHILDREN; cornerId++) {

    /* retrieve binary representation */
    d[0] = (cornerId & 0x01);
    d[1] = (cornerId & 0x02) >> 1;
    d[2] = (cornerId & 0x04) >> 2;

    if (geom != NULL) {
      
      p4est_qcoord_to_vertex_with_geom (c, geom, treeid,
					q->x + d[0] * h,
					q->y + d[1] * h,
					q->z + d[2] * h,
					v + 3*cornerId);
    } else {
      
      p4est_qcoord_to_vertex (c, treeid,
			      q->x + d[0] * h,
			      q->y + d[1] * h,
			      q->z + d[2] * h,
			      v + 3*cornerId);
    }
    
  } /* end for cornerId */

  /* 
   * divide an hexahedron into 5 tetrahedrons  with nodes:
   *
   * - 0,1,3,5
   * - 0,4,5,6
   * - 0,2,3,6
   * - 3,5,6,7
   * - 0,3,5,6
   *
   * see figure at 
   * http://www.cvel.clemson.edu/modeling/EMAG/EMAP/emap4/Meshing.html
   */

  int tetrahedron[5][4] = {
    {0,1,3,5},
    {0,4,5,6},
    {0,2,3,6},
    {3,5,6,7},
    {0,3,5,6}
  };
  
  /* 
   * volume of a tetrahedron:
   * 
   * V = 1/6 | det(AB,AC,AD) |
   */
  double tmp = 0;
  int iTet;

  for (iTet=0; iTet<5; iTet++) {

    double *v0 = v+tetrahedron[iTet][0]*3;
    double *v1 = v+tetrahedron[iTet][1]*3;
    double *v2 = v+tetrahedron[iTet][2]*3;
    double *v3 = v+tetrahedron[iTet][3]*3;
    
    double e01[3];
    double e02[3];
    double e03[3];
    
    e01[0] = v1[0]-v0[0];
    e01[1] = v1[1]-v0[1];
    e01[2] = v1[2]-v0[2];
    
    e02[0] = v2[0]-v0[0];
    e02[1] = v2[1]-v0[1];
    e02[2] = v2[2]-v0[2];
    
    e03[0] = v3[0]-v0[0];
    e03[1] = v3[1]-v0[1];
    e03[2] = v3[2]-v0[2];
    
    tmp = 0;
    /* compute tmp = determinant of (e01,e02,e03) */
    tmp += ( e01[0] * (e02[1]*e03[2] - e02[2]*e03[1]) );
    tmp -= ( e01[1] * (e02[0]*e03[2] - e02[2]*e03[0]) );
    tmp += ( e01[2] * (e02[0]*e03[1] - e02[1]*e03[0]) );
    volume += 1.0/6*fabs(tmp);
  } /* end for iTet */
  
#else

  /* 
   * divide a quadrangle into 2 triangles 
   * - 0,1,2
   * - 1,2,3
   */

  /* 
   * area of a triangle A,B,C :
   * S = 1/2 || AB ^ AC ||, where
   * AB = (b0, b1, b2)
   * AC = (c0, c1, c2)
   * so that S = 1/2 * sqrt ( (b1c2-b2c1)**2 +
   *                          (b2c0-b0c2)**2 +
   *                          (b0c1-b1c0)**2 )
   */

  double v[3*P4EST_CHILDREN];
  
  /* retrieve corner coordinates in real space */
  for ( cornerId = 0; cornerId < P4EST_CHILDREN; cornerId++) {

    /* retrieve binary representation */
    d[0] = (cornerId & 0x01);
    d[1] = (cornerId & 0x02) >> 1;

    if (geom != NULL) {
      
      p4est_qcoord_to_vertex_with_geom (c, geom, treeid,
					q->x + d[0] * h,
					q->y + d[1] * h,
					v + 3*cornerId);
    } else {
      
      p4est_qcoord_to_vertex (c, treeid,
			      q->x + d[0] * h,
			      q->y + d[1] * h,
			      v + 3*cornerId);
    }
    
  } /* end for cornerId */

  int triangle[2][3] = {
    {0,1,2},
    {1,2,3}
  };

  int iTriangle;
  
  for (iTriangle = 0; iTriangle<2; iTriangle++) {

    double *v0 = v+triangle[iTriangle][0]*3;
    double *v1 = v+triangle[iTriangle][1]*3;
    double *v2 = v+triangle[iTriangle][2]*3;
    
    double e01[3] = { v1[0]-v0[0],
		      v1[1]-v0[1],
		      v1[2]-v0[2]};
    
    double e02[3] = { v2[0]-v0[0],
		      v2[1]-v0[1],
		      v2[2]-v0[2]};
    
    volume += 0.5 * sqrt( SC_SQR(e01[1]*e02[2]-e01[2]*e02[1]) +
			  SC_SQR(e01[2]*e02[0]-e01[0]*e02[2]) +
			  SC_SQR(e01[0]*e02[1]-e01[1]*e02[0]) );
  }
    
#endif

  return volume;

} /* quadrant_cell_volume_general */
