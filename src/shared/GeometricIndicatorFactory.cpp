#include "GeometricIndicatorFactory.h"

#include "Solver.h"
#include "quadrant_utils.h"

#include <cmath>

int
geom_indicator_radial (p4est_t *p4est, p4est_topidx_t which_tree,
                       p4est_quadrant_t *quadrant) {

  Solver             *solver = solver_from_p4est (p4est);

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t   *geom   = solver->get_geom_compute();

  /* get config reader */
  ConfigReader       *cfg    = solver->m_cfg;

  double  XYZ[3] = { 0, 0, 0 };
  double  dx, dy, dz;

  // prevent from refining too much
  if (quadrant->level >= solver->m_max_refine)
    return 0;

  // get the physical coordinates of the center of the quad
  quadrant_center_vertex (p4est->connectivity, geom, which_tree, quadrant, XYZ);

  // get the size of the quad
  quadrant_get_dx_dy_dz (p4est->connectivity, geom, quadrant, which_tree, &dx, &dy, &dz);

  double x_c = cfg->config_read_double ("settings.radial_refine_center_x", 0.5);
  double y_c = cfg->config_read_double ("settings.radial_refine_center_y", 0.5);
#ifdef USE_3D
  double z_c = cfg->config_read_double ("settings.radial_refine_center_z", 0.5);
#endif

  double eps_refine = cfg->config_read_double("settings.radial_refine_epsilon", 0.5);

  double epsilon = SC_SQR((XYZ[0] - x_c) / dx)
                 + SC_SQR((XYZ[1] - y_c) / dy);
#ifdef USE_3D
  epsilon +=       SC_SQR((XYZ[2] - z_c) / dz);
#endif

  if (epsilon < P4EST_DIM / (4.0 * SC_SQR(eps_refine)))
    return 1;

  return 0;

} // geom_indicator_radial

GeometricIndicatorFactory::GeometricIndicatorFactory() {
  // Register the indicators here

  registerIndicator("none", nullptr);
  registerIndicator("radial", geom_indicator_radial);

} // GeometricIndicatorFactory::GeometricIndicatorFactory
