#ifndef SCALAR_LIMITER_FACTORY_H_
#define SCALAR_LIMITER_FACTORY_H_

#include <string>
#include <map>

#include "canoP_base.h"

double
limiter_minmod (double ul, double ur,
		double dx, double dxl, double dxr);
double
limiter_maxmod (double ul, double ur,
		double dx, double dxl, double dxr);

/**
 * A generic Limiter function factory.
 *
 * The actual complete function pointer type is passed as a template parameter.
 *
 * \tparam scalar_limiter_type is the function pointer type
 *
 * e.g. (for ramses scheme):
 * double (*) (double ul, double ur,
 *             double dx, double dxl, double dxr);
 */
template<typename scalar_limiter_type>
class ScalarLimiterFactoryTmpl {

public:
  ScalarLimiterFactoryTmpl();
  virtual ~ScalarLimiterFactoryTmpl() {};

  /**
   * \brief Define limiter as a function pointer.
   *
   * Scalar value limiter function pointer type.
   *
   */
  using scalar_limiter_t = scalar_limiter_type;

  private:
  /**
   * Map to associate a label with a a limiter.
   */
  using LimiterMap = std::map<std::string, scalar_limiter_t>;
  LimiterMap m_limiterMap;

public:
  /**
   * Routine to insert an limiterr function into the map.
   * Note that this register function can be used to serve 
   * at least two different purposes:
   * - in the concrete factory: register existing limiter's
   * - in some client code, register a limiter from a plugin code, at runtime.
   */
  void registerLimiter(const std::string& key, scalar_limiter_t lim) {
    m_limiterMap[key] = lim;
  };

  /**
   * \brief Retrieve one of the possible limiters by name.
   *
   */
  virtual scalar_limiter_t limiter_byname (const std::string &name) {

    // find the limiterr in the register map
    typename LimiterMap::iterator it = m_limiterMap.find(name);

    // if found, just return it
    if ( it != m_limiterMap.end() )
      return it->second;
    
    // if not found, return null pointer
    // it is the responsability of the client code to deal with
    // the possibility to have a nullptr as function pointer (does nothing).
    //
    // Note that the concrete factory may chose a different behavior
    // and return a valid default callback (why not ?).
    //CANOP_GLOBAL_INFOF ("Unrecognized limiter \"%s\".\n", name);
    return nullptr;

  }; // limiter_byname

}; // class ScalarLimiterfactoryTmpl

// define one scalar limiter function pointer type
using scalar_limiter_ramses_t = double (*) (double ul, double ur,
					    double dx, double dxl, double dxr);


/**
 * Define a concrete factory.
 *
 * For backward compatility, we need this here.
 * This could be refactored, so that only actual scheme define their own
 * scalar limiter factory.
 */
using ScalarLimiterFactory = ScalarLimiterFactoryTmpl<scalar_limiter_ramses_t>;

  
#endif // SCALAR_LIMITER_FACTORY_H_
