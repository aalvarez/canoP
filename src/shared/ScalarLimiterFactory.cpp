#include "ScalarLimiterFactory.h"
#include "utils.h" // for UNUSED and ISFUZZYNULL macros

#include <cmath>

const double        beta = 1.5;


// ====================================================================
// ====================================================================
double
limiter_minmod (double ul, double ur,
		double dx, double dxl, double dxr)
{
  UNUSED(dx);
  if ((ul * ur) < 0) {
    return 0;
  }
  else if (fabs (ul/dxl) < fabs (ur/dxr)) {
    return ul/dxl;
  }
  else {
    return ur/dxr;
  }
} // limiter_minmod

// ====================================================================
// ====================================================================
double
limiter_maxmod (double ul, double ur,
		double dx, double dxl, double dxr)
{
  UNUSED(dx);
  if ((ul * ur) < 0) {
    return 0;
  }
  else if (fabs (ul/dxl) > fabs (ur/dxr)) {
    return ul/dxl;
  }
  else {
    return ur/dxr;
  }
} // limiter_maxmod

// ====================================================================
// ====================================================================
double
limiter_mc (double ul, double ur,
	    double dx, double dxl, double dxr)
{
  // return limiter_minmod (limiter_minmod (ul, ur), 0.5 * (ur - ul)); ?????
  return limiter_minmod (2 * limiter_minmod (ul/dx, ur/dx, 1, 1, 1), 
			 (ur + ul)/(dxl + dxr),
			 1, 1, 1);
} // limiter_mc

// ====================================================================
// ====================================================================
double
limiter_smart (double ul, double ur,
	       double dx, double dxl, double dxr)
{
  UNUSED(dx);
  UNUSED(dxl);
  UNUSED(dxr);
  
  if (ISFUZZYNULL (ur)) {
    return 0;
  }

  double              r = ul / ur;
  return fmax (0, fmin (fmin (2.0 * r, 0.25 + 0.75 * r), 4)) * ur;

} // limiter_smart

// ====================================================================
// ====================================================================
double
limiter_superbee (double ul, double ur,
		  double dx, double dxl, double dxr)
{
  UNUSED(dx);
  UNUSED(dxl);
  UNUSED(dxr);
  if (ISFUZZYNULL (ur)) {
    return 0;
  }

  double              r = ul / ur;
  UNUSED(r);

  //return fmax (0, fmax (fmin (2.0 * r, 1), fmin (r, 2))) * ur;
  return limiter_maxmod(limiter_minmod(2 * ul/dx, ur/dxr, 1, 1, 1),
			limiter_minmod(2 * ur/dx, ul/dxl, 1, 1, 1),
			1, 1, 1);
} // limiter_superbee

// ====================================================================
// ====================================================================
double
limiter_sweby (double ul, double ur, double dx, double dxl, double dxr)
{
  UNUSED(dx);
  UNUSED(dxl);
  UNUSED(dxr);
  if (ISFUZZYNULL (ur)) {
    return 0;
  }

  double              r = ul / ur;
  return fmax (0, fmax (fmin (beta * r, 1), fmin (r, beta))) * ur;

} // limiter_sweby

// ====================================================================
// ====================================================================
double
limiter_charm (double ul, double ur, double dx, double dxl, double dxr)
{
  UNUSED(dx);
  UNUSED(dxl);
  UNUSED(dxr);
  if (ISFUZZYNULL (ur)) {
    return 0;
  }

  double              r = ul / ur;
  return r * (3.0 * r + 1) / ( (r + 1) * (r + 1) ) * (r > 0) * ur;

} // limiter_charm

// ====================================================================
// ====================================================================
double
limiter_osher (double ul, double ur, double dx, double dxl, double dxr)
{
  UNUSED(dx);
  UNUSED(dxl);
  UNUSED(dxr);
  if (ISFUZZYNULL (ur)) {
    return 0;
  }

  double              r = ul / ur;
  return fmax (0, fmin (r, beta)) * ur;

} // limiter_osher

// ====================================================================
// ====================================================================
double
limiter_mc_ramses (double ul, double ur,
		   double dx, double dxl, double dxr)
{
  UNUSED(dx);
  
  double slope_type = 2.0; // FIX ME: make this configurable

  double dlft = slope_type*ul/dxl;
  double drgt = slope_type*ur/dxr;
  double dcen = 0.5*(ul+ur)/((dxl+dxr)*0.5);

  double dsgn = (dcen >= 0) ? 1.0 : -1.0;
  double slop = fmin( fabs(dlft), fabs(drgt) );

  if ((ul * ur) < 0) {
    return 0;
  } else {
    return dsgn * fmin( slop, fabs(dcen) );
  }

} // limiter_mc_ramses

// =======================================================
// =======================================================
template<>
ScalarLimiterFactoryTmpl<scalar_limiter_ramses_t>::ScalarLimiterFactoryTmpl()
{

  registerLimiter("minmod",    limiter_minmod);
  registerLimiter("mc",        limiter_mc);
  registerLimiter("smart",     limiter_smart);
  registerLimiter("superbee",  limiter_superbee);
  registerLimiter("sweby",     limiter_sweby);
  registerLimiter("charm",     limiter_charm);
  registerLimiter("osher",     limiter_osher);
  registerLimiter("mc_ramses", limiter_mc_ramses);

} // ScalarLimiterFactory
