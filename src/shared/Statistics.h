#ifndef STATISTICS_H_
#define STATISTICS_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

#include <string>
#include <map>
#include <ctime>

#include <mpi.h>

/**
 * Compute and gather performance statistics of all parts of AMR algorithms.
 */
class Statistics
{

public:
  /**
   * \brief Accumulation types.
   *
   * There are two accumulations: at every time step over MPI tasks, and over
   * all time steps. The \c timer_accum::none value is only valid over MPI
   * tasks, where it prevents communications.
   */
  enum class timer_accum {
    sum, min, max, mean, none
  };

  /**
   * \brief Add a new timer.
   *
   * \param name           A name for the timer
   * \param mpi_accum      The accumulation over MPI tasks
   * \param time_accum     The accumulation over time steps
   *
   * \return true if the timer has been created, false if the name already exists
   */
  static bool add_timer (const char *name, timer_accum mpi_accum, timer_accum time_accum);

protected:
  struct timer_info_t
  {
    const char *name;
    timer_accum mpi_accum;
    timer_accum time_accum;

    timer_info_t(): name(nullptr) {} // used by map::operator[]
    timer_info_t(const char *name_, timer_accum mpi_accum_, timer_accum time_accum_):
      name(name_), mpi_accum(mpi_accum_), time_accum(time_accum_) {}
  };

  using timer_info_map = std::map<std::string, timer_info_t>;

  static timer_info_map s_timer_info;

public:
  /**
   * \brief Create a new statistics structure.
   *
   * All fields are initialized to TYPE_MAX and TYPE_MIN, as need be.
   */
  Statistics(MPI_Comm mpicomm, int mpisize,
	     int mpirank,int verboseLevel);

  /**
   * \brief Destroy the statistics struct.
   */
  ~Statistics();

  MPI_Comm            m_mpicomm;  // the global communicator
  int                 m_mpisize;  // number of processses
  int                 m_mpirank;  // the current process

  // two arrays of size mpirank that contain at position mpirank the
  // min / max for the current process. Before writing, this is gathered
  // in process 0.
  p4est_locidx_t     *m_local_num_quadrants[2];

  // global min / max number of quadrants. Sum of the local_num_quadrants
  p4est_gloidx_t      m_global_num_quadrants[2];

  std::time_t         m_start; //!< run start timestamp
  std::time_t         m_end;   //!< run end timestamp

  int                 m_iterations;       //!< number of iterations
  double              m_t;        //!< final time
  double              m_dt[2];    //!< min / max time step
  int                 m_level[2]; //!< min / max level

  double              m_norml1;   //!< L1 norm
  double              m_norml2;   //!< L2 norm

  std::map<std::string, double> m_timers;

  int                 m_verboseLevel;

  /**
   * \brief Update the min / max time step from this new value.
   */
  void                statistics_dt (double dt);
  
  /**
   * \brief Update the min / max levels from this new values.
   */
  void                statistics_level (int min, int max);
  
  /**
   * \brief Update the min / max local number of quadrants from this new value.
   */
  void                statistics_num_quadrants (int lnq);

  /**
   * \brief Update a timer by its name.
   */
  void                statistics_timing_byname (std::string name, double t);
  
  /**
   * \brief Update the min / max time to advance by one iteration.
   */
  void                statistics_timing_scheme (double t);
  
  /**
   * \brief Update the min / max time to write the solution this new value.
   */
  void                statistics_timing_io (double t);
  
  /**
   * \brief Update the min / max time to adapt the mesh from this new value.
   */
  void                statistics_timing_adapt (double t);
  void                statistics_timing_adapt_mark (double t);
  void                statistics_timing_adapt_balance (double t, double *stat_time);
  void                statistics_timing_adapt_partition (double t);
  
  /**
   * \brief Write the data inside the statistics struct to a file.
   *
   * Only process 0 will write to the file. Before writing, a global reduce
   * and gather is done on all the members to get the corresponding global
   * values.
   */
  void                statistics_write (const std::string &filename);

private:
  /**
   * \brief Get global values for all the members of the statistics struct.
   */
  void                statistics_reduce ();
  
}; // class Statistics

#endif // STATISTICS_H_
