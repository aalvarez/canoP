/*
 * The following code is adapted from original p4est example step4.
 *
 * Use bi/tri-linear finite elements to solve Poisson problem.
 * - higher order finite element are really complex to implement:
 *   indeed, multiple nodes/degree of freedom must be interpolated
 *   on hanging faces, using nodes/dof on the other (larger) side 
 *   of the face. The combinatorics of all possibilties is complex.
 * 
 * TODO:
 * - take into account cell-average values versus node values.
 * - change border conditions.
 *
 */

/*
  This file is part of p4est.
  p4est is a C library to manage a collection (a forest) of multiple
  connected adaptive quadtrees or octrees in parallel.

  Copyright (C) 2010 The University of Texas System
  Written by Carsten Burstedde, Lucas C. Wilcox, and Tobin Isaac

  p4est is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  p4est is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with p4est; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/* 
 * p4est has two separate interfaces for 2D and 3D, p4est*.h and p8est*.h.
 * Most API functions are available for both dimensions.  The header file
 * p4est_to_p8est.h #define's the 2D names to the 3D names such that most code
 * only needs to be written once.  In this example, we rely on this. 
 */

#include "PoissonFESolver.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_ghost.h>
#include <p4est_lnodes.h>
#include <p4est_vtk.h>
#else
#include <p8est_bits.h>
#include <p8est_ghost.h>
#include <p8est_lnodes.h>
#include <p8est_vtk.h>
#endif

#include <cmath>

#include "canoP_base.h"
#include "Lnode_Utils.h"
#include "quadrant_utils.h"

static const int    zero = 0;                   /**< Constant zero. */
static const int    ones = P4EST_CHILDREN - 1;  /**< One bit per dimension. */

/** Determine the boundary status on the unit square/cube.
 * Check if a given quadrant touches the domain external border.
 *
 * \param [in] p4est    Can be used to access the connectivity.
 * \param [in] tt       The tree number (always zero for the unit square).
 * \param [in] node     The corner node of an element to be examined.
 * \return              True for Dirichlet boundary, false otherwise.
 */
static int
is_boundary_unitsquare (p4est_t * p4est,
			p4est_topidx_t tt,
                        p4est_quadrant_t * node)
{
  /* For this simple connectivity it is sufficient to check x, y (and z). */
  return (node->x == 0 || node->x == P4EST_ROOT_LEN ||
          node->y == 0 || node->y == P4EST_ROOT_LEN ||
#ifdef P4_TO_P8
          node->z == 0 || node->z == P4EST_ROOT_LEN ||
#endif
          0);
}

/** Compute the inner product of two node vectors in parallel.
 *
 * \param [in] p4est          The forest is not changed.
 * \param [in] lnodes         The node numbering is not changed.
 * \param [in] v1             First node vector.
 * \param [in] v2             Second node vector.
 * \return                    Parallel l_2 inner product over the domain.
 */
static double
vector_dot (p4est_t * p4est,
	    p4est_lnodes_t * lnodes,
            const double *v1,
	    const double *v2)
{
  const int           nown = lnodes->owned_count;
  int                 mpiret;
  int                 lnid;
  double              lsum, gsum;

  // We only sum the locally owned values to avoid double counting,
  // i.e. don't use the "ghost" nodes.
  lsum = 0.;
  for (lnid = 0; lnid < nown; ++lnid) {
    lsum += v1[lnid] * v2[lnid];
  }

  // The reduction result is made available on all processors.
  mpiret = sc_MPI_Allreduce (&lsum, &gsum, 1, sc_MPI_DOUBLE, sc_MPI_SUM,
                             p4est->mpicomm);
  SC_CHECK_MPI (mpiret);

  // return global sum
  return gsum;
  
} /* vector_dot */

/** 
 * Compute y := y + a * x (node vectors).
 *
 * \param [in] p4est          The forest is not changed.
 * \param [in] lnodes         The node numbering is not changed.
 * \param [in] a              The scalar.
 * \param [in] x              First node vector.
 * \param [in,out] y          Second node vector.
 */
static void
node_vector_axpy (p4est_t * p4est,
		  p4est_lnodes_t * lnodes,
		  double a,
		  const double *x,
		  double *y)
{
  const int           nloc = lnodes->num_local_nodes;
  int                 lnid;

  // All nodes contribute: locally owned and ghosts.
  for (lnid = 0; lnid < nloc; ++lnid) {
    y[lnid] += a * x[lnid];
  }
  
} /* node_vector_axpy */

/** 
 * Compute y := x + b * y.
 *
 * \param [in] p4est          The forest is not changed.
 * \param [in] lnodes         The node numbering is not changed.
 * \param [in] x              First node vector.
 * \param [in] b              The scalar.
 * \param [in,out] y          Second node vector.
 */
static void
vector_xpby (p4est_t * p4est,
	     p4est_lnodes_t * lnodes,
	     const double *x,
             double b,
	     double *y)
{
  const int           nloc = lnodes->num_local_nodes;
  int                 lnid;
  double              yy;

  // All nodes contribute: locally owned and ghosts.
  for (lnid = 0; lnid < nloc; ++lnid) {
    yy = y[lnid];
    y[lnid] = x[lnid] + b * yy;
  }
  
} /* vector_xpby */

/** Zero all entries of a vector.
 *
 * \param [in] p4est          The forest is not changed.
 * \param [in] lnodes         The node numbering is not changed.
 * \param [out] x             The vector.
 */
static void
vector_zero (p4est_t * p4est,
	     p4est_lnodes_t * lnodes,
	     double *x)
{
  const int           nloc = lnodes->num_local_nodes;

  memset (x, 0., nloc * sizeof (double));
  
} /* vector_zero */

/** 
 * copy a node vector.
 *
 * \param [in] p4est          The forest is not changed.
 * \param [in] lnodes         The node numbering is not changed.
 * \param [in] x              Input node vector.
 * \param [out] y             output node vector.
 */
static void
node_vector_copy (p4est_t * p4est,
	     p4est_lnodes_t * lnodes,
	     const double *x,
             double *y)
{
  const int           nloc = lnodes->num_local_nodes;

  memcpy (y, x, nloc * sizeof (double));
  
} /* node_vector_copy */

/** Allocate storage for processor-relevant nodal degrees of freedom.
 *
 * \param [in] lnodes   This structure is queried for the node count.
 * \return              Allocated double array; must be freed with CANOP_FREE.
 */
static double      *
allocate_node_vector (p4est_lnodes_t * lnodes)
{

  return CANOP_ALLOC (double, lnodes->num_local_nodes);
  
} /* allocate_node_vector */


/** 
 * Interpolate right hand side and exact solution onto mesh nodes.
 *
 * \param [in] p4est          The forest is not changed.
 * \param [in] lnodes         The node numbering is not changed.
 * \param [out] rhs_eval      Is allocated and filled with function values.
 * \param [out] uexact_eval   Is allocated and filled with function values.
 * \param [out] pbc           Boolean flags for Dirichlet boundary nodes.
 */
static void
interpolate_functions (p4est_t * p4est, p4est_lnodes_t * lnodes,
                       double **rhs_eval, double **uexact_eval, int8_t ** pbc)
{
  const p4est_locidx_t nloc = lnodes->num_local_nodes;
  int                 anyhang, hanging_corner[P4EST_CHILDREN];
  int                 i;        /* We use plain int for small loops. */
  double             *rhs, *uexact;
  double              vxyz[3];  /* We embed the 2D vertices into 3D space. */
  int8_t             *bc;
  p4est_topidx_t      tt;       /* Connectivity variables have this type. */
  p4est_locidx_t      k, q, Q;  /* Process-local counters have this type. */
  p4est_locidx_t      lni;      /* Node index relative to this processor. */
  p4est_tree_t       *tree;     /* Pointer to one octree */
  p4est_quadrant_t   *quad, *parent, sp, node;
  sc_array_t         *tquadrants;       /* Quadrant array for one tree */

  rhs = *rhs_eval = allocate_node_vector (lnodes);
  uexact = *uexact_eval = allocate_node_vector (lnodes);
  bc = *pbc = CANOP_ALLOC (int8_t, nloc);
  memset (bc, -1, sizeof (int8_t) * nloc);      /* Indicator for visiting. */

  /* 
   * We need to compute the xyz locations of non-hanging nodes to evaluate the
   * given functions.  For hanging nodes, we have to look at the corresponding
   * independent nodes.  Usually we would cache this information, here we only
   * need it once and throw it away again.
   * We also compute boundary status of independent nodes.
   */
  for (tt = p4est->first_local_tree, k = 0;
       tt <= p4est->last_local_tree; ++tt) {

    // Get current tree
    tree = p4est_tree_array_index (p4est->trees, tt); 

    // quads of current tree
    tquadrants = &tree->quadrants;

    // number of quads in current tree
    Q = (p4est_locidx_t) tquadrants->elem_count;

    // loop over all quads in current tree
    for (q = 0; q < Q; ++q, ++k) {
      
      // Users might aggregate the above code into a more compact iterator.

      // Get current quad
      quad = p4est_quadrant_array_index (tquadrants, q);

      /* We need to determine whether any node on this element is hanging. */
      anyhang = lnodes_decode (lnodes->face_code[k], hanging_corner);
      if (!anyhang) {
	
	// No hanging nodes. Parent quad is not required.
	// Defensive programming, set to null pointer.
        parent = nullptr;
	
      } else {
	
        // At least one node is hanging.  We need the parent quadrant to
	// find the location of the corresponding non-hanging node.
        parent = &sp;
        p4est_quadrant_parent (quad, parent);

      }

      // Loop over current quad's corners (there are P4EST_CHILDREN corners).
      for (i = 0; i < P4EST_CHILDREN; ++i) {

	// Retrieve the node ids (lni) of each corner of current cell.
	lni = lnodes->element_nodes[P4EST_CHILDREN * k + i];
        CANOP_ASSERT (lni >= 0 && lni < nloc);

	// check if node has not been previously visited, this happens
	// very often since a node touches multiple quads.
	if (bc[lni] < 0) {

	  // check if current corner is a hanging corner.
          if (anyhang && hanging_corner[i] >= 0) {

	    // This node is hanging; access the referenced node instead.
	    // node will contain corner coordinates, computed from parent cell.
            p4est_quadrant_corner_node (parent, i, &node);
	    
          } else {

	    // This is not a hanging corner, just use the regular current quad
	    // to retrieve corner node coordinates.
            p4est_quadrant_corner_node (quad, i, &node);

          }

          // Determine boundary status of independent node.
          bc[lni] = is_boundary_unitsquare (p4est, tt, &node);

          // Transform per-tree reference coordinates into physical space.
          p4est_qcoord_to_vertex (p4est->connectivity, tt,
				  node.x,
				  node.y,
#ifdef P4_TO_P8
                                  node.z,
#endif
                                  vxyz);

          // Use physical space coordinates to evaluate functions
	  // TO BE MODIFIED for our need: e.g., in astrophysics rhs will
	  // come from cell average density.
	  
	  /* 
	   * if rhs is analytically known use func_rhs (vxyz)
	   * Use rho as the rhs, rho must be recomputed at node, 
	   * from the neighbor cell-averaged values 
	   */
          rhs[lni] = 0.0; 

	  /* 
	   * if exact solution is analytically known use func_uexact (vxyz)
	   * to compute solution values at nodes.
	   */
	  uexact[lni] = 0.0;
        }
      }
    }
  }
  
} /* interpolate_functions */

/** 
 * Apply a finite element matrix to a node vector, y = Mx.
 *
 * \param [in] p4est          The forest is not changed.
 * \param [in] lnodes         The node numbering is not changed.
 * \param [in] bc             Boolean flags for Dirichlet boundary nodes.
 *                            If nullptr, no special action is taken.
 * \param [in] stiffness      If false use scaling for the mass matrix,
 *                            if true use the scaling for stiffness matrix.
 * \param [in] matrix         A 4x4 matrix computed on the reference element.
 * \param [in] in             Input vector x.
 * \param [out] out           Output vector y = Mx.
 *
 * Note about the boundary conditions: on boundary nodes \c (bc[i] != 0),
 * the values of \a x and \a y are the same.
 */
static void
multiply_matrix (p4est_t * p4est, p4est_lnodes_t * lnodes, const int8_t * bc,
                 int stiffness,
                 double (*matrix)[P4EST_CHILDREN][P4EST_CHILDREN],
                 const double *in, double *out)
{
  const int           nloc = lnodes->num_local_nodes;
  const int           nown = lnodes->owned_count;
  int                 c, h;
  int                 i, j, k;
  int                 q, Q;
  int                 anyhang, hanging_corner[P4EST_CHILDREN];
  int                 isboundary[P4EST_CHILDREN];
  int                 ncontrib;
  const int          *contrib_corner;
  double              factor, sum, inloc[P4EST_CHILDREN];
  sc_array_t         *tquadrants;
  p4est_topidx_t      tt;
  p4est_locidx_t      lni, all_lni[P4EST_CHILDREN];
  p4est_tree_t       *tree;
  p4est_quadrant_t   *quad;

  // Initialize the output vector.
  if (bc == nullptr) {
    
    // No boundary values, output vector has all zero values.
    for (lni = 0; lni < nloc; ++lni) {
      out[lni] = 0.;
    }
    
  } else {
    
    // We have boundary conditions, switch on the locally owned nodes.
    for (lni = 0; lni < nown; ++lni) {
      out[lni] = bc[lni] ? in[lni] : 0.;
    }

    // The node values owned by other processors will be added there.
    for (lni = nown; lni < nloc; ++lni) {
      out[lni] = 0.;
    }
    
  }

  // Loop over local quadrants to apply the element matrices.
  for (tt = p4est->first_local_tree, k = 0;
       tt <= p4est->last_local_tree; ++tt) {
    
    tree = p4est_tree_array_index (p4est->trees, tt);
    tquadrants = &tree->quadrants;
    Q = (p4est_locidx_t) tquadrants->elem_count;
    for (q = 0; q < Q; ++q, ++k) {
      quad = p4est_quadrant_array_index (tquadrants, q);

      // If we are on the 2D unit square, the Jacobian determinant is h^2;
      // for the stiffness matrix this cancels with the inner derivatives.
      // On the 3D unit cube we have an additional power of h.
#ifndef P4_TO_P8
      factor = !stiffness ? pow (.5, 2. * quad->level) : 1.;
#else
      factor = pow (.5, (!stiffness ? 3. : 1.) * quad->level);
#endif

      // Loop on corners of current quad
      for (i = 0; i < P4EST_CHILDREN; ++i) {
	
        // Cache some information on corner nodes.
        lni = lnodes->element_nodes[P4EST_CHILDREN * k + i];
        isboundary[i] = (bc == nullptr ? 0 : bc[lni]);

	// small vector with input data on corner nodes
	//inloc[i] = !isboundary[i] ? in[lni] : 0.;
	// the symmetry of the stiffness matrix should imply
	// inloc[i] = 0 if isboundary[i], but in fact only the restriction on
	// the interior nodes matters in terms of symmetry for the conjugate
	// gradient.
	inloc[i] = in[lni];

	// array of local node indexes of current quad's corners
	all_lni[i] = lni;
	
      }

      // Figure out the hanging corners on this element, if any.
      anyhang = lnodes_decode (lnodes->face_code[k], hanging_corner);

      if (!anyhang) {
	
        // No hanging nodes on this element; just apply the matrix.
        for (i = 0; i < P4EST_CHILDREN; ++i) {
          if (!isboundary[i]) {
            sum = 0.;
            for (j = 0; j < P4EST_CHILDREN; ++j) {
              sum += (*matrix)[i][j] * inloc[j];
            }
            out[all_lni[i]] += factor * sum;
          }
        }
	
      } else {
	
        // Compute input values at hanging nodes by interpolation.
        lnodes_interpolate_hanging (lnodes->face_code[k], inloc);

        // Apply element matrix and then the transpose interpolation.
        for (i = 0; i < P4EST_CHILDREN; ++i) {

	  sum = 0.;
          for (j = 0; j < P4EST_CHILDREN; ++j) {
            sum += (*matrix)[i][j] * inloc[j];
          }

	  if (hanging_corner[i] == -1) {

	    /* This node is not hanging, there is no need to transform. */
            c = 0;
            ncontrib = 1;
            contrib_corner = &i;
            sum *= factor;
	    
          } else {
	    
            /* This node is hanging.  Use hanging node relations from the
             * reference quadrant by transforming corner numbers with ^ c. */
            c = hanging_corner[i];      /* Child id of quadrant. */
            ncontrib = lnodes_corner_num_hanging[i ^ c];
            contrib_corner = lnodes_corner_to_hanging[i ^ c];
            sum *= factor / (double) ncontrib;
	    
          }
	  
          // The result is added into the output vector.
          for (j = 0; j < ncontrib; ++j) {
	    
            h = contrib_corner[j] ^ c;  /* Inverse transform of node number. */
            if (!isboundary[h]) {
              out[all_lni[h]] += sum;
            }
	    
          }
        }
      }
    }
  }

  /* Parallel sum of result. */
  lnodes_share_sum (p4est, lnodes, out);
  
} /* multiply_matrix */

/** Set Dirichlet boundary values of a node vector.
 *
 * \param [in] lnodes         The node numbering is not changed.
 * \param [in] bc             Boolean flags for Dirichlet boundary nodes.
 *                            If nullptr, this function does nothing.
 * \param [in] u              Source for Dirichlet boundary values.
 * \param [in,out] v          Dirichlet nodes are overwritten with values
 *                            from \a u.
 */
static void
set_dirichlet (p4est_lnodes_t * lnodes, const int8_t * bc, const double *u, double *v)
{
  const int           nloc = lnodes->num_local_nodes;
  p4est_locidx_t      lni;

  if (bc == nullptr) {
    return;
  }
  for (lni = 0; lni < nloc; ++lni) {
    if (bc[lni]) {
      v[lni] = u[lni];
    }
  }
} /* set_dirichlet */

/** Multiply the mass matrix with a vector of ones to compute the volume.
 * \param [in] p4est          The forest is not changed.
 * \param [in] lnodes         The node numbering is not changed.
 * \param [in] matrix         The mass matrix should be passed in here.
 * \param [in] tmp            Must be allocated, entries are undefined.
 * \param [in,out] lump       Must be allocated, receives matrix * ones.
 */
static void
test_area (p4est_t * p4est, p4est_lnodes_t * lnodes,
           double (*matrix)[P4EST_CHILDREN][P4EST_CHILDREN],
           double *tmp, double *lump)
{
  const int           nloc = lnodes->num_local_nodes;
  double              dot;
  p4est_locidx_t      lni;

  for (lni = 0; lni < nloc; ++lni) {
    tmp[lni] = 1.;
  }
  multiply_matrix (p4est, lnodes, nullptr, 0, matrix, tmp, lump);
  dot = vector_dot (p4est, lnodes, tmp, lump);
  dot = sqrt (dot);

  CANOP_GLOBAL_PRODUCTIONF ("Area of domain: %g\n", dot);
} /* test_area */

/** Execute the conjugate gradient method.
 * \param [in] p4est     The forest is not changed.
 * \param [in] lnodes    The node numbering is not changed.
 * \param [in] bc        Boolean flags for Dirichlet boundary nodes.
 * \param [in] stiffness If false use scaling for the mass matrix,
 *                       if true use the scaling for stiffness matrix.
 * \param [in] matrix    The mass matrix should be passed in here.
 * \param [in] b         The right hand side vector.
 * \param [in,out] x     Input: the initial guess. Output: the solution.
 * \param [in] imax      The maximum number of iterations.
 * \param [in] tol       The convergence threshold (relative to the initial residual).
 * \param [out] info     Feedback about number of iterations and final residual.
 *
 * \return Non-zero iff the solver converged.
 *
 * Note about the boundary conditions: on boundary nodes \c (bc[i] != 0),
 * the values of \a x and \a b must be the same.
 */
static int
solve_by_cg (p4est_t * p4est, p4est_lnodes_t * lnodes, const int8_t * bc,
             int stiffness,
             double (*matrix)[P4EST_CHILDREN][P4EST_CHILDREN],
             const double *b, double *x,
             int imax, double tol, poisson_info_t *info = nullptr)
{
  int                 i;
  int                 converged;
  double              alpha, beta, pAp;
  double              rr, rrnew, rrorig;
  double             *aux[4];
  double             *r, *p, *Ap;

  for (i = 0; i < 3; ++i) {
    aux[i] = allocate_node_vector (lnodes);
  }
  r = aux[0];
  p = aux[1];
  Ap = aux[2];

  // Initial residual is taken from b (as if the initial guess was 0)
  rrorig = vector_dot (p4est, lnodes, b, b);

  // Compute the initial residual
  multiply_matrix (p4est, lnodes, bc, stiffness, matrix, x, r);
  vector_xpby (p4est, lnodes, b, -1.0, r);
  rr = vector_dot (p4est, lnodes, r, r);

  node_vector_copy (p4est, lnodes, r, p);

  for (i = 0; i < imax && rr > rrorig * tol * tol; i++) {
    multiply_matrix (p4est, lnodes, bc, stiffness, matrix, p, Ap);
    pAp = vector_dot (p4est, lnodes, p, Ap);
    alpha = rr / pAp;
    node_vector_axpy (p4est, lnodes, alpha, p, x);
    node_vector_axpy (p4est, lnodes, -alpha, Ap, r);
    rrnew = vector_dot (p4est, lnodes, r, r);
    beta = rrnew / rr;
    vector_xpby (p4est, lnodes, r, beta, p);
    CANOP_GLOBAL_VERBOSEF ("%03d: r'r %g alpha %g beta %g\n",
                           i, rr, alpha, beta);
    rr = rrnew;
  }
  if (i < imax) {
    converged = 1;
    CANOP_GLOBAL_PRODUCTIONF ("cg converged to %g in %d iterations\n",
                              sqrt (rr / rrorig), i);
  }
  else {
    converged = 0;
    CANOP_GLOBAL_PRODUCTIONF ("cg did not converge (%g) in %d iterations\n",
                              sqrt (rr / rrorig), imax);
  }

  if (info != nullptr) {
    info->iterations = i;
    info->residual = sqrt (rr / rrorig);
  }

  /* Free temporary storage. */
  for (i = 0; i < 3; ++i) {
    CANOP_FREE (aux[i]);
  }

  return converged;
} /* solve_by_cg */

/** 1D mass matrix on the reference element [0, 1]. */
static const double m_1d[2][2] = {
  {1 / 3., 1 / 6.},
  {1 / 6., 1 / 3.},
};
/** 1D stiffness matrix on the reference element [0, 1]. */
static const double s_1d[2][2] = {
  {1., -1.},
  {-1., 1.},
};
/** (P4EST_DIM)d mass matrix on the reference element */
static double mass_dd[P4EST_CHILDREN][P4EST_CHILDREN];
/** (P4EST_DIM)d stiffness matrix on the reference element */
static double stiffness_dd[P4EST_CHILDREN][P4EST_CHILDREN];

/** Flag to initialize only once */
static int poisson_init_done = 0;

/** Initialize useful variables for the Poisson solver */
static void
init_poisson (void)
{
  int                 i, j, k, l;
#ifdef P4_TO_P8
  int                 m, n;
#endif

  if (poisson_init_done)
    return;

  // Compute entries of reference mass and stiffness matrices in 2D.
  // In this example we can proceed without numerical integration.
  for (l = 0; l < 2; ++l) {
    for (k = 0; k < 2; ++k) {
      for (j = 0; j < 2; ++j) {
        for (i = 0; i < 2; ++i) {
#ifndef P4_TO_P8
          mass_dd[2 * j + i][2 * l + k] = m_1d[i][k] * m_1d[j][l];
          stiffness_dd[2 * j + i][2 * l + k] =
            s_1d[i][k] * m_1d[j][l] + m_1d[i][k] * s_1d[j][l];
#else
          for (n = 0; n < 2; ++n) {
            for (m = 0; m < 2; ++m) {
              mass_dd[4 * i + 2 * n + m][4 * l + 2 * k + j] =
                m_1d[m][j] * m_1d[n][k] * m_1d[i][l];
              stiffness_dd[4 * i + 2 * n + m][4 * l + 2 * k + j] =
                s_1d[m][j] * m_1d[n][k] * m_1d[i][l] +
                m_1d[m][j] * s_1d[n][k] * m_1d[i][l] +
                m_1d[m][j] * m_1d[n][k] * s_1d[i][l];
            }
          }
#endif
        }
      }
    }
  }

  // Skip further calls to init_poisson
  poisson_init_done = 1;
} /* init_poisson */

/** Solve Poisson's equation using the given lnodes and data
 * \param [in]     p4est    Solve the PDE with the given mesh refinement.
 * \param [in]     lnodes   Use these Gauss-Lobatto nodes (degree 1).
 * \param [in]     bc       Boolean indicator, true for boundary nodes
 * \param [in]     rhs_in   Input RHS
 * \param [in,out] u_out    Solution (input: initial guess and boundary values)
 * \param [in]     imax     The maximum number of iterations.
 * \param [in]     tol      The convergence threshold (relative to the initial residual).
 * \param [out]    info     Feedback about number of iterations and final residual.
 *
 * \return Non-zero iff the solver converged.
 */
int
solve_poisson_lnodes (p4est_t * p4est, p4est_lnodes_t *lnodes,
        int8_t *bc, double *rhs_in, double *u_out,
        int imax, double tol, poisson_info_t *info)
{
  double *rhs_fe;
  int     converged;

  init_poisson();

  // Apply mass matrix to create right hand side FE vector.
  rhs_fe = allocate_node_vector (lnodes);
  multiply_matrix (p4est, lnodes, bc, 0, &mass_dd, rhs_in, rhs_fe);

  // Transfer boundary conditions to the RHS
  set_dirichlet (lnodes, bc, u_out, rhs_fe);

  // Run conjugate gradient method with initial value zero.
  converged = solve_by_cg (p4est, lnodes, bc, 1, &stiffness_dd, rhs_fe, u_out, imax, tol, info);

  // Free finite element vectors
  CANOP_FREE (rhs_fe);

  return converged;
} /* solve_poisson_lnodes */


/** Execute the numerical part of the example: Solve Poisson's equation.
 * \param [in]  p4est    Solve the PDE with the given mesh refinement.
 * \param [out] info     Feedback about number of iterations and final residual.
 *
 * \return Non-zero iff the solver converged.
 */
int
solve_poisson (p4est_t * p4est, poisson_info_t *info)
{
  double             *rhs_eval, *uexact_eval, *lump;
  double             *rhs_fe, *u_fe, *u_diff, *diff_mass;
  double              err2, err;
  int                 converged;
  int8_t             *bc;
  p4est_ghost_t      *ghost;
  p4est_lnodes_t     *lnodes;

  // Initialize the poisson solver ; we need the mass matrix
  init_poisson();

  // Create the ghost layer to learn about parallel neighbors.
  ghost = p4est_ghost_new (p4est, P4EST_CONNECT_FULL);

  // Create a node numbering for continuous linear finite elements.
  lnodes = p4est_lnodes_new (p4est, ghost, 1);

  // Destroy the ghost structure -- no longer needed after node creation.
  p4est_ghost_destroy (ghost);
  ghost = nullptr;

  // Test mass matrix multiplication by computing the area of the domain.
  rhs_fe = allocate_node_vector (lnodes);
  lump = allocate_node_vector (lnodes);
  test_area (p4est, lnodes, &mass_dd, rhs_fe, lump);

  // Interpolate right hand side and exact solution onto mesh nodes.
  interpolate_functions (p4est, lnodes, &rhs_eval, &uexact_eval, &bc);

  // Solve Poisson equation
  u_fe = allocate_node_vector (lnodes);
  converged = solve_poisson_lnodes (p4est, lnodes, bc, rhs_eval, u_fe, 100, 1e-6, info);

  // Compute the pointwise difference with the exact vector.
  u_diff = allocate_node_vector (lnodes);
  node_vector_copy (p4est, lnodes, u_fe, u_diff);
  node_vector_axpy (p4est, lnodes, -1., uexact_eval, u_diff);

  // Compute the L2 difference with the exact vector.
  // We know that this is over-optimistic: Quadrature will be sharper.
  // We could also reuse another vector instead of allocating a new one.
  diff_mass = allocate_node_vector (lnodes);
  multiply_matrix (p4est, lnodes, bc, 0, &mass_dd, u_diff, diff_mass);
  err2 = vector_dot (p4est, lnodes, diff_mass, u_diff);
  err = sqrt (err2);
  CANOP_GLOBAL_PRODUCTIONF ("||u_fe - u_exact||_L2 = %g\n", err);

  // Free finite element node vectors
  CANOP_FREE (diff_mass);
  CANOP_FREE (u_diff);
  CANOP_FREE (u_fe);
  CANOP_FREE (lump);
  CANOP_FREE (rhs_fe);
  CANOP_FREE (rhs_eval);
  CANOP_FREE (uexact_eval);
  CANOP_FREE (bc);

  /* We are done with the FE node numbering. */
  p4est_lnodes_destroy (lnodes);

  return converged;
} /* solve_poisson */


void
compute_barycenter(p4est_t *p4est,
                   p4est_geometry_t *geom,
                   std::function<double(p4est_quadrant_t *)> quad_getter,
                   std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_volume,
                   double *psum,
                   double pcenter[3])
{
  p4est_topidx_t      tt;
  p4est_tree_t       *tree;
  p4est_quadrant_t   *quad;
  sc_array_t         *tquadrants;
  p4est_locidx_t      q, Q;

  MPI_Comm            mpicomm = p4est->mpicomm;
  int                 mpiret;

  double              quad_data, volume, term;
  double              lsum = 0, sum;
  double              lcenter[3], center[3], XYZ[3];

  memset(lcenter, 0, 3 * sizeof(double));
  memset(XYZ, 0, 3 * sizeof(double));

  // compute local averages
  for (tt = p4est->first_local_tree;
      tt <= p4est->last_local_tree; ++tt) {
    tree = p4est_tree_array_index(p4est->trees, tt);
    tquadrants = &tree->quadrants;
    Q = (p4est_locidx_t) tquadrants->elem_count;
    for (q = 0; q < Q; ++q) {
      quad = p4est_quadrant_array_index(tquadrants, q);

      quad_data = quad_getter(quad);
      volume = quad_volume(p4est, tt, quad);
      term = quad_data * volume;

      quadrant_center_vertex(p4est->connectivity, geom, tt, quad, XYZ);

      lsum += term;
      for (int i = 0; i < P4EST_DIM; ++i)
        lcenter[i] += XYZ[i] * term;
    } // loop on quadrants
  } // loop on trees

  // compute the sum over all MPI ranks
  mpiret = MPI_Allreduce(  &lsum,   &sum,         1, MPI_DOUBLE, MPI_SUM, mpicomm);
  SC_CHECK_MPI(mpiret);
  mpiret = MPI_Allreduce(lcenter, center, 3, MPI_DOUBLE, MPI_SUM, mpicomm);
  SC_CHECK_MPI(mpiret);

  // compute the average for the center
  for (int i = 0; i < 3; ++i)
    center[i] /= sum;

  // copy data to the output pointers
  if (psum != nullptr)
    *psum = sum;
  if (pcenter != nullptr)
    memcpy(pcenter, center, 3 * sizeof(double));
}


double
compute_average(p4est_t *p4est,
                std::function<double(p4est_quadrant_t *)> quad_getter,
                std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_volume)
{
  p4est_topidx_t      tt;
  p4est_tree_t       *tree;
  p4est_quadrant_t   *quad;
  sc_array_t         *tquadrants;
  p4est_locidx_t      q, Q;

  MPI_Comm            mpicomm = p4est->mpicomm;
  int                 mpiret;

  double              quad_data, volume;
  double              lsum[2], sum[2]; // [0] is the volume, [1] is the integral

  memset(lsum, 0, 2 * sizeof(double));

  // compute local integrals
  for (tt = p4est->first_local_tree;
      tt <= p4est->last_local_tree; ++tt) {
    tree = p4est_tree_array_index(p4est->trees, tt);
    tquadrants = &tree->quadrants;
    Q = (p4est_locidx_t) tquadrants->elem_count;
    for (q = 0; q < Q; ++q) {
      quad = p4est_quadrant_array_index(tquadrants, q);

      quad_data = quad_getter(quad);
      volume = quad_volume(p4est, tt, quad);

      lsum[0] += volume;
      lsum[1] += quad_data * volume;
    } // loop on quadrants
  } // loop on trees

  // compute the sum over all MPI ranks
  mpiret = MPI_Allreduce(lsum, sum, 2, MPI_DOUBLE, MPI_SUM, mpicomm);
  SC_CHECK_MPI(mpiret);

  // compute the average
  return sum[1] / sum[0];
}

double
monopole_expansion(double sum, double center[3], double XYZ[3])
{
  double factor, radius;

  radius = std::sqrt(
        SC_SQR(XYZ[0] - center[0])
      + SC_SQR(XYZ[1] - center[1])
#ifdef P4_TO_P8
      + SC_SQR(XYZ[2] - center[2])
#endif
      );

#ifndef P4_TO_P8
  factor = -1. / (2. * M_PI) * std::log(radius);
#else
  factor = 1. / (4. * M_PI * radius);
#endif

  return factor * sum;
}


double
nodes_L2_norm (p4est_t * p4est, p4est_lnodes_t * lnodes, double *u)
{
  double             *Mu = allocate_node_vector (lnodes);
  double              res;

  init_poisson();

  multiply_matrix (p4est, lnodes, nullptr, 0, &mass_dd, u, Mu);
  res = vector_dot (p4est, lnodes, u, Mu);

  CANOP_FREE (Mu);

  return std::sqrt(res);
} /* nodes_L2_norm */
