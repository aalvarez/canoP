/*
  This file is part of p4est.
  p4est is a C library to manage a collection (a forest) of multiple
  connected adaptive quadtrees or octrees in parallel.

  Copyright (C) 2012 Carsten Burstedde

  p4est is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  p4est is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with p4est; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/*
 * C++ refactoring.
 */

#include "SolverWrap.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#else
#include <p8est_bits.h>
#endif

#include <time.h>

#include "canoP_base.h"

#define UNUSED(x) ((void)(x))

// =======================================================
// ==== STATIC UTILITIES IMPL ============================
// =======================================================
/*
 * In these utilities, the SolverWrap pointer is retrieve 
 * from the p4est user_pointer data.
 */

// =======================================================
// =======================================================
/**
 *
 */
static int
refine_callback (p4est_t * p4est,
		 p4est_topidx_t which_tree,
                 p4est_quadrant_t * q)
{

  UNUSED (which_tree);
  UNUSED (q);

  // get the solver p4est interface
  SolverWrap *pp = static_cast<SolverWrap *> (p4est->user_pointer);
  
  const p4est_locidx_t old_counter = pp->m_inside_counter++;
  const uint8_t        flag        = pp->m_flags[old_counter];

  CANOP_ASSERT (0 <= old_counter);
  CANOP_ASSERT (0 <= pp->m_num_replaced
                && pp->m_num_replaced <= pp->m_num_refine_flags);

  // copy current flag since we cannot be certain that refinement occurs
  pp->m_flags[old_counter] = 0;
  pp->m_temp_flags[old_counter + (P4EST_CHILDREN - 1) * pp->m_num_replaced] =
    (uint8_t) (flag & ~P4EST_WRAP_REFINE);

  return flag & P4EST_WRAP_REFINE;

} // refine_callback

// =======================================================
// =======================================================
/**
 *
 */
static int
coarsen_callback (p4est_t * p4est,
		  p4est_topidx_t which_tree,
                  p4est_quadrant_t * q[])
{
  
  UNUSED (which_tree);
  UNUSED (q);

  // get the solver p4est interface
  SolverWrap *pp = (SolverWrap *) p4est->user_pointer;

  const p4est_locidx_t old_counter = pp->m_inside_counter++;
  int                 k;

  // are we not coarsening at all, just counting?
  if (q[1] == nullptr) {
    return 0;
  }

  // now we are possibly coarsening
  for (k = 0; k < P4EST_CHILDREN; ++k) {
    if (!(pp->m_temp_flags[old_counter + k] & P4EST_WRAP_COARSEN)) {
      return 0;
    }
  }

  // we are definitely coarsening
  pp->m_inside_counter += P4EST_CHILDREN - 1;
  ++pp->m_num_replaced;
  return 1;
  
} // coarsen_callback

// =======================================================
// =======================================================
void
wrap_replace_flags (p4est_t * p4est, p4est_topidx_t which_tree,
                    int num_outgoing, p4est_quadrant_t * outgoing[],
                    int num_incoming, p4est_quadrant_t * incoming[])
{

  UNUSED (which_tree);
  UNUSED (num_outgoing);
  UNUSED (outgoing);
  UNUSED (num_incoming);
  UNUSED (incoming);

  // get the solver p4est interface
  SolverWrap *solverWrap = static_cast<SolverWrap *> (p4est->user_pointer);

  // in case we're in the balance phase, this is not needed
  if (solverWrap->m_flags.size() == 0) {
    return;
  }
  const p4est_locidx_t new_counter =
    solverWrap->m_inside_counter - 1 +
    (P4EST_CHILDREN - 1) * solverWrap->m_num_replaced++;
  const uint8_t       flag = solverWrap->m_temp_flags[new_counter];
  int                 k;

  // this function is only called when refinement actually happens
  CANOP_ASSERT (num_outgoing == 1 && num_incoming == P4EST_CHILDREN);
  CANOP_ASSERT (!(flag & (P4EST_WRAP_REFINE | P4EST_WRAP_COARSEN)));

  for (k = 1; k < P4EST_CHILDREN; ++k) {
    solverWrap->m_temp_flags[new_counter + k] = flag;
  }
  
} // wrap_replace_flags

// =======================================================
// ==== CLASS SOLVERWRAP IMPL ============================
// =======================================================


// =======================================================
// =======================================================
SolverWrap::SolverWrap (ConfigReader * cfg) :
  
  m_p4est_dim       (P4EST_DIM),
  m_p4est_half      (P4EST_HALF),
  m_p4est_faces     (P4EST_FACES),
  m_p4est_children  (P4EST_CHILDREN),

  m_conn            (nullptr),
  m_geom_compute    (nullptr),
  m_geom_io         (nullptr),
  m_p4est           (nullptr),

  m_weight_exponent (0),
  m_flags           ( ),
  m_temp_flags      ( ),
  m_num_refine_flags(0),
  m_inside_counter  (0),
  m_num_replaced    (0),

  m_ghost           (nullptr),
  m_mesh            (nullptr),
  m_ghost_aux       (nullptr),
  m_mesh_aux        (nullptr),

  m_user_pointer    (nullptr),
  m_cfg             (cfg)
{
  
} // SolverWrap::SolverWrap

// =======================================================
// =======================================================
SolverWrap::~SolverWrap()
{

  if (m_mesh_aux != nullptr) {
    p4est_mesh_destroy (m_mesh_aux);
  }

  if (m_ghost_aux != nullptr) {
    p4est_ghost_destroy (m_ghost_aux);
  }

  p4est_mesh_destroy (m_mesh);
  p4est_ghost_destroy (m_ghost);

  p4est_destroy (m_p4est);
  p4est_connectivity_destroy (m_conn);

  if (m_geom_compute != nullptr) {
    p4est_geometry_destroy (m_geom_compute);
  }
  if (m_geom_io != nullptr) {
    p4est_geometry_destroy (m_geom_io);
  }

} // SolverWrap::~SolverWrap

// =======================================================
// =======================================================
void
SolverWrap::setup (p4est_connectivity_t * conn,
		   p4est_geometry_t * geom_compute,
		   p4est_geometry_t * geom_io,
		   p4est_t * p4est)
{

  // p4est must have been allocated and constructed
  CANOP_ASSERT (p4est != nullptr);
  
  m_conn            = conn;
  m_geom_compute    = geom_compute;
  m_geom_io         = geom_io;
  m_p4est           = p4est;

  m_weight_exponent = 0;

  // allocate memory and reset m_flags
  m_flags.clear();
  m_flags.resize(m_p4est->local_num_quadrants, 0);
  //std::fill (m_flags.begin(), m_flags.end(), 0);
  
  m_num_refine_flags = m_inside_counter = m_num_replaced = 0;
    
  // m_temp_flags is not allocated by default, size is 0
  
  // allocate ghost and mesh
  m_ghost = p4est_ghost_new    (m_p4est, P4EST_CONNECT_FACE);
  m_mesh  = p4est_mesh_new_ext (m_p4est, m_ghost, 1, 0,
			       P4EST_CONNECT_FACE);

  m_ghost_aux = nullptr;
  m_mesh_aux = nullptr;
  m_match_aux = 0;

  // setup p4est user pointer (with the SolverWrap pointer)
  m_p4est->user_pointer = (void *) this;

  // will be set in calling routine (Solver constructor)
  m_cfg = nullptr;
  
} // SolverWrap::setup

// =======================================================
// =======================================================
void
SolverWrap::setup_user_pointer (void * user_pointer)
{

  // setup the SolverWrap user pointer (with the Solver pointer)
  m_user_pointer = user_pointer;
  
} // SolverWrap::setup_user_pointer

// =======================================================
// =======================================================
void
SolverWrap::set_cfg (ConfigReader *cfg)
{

  m_cfg = cfg;

} // SolverWrap::set_cfg

// =======================================================
// =======================================================

p4est_ghost_t      *
SolverWrap::get_ghost ()
{

  return m_match_aux ? m_ghost_aux : m_ghost;

} // SolverWrap::get_ghost

// =======================================================
// =======================================================
p4est_mesh_t       *
SolverWrap::get_mesh ()
{

  return m_match_aux ? m_mesh_aux : m_mesh;

} // SolverWrap::get_mesh

// =======================================================
// =======================================================
void
SolverWrap::mark_refine (p4est_topidx_t which_tree,
			 p4est_locidx_t which_quad)
{
  
  p4est_t            *p4est = m_p4est;
  p4est_tree_t       *tree;
  p4est_locidx_t      pos;
  uint8_t             flag;

  // make sure current tree is in the valid range
  CANOP_ASSERT (p4est->first_local_tree <= which_tree);
  CANOP_ASSERT (which_tree <= p4est->last_local_tree);

  // make sure current quad is in the valid range
  tree = p4est_tree_array_index (p4est->trees, which_tree);
  CANOP_ASSERT (0 <= which_quad &&
                (size_t) which_quad < tree->quadrants.elem_count);

  // compute position in local MPI domain
  pos = tree->quadrants_offset + which_quad;
  CANOP_ASSERT (0 <= pos && pos < p4est->local_num_quadrants);

  flag = m_flags[pos];
  if (!(flag & P4EST_WRAP_REFINE)) {
    m_flags[pos] |= P4EST_WRAP_REFINE;
    ++m_num_refine_flags;
  }
  m_flags[pos] &= ~P4EST_WRAP_COARSEN;
  
} // SolverWrap::mark_refine

// =======================================================
// =======================================================
void
SolverWrap::mark_coarsen (p4est_topidx_t which_tree,
                          p4est_locidx_t which_quad)
{
  
  p4est_t            *p4est = m_p4est;
  p4est_tree_t       *tree;
  p4est_locidx_t      pos;
  uint8_t             flag;

  // make sure current tree is in the valid range
  CANOP_ASSERT (p4est->first_local_tree <= which_tree);
  CANOP_ASSERT (which_tree <= p4est->last_local_tree);

  // make sure current quad is in the valid range
  tree = p4est_tree_array_index (p4est->trees, which_tree);
  CANOP_ASSERT (0 <= which_quad &&
                (size_t) which_quad < tree->quadrants.elem_count);

  // compute position in local MPI domain
  pos = tree->quadrants_offset + which_quad;
  CANOP_ASSERT (0 <= pos && pos < p4est->local_num_quadrants);

  flag = m_flags[pos];
  if (flag & P4EST_WRAP_REFINE) {
    return;
  }
  m_flags[pos] |= P4EST_WRAP_COARSEN;
  
} // SolverWrap::mark_coarsen

// =======================================================
// =======================================================
int8_t
SolverWrap::get_flag (p4est_topidx_t which_tree,
		      p4est_locidx_t which_quad)
{
  
  p4est_t            *p4est = m_p4est;
  p4est_tree_t       *tree;
  p4est_locidx_t      pos;

  // make sure current tree is in the valid range
  CANOP_ASSERT (p4est->first_local_tree <= which_tree);
  CANOP_ASSERT (which_tree <= p4est->last_local_tree);

  // make sure current quad is in the valid range
  tree = p4est_tree_array_index (p4est->trees, which_tree);
  CANOP_ASSERT (0 <= which_quad &&
                (size_t) which_quad < tree->quadrants.elem_count);

  
  // compute position in local MPI domain
  pos = tree->quadrants_offset + which_quad;
  CANOP_ASSERT (0 <= pos && pos < p4est->local_num_quadrants);

  return m_flags[pos];
  
} // SolverWrap::get_flag

// =======================================================
// =======================================================
int
SolverWrap::adapt (p4est_init_t init_fn,
                   p4est_replace_t replace_on_refine,
                   p4est_replace_t replace_on_coarsen,
		   double *stat_time)
{
  
  int                 changed;
#ifdef P4EST_DEBUG
  p4est_locidx_t      jl, local_num;
#endif
  p4est_gloidx_t      global_num;
  p4est_t            *p4est = m_p4est;
  
  
  clock_t t_start = 0;
  double time     = 0;

  CANOP_ASSERT (m_mesh != nullptr);
  CANOP_ASSERT (m_ghost != nullptr);
  CANOP_ASSERT (m_mesh_aux == nullptr);
  CANOP_ASSERT (m_ghost_aux == nullptr);
  CANOP_ASSERT (m_match_aux == 0);

  CANOP_ASSERT (m_temp_flags.size() == 0);
  CANOP_ASSERT (m_num_refine_flags >= 0 &&
                m_num_refine_flags <= p4est->local_num_quadrants);

  // This allocation is optimistic when not all refine requests are honored
  m_temp_flags.resize(p4est->local_num_quadrants +
  		      (P4EST_CHILDREN - 1) * m_num_refine_flags);
  std::fill(m_temp_flags.begin(), m_temp_flags.end(), 0);
  
  // Execute refinement
  m_inside_counter = m_num_replaced = 0;
#ifdef P4EST_DEBUG
  local_num = p4est->local_num_quadrants;
#endif
  global_num = p4est->global_num_quadrants;
  
  t_start = (double) clock ();

  // let p4est perform the actual refinement
  // - non-recursive
  // - allowed level is the maxlevel (P4EST_QMAXLEVEL)
  // - refine_callback is a function pointer (can't be nullptr), this is where the flag are checked,
  //   the callback must return true if the quadrant is to be refined. When non-recursive, only existing
  //   quadrant are checked, when recursive is activated then all quadrant are checked, existing ones and
  //   newly created ones.
  // - init_fn will be non-nullptr only in the initial refinement (when computing the init condition)
  // - replace_on_refine is specified in Solver::adapt to be
  //   wrap_replace_flags (initial condition) or quadrant_replace_fn (otherwise)
  //   note that wrap_replace_flags is defined here in SolverWrap
  //   while quadrant_replace_fn is define in Solver.
  //   Please note that quadrant_replace_fn is used to change quadrant's user data
  //   when we will refactor for external memory storage (e.g. using Kokkos library)
  //   this operation will be done is a separate call.
  //
  // TODO: some clarification / simplication could be made here.
  p4est_refine_ext (p4est, 0, -1, refine_callback, init_fn,
                    replace_on_refine);
  
  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  stat_time[0] = time;
  
  CANOP_ASSERT (m_inside_counter == local_num);
  CANOP_ASSERT (p4est->local_num_quadrants - local_num ==
                m_num_replaced * (P4EST_CHILDREN - 1));
  changed = global_num != p4est->global_num_quadrants;

  /* Execute coarsening */
  m_inside_counter = m_num_replaced = 0;
#ifdef P4EST_DEBUG
  local_num = p4est->local_num_quadrants;
#endif
  global_num = p4est->global_num_quadrants;
  
  t_start = (double) clock ();
  
  // let p4est perform the actual refinement
  // - non-recursive
  // - callback_orphans=1 means  coarsen_callback is also called when siblings are not "available" (i.e. ghost quadrants)
  // - coarsen callback is a function pointer (that can't be nullptr), this is WHERE action is, i.e.
  //   where the flags of current quad and siblings are checked; if they all agree on coarsenning
  //   then they will disappear and be replaced by the coarse quadrant
  // - init_fn will be non-nullptr only in the initial refinement (when computing the init condition)
  // - replace_on_coarsen is where quadrant's user_data is changed
  p4est_coarsen_ext (p4est, 0, 1, coarsen_callback, init_fn,
                     replace_on_coarsen);
  
  
  time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
  stat_time[1] = time;
  
  
  CANOP_ASSERT (m_inside_counter == local_num);
  CANOP_ASSERT (local_num - p4est->local_num_quadrants ==
                m_num_replaced * (P4EST_CHILDREN - 1));
  changed = changed || global_num != p4est->global_num_quadrants;

  /* Free temporary flags */
  m_temp_flags.clear();

  /* Only if refinement and/or coarsening happened do we need to balance */
  if (changed) {

    m_flags.clear();
    
    t_start = (double) clock ();
	
    p4est_balance_ext (p4est, P4EST_CONNECT_CORNER, init_fn,
                       replace_on_coarsen);
	
    time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
    stat_time[2] = time;

    // resize to new size and reset m_flags
    m_flags.resize(p4est->local_num_quadrants, 0);
    std::fill(m_flags.begin(), m_flags.end(), 0);
    
    t_start = (double) clock ();
    
    m_ghost_aux = p4est_ghost_new (p4est, P4EST_CONNECT_FACE);
    
    time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
    stat_time[3] = time;
    
    t_start = (double) clock ();
    
    m_mesh_aux = p4est_mesh_new_ext (p4est, m_ghost_aux, 1, 0,
				     P4EST_CONNECT_FACE);
    
    time = (double) (clock () - t_start) / CLOCKS_PER_SEC;
    stat_time[4] = time;
    
    m_match_aux = 1;
  }
#ifdef P4EST_DEBUG
  else {
    for (jl = 0; jl < p4est->local_num_quadrants; ++jl) {
      CANOP_ASSERT (m_flags[jl] == 0);
    }
  }
#endif
  m_num_refine_flags = 0;
  
  return changed;
  
} // SolverWrap::adapt

// =======================================================
// =======================================================
int
SolverWrap::adapt_geometric (p4est_init_t init_fn,
                             p4est_refine_t geom_indicator)
{

  int                 changed;
  p4est_gloidx_t      global_num;
  p4est_t            *p4est = m_p4est;


  CANOP_ASSERT (m_mesh != nullptr);
  CANOP_ASSERT (m_ghost != nullptr);
  CANOP_ASSERT (m_mesh_aux == nullptr);
  CANOP_ASSERT (m_ghost_aux == nullptr);
  CANOP_ASSERT (m_match_aux == 0);

  // Execute refinement
  global_num = p4est->global_num_quadrants;
  p4est_refine (p4est, 1, geom_indicator, init_fn);
  changed = global_num != p4est->global_num_quadrants;

  /* Only if refinement happened do we need to balance */
  if (changed) {

    m_flags.clear();

    p4est_balance (p4est, P4EST_CONNECT_CORNER, init_fn);

    // resize to new size and reset m_flags
    m_flags.resize(p4est->local_num_quadrants, 0);
    std::fill(m_flags.begin(), m_flags.end(), 0);

    m_ghost_aux = p4est_ghost_new (p4est, P4EST_CONNECT_FACE);

    m_mesh_aux = p4est_mesh_new_ext (p4est, m_ghost_aux, 1, 0,
                                     P4EST_CONNECT_FACE);

    m_match_aux = 1;
  }

  return changed;

} // SolverWrap::adapt_geometric

// =======================================================
// =======================================================
static int
partition_weight (p4est_t * p4est,
		  p4est_topidx_t which_tree,
                  p4est_quadrant_t * quadrant)
{
  
  UNUSED (which_tree);

  // get the solver p4est interface
  SolverWrap      *solverWrap = (SolverWrap *) p4est->user_pointer;

  return 1 << ((int) quadrant->level * solverWrap->m_weight_exponent);
  
} // partition_weight

// =======================================================
// =======================================================
int
SolverWrap::partition (int weight_exponent)
{
  
  int                 changed;

  CANOP_ASSERT (m_ghost != nullptr);
  CANOP_ASSERT (m_mesh != nullptr);
  CANOP_ASSERT (m_ghost_aux != nullptr);
  CANOP_ASSERT (m_mesh_aux != nullptr);
  CANOP_ASSERT (m_match_aux == 1);

  // will be reallocated later
  p4est_mesh_destroy (m_mesh);
  p4est_ghost_destroy (m_ghost);
  m_match_aux = 0;

  /* In the future the flags could be used to pass partition weights */
  /* We need to lift the restriction on 64 bits for the global weight sum */
  CANOP_ASSERT (weight_exponent == 0 || weight_exponent == 1);
  m_weight_exponent = weight_exponent;
  changed =
    p4est_partition_ext (m_p4est, 1,
                         weight_exponent ? partition_weight : nullptr) > 0;

  if (changed) {
    // resize  m_flags and reset
    m_flags.clear();
    m_flags.resize(m_p4est->local_num_quadrants, 0);
    std::fill (m_flags.begin(), m_flags.end(), 0);

    // realloc ghost and mesh
    m_ghost = p4est_ghost_new (m_p4est, P4EST_CONNECT_FACE);
    m_mesh  = p4est_mesh_new_ext (m_p4est, m_ghost, 1, 0, P4EST_CONNECT_FACE);
  }
  else {
    // size didn't change, just reset
    std::fill (m_flags.begin(), m_flags.end(), 0);

    // use auxiliaries
    m_ghost     = m_ghost_aux;
    m_mesh      = m_mesh_aux;
    m_ghost_aux = nullptr;
    m_mesh_aux  = nullptr;
  }

  return changed;
  
} // SolverWrap::partition

// =======================================================
// =======================================================
void
SolverWrap::complete ()
{

  CANOP_ASSERT (m_ghost != nullptr);
  CANOP_ASSERT (m_mesh != nullptr);
  CANOP_ASSERT (m_ghost_aux != nullptr);
  CANOP_ASSERT (m_mesh_aux != nullptr);
  CANOP_ASSERT (m_match_aux == 0);

  // destroy auxiliary structures
  p4est_mesh_destroy (m_mesh_aux);
  p4est_ghost_destroy (m_ghost_aux);
  m_ghost_aux = nullptr;
  m_mesh_aux = nullptr;

} // SolverWrap::complete

