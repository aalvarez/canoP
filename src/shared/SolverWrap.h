/*
 * This file was forked from p4est_wrap.h. Modifications:
 * * create a solver_wrap_new_ext that takes all the args of
 * our own p4est_replace_t function and initializing p4est with the
 * extended options.
 */

/*
 * C++ refactoring.
 */

/*
  This file is part of p4est.
  p4est is a C library to manage a collection (a forest) of multiple
  connected adaptive quadtrees or octrees in parallel.

  Copyright (C) 2012 Carsten Burstedde

  p4est is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  p4est is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with p4est; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef SOLVER_WRAP_H_
#define SOLVER_WRAP_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_extended.h>
#include <p4est_connectivity.h>
#include <p4est_geometry.h>
#include <p4est_mesh.h>
#else
#include <p8est_extended.h>
#include <p8est_connectivity.h>
#include <p8est_geometry.h>
#include <p8est_mesh.h>
#endif

class ConfigReader;

#include <vector>

/*** COMPLETE INTERNAL STATE OF P4EST ***/

/**
 * This enum defines bit field mask to enable/disable refinement / coarsening flags
 */
enum SOLVER_WRAP_FLAGS
{
  P4EST_WRAP_NONE = 0,
  P4EST_WRAP_REFINE = 0x01,
  P4EST_WRAP_COARSEN = 0x02
};

/**
 * This is a low-level class storing several data structures for interacting 
 * with p4est internal implementation.
 *
 * Some of the code here has been adapted from the original p4est_wrap.c
 * utility, designed to simplify / clarify the implementation of the
 * adapt cycle (refine / coarsen / balance).
 *
 * NOTE: solver_wrap_t expects p4est->user_pointer to point back to it, so
 * make sure that you save whatever you had in there before and set it to
 * the solver_wrap before starting an adapt-partition-complete cycle.
 */
class SolverWrap
{

public:
  
  /** 
   * SolverWrap constructor, see p4est_new_ext.
   *
   * The default constructor initializes almost nothing.
   * 
   * The genuine initialization is done in method setup. Method setup
   * can only be called after p4est is create (call to p4est_new), 
   * so that the local number of quadrants are known (needed forest
   * memory allocation).
   *
   * Added a p4est_replace_t member that will be used when adapting.
   */
  SolverWrap (ConfigReader * cfg);
  
  /**
   * Desctructor.
   */
  ~SolverWrap();

  /* 
   * these members are considered public and read-only 
   */
  int                   m_p4est_dim;      /**< alias to P4EST_DIM */
  int                   m_p4est_half;     /**< alias to P4EST_HALF */
  int                   m_p4est_faces;    /**< alias to P4EST_FACES */
  int                   m_p4est_children; /**< alias to P4EST_CHILDREN */
  p4est_connectivity_t *m_conn;           /**< p4est connectivity */
  p4est_geometry_t     *m_geom_compute;   /**< geometry used for computation */
  p4est_geometry_t     *m_geom_io;        /**< geometry used for io */
  p4est_t              *m_p4est;          /**< the main p4est data structure */

  /* 
   * anything below here is considered private und should not be touched 
   */
  
  /** weight flag used in partition / load balancing */
  int                   m_weight_exponent; 
  
  /** array of flags (one per local quadrant, dynamic allocation) to indicate a cell to refine or coarsen */
  std::vector<uint8_t>  m_flags;         
  std::vector<uint8_t>  m_temp_flags;
  //uint8_t m_flags;         
  //uint8_t m_temp_flags;

  /** 
   * counter, determine the number of quadrants to be refined.
   * It is incremented in mark_refine, and decremented in mark_coarsen (if quadrant
   *  has already been marked previously for refinement).
   */
  p4est_locidx_t        m_num_refine_flags;

  /**
   * counter.
   *
   * incremented inside refine_callback (p4est_refine_ext), used for 
   * cross-checking afterwards, that the callback was actually called
   * the right number of times.
   */
  p4est_locidx_t        m_inside_counter;

  /**
   * actual number of coarsened quadrants; incremented inside coarsen_callback.
   */
  p4est_locidx_t        m_num_replaced;

  /* for ghost and mesh use methods get_ghost, get_mesh declared below */
  p4est_ghost_t        *m_ghost;
  p4est_mesh_t         *m_mesh;
  p4est_ghost_t        *m_ghost_aux;
  p4est_mesh_t         *m_mesh_aux;
  int                   m_match_aux;

  /* added members */
  void                 *m_user_pointer;
  ConfigReader         *m_cfg;

  /**
   * This is the genuine initialization.
   *
   * Take care that setup can only be called after p4est_new (p4est data structure
   * created).
   *
   * This is where memory allocation is done for
   * mesh, ghost, flags ... 
   *
   * Note that user_pointer here is used to pass a pointer to the Solver.
   */
  void                 setup (p4est_connectivity_t * conn,
			      p4est_geometry_t * geom_compute,
			      p4est_geometry_t * geom_io,
			      p4est_t * p4est);

  /**
   * Setup the SolverWrap user pointer (with the Solver pointer).
   */
  void                 setup_user_pointer (void * user_pointer);
  
  /** 
   * Set the ConfigReader member.
   */
  void                  set_cfg (ConfigReader *cfg);

  /** Return the appropriate ghost layer.
   * This function is necessary since two versions may exist simultaneously
   * after refinement and before partition/complete.
   * */
  p4est_ghost_t        *get_ghost ();
  
  /** Return the appropriate mesh structure.
   * This function is necessary since two versions may exist simultaneously
   * after refinement and before partition/complete.
   * */
  p4est_mesh_t         *get_mesh ();
  
  /** 
   * Return the geometry structure for computation.
   */
  p4est_geometry_t     *get_geometry_compute () {return m_geom_compute;};

  /**
   * Init compute geometry.
   */ 
  void                  set_geometry_compute (p4est_geometry_t *geom) {m_geom_compute = geom;};
  
  /** 
   * Return the geometry structure for io.
   */
  p4est_geometry_t     *get_geometry_io () {return m_geom_io;};
  
  /**
   * Init io geometry.
   */ 
  void                  set_geometry_io (p4est_geometry_t *geom) {m_geom_io = geom;};

  /** Mark a local element for refinement.
   * This will cancel any coarsening mark set previously for this element.
   * \param [in] which_tree The number of the tree this element lives in.
   * \param [in] which_quad The number of this element relative to its tree.
   */
  void                  mark_refine (p4est_topidx_t which_tree,
				     p4est_locidx_t which_quad);
  
  /** Mark a local element for coarsening.
   * This will cancel any refinement mark set previously for this element.
   * \param [in] which_tree The number of the tree this element lives in.
   * \param [in] which_quad The number of this element relative to its tree.
   */
  void                  mark_coarsen (p4est_topidx_t which_tree,
				      p4est_locidx_t which_quad);

  /**
   * Get refine/coarsen flag associated to a given quad identified by the couple (which_tree, which_quad)
   *  which its belongs to.
   *
   * bit 0 indicates if the quad is marked for refinement
   * bit 1 indicates if the quad is marked for coarsening
   *
   * \param [in] which_tree The number of the tree this element lives in.
   * \param [in] which_quad The number of this element relative to its tree.
   *
   * \return flag
   */
  int8_t                get_flag (p4est_topidx_t which_tree,
				  p4est_locidx_t which_quad);

  /** 
   * Call p4est_refine, p4est_coarsen, and p4est_balance to update pp->p4est.
   * Checks pp->flags as per-quadrant input against solver_wrap_flags_t.
   * The pp->flags array is updated along with p4est and reset to zeros.
   * Creates ghost_aux and mesh_aux to represent the intermediate mesh.
   *
   * This routine is mainly called inside Solver::adapt_cycle
   *
   * \return          boolean whether p4est has changed.
   *                  If true, partition must be called.
   *                  If false, partition must not be called, and
   *                  complete must not be called either.
   */
  int                   adapt (p4est_init_t init_fn,
			       p4est_replace_t replace_on_refine,
			       p4est_replace_t replace_on_coarsen,
			       double *stat_time);

  /**
   * Call p4est_refine, and p4est_balance to update pp->p4est.
   * The refinement is made according to the provided geometric indicator.
   * The pp->flags array is updated along with p4est and reset to zeros.
   * Creates ghost_aux and mesh_aux to represent the intermediate mesh.
   * \return          boolean whether p4est has changed.
   *                  If true, partition must be called.
   *                  If false, partition must not be called, and
   *                  complete must not be called either.
   */
  int                   adapt_geometric (p4est_init_t init_fn,
                                         p4est_refine_t geom_indicator);
  
  /** 
   * Call p4est_partition for equal leaf distribution.
   * Frees the old ghost and mesh first and updates pp->flags along with p4est.
   * The pp->flags array is reset to zeros.
   * Creates ghost and mesh to represent the new mesh.
   * \param [in] weight_exponent      Integer weight assigned to each leaf
   *                  according to 2 ** (level * exponent).  Passing 0 assigns
   *                  equal weight to all leaves.  Passing 1 increases the
   *                  leaf weight by a factor of two for each level increase.
   *                  CURRENTLY ONLY 0 AND 1 ARE LEGAL VALUES.
   * \return          boolean whether p4est has changed.
   *                  If true, complete must be called.
   *                  If false, complete must not be called.
   */
  int                   partition (int weight_exponent);
  
  /** Free memory for the intermediate mesh.
   * Sets mesh_aux and ghost_aux to nullptr.
   * This function must be used if both refinement and partition effect changes.
   * After this call, we are ready for another mark-refine-partition cycle.
   */
  void                  complete ();
  
}; // class SolverWrap


/** This function has to be called from the user defined replace function.
 *
 * This handles the flags inside the wrapper and is only called when
 * refinement actually happens, it should not be called while coarsening.
 *
 * It is needed because refinement does not always happen when a
 * p4est_refine_t function returns 1, so we need to handle that case.
 *
 * It is passed as the last argument to p4est_refine_ext.
 */
void
wrap_replace_flags (p4est_t * p4est,
		    p4est_topidx_t which_tree,
		    int num_outgoing,
		    p4est_quadrant_t * outgoing[],
		    int num_incoming,
		    p4est_quadrant_t * incoming[]);


#endif // SOLVER_WRAP_H_
