#ifndef ITERATORS_H_
#define ITERATORS_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est_iterate.h>
#else
#include <p8est_iterate.h>
#endif

#include <map>

/**
 * typedef to define an alias to p4est_iter_volume_t
 */
using iterator_t = void (*) (p4est_iter_volume_info_t *info, void *user_data);

/**
 * Container of iterator function pointer.
 * Map int (enum IteratorID) to an actual function pointer.
 */
using iterators_list_t = std::map<int,iterator_t>;

// /**
//  * Enum to ease indexing a specific iterator function pointer.
//  */
// enum IteratorsID {
//   QUADRANT_PRINT = 0,
//   COMPUTE_MEAN_VALUES = 1,
//   GATHER_STATISTICS = 2,
//   START_STEP = 3,
//   END_STEP = 4,
//   MARK_ADAPT = 5,
//   CFL_TIME_STEP = 6,
//   RECONSTRUCT_GRADIENT = 7,
//   TRACE = 8,
//   UPDATE = 9,
//   GRAVITY_SOURCE_TERM = 10
// };

/**
 * Iterator ID (replacement to enum IteratorsID).
 */
struct IT_ID {

  static constexpr int QUADRANT_PRINT = 0;
  static constexpr int COMPUTE_MEAN_VALUES = 1;
  static constexpr int GATHER_STATISTICS = 2;
  static constexpr int START_STEP = 3;
  static constexpr int END_STEP = 4;
  static constexpr int MARK_ADAPT = 5;
  static constexpr int CFL_TIME_STEP = 6;
  static constexpr int RECONSTRUCT_GRADIENT = 7;
  static constexpr int TRACE = 8;
  static constexpr int UPDATE = 9;
  static constexpr int GRAVITY_SOURCE_TERM = 10;
  static constexpr int LEVEL_STATISTICS = 11;
};

/**
 * A simple iterator to print quadrant information.
 */
void iterator_quadrant_print (p4est_iter_volume_info_t * info,
			      void *user_data);

/**
 * A simple iterator to gather quadrant level statistics.
 *
 * The @a user_data parameter is interpreted as unsigned long[], with one
 * element per level between Solver::m_min_refine and Solver::m_max_refine.
 */
void iterator_level_statistics (p4est_iter_volume_info_t * info,
                                void *user_data);

/**
 * Main iterest of this class is to hold an array of 
 * iterator callback pointers.
 *
 * This array is most empty. A scheme-specific derived class will fill
 * the array, or even enlarge the array with new iterators.
 *
 * Right now, iterators have type iterator_t (alias to a p4est callback type).
 * I didn't found as easy way to specify iterators as generic (qdata_t 
 * template'd) member of a base class; which in turn could be
 * overriden in a derived class, mostly because.
 *
 * The code is ugly right now; we are forced to partly duplicate actual
 * iterators callback implementation.
 */
class IteratorsBase
{
public:
  IteratorsBase();
  virtual ~IteratorsBase();

  //! list of iterators given as p4est_iter_volume_t
  iterators_list_t iteratorsList;

  virtual void fill_iterators_list();
  
}; // class IteratorsBase


/**
 * An abstract base class from which each numerical scheme can derive
 * to obtain actual implementation.
 *
 * Each iterator uses the p4est volume callback signature.
 *
 * Some iterator callback implementation will require an actual qdata
 * implementation; 
 * that should be done in the derived class.
 *
 */
// template<typename someUserDataTypes>
// class Iterators : public IteratorsBase {

//   using qdata_t       = typename someUserDataTypes::qdata_t;
//   using qdatavar_t    = typename someUserDataTypes::qdatavar_t;
//   using qdatarecons_t = typename someUserDataTypes::qdatarecons_t;

// public:
//   Iterators() : IteratorsBase() {};
//   virtual ~Iterators() {};

//   //void fill_iterator
  
//   static void iterator_quadrant_print (p4est_iter_volume_info_t *
// 				       info, void *user_data);
  
//   static void iterator_compute_mean_values (p4est_iter_volume_info_t *
// 					    info, void *user_data);
  
//   static void iterator_gather_statistics (p4est_iter_volume_info_t *
// 					  info, void *user_data);
  
//   static void iterator_start_step (p4est_iter_volume_info_t *
// 				   info, void *user_data);
  
//   static void iterator_end_step (p4est_iter_volume_info_t *
// 				 info, void *user_data);
  
//   static void iterator_mark_adapt (p4est_iter_volume_info_t *
// 				   info, void *user_data);

// }; // class Iterators
  
#endif // ITERATORS_H_
