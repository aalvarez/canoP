
#ifndef LNODE_UTILS_H
#define LNODE_UTILS_H

/** Utilities to manipulate p4est order 1 lnodes
 */

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#include <p4est_geometry.h>
#include <p4est_lnodes.h>
#include <p4est_mesh.h>
#else
#include <p8est.h>
#include <p8est_geometry.h>
#include <p8est_lnodes.h>
#include <p8est_mesh.h>
#endif

#include <functional>

/** Number of possible independent nodes for each hanging node.
 *
 * The reference corner is assumed to be 0, the indices have to be flipped
 * consequently. Assuming the hanging corner is \c c:
 *
 * \code
 * int num_hanging[P4EST_CHILDREN];
 * int ids_hanging[P4EST_CHILDREN][P4EST_HALF];
 * for (int i = 0; i < P4EST_CHILDREN; ++i) {
 *   num_hanging[i] = lnodes_corner_num_hanging[i ^ c];
 *
 *   for (int j = 0; j < num_hanging[i]; ++j) {
 *     ids_hanging[i][j] = lnodes_corner_to_hanging[i ^ c][j] ^ c;
 *   }
 * }
 * \endcode
 */
extern const int  lnodes_corner_num_hanging[P4EST_CHILDREN];
/** Independent nodes for each hanging node.
 * \see lnodes_corner_num_hanging
 */
extern const int *lnodes_corner_to_hanging[P4EST_CHILDREN];

/** Decode the information from p{4,8}est_lnodes_t for a given element.
 *
 * \see p4est_lnodes.h for an in-depth discussion of the encoding.
 * \param [in] face_code         Bit code as defined in p{4,8}est_lnodes.h.
 *                               There is one face_code value per element.
 *                               If face_code is 0: no hanging node
 *                               else we need to decode which node(s) 
 *                               is(are) hanging
 * \param [out] hanging_corner   Undefined if no node is hanging.
 *                               If any node is hanging, this contains
 *                               one integer per corner, which is -1
 *                               for corners that are not hanging,
 *                               and the number of the non-hanging
 *                               corner on the hanging face/edge otherwise.
 *                               For faces in 3D, it is diagonally opposite.
 * \return true if any node is hanging, false otherwise.
 *
 * face_code is a  8bit unsigned integer in 2D.
 * face_code = [<--unused bits-->|<--  P4EST_DIM bits-->|<--P4EST_DIM bits-->]
 *             [                 |  hanging face code   | common cornerId to ]
 *             [                 |                      | all hanging faces  ]
 *             [                 | At least one bit     | any in  000<->111  ]
 *             [                 | is non zero          |                    ]
 *
 * face_code is a 16bit unsigned integer in 3D.
 * face_code = [<--unused bits-->|<--2*P4EST_DIM bits-->|<--P4EST_DIM bits-->]
 *             [                 |  hanging face code   | common cornerId to ]
 *             [                 |                      | all hanging faces  ]
 *             [                 | At least 3 bits      | any in  000<->111  ]
 *             [                 | are non zero         |                    ]
 *
 * In 2D and 3D, there are always 2 diagonally opposite corners that are
 * non-hanging.
 * So it means that:
 * - in 2D, we only need to provide information for 4-2=2 corners, one bit each.
 * - in 3D, we only need to provide information for 8-2=6 corners, one bit each
 *
 *
 * NOTE on facecode, starting by the least significant bits.
 * 1. In 2D/3D, a given quadrant can have at most P4EST_DIM hanging faces, 
 *    and these hanging faces necessarily share a common corner.
 *
 *    The P4EST_DIM least significant bits encode the corner number information
 *    (see drawing below).
 *
 * 2. The next P4EST_DIM bits encode the information about the
 *    direction of the hanging faces. As there can be only 1 hanging face per 
 *    direction, one only need P4EST_DIM bits.
 *    1 means the face is hanging.
 *
 * 3. In 3D, the next P4EST_DIM bits encode the information about the direction
 *    of the hanging edges. As there can be only 3 directions, one only needs
 *    P4EST_DIM bits. 1 means the edges in this direction are hanging.
 *
 * Below, on the left, q1 has 1 hanging face (face number 0 in z-order);
 * the information encoded for q1 is the corner number that touches q0
 * and match a q0 corner. Here it is corner #0, so the 2 bits are 00.
 *
 * On the right, q1 has one hanging face (face number 1), the encoded corner is 
 * corner #1.
 *   ____________________         ____________________
 *  |         |     |    |       |    |    |          |
 *  |         |     |    |       |    |    |          |
 *  |   q0    X_____|____|       |____|____X    q0    |
 *  |         |     |    |       |    |    |          |
 *  |         | q1  |    |       |    | q1 |          |
 *  |_________0_____|____|       |____|____1__________| 
 *  |         |     |    |       |    |    |          |
 *  |         |     |    |       |    |    |          |
 *  |         |_____|____|       |____|____|          |
 *  |         |     |    |       |    |    |          |
 *  |         |     |    |       |    |    |          |
 *  |_________|_____|____|       |____|____|__________|
 *
 * Below, q1 has 2 hanging faces (face number 0 and 2 in z-order);
 * the information encoded for q1 is the common corner number that touches both q0
 * and q2 (the larger neighbors).
 * On the left,  the encoded corner is #0
 * On the right, the encoded corner is #3 
 *   ____________________         ______________________
 *  |         |     |    |       |          |           |
 *  |         |     |    |       |          |           |
 *  |   q0    X_____|____|       |    q0    |           |
 *  |         |     |    |       |          |           |
 *  |         | q1  |    |       |          |           |
 *  |_________0_____X____|       |____X_____3___________|
 *  |         |          |       |    |     |           |
 *  |         |          |       |    |  q1 |           |
 *  |         |     q2   |       |____|_____X     q2    |
 *  |         |          |       |    |     |           |
 *  |         |          |       |    |     |           |
 *  |_________|__________|       |____|_____|___________|
 *
 *  NOTE about decoding :
 *
 *  1. h = c ^ (1<<i)  explore all the node number linked by a direct edge to c
 *  2. h and h^ones are separated by a large diagonal of the cell cube.
 */
int
lnodes_decode (p4est_lnodes_code_t face_code,
               int hanging_corner[P4EST_CHILDREN]);

/** Compute values at hanging nodes by interpolation.
 *
 * Here we only consider bi/trilinear finite elements, 
 * so interpolation is very simple in that case.
 *
 * A face hanging node in 3D depends on the four corner nodes of the face,
 * edge hanging nodes or face hanging nodes in 2D depend on two nodes.
 * This function works in place, we have to be careful about the ordering.
 * Face hanging node values are not reused, so they are overwritten first.
 *
 * \param [in] face_code    This number encodes the child id of the quadrant
 *                          and the hanging status of faces and edges.
 * \param [in,out] inplace  On input, the values at the independent nodes.
 *                          On output, interpolated to hanging node locations.
 */
void
lnodes_interpolate_hanging (p4est_lnodes_code_t face_code,
                           double inplace[P4EST_CHILDREN]);

/** Parallel sum of values in node vector across all sharers.
 *
 * \param [in] p4est      The mesh is not changed.
 * \param [in] lnodes     The node numbering is not changed.
 * \param [in,out] v      On input, vector with local contributions.
 *                        On output, the node-wise sum across all sharers.
 */
void
lnodes_share_sum (p4est_t * p4est, p4est_lnodes_t * lnodes, double *v);

/** Interpolate lnode data from quadrant data
 *
 * \param[in] p4est The forest
 * \param[in] lnodes The lnode numbering
 * \param[in] quad_getter The data getter
 * \param[in] quad_volume The volume getter (arguments: the forest, the tree id, the quadrant)
 * \param[out] lnodes_data An array to store the data to (size \c lnodes->num_local_nodes)
 *
 * The interpolation is designed to conserve the integral of the field. The
 * value at an interior node is the volume-weighted average of the values in the
 * neighbouring quadrants, with a correction in the case of hanging nodes.
 *
 * The inter-tree boundary behaviour respects the global connectivity. On a
 * domain boundary (not between two trees), the values at the nodes only take
 * into account the existing quadrants (again doing the weighted average).
 */
void
interp_quad_to_lnodes (p4est_t * p4est, p4est_lnodes_t * lnodes,
                       std::function<double(p4est_quadrant_t *)> quad_getter,
                       std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_volume,
                       double *lnodes_data);

/** Interpolate quadrant data from lnode data
 *
 * \param[in] p4est The forest
 * \param[in] lnodes The lnode numbering
 * \param[in] lnodes_data The node data array (size \c lnodes->num_local_nodes)
 * \param[in] quad_setter The data setter, called once per quad with the value to set
 *
 * The value in every quadrant is computed to be the average of the values at
 * its corner nodes. If a node is hanging, its value is interpolated from the
 * neighbouring non-hanging nodes. This corresponds to the Q1 finite element
 * average on the quadrant.
 * \see lnodes_interpolate_hanging
 */
void
interp_lnodes_to_quad (p4est_t * p4est, p4est_lnodes_t * lnodes,
                       const double *lnodes_data,
                       std::function<void(p4est_quadrant_t *, double)> quad_setter);

/** Interpolate quadrant-centered gradient from lnode data
 *
 * \param[in] p4est The forest
 * \param[in] lnodes The lnode numbering
 * \param[in] quad_size The size getter (arguments: the forest, the tree id, the quadrant)
 * \param[in] lnodes_data The node data array (size \c lnodes->num_local_nodes)
 * \param[in] quad_setter The data setter, called once per quad with the vector to set (size P4EST_DIM)
 *
 * The gradient in every quadrant is computed as the difference of the face
 * averages in each direction. The values at hanging nodes are interpolated the
 * usual way. This interpolation corresponds to the quadrant average of the
 * gradient of the Q1 finite element function.
 * \see interp_lnodes_to_quad, lnodes_interpolate_hanging
 */
void
gradient_lnodes_to_quad (p4est_t * p4est, p4est_lnodes_t * lnodes,
                         std::function<double(p4est_t *, p4est_topidx_t, p4est_quadrant_t *)> quad_size,
                         const double *lnodes_data,
                         std::function<void(p4est_quadrant_t *, double *)> quad_setter);

/** Flag boundary nodes.
 *
 * \param [in]  p4est          The forest.
 * \param [in]  mesh           The mesh.
 * \param [in]  lnodes         The lnode numbering.
 * \param [out] bc             The flag is non-zero for boundary nodes.
 *
 * The value of the flag encodes which boundary is concerned: for each face f
 * (in usual quadrant face order), the fth bit is 1 iff the node touches a
 * boundary via this face.
 */
void
lnodes_flag_boundaries (p4est_t *p4est, p4est_mesh_t *mesh, p4est_lnodes_t *lnodes,
                        int8_t *bc);

/** Apply Dirichlet boundary conditions.
 *
 * \param [in]  p4est          The forest.
 * \param [in]  mesh           The geometry (to compute lnode positions).
 * \param [in]  lnodes         The lnode numbering.
 * \param [in]  bc             The flag is non-zero for boundary nodes.
 * \param [in]  func_bc        The function called for each boundary node.
 * \param [out] u              The output vector.
 *
 * The function is called for each boundary node with its physical coordinates
 * and the corresponding value in the output vector is set to the return value.
 */
void
lnodes_set_boundaries(p4est_t *p4est, p4est_geometry_t *geom, p4est_lnodes_t *lnodes,
                      const int8_t *bc, std::function<double(double[3])> func_bc,
                      double *u);

/** Evaluate a function at every node.
 *
 * \param [in]  p4est          The forest.
 * \param [in]  mesh           The geometry (to compute lnode positions).
 * \param [in]  lnodes         The lnode numbering.
 * \param [in]  func           The function called for each node.
 * \param [out] u              The output vector.
 *
 * The function is called for each node with its physical coordinates and the
 * corresponding value in the output vector is set to the return value.
 */
void
lnodes_eval(p4est_t *p4est, p4est_geometry_t *geom, p4est_lnodes_t *lnodes,
            std::function<double(double[3])> func, double *u);

#endif /* LNODE_UTILS_H */

