#ifndef CANOP_LIMITER_H_
#define CANOP_LIMITER_H_

//! define our function pointer limiter type
using regular_limiter_t = double (*) (double r);

/**
 * A large list of flux limiter function can be found on wikipedia:
 * https://en.wikipedia.org/wiki/Flux_limiter
 */
double compute_phi_van_albada(double r);
double compute_phi_ospre(double r);
double compute_phi_charm(double r);

#endif // CANOP_LIMITER_H_
