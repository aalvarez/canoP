#ifndef GEOMETRIC_INDICATOR_FACTORY_H_
#define GEOMETRIC_INDICATOR_FACTORY_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

#include <string>
#include <map>

#include "canoP_base.h"

/**
 * An abstract base class to define a common interface for concrete indicators factory.
 *
 * The main purpose is to return an IndicatorCallback, i.e. a function pointer
 * that will be passed to p4est later.
 *
 * The idea here it to define a map between a name and the actual indicator function
 * defined by the function pointer type indicatorCallback.
 *
 */
class GeometricIndicatorFactory {

public:
  GeometricIndicatorFactory();
  virtual ~GeometricIndicatorFactory() {};

  /**
   * \brief Define IndicatorCallback as a function pointer.
   *
   * Indicators functions compute a local value used to decide if a given cell
   * must be refined or coarsened.
   *
   */
  using IndicatorCallback = p4est_refine_t;


private:
  /**
   * Map to associate a label with a an indicator.
   */
  using IndicatorCallbackMap = std::map<std::string, IndicatorCallback>;
  IndicatorCallbackMap m_callbackMap;

public:
  /**
   * Routine to insert an indicator function into the map.
   * Note that this register function can be used to serve
   * at least two different purposes:
   * - in the concrete factory: register existing callback's
   * - in some client code, register a callback from a plugin code, at runtime.
   */
  void registerIndicator(const std::string& key, IndicatorCallback cb) {
    m_callbackMap[key] = cb;
  };

  /**
   * \brief Retrieve one of the possible indicators by name.
   *
   * Allowed default names are defined in the concrete factory.
   */
  virtual IndicatorCallback callback_byname (const std::string &name) {
    // find the indicator in the register map
    typename IndicatorCallbackMap::iterator it = m_callbackMap.find(name);

    // if found, just return it
    if ( it != m_callbackMap.end() )
      return it->second;

    // if not found, return null pointer
    // it is the responsability of the client code to deal with
    // the possibility to have a nullptr callback (does nothing).
    //
    // Note that the concrete factory may chose a different behavior
    // and return a valid default callback (why not ?).
    CANOP_GLOBAL_PRODUCTION  ("#### WARNING: ####\n");
    CANOP_GLOBAL_PRODUCTIONF ("%s: is not recognized as a valid indicator key.\n",name.c_str());
    CANOP_GLOBAL_PRODUCTION  ("#### WARNING: ####\n");
    return nullptr;
  };

}; // class GeometricIndicatorFactory

#endif // GEOMETRIC_INDICATOR_FACTORY_H_
