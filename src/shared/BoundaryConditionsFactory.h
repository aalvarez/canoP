#ifndef BOUNDARY_CONDITIONS_FACTORY_H_
#define BOUNDARY_CONDITIONS_FACTORY_H_

#ifdef USE_3D
#include <p4est_to_p8est.h>
#endif

#ifndef P4_TO_P8
#include <p4est.h>
#else
#include <p8est.h>
#endif

#include <map>

#include "canoP_base.h"

/**
 * An abstract base class to define a common interface for concrete boundary
 * conditions factories.
 *
 * Template parameter will be specified when deriving the concrete factory.
 * Template parameter is supposed to be a struct holding data for a quadrant.
 *
 * The main purpose is to return an BC_Callback, i.e. a function pointer
 * that will be passed to p4est later.
 *
 * The idea here it to define a map between a name and the actual
 * boundary condition function defined by the function pointer type 
 * BC_Callback.
 *
 * Each derived class will have to define and register its own BC_Callback's.
 *
 * The BC_CallbackMap is empty, and supposed to be filled in the concrete 
 * derived class.
 */
template <typename qdata_t>
class BoundaryConditionsFactory {

public:
  BoundaryConditionsFactory() {};
  virtual ~BoundaryConditionsFactory() {};
  
  /**
   * \brief Define BC_Callback as a function pointer.
   *
   * BoundaryConditions functions compute a local value used to provide
   * userdata to aghost quadrant.
   *
   * ghost_qdata is the output, all other parameters are input only.
   */
  using BC_Callback = void (*) (p4est_t * p4est,
				p4est_topidx_t which_tree,
				p4est_quadrant_t * q,
				int face,
				qdata_t * ghost_qdata);

private:
  /**
   * Map to associate a label with a a boundary condition function pointer.
   */
  using BC_CallbackMap = std::map<std::string, BC_Callback>;
  BC_CallbackMap m_callbackMap;
  
public:
  /**
   * Routine to insert a BC function pointer into the map.
   * Note that this register function can be used to serve 
   * at least two different purposes:
   * - in the concrete factory: register existing callback's
   * - in some client code, register a callback from a plugin code, at runtime.
   */
  void registerBoundaryConditions(const std::string& key, BC_Callback cb) {
    m_callbackMap[key] = cb;
  };

  /**
   * \brief Retrieve one of the possible BC function pointer by name.
   *
   * Allowed default names are defined in the concrete factory.
   */
  virtual BC_Callback callback_byname (const std::string &name) {
    // find the boundary conditions function pointer in the register map
    typename BC_CallbackMap::iterator it = m_callbackMap.find(name);
    
    // if found, just return it
    if ( it != m_callbackMap.end() )
      return it->second;
    
    // if not found, return null pointer
    // it is the responsability of the client code to deal with
    // the possibility to have a nullptr callback (does nothing).
    //
    // Note that the concrete factory may chose a different behavior
    // and return a valid default callback (why not ?).
    CANOP_GLOBAL_PRODUCTION  ("#### WARNING: ####\n");
    CANOP_GLOBAL_PRODUCTIONF ("%s: is not recognized as a valid boundary condition key.\n",name.c_str());
    CANOP_GLOBAL_PRODUCTION  ("#### WARNING: ####\n");
    return nullptr;
  };
  
}; // class BoundaryConditionsFactory

#endif // BOUNDARY_CONDITIONS_FACTORY_H_
