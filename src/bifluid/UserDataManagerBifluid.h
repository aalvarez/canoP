#ifndef USERDATA_MANAGER_BIFLUID_H_
#define USERDATA_MANAGER_BIFLUID_H_

#include "UserDataManager.h"
#include "userdata_bifluid.h"

namespace canop {

namespace bifluid {

using UserDataTypesBifluid =
  UserDataTypes<Qdata,
		QdataVar,
		QdataRecons>;

class UserDataManagerBifluid : public UserDataManager<UserDataTypesBifluid>
{
  
public:
  UserDataManagerBifluid(bool useP4estMemory);
  virtual ~UserDataManagerBifluid();

  virtual void qdata_set_variables (qdata_t * data,
				    qdatavar_t * w);
  
  virtual void quadrant_set_state_data (p4est_quadrant_t * dst,
					p4est_quadrant_t * src);

  virtual void quadrant_compute_mean(p4est_quadrant_t *qmean,
				     p4est_quadrant_t *quadout[P4EST_CHILDREN]);

  virtual void quadrant_copy_w_to_wnext (p4est_quadrant_t * q);
  virtual void quadrant_copy_wnext_to_w (p4est_quadrant_t * q);
  
  double qdata_get_rho_gas (p4est_quadrant_t * q);
  double qdata_get_rho_liquid (p4est_quadrant_t * q);
  double qdata_get_momentum (p4est_quadrant_t * q, int direction);
  double qdata_get_alpha (p4est_quadrant_t * q);
  double qdata_get_pressure (p4est_quadrant_t * q);
  double qdata_get_a (p4est_quadrant_t * q);

  void qdata_vget_momentum (p4est_quadrant_t * q, size_t dim, double * val);


  void qdata_reconstructed_zero(qdatarecons_t *q);
  void qdata_reconstructed_copy(qdatarecons_t *q1, 
				const qdatarecons_t *q2);

  void reconstruct_variables (const qdatavar_t &w,
			      qdatarecons_t &r);

  void compute_delta(const qdatarecons_t w,const qdatarecons_t *wn,
		     qdatarecons_t *delta,
		     scalar_limiter_t limiter,
		     double dx, double dxl, double dxr);  
  
  void qdata_reconstructed_check (qdatarecons_t w, const char *plus);
  void qdata_variables_check (qdatavar_t w, const char *plus);
  void qdata_check_variables (qdata_t * data);

  void quadrant_check_data (p4est_quadrant_t * q);

  /**
   * Return a pointer to a member that is a qdata field "getter".
   */
  virtual qdata_getter_t qdata_getter_byname(const std::string &varName);

  /**
   * Return a pointer to a member that is a qdata vector field "getter".
   */
  virtual qdata_vector_getter_t qdata_vgetter_byname(const std::string &varName);

  /**
   * Return the field type for the given variable.
   */
  virtual qdata_field_type qdata_type_byname(const std::string &varName);

}; // UserDataManagerBifluid

} // namespace bifluid

} // namespace canop

#endif // USERDATA_MANAGER_BIFLUID_H_
