#include "SolverCoreBifluid.h"

#include "InitialConditionsFactoryBifluid.h"
#include "BoundaryConditionsFactoryBifluid.h"
#include "IndicatorFactoryBifluid.h"

#include "ConfigReader.h"

namespace canop {

namespace bifluid {

// =======================================================
// =======================================================
SolverCoreBifluid::SolverCoreBifluid(ConfigReader *cfg) :
  SolverCore<Qdata>(cfg)
{

  /*
   * Initial condition callback setup.
   */
  InitialConditionsFactoryBifluid IC_Factory;
  m_init_fn = IC_Factory.callback_byname (m_init_name);

  /*
   * Boundary condition callback setup.
   */
  BoundaryConditionsFactoryBifluid BC_Factory;
  m_boundary_fn = BC_Factory.callback_byname (m_boundary_name);

  /*
   * Refine/Coarsen callback setup.
   */
  IndicatorFactoryBifluid indic_Factory;
  m_indicator_fn = indic_Factory.callback_byname (m_indicator_name);

  /*
   * Geometric refine callback setup.
   */
  GeometricIndicatorFactory geom_indic_Factory;
  m_geom_indicator_fn = geom_indic_Factory.callback_byname (m_geom_indicator_name);

  
} // SolverCoreBifluid::SolverCoreBifluid


// =======================================================
// =======================================================
SolverCoreBifluid::~SolverCoreBifluid()
{
} // SolverCoreBifluid::~SolverCoreBifluid

} // namespace canop

} // namespace bifluid
