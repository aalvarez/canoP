#ifndef INITIAL_CONDITIONS_FACTORY_BIFLUID_H_
#define INITIAL_CONDITIONS_FACTORY_BIFLUID_H_

#include "InitialConditionsFactory.h"
#include "bifluid/userdata_bifluid.h"

namespace canop {

namespace bifluid {

/**
 * defines the level of void in the SOD-like test cases 
 */
struct StarState {

  static const double lambda;
  static double uStar;
  static double pStar;
  static double cStar;
  static int    flagStar;

}; // struct StarState

// ================================================================
// ================================================================
/**
 *
 */
class InitialConditionsFactoryBifluid : public InitialConditionsFactory
{

  using qdata_t = Qdata;
  
public:
  InitialConditionsFactoryBifluid();
  virtual ~InitialConditionsFactoryBifluid() {};
  
}; // class InitialConditionsFactoryBifluid


/*
 * utilities
 */

scalar_init_t
init_alpha_exact_byname (const std::string &name);

/*
 * TWO PHASE GAUSSIAN
 */
void
gaussian_velocity_main (double x, double y, double z, double u[P4EST_DIM]);

double
gaussian_alpha (double x, double y, double z, double t);

double
gaussian_rho_gas (double x, double y, double z, double t);

/*
 * TWO PHASE DISK
 */
double
disk_alpha (double x, double y, double z, double t);

double
disk_rho_gas (double x, double y, double z, double t);

/*
 * TWO PHASE SOD
 */
double
sod_alpha (double x, double y, double z, double t);

double
sod_rho_gas (double x, double y, double z, double t);

/*
 * TWO PHASE SOD2
 */
double
sod2_alpha (double x, double y, double z, double t);

double
sod2_rho_gas (double x, double y, double z, double t);

void
sod2_velocity (double x, double y, double z, double u[P4EST_DIM]);

double
sod2_exact (double x, double y, double z, double t);

/*
 * TWO PHASE DROP
 */
double
drop_in_water_alpha (double x, double y, double z, double t);

double
drop_in_water_rho_gas (double x, double y, double z, double t);

void
drop_in_water_velocity (double x, double y, double z, double u[P4EST_DIM]);

/*
 * TWO PHASE Equilibrium Sea
 */
double
sea_alpha (double x, double y, double z, double t);

double
sea_rho_gas (double x, double y, double z, double t);

void
sea_velocity (double x, double y, double z, double u[P4EST_DIM]);

/*
 * TWO PHASE DAM BREAK
 */
double
dam_break_alpha (double x, double y, double z, double t);

double
dam_break_rho_gas (double x, double y, double z, double t);

/*
 * other velocity initialization
 */
void
zero_velocity (double x, double y, double z, double u[P4EST_DIM]);
void
diagonal_velocity (double x, double y, double z, double u[P4EST_DIM]);


/**
 * \brief Uniform values.
 *
 * Very simple initial condition that gives a uniform rho[0] and rho[1]
 * (= 1.0) as well as a uniform alpha ( = 0.5). The velocity is only
 * horizontal (1, 0, 0).
 *
 * NOTE:
 */
void                init_uniform_scalar (p4est_t * p4est,
                                         p4est_topidx_t which_tree,
                                         p4est_quadrant_t * quad);


/**
 * \brief Gaussian alpha for the two phase model.
 *
 * Alpha is initialized inside a circle to:
 *  alpha = | cos (pi * r / 0.6)^4     if (r < 0.3)
 *          | 0.1                      otherwise
 *
 * rho[0] = 1 * alpha and rho[1] = 1000 * alpha. The velocity is diagonal
 * (0.1, 0.1, 0).
 */
void                init_cvv_gaussian (p4est_t * p4est,
                                       p4est_topidx_t which_tree,
                                       p4est_quadrant_t * quad);

/**
 * \brief An indicator function that represents a disk.
 *
 * Same as for the gaussian, but alpha is a disk centered in (0.2, 0.5) with
 * a radius of 0.1.
 */
void                init_cvv_disk (p4est_t * p4est,
                                   p4est_topidx_t which_tree,
                                   p4est_quadrant_t * quad);

/**
 * \brief A discontinuity in rho.
 *
 * The domain this time is [-1, 1] x [0, 1]. Alpha is 1 - lambda on
 * x in [-1, 0] and lambda on [0, 1]. rho[0] is then initialized to
 * 100 on the left and 1 on the right. rho[1] is 10000 on the left and
 * 1000 on the right.
 *
 * The velocity is (0, 0, 0) everywhere.
 *
 * This gives rise to a shock on the right and a rarefaction wave on the
 * left of x = 0.
 */
void                init_cvv_sod (p4est_t * p4est,
                                  p4est_topidx_t which_tree,
                                  p4est_quadrant_t * quad);

/**
 * \brief Generate a void.
 *
 * Same domain as for SOD.
 *
 * A uniform mass density everywhere. This time the velocity is given by
 *      u_x  = |  2          x < 0
 *             | -2          x > 0
 *
 * This produces a rarefaction wave on each side of x = 0.
 */
void                init_cvv_sod2 (p4est_t * p4est,
                                   p4est_topidx_t which_tree,
                                   p4est_quadrant_t * quad);

void                init_cvv_dam_break (p4est_t * p4est,
                                        p4est_topidx_t which_tree,
                                        p4est_quadrant_t * quad);

void                init_cvv_drop_in_water (p4est_t * p4est,
                                            p4est_topidx_t which_tree,
                                            p4est_quadrant_t * quad);

void                init_cvv_sea (p4est_t * p4est,
				  p4est_topidx_t which_tree,
				  p4est_quadrant_t * quad);

/*
 * SOD problem specific.
 */
void
compute_sod2_star ();

} // namespace bifluid

} // namespace canop

#endif // INITIAL_CONDITIONS_FACTORY_BIFLUID_H_
