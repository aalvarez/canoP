#ifndef ITERATORS_BIFLUID_H_
#define ITERATORS_BIFLUID_H_

#include "Iterators.h"

namespace canop {

namespace bifluid {

/*
 * NB: if you need to add new iterators:
 * 1. resize vector iterator_list_t
 * 2. create a new enum to ease indexing this vector
 * 3. add new iterator to the vector in fill_iterators_list method.
 */

class IteratorsBifluid : public IteratorsBase
{

public:
  IteratorsBifluid();
  ~IteratorsBifluid();

  void fill_iterators_list();
  
};  // class IteratorsBifluid

} // namespace canop

} // namespace bifluid

#endif // ITERATORS_BIFLUID_H_
