#ifndef SOLVER_CORE_BIFLUID_H_
#define SOLVER_CORE_BIFLUID_H_

#include "SolverCore.h"

#include "userdata_bifluid.h"

class ConfigReader;

namespace canop {

namespace bifluid {

/**
 * This is were Initial and Border Conditions callbacks are setup.
 */
class SolverCoreBifluid : public SolverCore<Qdata>
{

public:
  SolverCoreBifluid(ConfigReader *cfg);
  ~SolverCoreBifluid();

  /**
   * Static creation method called by the solver factory.
   */
  static SolverCoreBase* create(ConfigReader *cfg)
  {
    return new SolverCoreBifluid(cfg);
  }
  
}; // SolverCoreBifluid

} // namespace bifluid

} // namespace canop

#endif // SOLVER_CORE_BIFLUID_H_
