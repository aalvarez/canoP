#include "canoP_base.h"
#include "Solver.h"
#include "QuadrantNeighborUtils.h"
#include "quadrant_utils.h"

#ifndef P4_TO_P8
#include <p4est_bits.h>
#include <p4est_mesh.h>
#include <p4est_geometry.h>
#else
#include <p8est_bits.h>
#include <p8est_mesh.h>
#include <p8est_geometry.h>
#endif

#include <sc.h> // for SC_LP_ERROR macro
#include <cmath>

#include "SolverBifluid.h"
#include "SolverCoreBifluid.h"
#include "SolverWrap.h"
#include "InitialConditionsFactoryBifluid.h"
#include "IteratorsBifluid.h"
#include "UserDataManagerBifluid.h"
#include "userdata_bifluid.h"

#include "SuliciuPar.h" // for suliciu_thermo_*

#include "Statistics.h"

namespace canop {

namespace bifluid {

// get userdata specific stuff
using qdata_t               = Qdata;
using qdata_variables_t     = QdataVar;
using qdata_reconstructed_t = QdataRecons;

using UserDataTypesBifluid =
  UserDataTypes<Qdata,
		QdataVar,
		QdataRecons>;

using QuadrantNeighborUtilsBifluid =
  QuadrantNeighborUtils<UserDataTypesBifluid>;

// =======================================================
// =======================================================
static void
iterator_compute_mean_values (p4est_iter_volume_info_t * info,
			      void *user_data)
{

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverBifluid *solver = static_cast<SolverBifluid*>(solver_from_p4est (info->p4est));
  
  UserDataManagerBifluid
    *userdata_mgr = static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());
  
  qdata_variables_t  *mean = (qdata_variables_t *) user_data;

  double              dx = quadrant_length (info->quad);
  double              area = pow (dx, P4EST_DIM);
  qdata_t            *data = userdata_mgr->quadrant_get_qdata (info->quad);

  UNUSED (dx);
  UNUSED (area);
  UNUSED (data);

  mean->arho[0] += area * data->w.arho[0];
  mean->arho[1] += area * data->w.arho[1];
  
} // iterator_compute_mean_values

// =======================================================
// =======================================================
static void
iterator_gather_statistics (p4est_iter_volume_info_t * info,
			    void *user_data)
{
  UNUSED (user_data);
  UNUSED (info);

  p4est_t            *p4est = info->p4est;
  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverBifluid      *solver = static_cast<SolverBifluid*>(solver_from_p4est (info->p4est));

  UserDataManagerBifluid *userdata_mgr =
    static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());

  p4est_geometry_t   *geom_compute = solver->get_geom_compute();

  scalar_init_t       alpha_exact_fn = init_alpha_exact_byname (solver->m_solver_core->get_init_name());
  double              alpha_exact = 0.0;

  p4est_quadrant_t   *quad = info->quad;
  double              dx = quadrant_length (quad);
  double              area = pow (dx, P4EST_DIM);

  qdata_t            *data = userdata_mgr->quadrant_get_qdata (quad);
  double              alpha = data->w.alpha;

  if (alpha_exact_fn == nullptr) {
    return;
  }

  // get the coordinates of the center of the quad
  double              X[3] = { 0, 0, 0 };
  quadrant_center_vertex (p4est->connectivity, geom_compute, info->treeid, quad, X);

  /* compute the two error norms, weighted by the area of the quadrant */
  alpha_exact = alpha_exact_fn (X[0], X[1], X[2], solver->m_t);

  solver->m_stats->m_norml1 += area * fabs (alpha - alpha_exact);
  
  solver->m_stats->m_norml2 += area * SC_SQR (alpha - alpha_exact);

  solver->m_minlevel = SC_MIN (solver->m_minlevel, info->quad->level);
  solver->m_maxlevel = SC_MAX (solver->m_maxlevel, info->quad->level);

} // iterator_gather_statistics

// =======================================================
// =======================================================
static void
iterator_start_step (p4est_iter_volume_info_t * info,
		     void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverBifluid *solver = static_cast<SolverBifluid*>(solver_from_p4est (info->p4est));
  
  UserDataManagerBifluid
    *userdata_mgr = static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());

  userdata_mgr->quadrant_copy_w_to_wnext(info->quad);
  
} // iterator_start_step

// =======================================================
// =======================================================
static void
iterator_end_step (p4est_iter_volume_info_t * info,
		   void *user_data)
{
  UNUSED (user_data);

  // retrieve our Solver, UserDataManager and finaly our quadrant data
  SolverBifluid *solver = static_cast<SolverBifluid*>(solver_from_p4est (info->p4est));
  
  UserDataManagerBifluid
    *userdata_mgr = static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());

  qdata_t *data = userdata_mgr->quadrant_get_qdata (info->quad);
  qdata_variables_t &w = data->wnext;
  
  /* on the two phase model, update auxiliary variables */
  w.alpha    = suliciu_thermo_alpha    (w.arho[0], w.arho[1]);
  w.pressure = suliciu_thermo_pressure (w.arho[0], w.arho[1], w.alpha);
  w.a        = suliciu_thermo_a        (w.arho[0], w.arho[1], w.alpha);

  SC_CHECK_ABORTF (!std::isnan (w.a), "a is nan: rho0 %g rho1 %g alpha %g",
                   w.arho[0], w.arho[1], w.alpha);
  
  userdata_mgr->quadrant_copy_wnext_to_w(info->quad);
  
} // iterator_end_step


// =======================================================
// =======================================================
static void
iterator_mark_adapt (p4est_iter_volume_info_t * info,
		     void *user_data)
{
  UNUSED (user_data);

  p4est_t            *p4est = info->p4est;
  Solver             *solver = solver_from_p4est (p4est);
  SolverWrap         *solverWrap = solver->m_wrap;

  UserDataManagerBifluid *userdata_mgr =
    static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());
  
  SolverCoreBifluid *solverCore =
    static_cast<SolverCoreBifluid *>(solver->m_solver_core);

  p4est_ghost_t      *ghost  = solver->get_ghost();
  p4est_mesh_t       *mesh   = solver->get_mesh();
  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());
  qdata_t            *qdata;
  int                 level = info->quad->level;

  int                 boundary = 0;
  int                 face = 0;
  double              eps = 0.0;
  double              eps_max = -1.0;

  p4est_quadrant_t   *neighbor = NULL;
  qdata_t            *ndata = NULL;

  //indicator_t         indicator = solver->m_indicator_fn;
  indicator_info_t    iinfo;
  iinfo.face = 0;
  iinfo.epsilon = solver->m_epsilon_refine;
  iinfo.quad_current = info->quad;
  iinfo.user_data = nullptr;

  /* init face neighbor iterator */
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, p4est, ghost, mesh,
                                  info->treeid, info->quadid);

  /* loop over neighbors */
  neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, NULL, NULL);
  while (neighbor) {
    ndata = QuadrantNeighborUtilsBifluid::mesh_face_neighbor_data (&mfn, ghost_data, &boundary);

    /* mfn.face is initialized to 0 by p4est_mesh_face_neighbor_init2 and
     * is incremented:
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has
     *    smaller or equal level
     *  * when calling p4est_mesh_face_neighbor_next and the neighbor has a
     *    bigger level, but only after seeing the 2 (or 4) smaller neighbors.
     */
    iinfo.face = mfn.face + (mfn.subface > 0) - 1;
    iinfo.quad_neighbor = neighbor;

    /* compute the indicator */
    qdata = userdata_mgr->quadrant_get_qdata (info->quad);
    eps = solverCore->m_indicator_fn (qdata, ndata, &iinfo);
    eps_max = SC_MAX (eps_max, eps);

    /* if the neighbor is on the boundary, the data was allocated on the fly,
     * so we have to deallocate it.
     */
    if (boundary == 1) {
      sc_mempool_free (info->p4est->user_data_pool, ndata);
    }

    neighbor = p4est_mesh_face_neighbor_next (&mfn, NULL, NULL, &face, NULL);

  } // end while. all neighbors visited.

  /* we suppose that epsilon_refine >= epsilon_coarsen */
  if (level > solver->m_min_refine && eps_max < solver->m_epsilon_coarsen) {
    solverWrap->mark_coarsen(info->treeid, info->quadid);
  } else if (level < solver->m_max_refine && eps_max > solver->m_epsilon_refine) {
    solverWrap->mark_refine(info->treeid, info->quadid);
  } else {
    return;
  }

} // iterator_mark_adapt

// =======================================================
// =======================================================
static void
iterator_suliciu_update (p4est_iter_volume_info_t * info,
			 void *user_data)
{

  // retrieve our Solver, UserDataManager and finaly our
  // quadrant data
  SolverBifluid      *solver = static_cast<SolverBifluid*>(solver_from_p4est (info->p4est));

  UserDataManagerBifluid
    *userdata_mgr = static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());


  int                 direction = *(int *) user_data;
  double              dx = quadrant_length (info->quad);
  double              scale_flux = pow (2, P4EST_DIM - 1);
  double              scale_gradient = 1;
  int                 hancock = 0;

  p4est_locidx_t      quadid = info->quadid;
  p4est_topidx_t      treeid = info->treeid;

  qdata_t            *data[2] = { nullptr, nullptr };
  int                 qf[2] = { -1, -1 };
  quadrant_neighbor_t<qdata_t> n;
  
  if (solver->m_time_order==2){
    hancock = 1;
  }

  // get the data and the variables for the current cell
  data[0] = userdata_mgr->quadrant_get_qdata (info->quad);

  for (int face = 0; face < 2; ++face) {
    // get the current face in the given direction
    qf[0] = QuadrantNeighborUtilsBase::direction_faces[direction][face];

    // get the qdata_t of the neighbors on this face
    QuadrantNeighborUtilsBifluid::quadrant_get_face_neighbor
      (solver, quadid, treeid, qf[0], &n);
    qf[1] = n.face;

    scale_gradient = pow (2, (info->quad->level - n.level));
    scale_flux = SC_MAX (n.level - info->quad->level, 0);
    scale_flux = pow (2, scale_flux * (P4EST_DIM - 1));

    if (n.is_hanging) {
      // half neighbor
      for (int i = 0; i < P4EST_HALF; ++i) {
        data[1] = n.is.hanging.data[i];
        suliciu_update_face (data, qf, solver->m_dt, dx,
			     scale_gradient,
                             scale_flux, direction, hancock);
      }
    } else {
      // equal or bigger neighbor
      data[1] = n.is.full.data;
      suliciu_update_face (data, qf, solver->m_dt, dx,
			   scale_gradient,
                           scale_flux, direction, hancock);
    }

    if (n.tree_boundary) {
      sc_mempool_free (info->p4est->user_data_pool, n.is.full.data);
    }
  }

  // add gravity in y direction (Godunov splitting)
  // TODO: add Strang splitting.
  if (direction == 1) {
    using namespace constants;
    double rho = data[0]->wnext.arho[0] + data[0]->wnext.arho[1];
    data[0]->wnext.momentum[1] -= solver->m_dt * gravity * rho;
  }
  
} // iterator_suliciu_update

// =======================================================
// =======================================================
static double
velocity_norm (double u[P4EST_DIM])
{
  double              norm = 0.0;
  for (int i = 0; i < P4EST_DIM; ++i) {
    norm += fabs (u[i]);
  }

  return norm;
} // velocity_norm

// =======================================================
// =======================================================
static void
iterator_suliciu_cfl (p4est_iter_volume_info_t * info,
		      void *user_data)
{

  // retrieve our Solver, UserDataManager and finaly our
  // quadrant data
  SolverBifluid *solver = static_cast<SolverBifluid*>(solver_from_p4est (info->p4est));

  UserDataManagerBifluid
    *userdata_mgr = static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());

  p4est_ghost_t      *ghost = solver->get_ghost ();
  p4est_mesh_t       *mesh = solver->get_mesh ();
  qdata_t            *ghost_data = static_cast<qdata_t *>(userdata_mgr->get_ghost_data_ptr());
  double              dx = quadrant_length (info->quad);
  double              scale = 1.0;

  double             *max_v_over_dx = (double *) user_data;

  p4est_quadrant_t   *neighbor = nullptr;
  suliciu_variables_t wq, wn;
  qdata_t            *data[2] = { nullptr, nullptr };
  data[0] = userdata_mgr->quadrant_get_qdata (info->quad);
  
  int                 face[2] = { 0, 0 };
  int                 vd[3] = { 0, 1, 2 };
  double              vmax = -1.0;
  int                 boundary = 0;
  double              a_max = 0;
  double              rho_min = 0.0;
  double              u_max = 0.0;

  // init face neighbor iterator
  p4est_mesh_face_neighbor_t mfn;
  p4est_mesh_face_neighbor_init2 (&mfn, info->p4est, ghost, mesh,
                                  info->treeid, info->quadid);

  // loop over neighbors
  neighbor = p4est_mesh_face_neighbor_next (&mfn,
					    nullptr, /* num tree */
					    nullptr, /* num quad */
					    &face[1],
					    nullptr); /* MPI rank of owner*/
  while (neighbor) {
    data[1] = QuadrantNeighborUtilsBifluid::mesh_face_neighbor_data (&mfn, ghost_data, &boundary);
    userdata_mgr->qdata_check_variables (data[1]);

    // compute the correct face ids
    face[0] = mfn.face + (mfn.subface > 0) - 1;
    
    if (boundary == 0) {

      /* decode the face information stored in p4est_mesh_t */
      face[1] = (face[1] + FACE_NEGATIVE_OFFSET) % P4EST_FACES;

    } else {

      /* if the quad is on the boundary, p4est_mesh_t gives the same face
       * as the interior one, so we need to compute its dual */
      face[1] = p4est_face_dual[face[0]];

    }

    // get neighbor data
    scale = pow (2, (info->quad->level - neighbor->level));
    wq.init (data[0], face[0], vd, dx, 0, 0);
    wn.init (data[1], face[1], vd, scale * dx, 0, 0);

    CANOP_ASSERT (!ISFUZZYNULL (wq.rho) && !std::isnan (wq.rho));
    CANOP_ASSERT (!ISFUZZYNULL (wn.rho) && !std::isnan (wn.rho));

    a_max   = SC_MAX (wq.a, wn.a);
    rho_min = SC_MIN (wq.rho, wn.rho);
    u_max   = SC_MAX (velocity_norm (wq.velocity),
		      velocity_norm (wn.velocity));
    vmax    = u_max + a_max / rho_min;

    if (std::isnan (vmax)) {
      SC_ABORTF ("vmax is nan: rhomin %g amax %g umax %g",
                 rho_min, a_max, u_max);
    }

    // if the neighbor is on the boundary, new data has been allocated by
    // mesh_face_neighbor_data that we have to deallocate
    if (boundary == 1) {
      sc_mempool_free (info->p4est->user_data_pool, data[1]);
    }
	
	
    // next!
    neighbor =
      p4est_mesh_face_neighbor_next (&mfn, nullptr, nullptr, &face[1], nullptr);

	
    // update the global max of velocity over dx
    *max_v_over_dx = SC_MAX (*max_v_over_dx, vmax/dx);
    
  }


  // also compute the min and max levels in the mesh
  solver->m_minlevel = SC_MIN (solver->m_minlevel, info->quad->level);
  solver->m_maxlevel = SC_MAX (solver->m_maxlevel, info->quad->level);

}

// =======================================================
// =======================================================
IteratorsBifluid::IteratorsBifluid() : IteratorsBase() {

  this->fill_iterators_list();

} // IteratorsBifluid::IteratorsBifluid

// =======================================================
// =======================================================
IteratorsBifluid::~IteratorsBifluid()
{

} // IteratorsBifluid::~IteratorsBifluid

// =======================================================
// =======================================================
void IteratorsBifluid::fill_iterators_list()
{

  iteratorsList[IT_ID::COMPUTE_MEAN_VALUES] = iterator_compute_mean_values;
  iteratorsList[IT_ID::GATHER_STATISTICS]   = iterator_gather_statistics;
  iteratorsList[IT_ID::START_STEP]          = iterator_start_step;
  iteratorsList[IT_ID::END_STEP]            = iterator_end_step;
  iteratorsList[IT_ID::MARK_ADAPT]          = iterator_mark_adapt;
  iteratorsList[IT_ID::CFL_TIME_STEP]       = iterator_suliciu_cfl;
  iteratorsList[IT_ID::UPDATE]              = iterator_suliciu_update;
  
} // IteratorsBifluid::fill_iterators_list

} // namespace canop

} // namespace bifluid
