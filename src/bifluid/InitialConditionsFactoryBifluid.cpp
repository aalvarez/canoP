#include "InitialConditionsFactoryBifluid.h"

#include "SolverBifluid.h"
#include "UserDataManagerBifluid.h"
#include "quadrant_utils.h"

#include "bifluid/userdata_bifluid.h"

#include "SuliciuPar.h" // for namespace constants
#include "newton.h"  // for StateLoc

namespace canop {

namespace bifluid {

using qdata_t               = Qdata; 
using qdata_variables_t     = QdataVar; 
using qdata_reconstructed_t = QdataRecons;

const double StarState::lambda = 1.0e-7;
double StarState::uStar = 0.0;
double StarState::pStar = 0.0;
double StarState::cStar = 0.0;
int    StarState::flagStar = 0;

// ================================================================
//                            GENERAL
// ================================================================

/**************************************************************************
 *                        TWO PHASE GAUSSIAN
 * uses gaussian_alpha and gaussian_velocity_main
 **************************************************************************/

void
gaussian_velocity_main (double x, double y, double z, double u[P4EST_DIM])
{
  
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  u[0] = 1.0;
  u[1] = 1.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
  
} // gaussian_velocity_main

// ================================================================
// ================================================================
double
gaussian_alpha (double x, double y, double z, double t)
{
  double              u[3];
  gaussian_velocity_main (x, y, z, u);

  double              cx = (0.5 + u[0] * t);
  double              cy = (0.5 + u[1] * t);
#ifdef USE_3D
  double              cz = (0.5 + u[2] * t);
#endif

  double              r = sqrt (fmin (pow (x - (cx - floor (cx + 0.5)), 2),
                                      pow (x - cx + floor (cx - 0.5), 2)) +
                                fmin (pow (y - (cy - floor (cy + 0.5)), 2),
                                      pow (y - cy + floor (cy - 0.5), 2)) +
#ifdef USE_3D
                                fmin (pow (z - (cz - floor (cz + 0.5)), 2),
                                      pow (z - cz + floor (cz - 0.5), 2)) +
#endif
                                0);

  const double &lambda = StarState::lambda;

  return (r > 0.3 ? lambda : lambda + (1.0-lambda) * pow (cos (M_PI * r / 0.6), 4));
  
} // gaussian_alpha

double
gaussian_rho_gas (double x, double y, double z, double t)
{
  
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

  return 1.0;
  
} // gaussian_rho_gas

/**************************************************************************
 *                         TWO PHASE DISK
 * can use any velocity
 **************************************************************************/
double
disk_alpha (double x, double y, double z, double t)
{
  
  double              u[3];
  gaussian_velocity_main (x, y, z, u);
  
  double              cx = (0.5 + u[0] * t);
  double              cy = (0.5 + u[1] * t);
#ifdef USE_3D
  double              cz = (0.5 + u[2] * t);
#endif
  
  double              r = sqrt (fmin (pow (x - (cx - floor (cx + 0.5)), 2),
                                      pow (x - cx + floor (cx - 0.5), 2)) +
                                fmin (pow (y - (cy - floor (cy + 0.5)), 2),
                                      pow (y - cy + floor (cy - 0.5), 2)) +
#ifdef USE_3D
                                fmin (pow (z - (cz - floor (cz + 0.5)), 2),
                                      pow (z - cz + floor (cz - 0.5), 2)) +
#endif
                                0);

  const double &lambda = StarState::lambda;
  
  return (r > 0.1 ? lambda : 1.0 - lambda);
  
} // disk_alpha

double
disk_rho_gas (double x, double y, double z, double t)
{
  
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

  return 1.0;
  
} // disk_rho_gas

/**************************************************************************
 *                         TWO PHASE SOD
 * initial velocity is 0 in all directions
 **************************************************************************/
double
sod_alpha (double x, double y, double z, double t)
{
  
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);
  const double &lambda = StarState::lambda;
  
  return (x < 1.0 ? (1.0 - lambda) : lambda);
  
  /*
  UNUSED (z);
  UNUSED (t);
  
  double              r = sqrt (SC_SQR (x - 0.5) + SC_SQR (y - 0.5) +
#ifdef USE_3D
                                SC_SQR (z - 0.5) +
#endif
                                0);
  
  return (r < 0.2 ? lambda : 1.0 - lambda);
  */
  
} // sod_alpha

double
sod_rho_gas (double x, double y, double z, double t)
{
  
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

  return (x < 1.0 ? 100.0 : 1.0);
  
  
  /*
  UNUSED (z);
  UNUSED (t);
  
  double              r = sqrt (SC_SQR (x - 0.5) + SC_SQR (y - 0.5) +
#ifdef USE_3D
                                SC_SQR (z - 0.5) +
#endif
                                0);
  
  return (r > 0.2 ? 1.0 : 100.0);
  */
   
} // sod_rho_gas

/**************************************************************************
 *                         TWO PHASE SOD2
 **************************************************************************/
double
sod2_alpha (double x, double y, double z, double t)
{
  
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

  return 0.1;
  
} // sod2_alpha

double
sod2_rho_gas (double x, double y, double z, double t)
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

  return 1.0;
  
} // sod2_rho_gas

void
sod2_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  
  UNUSED (y);
  UNUSED (z);

  u[0] = (2 * (x > 1.0) - 1) * 0.1;
  u[1] = 0.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
  
} // sod2_velocity

double
sod2_exact (double x, double y, double z, double t)
{	
  // returns exact density of sod2 problem
  using namespace constants;
  
  qdata_sod2_t local_state;
  
  if ( ISFUZZYNULL(t) ||
      !ISFUZZYNULL(floor(fabs(log(fabs(y-0.5))/log(2)))-fabs(log(fabs(y-0.5))/log(2)))){
    local_state.alpha		= sod2_alpha(x,y,z,0);
    local_state.rho_gas		= sod2_rho_gas(x,y,z,0);
    local_state.pressure	= p0[0] + SC_SQR(c[0])*
      (local_state.rho_gas - rho0[0]);
    local_state.rho_liquid	= rho0[1] + 1/SC_SQR(c[1])*
      (local_state.pressure - p0[1]);
    
    local_state.rho  = local_state.alpha * local_state.rho_gas + 
      (1-local_state.alpha) * local_state.rho_liquid;
    
    //CANOP_GLOBAL_INFOF("Here no computation : t = %18.14e  ;  y = %18.14e  \n",t,y);
    
  } else {
    
    double  xi = (x-1)/t;
    
    CANOP_GLOBAL_INFOF("For xi = %18.14e  \n",xi);
    
    //initial variables
    double alpha0, Y0, rho_gas0, rho_liquid0, pressure0;
    double velocity[3];
    double uL, uR;
    
    rho_gas0     = sod2_rho_gas(1.0,0.5,0,0);
    alpha0		 = sod2_alpha  (1.0,0.5,0,0);
    
    sod2_velocity(0.5,0.5,0, velocity);
    uL = velocity[0];
    
    sod2_velocity(1.5,0.5,0, velocity);
    uR = velocity[0];
    
    pressure0   = p0[0] + SC_SQR(c[0])*(rho_gas0 - rho0[0]);
    rho_liquid0 = rho0[1] + 1/SC_SQR(c[1])*(pressure0 - p0[1]);
    
    Y0 = alpha0 * rho_gas0 / (alpha0 * rho_gas0 + (1-alpha0) * rho_liquid0);
    
    CANOP_GLOBAL_INFOF("Y0 = %18.14e \n",Y0);
    
    StateLoc(Y0, pressure0, xi,
	     StarState::pStar, StarState::uStar, StarState::cStar,
	     uL, uR, &local_state);
    
    CANOP_GLOBAL_INFOF("Here, the exact states are :\n p = %18.14e \n u = %18.14e \n rho = %18.14e \n",
		    local_state.pressure,
		    local_state.velocity[0],
		    local_state.rho);
	
  }
  
  return local_state.rho;
  
} // sod2_exact

/**************************************************************************
 *                         TWO PHASE DROP
 **************************************************************************/
double
drop_in_water_alpha (double x, double y, double z, double t)
{
  UNUSED (z);
  UNUSED (t);

  //double              alpha;
  double              cx = (0.5 + 0.1 * t);
  double              cy = (0.4 + 0.1 * t);
#ifdef USE_3D
  double              cz = (0.5 + 0.1 * t);
#endif

  double              r = sqrt (SC_SQR (x - cx) + SC_SQR (y - cy) +
#ifdef USE_3D
                                SC_SQR (z - cz) +
#endif
                                0);

  return (y > 0.3 && r > 0.05 ? 1.0 : 0.0);
  
} // drop_in_water_alpha

double
drop_in_water_rho_gas (double x, double y, double z, double t)
{

  using namespace constants;
  double rho = 1.0;
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);
  
  if (y>0.3){

    rho = rho0[0] * exp(-1/SC_SQR(c[0])*gravity*(y-0.3));

  } else {
    
    double rho2 = rho0[1] * exp(-1/SC_SQR(c[1])*gravity*(y-0.3));
    rho = rho0[0]+SC_SQR(c[1])/SC_SQR(c[0])*(rho2-rho0[1]);

  }
  
  return rho;
  
} // drop_in_water_rho_gas

void
drop_in_water_velocity (double x, double y, double z, double u[P4EST_DIM])
{
 // double              rho =
 //   sod2_rho_gas (x, y, z, 0) + sod2_rho_liquid (x, y, z, 0);

  UNUSED (z);
  
  double              cx = (0.5);
  double              cy = (0.4);
#ifdef USE_3D
  double              cz = (0.5);
#endif

  double              r = sqrt (SC_SQR (x - cx) + SC_SQR (y - cy) +
#ifdef USE_3D
                                SC_SQR (z - cz) +
#endif
                                0);

  u[0] = 0.0;
  u[1] = (r > 0.05 ? 0.0 : -5.0);
#ifdef USE_3D
  u[2] = 0.0;
#endif
  
} // drop_in_water_velocity


/**************************************************************************
 *                         TWO PHASE Equilibrium Sea
 **************************************************************************/
double
sea_alpha (double x, double y, double z, double t)
{
  UNUSED (x);
  UNUSED (z);
  UNUSED (t);
  
  return (y > 0.3 ? 1.0 : 0.0);
  
} // sea_alpha

double
sea_rho_gas (double x, double y, double z, double t)
{
  
  using namespace constants;
  double rho = 1.0;
  UNUSED (x);
  UNUSED (z);
  UNUSED (t);
  
  if (y>0.3){
    rho = rho0[0] * exp(-1/SC_SQR(c[0])*gravity*(y-0.3));
  } else {
    double rho2 = rho0[1] * exp(-1/SC_SQR(c[1])*gravity*(y-0.3));
    rho = rho0[0]+SC_SQR(c[1])/SC_SQR(c[0])*(rho2-rho0[1]);
  }
  
  return rho;
  
} // sea_rho_gas

void
sea_velocity (double x, double y, double z, double u[P4EST_DIM])
{  

  UNUSED (x);
  UNUSED (y);
  UNUSED (z);
  
  u[0] = 0.0;
  u[1] = 0.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
  
} // sea_velocity


/**************************************************************************
 *                         TWO PHASE DAM BREAK
 * initial velocity is 0 in all directions
 **************************************************************************/
double
dam_break_alpha (double x, double y, double z, double t)
{
  
  UNUSED (y);
  UNUSED (z);
  UNUSED (t);

#ifndef USE_3D
  return (x < 0.3 && y < 0.5 ? 0.0 : 1.0);
#else
  return (x < 0.3 && y < 0.5 && z < 0.3 ? 0.0 : 1.0);
#endif
  
} // dam_break_alpha

double
dam_break_rho_gas (double x, double y, double z, double t)
{

  using namespace constants;
  UNUSED (x);
  UNUSED (z);
  UNUSED (t);

  return rho0[0] * (1 + gravity * (0.5 - y) / SC_SQR (c[0]));
  
} // dam_break_rho_gas

// OTHER VELOCITY FIELD INITIALIZATIONS
void
zero_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  u[0] = 0.0;
  u[1] = 0.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

void
diagonal_velocity (double x, double y, double z, double u[P4EST_DIM])
{
  UNUSED (x);
  UNUSED (y);
  UNUSED (z);

  u[0] = 1.0;
  u[1] = 1.0;
#ifdef USE_3D
  u[2] = 0.0;
#endif
}

// =======================================================
// =======INITIAL CONDITIONS CALLBACKS====================
// =======================================================

static void
init_me (p4est_t * p4est,
	 p4est_topidx_t which_tree,
	 p4est_quadrant_t * quad,
         scalar_init_t init_alpha,
	 scalar_init_t init_rho1,
         vector_init_t init_velocity)
{
  using namespace constants;

  Solver *solver = solver_from_p4est (p4est);

  UserDataManagerBifluid
    *userdata_mgr = static_cast<UserDataManagerBifluid *> (solver->get_userdata_mgr());

  qdata_variables_t   w;
  memset (&w, 0, sizeof (qdata_variables_t));

  /* geometry (if NULL, it means cartesian by default) */
  p4est_geometry_t *geom = solver->get_geom_compute();

  p4est_connectivity_t *conn = p4est->connectivity;
  
  /* get the coordinates of the center of the quad */
  double  X[3] = { 0, 0, 0 };

  quadrant_center_vertex (conn, geom, which_tree, quad, X);

  // ////////////////////////////////////////////////
  double              rho[2] = { 0, 0 };
  double              u[P4EST_DIM];

  /* initialize the main variables */
  w.alpha = init_alpha (X[0], X[1], X[2], 0);
  init_velocity (X[0], X[1], X[2], u);

  /* get the mass density in the gas */
  rho[0] = init_rho1 (X[0], X[1], X[2], 0);

  /* the two fluids have the same pressure, so we can compute it in the gas */
  w.pressure = p0[0] +
    SC_SQR (c[0]) * (rho[0] - rho0[0]);

  /* the pressure equilibrium gives us rho_liquid */
  rho[1] = (w.pressure - p0[1]) / SC_SQR (c[1]) + rho0[1];

  /* compute the conservative variables */
  w.arho[0] = w.alpha * rho[0];
  w.arho[1] = (1 - w.alpha) * rho[1];
  w.momentum[0] = (w.arho[0] + w.arho[1]) * u[0];
  w.momentum[1] = (w.arho[0] + w.arho[1]) * u[1];
#ifdef USE_3D
  w.momentum[2] = (w.arho[0] + w.arho[1]) * u[2];
#endif

  /* and compute the last variable */
  w.a = suliciu_thermo_a (w.arho[0], w.arho[1], w.alpha);

  userdata_mgr->quadrant_set_variables (quad, &w);
  userdata_mgr->quadrant_print_qdata (SC_LP_INFO, quad);
  
} // init_me


void
init_cvv_gaussian (p4est_t * p4est,
                   p4est_topidx_t which_tree,
		   p4est_quadrant_t * quad)
{
  
  init_me (p4est, which_tree, quad,
           gaussian_alpha, gaussian_rho_gas, gaussian_velocity_main);
  
} // init_cvv_gaussian

void
init_cvv_disk (p4est_t * p4est,
               p4est_topidx_t which_tree,
	       p4est_quadrant_t * quad)
{
  
  init_me (p4est, which_tree, quad,
           disk_alpha, disk_rho_gas, diagonal_velocity);
  
} // init_cvv_disk

void
init_cvv_sod (p4est_t * p4est,
              p4est_topidx_t which_tree,
	      p4est_quadrant_t * quad)
{
  
  init_me (p4est, which_tree, quad, sod_alpha, sod_rho_gas, zero_velocity);
  
} // init_cvv_sod

void
init_cvv_sod2 (p4est_t * p4est,
               p4est_topidx_t which_tree,
	       p4est_quadrant_t * quad)
{
  
  init_me (p4est, which_tree, quad, sod2_alpha, sod2_rho_gas, sod2_velocity);

} // init_cvv_sod2

void
init_cvv_dam_break (p4est_t * p4est,
                    p4est_topidx_t which_tree,
		    p4est_quadrant_t * quad)
{
  
  init_me (p4est, which_tree, quad,
           dam_break_alpha, dam_break_rho_gas, zero_velocity);
  
} // init_cvv_dam_break

void
init_cvv_drop_in_water (p4est_t * p4est,
                        p4est_topidx_t which_tree,
			p4est_quadrant_t * quad)
{
  
  init_me (p4est, which_tree, quad,
           drop_in_water_alpha, drop_in_water_rho_gas,
           drop_in_water_velocity);
  
} // init_cvv_drop_in_water


void
init_cvv_sea (p4est_t * p4est,
	      p4est_topidx_t which_tree, 
	      p4est_quadrant_t * quad)
{
  
  init_me (p4est, which_tree, quad,
           sea_alpha, sea_rho_gas,
           sea_velocity);

} // init_cvv_sea

/**
 * Initialize StarState "global variables".
 */
void
compute_sod2_star ()
{

  using namespace constants; // for accessing p0, rho0, etc...
  
  if (!StarState::flagStar ) {
    
    double alpha, Y, rho_gas, rho_liquid, pressure;
    double velocity[3];
    double uL, uR;
    
    rho_gas     = sod2_rho_gas(1.0,0.5,0,0);
    alpha	= sod2_alpha  (1.0,0.5,0,0);
    
    sod2_velocity(0.5,0.5,0, velocity);
    uL = velocity[0];
    
    sod2_velocity(1.5,0.5,0, velocity);
    uR = velocity[0];
    
    pressure   = p0[0] + SC_SQR(c[0])*(rho_gas - rho0[0]);
    rho_liquid = rho0[1] + 1/SC_SQR(c[1])*(pressure - p0[1]);
    
    Y = alpha * rho_gas / (alpha * rho_gas + (1-alpha) * rho_liquid);
    
    CANOP_GLOBAL_INFOF("Y = %18.14e  \n", Y);
    
    StarState::pStar = PressureStar(Y, pressure, uL, uR);  
    StarState::uStar = VelocityStar(Y, pressure, StarState::pStar, uL, uR);
    StarState::cStar = CSonStar(Y, StarState::pStar);
    StarState::flagStar = 1;
    
    CANOP_GLOBAL_INFOF("Star states are : p = %18.14e  ;  u = %18.14e  ;  c = %18.14e  \n",
		    StarState::pStar, StarState::uStar, StarState::cStar);
  }
} // compute_sod2_star

// ================================================================
// ================================================================
scalar_init_t
init_alpha_exact_byname (const std::string &name)
{

  if (        !name.compare ("cvv_gaussian") ) {

    return gaussian_alpha;

  } else if ( !name.compare ("cvv_disk") ) {

    return disk_alpha;

  } else if ( !name.compare ("cvv_sod2") ) {
    
    compute_sod2_star();
    return sod2_exact;
  }

  // the returned function pointer will be checked to be !=nullptr
  return nullptr; 
  
} // init_alpha_exact_byname

// ================================================================
// =========== CLASS InitialConditionsFactoryBifluid IMPL =========
// ================================================================
InitialConditionsFactoryBifluid::InitialConditionsFactoryBifluid() :
  InitialConditionsFactory()
{

  // register the above callback's
  registerIC("cvv_gaussian"      , init_cvv_gaussian);
  registerIC("cvv_disk"          , init_cvv_disk);
  registerIC("cvv_sod"           , init_cvv_sod);
  registerIC("cvv_sod2"          , init_cvv_sod2);
  registerIC("cvv_dam_break"     , init_cvv_dam_break);
  registerIC("cvv_drop_in_water" , init_cvv_drop_in_water);
  registerIC("cvv_sea"           , init_cvv_sea);
  
} // InitialConditionsFactoryBifluid

} // namespace canop

} // namespace bifluid
