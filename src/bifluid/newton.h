#ifndef NEWTON_H
#define NEWTON_H

#include "userdata_bifluid.h"
#include "SuliciuPar.h" // for namespace canoP::bifluid::constants

#ifndef P4_TO_P8
#include <p4est_iterate.h>
#else
#include <p8est_iterate.h>
#endif

namespace canop {

namespace bifluid {

double	PressureStar(double Y, double p, double uL, double uR);

double	CSonStar (double Y, double pStar);

double	VelocityStar (double Y, double p, double pStar, double uL, double uR);

void	StateLoc(double Y, double p, 
		 double xi, double pStar, double uStar, double cStar, 
		 double uL, double uR, qdata_sod2_t *local_state);

} // namespace canop

} // namespace bifluid

#endif // newton.h
