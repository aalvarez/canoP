#include "UserDataManagerBifluid.h"

#include "canoP_base.h"
#include "SuliciuPar.h" // for suliciu_thermo_* routines

namespace canop {

namespace bifluid {

/**
 * Aliases to give a name to a UserDataManager member used to get data.
 */
using bifluid_getter_t =
  double (UserDataManagerBifluid::*) (p4est_quadrant_t * q);
using bifluid_vgetter_t =
  void (UserDataManagerBifluid ::*) (p4est_quadrant_t * q, size_t dim, double * val);

// ====================================================================
// ====================================================================
UserDataManagerBifluid::UserDataManagerBifluid(bool useP4estMemory) :
  UserDataManager<UserDataTypesBifluid>()
{

  UNUSED(useP4estMemory);
  
} // UserDataManagerBifluid::UserDataManagerBifluid

// ====================================================================
// ====================================================================
UserDataManagerBifluid::~UserDataManagerBifluid()
{

} // UserDataManagerBifluid::~UserDataManagerBifluid

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::qdata_set_variables (qdata_t * data,
					     qdatavar_t * w)
{

  memcpy (&(data->w), w, sizeof (qdatavar_t));
  memcpy (&(data->wnext), w, sizeof (qdatavar_t));
  memset (&(data->delta), 0, P4EST_DIM * sizeof (qdatarecons_t));
  
} // UserDataManagerBifluid::qdata_set_variables

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::quadrant_set_state_data (p4est_quadrant_t * dst,
						 p4est_quadrant_t * src)
{

  qdata_t *data_dst = quadrant_get_qdata (dst);
  qdata_t *data_src = quadrant_get_qdata (src);

  memcpy (&(data_dst->w),     &(data_src->w),     sizeof (qdatavar_t));
  memcpy (&(data_dst->wnext), &(data_src->wnext), sizeof (qdatavar_t));
  memset (&(data_dst->delta), 0,      P4EST_DIM * sizeof (qdatarecons_t));

} // UserDataManagerBifluid::quadrant_set_state_variables

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::quadrant_compute_mean(p4est_quadrant_t *qmean,
					      p4est_quadrant_t *quadout[P4EST_CHILDREN])
{

  qdata_t     *child_data;
  qdatavar_t   mean;
  memset (&mean, 0, sizeof (qdatavar_t));
    
  for (int8_t i = 0; i < P4EST_CHILDREN; ++i) {
    child_data = quadrant_get_qdata (quadout[i]);

    mean.arho[0] += child_data->w.arho[0] / P4EST_CHILDREN;
    mean.arho[1] += child_data->w.arho[1] / P4EST_CHILDREN;
    for (int j = 0; j < P4EST_DIM; ++j) {
      mean.momentum[j] += child_data->w.momentum[j] / P4EST_CHILDREN;
    }

  }
  
  mean.alpha = suliciu_thermo_alpha (mean.arho[0], mean.arho[1]);
  mean.pressure =
    suliciu_thermo_pressure (mean.arho[0], mean.arho[1], mean.alpha);
  mean.a = suliciu_thermo_a (mean.arho[0], mean.arho[1], mean.alpha);
  
  // finally, copy all of that into quadin
  quadrant_set_variables (qmean, &mean);
      
} // quadrant_compute_mean

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::quadrant_copy_w_to_wnext (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  memcpy (&(data->wnext), &(data->w), sizeof (qdatavar_t));

} // UserDataManagerBifluid::quadrant_copy_w_to_wnext

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::quadrant_copy_wnext_to_w (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);
  memcpy (&(data->w), &(data->wnext), sizeof (qdatavar_t));

} // UserDataManagerBifluid::quadrant_copy_wnext_to_w
 
// ====================================================================
// ====================================================================
double
UserDataManagerBifluid::qdata_get_rho_gas (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.arho[0];
  
} // UserDataManagerBifluid::qdata_get_rho_gas

// ====================================================================
// ====================================================================
double
UserDataManagerBifluid::qdata_get_rho_liquid (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.arho[1];
  
} // UserDataManagerBifluid::qdata_get_rho_liquid

// ====================================================================
// ====================================================================
double
UserDataManagerBifluid::qdata_get_momentum (p4est_quadrant_t * q, int direction)
{
  qdata_t *data = quadrant_get_qdata (q);

  if (direction < P4EST_DIM) {
    return data->w.momentum[direction];
  } else {
    return 0;
  }
  
} // UserDataManagerBifluid::qdata_get_momentum

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::qdata_vget_momentum (p4est_quadrant_t * q, size_t dim, double * val)
{
  qdata_t *data = quadrant_get_qdata (q);

  for (size_t direction = 0; direction < dim; ++direction) {
    if (direction < P4EST_DIM) {
      val[direction] = data->w.momentum[direction];
    } else {
      val[direction] = 0;
    }
  }

} // UserDataManagerBifluid::qdata_vget_momentum

// ====================================================================
// ====================================================================
double
UserDataManagerBifluid::qdata_get_alpha (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.alpha;
  
} // UserDataManagerBifluid::qdata_get_alpha

// ====================================================================
// ====================================================================
double
UserDataManagerBifluid::qdata_get_pressure (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.pressure;
  
} // UserDataManagerBifluid::qdata_get_pressure

// ====================================================================
// ====================================================================
double
UserDataManagerBifluid::qdata_get_a (p4est_quadrant_t * q)
{
  qdata_t *data = quadrant_get_qdata (q);

  return data->w.a;
  
} // UserDataManagerBifluid::qdata_get_a

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::qdata_reconstructed_zero(qdatarecons_t *q)
{

  for (int i=0; i<NUM_FLUIDS; ++i)
    q->arho[i] = 0.0;
  for (int i=0; i<P4EST_DIM; ++i)
    q->velocity[i] = 0.0;

} // UserDataManagerBifluid::qdata_reconstructed_zero

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::qdata_reconstructed_copy(qdatarecons_t *q1, 
						 const qdatarecons_t *q2)
{

  memcpy( q1, q2, sizeof(qdatarecons_t) );

} // UserDataManagerBifluid::qdata_reconstructed_copy

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::reconstruct_variables (const qdatavar_t &w,
					       qdatarecons_t &r)
{

  double rho = w.arho[0] + w.arho[1];
  r.arho[0] = w.arho[0];
  r.arho[1] = w.arho[1];
  for (int i = 0; i < P4EST_DIM; ++i) {
    r.velocity[i] = w.momentum[i] / rho;
  }

} // UserDataManagerBifluid::reconstruct_variables

// ====================================================================
// ====================================================================
void UserDataManagerBifluid::compute_delta(const qdatarecons_t w,const qdatarecons_t *wn,
					   qdatarecons_t *delta,
					   scalar_limiter_t limiter,
					   double dx, double dxl, double dxr)
{

  qdatarecons_t       wl, wr;

  /* then we compute the reconstucted gradient on each pair (w_i, w, w_{i + 1}) */
  for (int i = 0; i < P4EST_HALF; ++i) {
    wl = wn[2 * i + 0];
    wr = wn[2 * i + 1];
    
    /* reconstruct rho, pressure and u */
    delta[i].arho[0] =
      limiter ((w.arho[0] - wl.arho[0]),
               (wr.arho[0] - w.arho[0]),
	       dx, dxl, dxr);
    delta[i].arho[1] =
      limiter ((w.arho[1] - wl.arho[1]),
               (wr.arho[1] - w.arho[1]),
	       dx, dxl, dxr);
    for (int j = 0; j < P4EST_DIM; ++j) {
      delta[i].velocity[j] = limiter ((w.velocity[j] - wl.velocity[j]),
                                      (wr.velocity[j] - w.velocity[j]),
				      dx, dxl, dxr);
    }
    
    if (ISFUZZYNULL(delta[0].arho[0]-5.60625610207933e-01)){
      CANOP_GLOBAL_INFOF("For the researched delta : \n");
      CANOP_GLOBAL_INFOF("w.arho      : %g \n",  w.arho[0]);
      CANOP_GLOBAL_INFOF("left  value : %g \n", wl.arho[0]);
      CANOP_GLOBAL_INFOF("right value : %g \n", wr.arho[0]);}

  } // end reconstucted gradient
  
  /*
   * we now have reconstructed values on the lower half and upper half of
   * quadrant, but we need a reconstructed value on the whole cell, so we just
   * limit the 2 (or 4 in 3D) values we have using the same limiter.
   */
  for (int i = 1; i < P4EST_HALF; ++i) {
    
    delta[0].arho[0] = limiter_minmod (delta[0].arho[0], delta[i].arho[0], 1, 1, 1);
    delta[0].arho[1] = limiter_minmod (delta[0].arho[1], delta[i].arho[1], 1, 1, 1);
    
    for (int j = 0; j < P4EST_DIM; ++j) {
      delta[0].velocity[j] =
        limiter_minmod (delta[0].velocity[j], delta[i].velocity[j], 1, 1, 1);
    }
    
  }
  
} // UserDataManagerBifluid::compute_delta
  
// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::qdata_reconstructed_check (qdatarecons_t w,
						   const char *plus)
{
  
  SC_CHECK_ABORTF (!isnan (w.arho[0]), "rho1%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.arho[1]), "rho2%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.velocity[0]), "ux%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.velocity[1]), "uy%s is nan\n", plus);
#ifdef P4_TO_P8
  SC_CHECK_ABORTF (!isnan (w.velocity[2]), "uz%s is nan\n", plus);
#endif

  SC_CHECK_ABORTF (!isinf (w.arho[0]), "rho1%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.arho[1]), "rho2%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.velocity[0]), "ux%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.velocity[1]), "uy%s is inf\n", plus);
#ifdef P4_TO_P8
  SC_CHECK_ABORTF (!isnan (w.velocity[2]), "uz%s is inf\n", plus);
#endif
  
} // UserDataManagerBifluid::qdata_reconstructed_check

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::qdata_variables_check (qdatavar_t w,
					       const char *plus)
{
  
  SC_CHECK_ABORTF (!isnan (w.arho[0]), "rho1%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.arho[1]), "rho2%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.momentum[0]), "ux%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.momentum[1]), "uy%s is nan\n", plus);
#ifdef P4_TO_P8
  SC_CHECK_ABORTF (!isnan (w.momentum[2]), "uz%s is nan\n", plus);
#endif
  SC_CHECK_ABORTF (!isnan (w.pressure), "pressure%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.alpha), "alpha%s is nan\n", plus);
  SC_CHECK_ABORTF (!isnan (w.a), "a%s is nan\n", plus);

  SC_CHECK_ABORTF (!isinf (w.arho[0]), "rho1%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.arho[1]), "rho2%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.momentum[0]), "ux%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.momentum[1]), "uy%s is inf\n", plus);
#ifdef P4_TO_P8
  SC_CHECK_ABORTF (!isnan (w.momentum[2]), "uz%s is inf\n", plus);
#endif
  SC_CHECK_ABORTF (!isinf (w.pressure), "pressure%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.alpha), "alpha%s is inf\n", plus);
  SC_CHECK_ABORTF (!isinf (w.a), "a%s is inf\n", plus);
  
} // UserDataManagerBifluid::qdata_variables_check

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::qdata_check_variables (qdata_t * data)
{
  
  qdata_variables_check (data->w, "");
  qdata_variables_check (data->wnext, "+");
  qdata_reconstructed_check (data->delta[0], "_dx");
  qdata_reconstructed_check (data->delta[1], "_dy");
#ifdef P4_TO_P8
  qdata_reconstructed_check (data->delta[2], "_dz");
#endif
  
} // UserDataManagerBifluid::qdata_check_variables

// ====================================================================
// ====================================================================
void
UserDataManagerBifluid::quadrant_check_data (p4est_quadrant_t * q)
{
  
  qdata_check_variables (quadrant_get_qdata (q));
  
} // UserDataManagerBifluid::quadrant_check_data


// ====================================================================
// ====================================================================
qdata_getter_t
UserDataManagerBifluid::qdata_getter_byname (const std::string &varName)
{

  qdata_getter_t base_getter = nullptr;
  bifluid_getter_t getter = nullptr;
  
  if (!varName.compare("rho_gas")) {
    
    getter = &UserDataManagerBifluid::qdata_get_rho_gas;
    
  } else if (!varName.compare("rho_liquid")) {
    
    getter = &UserDataManagerBifluid::qdata_get_rho_liquid;
    
  } else if (!varName.compare("alpha")) {
    
    getter = &UserDataManagerBifluid::qdata_get_alpha;
    
  } else if (!varName.compare("pressure")) {
    
    getter = &UserDataManagerBifluid::qdata_get_pressure;
    
  } else if (!varName.compare("a")) {
    
    getter = &UserDataManagerBifluid::qdata_get_a;
    
  }
  
  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_getter_t>(getter);

  } else {

    // print warning
    CANOP_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
    
  }
  
  return base_getter;
  
} // UserDataManagerBifluid::qdata_getter_byname


// ====================================================================
// ====================================================================
qdata_vector_getter_t
UserDataManagerBifluid::qdata_vgetter_byname (const std::string &varName)
{

  qdata_vector_getter_t base_getter = nullptr;
  bifluid_vgetter_t getter = nullptr;

  if (!varName.compare("momentum")) {
    getter = &UserDataManagerBifluid::qdata_vget_momentum;
  } else if (!varName.compare("velocity")) { // XXX: this is a bug, kept for backwards-compatibility
    getter = &UserDataManagerBifluid::qdata_vget_momentum;
  }

  if (getter != nullptr) {

    // convert to base class member function pointer
    base_getter = static_cast<qdata_vector_getter_t>(getter);

  } else {

    // print warning
    P4EST_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());

  }

  return base_getter;

} // UserDataManagerBifluid::qdata_vgetter_byname


// ====================================================================
// ====================================================================
qdata_field_type
UserDataManagerBifluid::qdata_type_byname(const std::string &varName)
{

  if (!varName.compare("rho_gas") ||
      !varName.compare("rho_liquid") ||
      !varName.compare("alpha") ||
      !varName.compare("pressure") ||
      !varName.compare("a"))
    return QDATA_FIELD_SCALAR;

  if (!varName.compare("momentum") ||
      !varName.compare("velocity"))
    return QDATA_FIELD_VECTOR;

  if (!varName.compare("alpha_exact"))
    return QDATA_FIELD_UNKNOWN; // XXX: should be scalar, but needs position

  // print warning
  P4EST_GLOBAL_INFOF ("Unrecognized getter: \"%s\"\n", varName.c_str());
  return QDATA_FIELD_UNKNOWN;

} // UserDataManagerBifluid::qdata_type_byname

} // namespace bifluid

} // namespace canop
