#ifndef SOLVER_BIFLUID_H_
#define SOLVER_BIFLUID_H_

#include "Solver.h"
#include "userdata_bifluid.h"

namespace canop {

namespace bifluid {

class SolverBifluid : public Solver
{

  using qdata_t = Qdata;
  
public:
  SolverBifluid(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory);
  virtual ~SolverBifluid();

  /**
   * override base class method.
   */
  void read_config();

  /**
   * Here we implement numerical scheme for Bifluid application.
   */
  void next_iteration_impl();

  /**
   * This is the actual routine where hdf5 files are written.
   */
  void write_hdf5(const std::string &filename);

  /**
   * Static creation method called by the solver factory.
   */
  static Solver* create(ConfigReader * cfg, MPI_Comm mpicomm, bool useP4estMemory)
  {
    return new SolverBifluid(cfg, mpicomm, useP4estMemory);
  }

private:
  /**
   * Apply numerical scheme in a directionnally split way.
   */
  void single_directional(int direction);

  /**
   * Function pointer to compute alpha (exact solution).
   */
  scalar_init_t m_alpha_exact_fn;
  
}; // class SolverBifluid

} // namespace bifluid

} // namespace canop

#endif // SOLVER_BIFLUID_H_
