#!/bin/sh

# get the install prefix
install_prefix=${1:-~/local/usr}
install_prefix=$(readlink -f ${install_prefix})

# if the directory doesn't exist, create it
[ -d ${install_prefix} ] || mkdir -p ${install_prefix}

# get lua version
lua_version=${2:-5.1.5}

# get lua
wget http://www.lua.org/ftp/lua-${lua_version}.tar.gz
tar zxvf lua-${lua_version}.tar.gz
cd lua-${lua_version}

make linux install INSTALL_TOP=${install_prefix}

