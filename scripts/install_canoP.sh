#!/bin/sh

# specify the p4est and libsc install prefix
lib_prefix=${1:-/usr}
lib_prefix=$(readlink -f ${lib_prefix})

# the install prefix for canoP is, by default, the same as for p4est
install_prefix=${2:-${lib_prefix}}

# launch cmake and point it to the right p4est_prefix
echo "Custom prefix: '${install_prefix}'"
[ -d "build" ] || mkdir build
cd build
cmake -DCMAKE_PREFIX_PATH=${lib_prefix} \
      -DCMAKE_INSTALL_PREFIX:PATH=${install_prefix} ..

# compile
make -j4 2>/dev/null

