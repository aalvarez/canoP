#!/bin/sh

# specify the install prefix for p4est and libsc
install_prefix=${1:-"/usr"}
install_prefix=$(readlink -f ${install_prefix})

# clone the p4est repo (or update if it exists)
if [ -d "p4est" ]; then
    echo "Updating 'p4est'..."
    cd p4est
    git pull --all
else
    echo "Cloning into 'p4est'..."
    git clone https://github.com/cburstedde/p4est.git 2>/dev/null
    cd p4est
fi

# get libsc
echo "Cloning into 'sc'..."
git submodule init 1>/dev/null
git submodule update 2>/dev/null

# create all the boostrap files and configure
echo "Configuring and compiling 'p4est' with prefix '${install_prefix}'"
./bootstrap 2>&1 1>/dev/null
./configure --prefix=${install_prefix} \
            --enable-mpi \
            --enable-mpiio \
            --enable-shared \
            CC=mpicc \
            CXX=mpicxx \
            FC=mpif90 \
            LDFLAGS="-L${install_prefix}/lib" \
            CFLAGS="-I${install_prefix}/include" \
            CPPFLAGS="-I${install_prefix}/include" \
            2>&1 1>/dev/null

# compile
make -j4 1>/dev/null
make install

